<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ include file="/WEB-INF/jsp/expHeader.jsp"%>
<%@ include file="/WEB-INF/jsp/MenuBar.jsp"%>
<!-- /***************************************************************/ -->
<div class="jumbotron">
<div class="container">




	<div id="Content" align="center" class="SecureBanking">

		<!-- Fetching Account number and account balanace of logged in user. -->
		<table cellpadding="15" cellspacing="15" border="1">
			<h3>Username : ${username}</h3></br>
			<tr>
				<th>Account number</th>
				<th>Balance</th>
			</tr>
			<c:if test="${not empty accountListForUser}">
				<c:forEach items="${accountListForUser}" var="account">
					<tr>
						<td><c:out value="${account.getMaskedAccountNo()}" /></td>
						<td><fmt:formatNumber value="${account.accountBalance}" type="CURRENCY" /></td>
					</tr>
				</c:forEach>
			</c:if>
		</table>

	</div>



</div>
</div>
</body>
</html>