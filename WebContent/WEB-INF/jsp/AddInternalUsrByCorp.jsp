<%@ include file="/WEB-INF/jsp/expHeader.jsp"%>
<%@ include file="/WEB-INF/jsp/navCorporateLvlMngr-bar.jsp"%>

<!-- /***************************************************************/ -->
<div class="jumbotron">
<div class="container">
  <div class="panel panel-default">
  <!-- Default panel contents -->
  <div class="panel-heading">Pending Requests</div>
  <div class="panel-body">
    <p>Please look into the below internal user requests</p>
  </div>

  <!-- Table -->
<form:form autocomplete="off">
				<table>
					<tbody>
						<tr>
							<th>Employee Name:</th>
							<td><input type="text" name="internalEmpName" id="internalEmpName"/></td>
						</tr>
						<tr>
							<th>Employee Id:</th>
							<td><input type="text" name="internalEmpId" id="internalEmpId"/></td>
						</tr>
						<tr>
							<th>Employee SSN Number:</th>
							<td><input type="text" name="internalEmpSsn" id="internalEmpSsn"/></td>
						</tr>
						<tr>
							<td><input type="submit" value="Add Internal User" /></td>
						</tr>
						
					</tbody>
				</table>
			</form:form></div>
</div>
</body>
</html>