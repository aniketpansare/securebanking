<%@ include file="/WEB-INF/jsp/expHeader.jsp"%>
<%@ include file="/WEB-INF/jsp/navCorporateLvlMngr-bar.jsp"%>
<!-- /***************************************************************/ -->
<div class="jumbotron">
	<div class="container">
		<form class="form-horizontal" method='POST'
			action="/SecureBanking/AdminHomePage.htm">
			<fieldset>
				<!-- Form Name -->
				<legend>Add an Internal User</legend>
				<!-- Text input-->
				<div class="form-group">
					<label class="col-md-4 control-label" for="FirstName">First
						Name*</label>
					<div class="col-md-4">
						<input id="FirstName" name="FirstName" type="text" placeholder=""
							class="form-control input-md" required="">

					</div>
				</div>
				<!-- Text input-->
				<div class="form-group">
					<label class="col-md-4 control-label" for="LastName">Last
						Name*</label>
					<div class="col-md-4">
						<input id="LastName" name="LastName" type="text" placeholder=""
							class="form-control input-md" required="">

					</div>
				</div>
				<!-- Text input-->
				<div class="form-group">
					<label class="col-md-4 control-label" for="SSN">Social
						Security Number*</label>
					<div class="col-md-4">
						<input id="SSN" name="SSN" type="text" placeholder=""
							class="form-control input-md" required="">

					</div>
				</div>
				<div class="form-group">
					<label class="col-md-4 control-label" for="DeptDropdown">Department*</label>
					<div class="col-md-4">
						<select id="DeptDropdown" name="DeptDropdown" class="form-control">
							<option value="0">None</option>
							<option value="1">Human Resources</option>
							<option value="2">IT and Technical Support</option>
							<option value="3">Transaction Monitoring</option>
							<option value="4">Sales</option>
						</select>
					</div>
				</div>
				<!-- Select Basic -->
				<div class="form-group">
					<label class="col-md-4 control-label" for="Role">Role*</label>
					<div class="col-md-4">
						<select id="Role" name="Role" class="form-control">
							<option value="0">None</option>
							<option value="1">Regular Employee</option>

						</select>
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-4 control-label" for="AddButton"></label>
					<div class="col-md-4">
						<button id="AddButton" name="AddButton" class="btn btn-default">Add</button>
					</div>

				</div>
			</fieldset>
		</form>
	</div>
</div>
</body>
</html>