<%@ include file="/WEB-INF/jsp/expHeader.jsp"%>
<%@ include file="/WEB-INF/jsp/AdminNavBar.jsp"%>

<!-- /***************************************************************/ -->
<div class="jumbotron">
<div class="container">

<c:if test="${not empty Error}">
	<div class="errorblock">
	${Error}
	</div>
</c:if> 

<c:if test="${not empty Success and empty Error}">
	<div class="errorblock">
	${Success}
	</div>
</c:if>
<c:if test="${empty Success and empty Error}">
<form:form class="form-horizontal" method="POST" modelAttribute="addInternalUserForm"	commandname="addInternalUserForm"
				action="AdminAddAccountToUser.htm">
				
		<!-- Form Name -->
		<legend> Add Account</legend>
		<fieldset>
       
		<div class="form-group">
            <form:label class="col-md-4 control-label" path="username">username:</form:label>
            <div class="col-md-4"><form:input path="username" class="form-control input-md" required="true"/></div>
            <form:errors path="username" />
        </div>
        
        <div class="form-group">
            <form:label class="col-md-4 control-label" path="name">Account Name:</form:label>
            <div class="col-md-4"><form:input path="name" class="form-control input-md" required="true"/></div>
            <form:errors path="name" />
        </div>
        
        <div class="form-group">
            <form:label class="col-md-4 control-label" path="description">Account Description:</form:label>
            <div class="col-md-4"><form:input path="description" class="form-control input-md" required="true"/></div>
            <form:errors path="description" />
        </div>
        
       <div class="form-group">
        	<label class="col-md-4 control-label" for="AddButton"></label>
  			<div class="col-md-4"><button id="AddButton" type="submit" name="AddButton" class="btn btn-default">Add Account</button></div>
  		</div>
        	
		</fieldset>		
</form:form>

</c:if>


</div>
</div>

</body>
</html>