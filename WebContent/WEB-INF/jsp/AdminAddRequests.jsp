<%@ include file="/WEB-INF/jsp/expHeader.jsp"%>
<%@ include file="/WEB-INF/jsp/AdminNavBar.jsp"%>

<!-- /***************************************************************/ -->
<div class="jumbotron">
	<div class="container">

		<c:if test="${not empty Error}">
			<div class="errorblock">${Error}</div>
		</c:if>
		<c:if test="${not empty Success and empty Error}">
			<div class="errorblock">${Success}</div>
		</c:if>

		<c:if test="${empty Success and not empty requests and empty Error}">
				<fieldset>
					<!-- Form Name -->
					<legend>Pending Add Requests</legend>
					<!-- Text input-->
					<div id="AddResults" class="panel panel-default">
						<div class="panel-body">
							<!-- Table -->
							<table class="table">
								<tr>
									<td>Request ID</td>
									<td>Request Date</td>
									<td>Requestor</td>
									<td>Subject User</td>
									<td>Description</td>
									<td>Type</td>
									<td>Status</td>
									<td colspan="2">Approve/Deny</td>
								</tr>
								<c:forEach items="${requests}" var="request">
								<form:form class="form-horizontal" method='POST' action="AdminAddRequests.htm">
								<tr>
									<td>${request.getId()}</td>
									<td>${request.getRequestDate()}</td>
									<td>${request.getRequestor().getUsername()}</td>
									<td>${request.getSubject().getUsername()}</td>
									<td>${request.getDescription()}</td>
									<td>${request.getRequestType()}</td>
									<td>${request.getRequestStatus()}</td>
									<td colspan="2">
										<!-- Button -->
												<button id="ApproveButton" name="approveParam" value="Approve"
													class="btn btn-success" type="submit">Approve</button>
												<button id="DenyButton" name="approveParam" value="Deny"
													class="btn btn-danger" type="submit">Deny</button>
									</td>
									<input style="visibility: hidden" name="requestId" value="${request.getId()}" hidden="true"></input>
								</tr>
								</form:form>
								</c:forEach>
							</table>
						</div>
					</div>
					
				</fieldset>
			</c:if>

	</div>
</div>
</body>
</html>
