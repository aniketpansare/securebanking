<%@ include file="/WEB-INF/jsp/expHeader.jsp"%>
<%@ include file="/WEB-INF/jsp/AdminNavBar.jsp"%>

<!-- /***************************************************************/ -->
<div class="jumbotron">
<div class="container">
 
<c:if test="${not empty Error}">
	<div class="errorblock">
	${Error}
	</div>
</c:if> 
 
<c:if test="${not empty Success and empty Error}">
	<div class="errorblock">
	The User is marked as deleted.
	</div>
</c:if>

<c:if test="${empty Success and empty Error}">
<form:form class="form-horizontal" method="POST" modelAttribute="addInternalUserForm"	commandname="addInternalUserForm"
				action="AdminDeleteInternalUser.htm">
				
		<!-- Form Name -->
		<legend> Delete Internal User</legend>
		<fieldset>
		<div class="form-group">
            <form:label class="col-md-4 control-label" path="username">username:</form:label>
            <div class="col-md-4"><form:input path="username" class="form-control input-md" required="true"/></div>
            <form:errors path="username" />
        </div>
        
        <div class="form-group">
        	<label class="col-md-4 control-label" for="DeleteButton"></label>
  			<div class="col-md-4"><button id="DeleteButton" type="submit" name="DeleteButton" class="btn btn-default">Delete</button></div>
  		</div>
        	
		</fieldset>		
</form:form>
</c:if>
 
 
</div>
</div>
</body>
</html>