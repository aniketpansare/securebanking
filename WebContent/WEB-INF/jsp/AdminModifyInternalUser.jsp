<%@ include file="/WEB-INF/jsp/expHeader.jsp"%>
<%@ include file="/WEB-INF/jsp/AdminNavBar.jsp"%>

<!-- /***************************************************************/ -->
<div class="jumbotron">
<div class="container">


<c:if test="${not empty Error}">
	<div class="errorblock">
	${Error}
	</div>
</c:if>


<c:if test="${not empty ModifySuccess and empty Error}">
	<div class="errorblock">
	The User data was modified successfully.
	</div>
</c:if>


<c:if test="${empty Success and empty ModifySuccess and empty Error}">
<form:form class="form-horizontal" method="POST" modelAttribute="addInternalUserForm"	commandname="addInternalUserForm"
				action="AdminModifyInternalUser.htm">
				
		<!-- Form Name -->
		<legend> Modify User PII</legend>
		<fieldset>
		<div class="form-group">
            <form:label class="col-md-4 control-label" path="username">username:</form:label>
            <div class="col-md-4"><form:input path="username" class="form-control input-md" required="true"/></div>
            <form:errors path="username" />
        </div>
        
        <div class="form-group">
        	<label class="col-md-4 control-label" for="Modify"></label>
  			<div class="col-md-4"><button id="Modify" type="submit" name="ModifyButton" class="btn btn-default">Modify</button></div>
  		</div>
        	
		</fieldset>		
</form:form>
</c:if>
 
<c:if test="${not empty Success and empty ModifySuccess and empty Error}">
 <form:form class="form-horizontal" method="POST" modelAttribute="addInternalUserForm"	commandname="addInternalUserForm"
				action="AdminModifyInternalUserData.htm">
				
		<!-- Form Name -->
		<legend> Modify Internal User</legend>
		<fieldset>
        <div class="form-group">
            <form:label class="col-md-4 control-label" path="name">Name:</form:label>
            <div class="col-md-4"><form:input path="name" class="form-control input-md" value="${userToModify.getData().getName()}" required="true"/></div>
            <form:errors path="name" />
        </div>
        <div class="form-group">
            <form:label class="col-md-4 control-label" path="number">Phone-No:</form:label>
            <div class="col-md-4"><form:input path="number" class="form-control input-md" value="${userToModify.getData().getContactNumber()}" required="true"/></div>
            <form:errors path="number" />
        </div>
        <div class="form-group">
            <form:label class="col-md-4 control-label" path="addr">Address:</form:label>
            <div class="col-md-4"><form:input path="addr" class="form-control input-md" value="${userToModify.getData().getAddress()}" required="true"/></div>
            <form:errors path="addr" />
        </div>
         <div class="form-group">
            <form:label class="col-md-4 control-label" path="email">Email:</form:label>
            <div class="col-md-4"><form:input path="email" class="form-control input-md" value="${userToModify.getData().getEmail()}" required="true"/></div>
            <form:errors path="email" />
        </div>
        <div class="form-group">
            <form:label class="col-md-4 control-label" path="social">SSN:</form:label>
            <div class="col-md-4"><form:input path="social" class="form-control input-md" value="${userToModify.getData().getSsn()}" required="true"/></div>
            <form:errors path="social" />
        </div>
        
        <div class="form-group">
            <form:label class="col-md-4 control-label" path="userStatus">Status:</form:label>
			<div class="col-md-4">
				<form:select class="form-control" path="userStatus" required="true">
					<form:option value="${userToModify.getStatus()}" selected="selected">${userToModify.getStatus()}</form:option>
					<form:options items="${userStatusList}"></form:options>
					</form:select>
			</div>
        </div>
        

        
         <!--style="visibility: hidden"-->
        <div class="form-group">
            <form:label class="col-md-4 control-label" path="username">username:</form:label>
            <div class="col-md-4"><form:input path="username" value="${userToModify.getUsername()}" class="form-control input-md" required="true" readonly="true"/></div>
            <form:errors path="username" />
        </div>
        
        <!--  
        <div class="form-group">
            <form:label class="col-md-4 control-label" path="password">Password:</form:label>
            <div class="col-md-4"><form:input path="password" type="password" value="" class="form-control input-md" required="true" /></div>
            <form:errors path="password" />
        </div>
        -->
                
        <!-- 
       <div class="form-group">
            <form:label class="col-md-4 control-label" path="userRole">Select Role:</form:label>
			<div class="col-md-4">
				<form:select class="form-control" path="userRole" required="true">
					<form:option value="${userToModify.getUserType()}">
					<form:options items="${userRoleList}"></form:options>
					</form:option>
				</form:select>
			</div>
        </div>
        
        <div class="form-group">
            <form:label class="col-md-4 control-label" path="departmentName">Select Department:</form:label>
			<div class="col-md-4">
				<form:select class="form-control" path="departmentName" required="true">
					<form:option value="">
					<form:options items="${deptListForUser}" itemValue="departmentName" itemLabel="departmentName"></form:options>
					</form:option>
				</form:select>
			</div>
        </div>
        
        
        <div class="form-group">
            <form:label class="col-md-4 control-label" path="password">Password:</form:label>
            <div class="col-md-4"><form:input path="password" type="password" class="form-control input-md" required="true"/></div>
            <form:errors path="password" />
        </div>
		-->

        <div class="form-group">
        	<label class="col-md-4 control-label" for="ModifyButton"></label>
  			<div class="col-md-4"><button id="ModifyButton" type="submit" name="ModifyButton" class="btn btn-default">Modify</button></div>
  		</div>
        	
		</fieldset>		
</form:form>
 </c:if>
 
 
 
 

</div>
</div>
</body>
</html>
