<!-- ###################### Menu Bar #################################-->
<div id='cssmenu'>
<ul>
   <li class='active'><a href='AdminHomePage.htm'><span>Home</span></a></li>
   <li class='has-sub'><a href='#'><span>Actions</span></a>
      <ul>
         <li class='has-sub'><a href='AdminAddInternalUser.htm'><span>Add User</span></a></li>
         <li class='has-sub'><a href='AdminDeleteInternalUser.htm'><span>Delete User</span></a></li>
         <li class='has-sub'><a href='AdminModifyInternalUser.htm'><span>Modify User</span></a></li>
         <li class='has-sub'><a href='AdminAddAccountToUser.htm'><span>Add Account</span></a></li>
         <li class='has-sub'><a href='AdminAddDepartmentToUser.htm'><span>Add Department</span></a></li>
      </ul>
   </li>
   <li class='has-sub'><a href='#'><span>Pending Requests</span></a>
      <ul>
         <li class='has-sub'><a href='AdminAddRequests.htm'><span>Add Requests</span></a></li>
         <li class='has-sub'><a href='AdminDeleteRequests.htm'><span>Delete Requests</span></a></li>
         <li class='has-sub'><a href='AdminTransferRequests.htm'><span>Transfer Requests</span></a></li>
      </ul>
   </li>
   <li class='last'><a href='logs.htm'><span>System Logs</span></a></li>
   <li><a href="<c:url value="/j_spring_security_logout" />"
			id="Logout" accesskey="5" title=""><span>Logout</span></a></li>
</ul>
</div>