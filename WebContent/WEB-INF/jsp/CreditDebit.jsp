<%@ include file="/WEB-INF/jsp/expHeader.jsp"%>
<%@ include file="/WEB-INF/jsp/MenuBar.jsp"%>
<!-- /***************************************************************/ -->
<div class="jumbotron">
<div class="container">



<c:if test="${not empty Error}">
	<div class="errorblock">
	${Error}
	</div>
</c:if> 
<c:if test="${not empty Success and empty Error}">
	<div>
	${Success}
	</div>
</c:if> 	

<c:if test="${empty Success and empty transaction and empty Error}">
<form:form class="form-horizontal" method="POST" modelAttribute="addInternalUserForm"	commandname="addInternalUserForm"
				action="CreditDebit.htm">
				
		<!-- Form Name -->
		<legend> Credit / Debit</legend>
		<fieldset>
		
		<div class="form-group">
        <form:label class="col-md-4 control-label" path="selectedAccountNo">Account No:</form:label>
			<div class="col-md-4">
				<form:select class="form-control" path="selectedAccountNo" required="true">
					<c:forEach items="${accounts}" var="account">
      					<form:option value="${account.getAccountNo()}">${account.getAccountName()}: ${account.getMaskedAccountNo()}</form:option>
				     </c:forEach>
				</form:select>
			</div>
        </div>
        
        <div class="form-group">
            <form:label class="col-md-4 control-label" path="amount">Amount:</form:label>
            <div class="col-md-4"><form:input path="amount" class="form-control input-md" required="true"/></div>
            <form:errors path="amount" />
        </div>
        
        <div class="form-group">
        <form:label class="col-md-4 control-label" path="creditDebitOption">Choose:</form:label>
			<div class="col-md-4">
				<form:select class="form-control" path="creditDebitOption" required="true">
						<form:option value="CREDIT">CREDIT</form:option>
						<form:option value="DEBIT">DEBIT</form:option>
				</form:select>
			</div>
        </div>
        
        <div class="form-group">
        	<label class="col-md-4 control-label" for="TransferButton"></label>
  			<div class="col-md-4"><button id="TransferButton" type="submit" name="TransferButton" class="btn btn-default">Submit</button></div>
  		</div>
  		
		</fieldset>		
</form:form>
</c:if>

</div>
</div>
</body>
</html>