<%@ include file="/WEB-INF/jsp/expHeader.jsp"%>
<%@ include file="/WEB-INF/jsp/navCorporateLvlMngr-bar.jsp"%>

<!-- /***************************************************************/ -->
<div class="jumbotron">
<div class="container">
 
 <form class="form-horizontal" method='POST' action="/SecureBanking/AdminHomePage.htm">
<fieldset>
<!-- Form Name -->
<legend>Delete an Internal User</legend>
<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="EmployeeID">Employee ID*</label>  
  <div class="col-md-4">
  <input id="EmployeeID" name="EmployeeID" type="text" placeholder="" class="form-control input-md" required="">
  </div>
</div>
<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="FirstName">First Name*</label>  
  <div class="col-md-4">
  <input id="FirstName" name="FirstName" type="text" placeholder="" class="form-control input-md" required="">
    
  </div>
</div>
<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="LastName">Last Name*</label>  
  <div class="col-md-4">
  <input id="LastName" name="LastName" type="text" placeholder="" class="form-control input-md" required="">
    
  </div>
</div>
<!-- Select Basic -->

<!-- Button -->
<div class="form-group">
  <label class="col-md-4 control-label" for="DeleteButton"></label>
  <div class="col-md-4">
  <table>
  <tr><td><button id="DeleteButton" name="DeleteButton" class="btn btn-default" style="color: #333333;
  background-color: #ffffff;border-color: #cccccc;">Delete</button></td>
  <td><button id="CorpReqButton" name="CorpReqButton" class="btn btn-default" style="color: #333333;
  background-color: #ffffff;border-color: #cccccc;">Send to Corporate Manager</button></td>
  </tr>
 
  </table>
    
      
  </div>
</div>

<!-- Button -->
<div class="form-group">
  <label class="col-md-4 control-label" for="CorpReqButton"></label>
  <div class="col-md-4">
  
  </div>
</div>

</fieldset>
</form>
 
 
</div>
</div>
</body>
</html>