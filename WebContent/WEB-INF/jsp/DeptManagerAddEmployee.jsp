<%@ include file="/WEB-INF/jsp/expHeader.jsp"%>
<%@ include file="/WEB-INF/jsp/DeptManagerNavBar.jsp"%>
<!-- /***************************************************************/ -->
<div class="jumbotron">
	<div class="container">

<c:if test="${not empty Error}">
	<div class="errorblock">
	${Error}
	</div>
</c:if> 

<c:if test="${not empty Success and empty Error}">
	<div class="errorblock">
	${Success}
	</div>
</c:if>
<c:if test="${empty Success and empty Error}">
<form:form class="form-horizontal" method="POST" modelAttribute="addInternalUserForm"	commandname="addInternalUserForm"
				action="DeptManagerAddEmployee.htm">
				
		<!-- Form Name -->
		<legend> Add Employee</legend>
		<fieldset>
		<div class="form-group">
            <form:label class="col-md-4 control-label" path="name">Name:</form:label>
            <div class="col-md-4"><form:input path="name" class="form-control input-md" required="true"/></div>
            <form:errors path="name" />
        </div>
        <div class="form-group">
            <form:label class="col-md-4 control-label" path="number">Phone-No:</form:label>
            <div class="col-md-4"><form:input path="number" class="form-control input-md" required="true"/></div>
            <form:errors path="number" />
        </div>
        <div class="form-group">
            <form:label class="col-md-4 control-label" path="addr">Address:</form:label>
            <div class="col-md-4"><form:input path="addr" class="form-control input-md" required="true"/></div>
            <form:errors path="addr" />
        </div>
        <div class="form-group">
            <form:label class="col-md-4 control-label" path="email">Email:</form:label>
            <div class="col-md-4"><form:input path="email" class="form-control input-md" required="true"/></div>
            <form:errors path="email" />
        </div>
        <div class="form-group">
            <form:label class="col-md-4 control-label" path="social">SSN:</form:label>
            <div class="col-md-4"><form:input path="social" class="form-control input-md" required="true"/></div>
            <form:errors path="social" />
        </div>
        
        <div class="form-group">
            <form:label class="col-md-4 control-label" path="userRole">Role:</form:label>
            <div class="col-md-4"><form:input path="userRole" class="form-control input-md" value="${userRole}" readonly="true" required="true"/></div>
            <form:errors path="userRole" />
        </div>
        
        <div class="form-group">
            <form:label class="col-md-4 control-label" path="departmentName">Select Department:</form:label>
			<div class="col-md-4">
				<form:select class="form-control" path="departmentName" required="true">
					<form:option value="">
					<form:options items="${deptListForUser}" itemValue="departmentName" itemLabel="departmentName"></form:options>
					</form:option>
				</form:select>
			</div>
        </div>

		<div class="form-group">
            <form:label class="col-md-4 control-label" path="username">username:</form:label>
            <div class="col-md-4"><form:input path="username" class="form-control input-md" required="true"/></div>
            <form:errors path="username" />
        </div>
        <!--  
        <div class="form-group">
            <form:label class="col-md-4 control-label" path="password">Password:</form:label>
            <div class="col-md-4"><form:input path="password" type="password" class="form-control input-md" required="true"/></div>
            <form:errors path="password" />
        </div>
		-->

        <div class="form-group">
        	<label class="col-md-4 control-label" for="AddButton"></label>
  			<div class="col-md-4"><button id="AddButton" type="submit" name="AddButton" class="btn btn-default">Add</button></div>
  		</div>
        	
		</fieldset>		
</form:form>

</c:if>

</div>
</div>

</body>
</html>