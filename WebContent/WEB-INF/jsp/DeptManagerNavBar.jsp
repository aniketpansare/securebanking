<!-- ###################### Menu Bar #################################-->
<div id='cssmenu'>
	<ul>
		<li class='has-sub'><a
			href='DeptManagerHomePage.htm'><span>Home</span></a> <!--       /SecureBanking/AddInternalUsrByCorp.htm -->
		<li class='has-sub'><a href='DeptManagerAddEmployee.htm'><span>Add
					Employee</span></a></li>
		<li class='has-sub'><a href='DeptManagerDeleteEmployee.htm'><span>Delete Employee</span></a></li>
		<li class='has-sub'><a href='DeptManagerTransferEmployee.htm'><span>Transfer Employee</span></a></li>
		<li class='has-sub'><a href='DeptManagerDelegateAccount.htm'><span>Delegate Account</span></a></li>
		<li class='has-sub'><a href='RevokeDelegatedAccount.htm'><span>Revoke Delgations</span></a></li>
		
		<li><a href="<c:url value="/j_spring_security_logout" />"
			id="Logout" accesskey="5" title=""><span>Logout</span></a></li>


	</ul>
</div>