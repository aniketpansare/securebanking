<%@ include file="/WEB-INF/jsp/expHeader.jsp"%>
<%@ include file="/WEB-INF/jsp/DeptManagerNavBar.jsp"%>
<!-- /***************************************************************/ -->
<div class="jumbotron">
<div class="container">

<c:if test="${not empty Error}">
	<div class="errorblock">
	${Error}
	</div>
</c:if> 	
<c:if test="${not empty Success and empty Error}">
	<div class="errorblock">
	${Success}
	</div>
</c:if>

<c:if test="${empty Success and empty listOfUsers and empty Error }">
	<div class="errorblock">
	There are No Active employees in departments belonging to this manager.
	Employees may be disabled or may be in pending transfer Approval.
	!!!! Check with Admin !!!
	</div>
</c:if>

<c:if test="${empty Success and not empty listOfUsers and empty Error}">
<form:form class="form-horizontal" method="POST" modelAttribute="addInternalUserForm"	commandname="addInternalUserForm"
				action="DeptManagerTransferEmployee.htm">
				
		<!-- Form Name -->
		<legend> Transfer Employee</legend>
		<fieldset>
		<div class="form-group">
        <form:label class="col-md-4 control-label" path="username">Username:</form:label>
			<div class="col-md-4">
				<form:select class="form-control" path="username" required="true">
					<form:option value="">
					<form:options items="${listOfUsers}" itemValue="username" itemLabel="username"></form:options>
					</form:option>
				</form:select>
			</div>
        </div>
        
        <div class="form-group">
            <form:label class="col-md-4 control-label" path="fromDepartmentName">From Department:</form:label>
			<div class="col-md-4">
				<form:select class="form-control" path="fromDepartmentName" required="true">
					<form:option value="">
					<form:options items="${allDeptList}" itemValue="departmentName" itemLabel="departmentName"></form:options>
					</form:option>
				</form:select>
			</div>
        </div>
        
        <div class="form-group">
            <form:label class="col-md-4 control-label" path="toDepartmentName">To Department:</form:label>
			<div class="col-md-4">
				<form:select class="form-control" path="toDepartmentName" required="true">
					<form:option value="">
					<form:options items="${allDeptList}" itemValue="departmentName" itemLabel="departmentName"></form:options>
					</form:option>
				</form:select>
			</div>
        </div>
        
        <div class="form-group">
        	<label class="col-md-4 control-label" for="TransferButton"></label>
  			<div class="col-md-4"><button id="TransferButton" type="submit" name="TransferButton" class="btn btn-default">Transfer Request</button></div>
  		</div>
        	
		</fieldset>		
</form:form>
</c:if>


</div>
</div>
</body>
</html>