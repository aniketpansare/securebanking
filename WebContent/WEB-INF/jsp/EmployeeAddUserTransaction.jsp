<%@ include file="/WEB-INF/jsp/expHeader.jsp"%>
<%@ include file="/WEB-INF/jsp/regEmpNav-bar.jsp"%>
<!-- /***************************************************************/ -->
<div class="jumbotron">
<div class="container">
 
<c:if test="${not empty Error}">
	<div class="errorblock">
	${Error}
	</div>
</c:if> 
<c:if test="${not empty Success and empty Error}">
	<div>
	${Success}
	</div>
</c:if> 	

<c:if test="${empty Success and empty transaction and empty Error}">
<form:form class="form-horizontal" method="POST" modelAttribute="addInternalUserForm"	commandname="addInternalUserForm"
				action="EmployeeAddUserTransaction.htm">
				
		<!-- Form Name -->
		<legend> Add Customer/Merchant Transaction</legend>
		<fieldset>
		
        <div class="form-group">
            <form:label class="col-md-4 control-label" path="fromAccountNo">From AccountNo (My / Delegated):</form:label>
            <div class="col-md-4">
               <form:select class="form-control" path="fromAccountNo" required="true">
					<form:option value="${curAccountNo}">
						<!--  label="--Select selectedAccountNo"-->
						<c:forEach items="${accounts}" var="account">
 								<option value="${account.getAccountNo()}"  <c:if test="${account.getAccountNo() == curAccountNo}">selected="selected"</c:if>>${account.getAccountName()}: ${account.getMaskedAccountNo()}</option>
					   </c:forEach>
					</form:option>
				</form:select>
			</div>
            <form:errors path="fromAccountNo" />
        </div>
        
        <div class="form-group">
            <form:label class="col-md-4 control-label" path="toAccountNo">To AccountNo:</form:label>
            <div class="col-md-4"><form:input path="toAccountNo" class="form-control input-md" required="true"/></div>
            <form:errors path="toAccountNo" />
        </div>
        
        <div class="form-group">
            <form:label class="col-md-4 control-label" path="amount">Amount:</form:label>
            <div class="col-md-4"><form:input path="amount" class="form-control input-md" required="true"/></div>
            <form:errors path="amount" />
        </div>
        
        <div class="form-group">
        	<label class="col-md-4 control-label" for="TransferButton"></label>
  			<div class="col-md-4"><button id="TransferButton" type="submit" name="TransferButton" class="btn btn-default">Transfer</button></div>
  		</div>
        	
		</fieldset>		
</form:form>
</c:if>
 
 
</div>
</div>
</body>
</html>