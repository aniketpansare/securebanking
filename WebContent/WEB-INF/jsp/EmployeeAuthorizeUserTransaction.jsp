<%@ include file="/WEB-INF/jsp/expHeader.jsp"%>
<%@ include file="/WEB-INF/jsp/regEmpNav-bar.jsp"%>
<!-- /***************************************************************/ -->
<div class="jumbotron">
<div class="container">
 
 <form class="form-horizontal" method='POST' action="/SecureBanking/AdminHomePage.htm">
<fieldset>
<!-- Form Name -->
<legend>Authorize Transaction</legend>
<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="userAccNum">User Account Number</label>  
  <div class="col-md-4">
  <input id="userAccNum" name="userAccNum" type="text" placeholder="" class="form-control input-md" required="">
  </div>
</div>
<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="employeeAccName">Account Holder Name</label>  
  <div class="col-md-4">
  <input id="employeeAccName" name="employeeAccName" type="text" placeholder="" class="form-control input-md" required="">
    
  </div>
</div>
<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="ssn">Last four digits of SSN</label>  
  <div class="col-md-4">
  <input id="ssn" name="ssn" type="text" placeholder="" class="form-control input-md" required="">
    
  </div>
</div>
<!-- Select Basic -->

<!-- Button -->
<div class="form-group">
  <label class="col-md-4 control-label" for="authorizeTransaction"></label>
  <div class="col-md-4">
    <button id="authorizeTransaction" name="authorizeTransaction" class="btn btn-default">Authorize Transaction</button>
  </div>
</div>
</fieldset>
</form>
 
 
</div>
</div>
</body>
</html>