<%@ include file="/WEB-INF/jsp/expHeader.jsp"%>
<%@ include file="/WEB-INF/jsp/regEmpNav-bar.jsp"%>
<!-- /***************************************************************/ -->
<div class="jumbotron">
<div class="container">
 
 <form class="form-horizontal" method='POST' action="/SecureBanking/AdminHomePage.htm">
<fieldset>
<!-- Form Name -->
<legend>Delete a Transaction</legend>
<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="FrmEmployeeAccNum">From Account Number</label>  
  <div class="col-md-4">
  <input id="FrmEmployeeAccNum" name="FrmEmployeeAccNum" type="text" placeholder="" class="form-control input-md" required="">
  </div>
</div>
<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="FrmEmployeeAccNum">To Account Number</label>  
  <div class="col-md-4">
  <input id="ToEmployeeAccNum" name="ToEmployeeAccNum" type="text" placeholder="" class="form-control input-md" required="">
    
  </div>
</div>
<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="LastName">Amount($)</label>  
  <div class="col-md-4">
  <input id="amount" name="amount" type="text" placeholder="" class="form-control input-md" required="">
    
  </div>
</div>
<!-- Select Basic -->

<!-- Button -->
<div class="form-group">
  <label class="col-md-4 control-label" for="deleteTransfer"></label>
  <div class="col-md-4">
    <button id="deleteTransfer" name="deleteTransfer" class="btn btn-default">Delete Transaction</button>
  </div>
</div>
</fieldset>
</form>
 
 
</div>
</div>
</body>
</html>