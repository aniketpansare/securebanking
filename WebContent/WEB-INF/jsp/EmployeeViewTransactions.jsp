<%@ include file="/WEB-INF/jsp/expHeader.jsp"%>
<%@ include file="/WEB-INF/jsp/regEmpNav-bar.jsp"%>
<!-- /***************************************************************/ -->
<div class="jumbotron">
	<div class="container">

		<div id="Content" class="SecureBanking">

			<div id="selectUserAccounts" align="center">
				<form:form class="form-horizontal" method="POST" modelAttribute="transactionFormData"
					commandname="transactionFormData"
					action="EmployeeViewTransactions.htm">
					
					<fieldset>
					<div class="form-group">
						<form:label class="col-md-4 control-label"
							path="selectedAccountNo">Select Account:</form:label>
						<div class="col-md-4">
							<form:select class="form-control" path="selectedAccountNo"
								onchange="submit()">
								<form:option value="${curAccountNo}">
									<!--  label="--Select selectedAccountNo"-->
									<c:forEach items="${accounts}" var="account">
										<option value="${account.getAccountNo()}"
											<c:if test="${account.getAccountNo() == curAccountNo}">selected="selected"</c:if>>${account.getAccountName()}:
											${account.getMaskedAccountNo()}</option>
									</c:forEach>
								</form:option>
							</form:select>
						</div>
						<form:errors path="selectedAccountNo" />
					</div>
					</fieldset>
					
				</form:form>
			</div>



			<table align="center" cellpadding="15" cellspacing="15">
				<tr>
					<th>Date</th>
					<th>From</th>
					<th>To</th>
					<th>Status</th>
					<th>Amount</th>
				</tr>
				<c:if test="${not empty transactions}">
					<c:forEach items="${transactions.iterator()}" var="transaction">
						<tr>
							<!-- <td><c:out value="${element.id}" /></td> -->
							<td><c:out value="${transaction.getTransactionDate()}" /></td>
							<td><c:out
									value="${transaction.getFromAccount().getMaskedAccountNo()}" /></td>
							<td><c:out
									value="${transaction.getToAccount().getMaskedAccountNo()}" /></td>
							<td><c:out value="${transaction.getTransactionStatus()}" /></td>
							<td><fmt:formatNumber
									value="${transaction.getTransactionAmount()}" type="CURRENCY" /></td>
						</tr>
					</c:forEach>
				</c:if>
			</table>
		</div>


	</div>
</div>
</body>
</html>