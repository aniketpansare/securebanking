<%@ include file="/WEB-INF/jsp/expHeader.jsp"%>
<%@ include file="/WEB-INF/jsp/regEmpNav-bar.jsp"%>
<!-- /***************************************************************/ -->
<div class="jumbotron">
<div class="container">
 
<c:if test="${not empty Error}">
	<div class="errorblock">
	${Error}
	</div>
</c:if> 	


<c:if test="${empty transactions and empty Error}">
<form:form class="form-horizontal" method="POST" modelAttribute="addInternalUserForm"	commandname="addInternalUserForm"
				action="EmployeeViewUserTransaction.htm">
				
		<!-- Form Name -->
		<legend> View User Transaction</legend>
		<fieldset>
		
		<div class="form-group">
            <form:label class="col-md-4 control-label" path="username">Username:</form:label>
            <div class="col-md-4"><form:input path="username" class="form-control input-md" required="true"/></div>
            <form:errors path="username" />
        </div>
        
        <div class="form-group">
            <form:label class="col-md-4 control-label" path="selectedAccountNo">AccountNo:</form:label>
            <div class="col-md-4"><form:input path="selectedAccountNo" class="form-control input-md" required="true"/></div>
            <form:errors path="selectedAccountNo" />
        </div>
        
        <div class="form-group">
        	<label class="col-md-4 control-label" for="ViewButton"></label>
  			<div class="col-md-4"><button id="ViewButton" type="submit" name="ViewButton" class="btn btn-default">View Transactions</button></div>
  		</div>
        	
		</fieldset>		
</form:form>
</c:if>
 
 <c:if test="${not empty transactions and empty Error}">
 <table align="center" cellpadding="15" cellspacing="15">
			<tr>
				<th>Date</th>
				<th>From</th>
				<th>To</th>
				<th>Status</th>
				<th>Amount</th>
			</tr>
			<c:if test="${not empty transactions}">
				<c:forEach items="${transactions.iterator()}" var="transaction">
					<tr>
						<!-- <td><c:out value="${element.id}" /></td> -->
						<td><c:out value="${transaction.getTransactionDate()}" /></td>
						<td><c:out value="${transaction.getFromAccount().getMaskedAccountNo()}" /></td>
						<td><c:out value="${transaction.getToAccount().getMaskedAccountNo()}" /></td>
						<td><c:out value="${transaction.getTransactionStatus()}" /></td>
						<td><fmt:formatNumber value="${transaction.getTransactionAmount()}" type="CURRENCY" /></td>
					</tr>
				</c:forEach>
			</c:if>
	</table>
 </c:if>
 
 
</div>
</div>
</body>
</html>