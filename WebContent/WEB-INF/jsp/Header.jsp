<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib tagdir='/WEB-INF/tags' prefix='sc'%>


<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<!-- ##########################  HEADER  ###################################-->

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />


<!-- Prevent page caching -->
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="expires" content="0" />


<title></title>
<meta name="keywords" content="" />
<meta name="description" content="" />
<link
	href="http://fonts.googleapis.com/css?family=Source+Sans+Pro:200,300,400,600,700,900"
	rel="stylesheet" />
<link href="/SecureBanking/resources/css/default.css" rel="stylesheet"
	type="text/css" media="all" />
<link href="/SecureBanking/resources/css/fonts.css" rel="stylesheet"
	type="text/css" media="all" />
<!-- <link href="/SecureBanking/resources/css/navi.css" rel="stylesheet"
	type="text/css" media="all" /> -->
<script src="/SecureBanking/resources/scripts/htmlDatePicker.js"
	type="text/javascript"></script>

<!--[if IE 6]><link href="default_ie6.css" rel="stylesheet" type="text/css" /><![endif]-->
</head>

<!--<div id="logo" class="container">
	
	 <h1>
		<a href="#">Secure Banking<span></span></a>
	</h1>
	<p>
		group 1<a href=""></a>
	</p> 
	<div id="search">
		<form action="#" method="post">
			<fieldset>
				<legend>Site Search</legend>

				<input type="submit" name="go" id="go" value="GO" /> <input
					type="text" value="Search the site&hellip;"
					onfocus="this.value=(this.value=='Search the site&hellip;')? '' : this.value ;" />


			</fieldset>
		</form>
	</div>
</div>-->
<div class="wrapper col1">
  <div id="head">
  <span>
		<img src="/SecureBanking/resources/images/logoSS.jpg" width="45%" height="110px"/>
	</span>
 <span style="top:1px;"><h1>Secure Banking</h1></span>