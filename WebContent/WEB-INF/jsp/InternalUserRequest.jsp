<%@ include file="/WEB-INF/jsp/Header.jsp" %>
<%@ include file="/WEB-INF/jsp/MenuBar-Admin.jsp" %>
<!-- ##################  Add Header and MenuBar ###############################-->
<body id="InternalUserRequests">
	<div id="Content" class="SecureBanking">
		<table align="center" cellpadding="15" cellspacing="15">
			<tr>
				<th><a href="/AddInternalUser.htm">Add Internal User</a></th>
			</tr>			
			<tr>
				<th> <a href="/ModifyInternalUser.htm">Modify Internal User</a> </th>
			</tr>
					<tr>
				<th> <a href="/DeleteInternalUser.htm">Delete Internal User</a> </th>
			</tr>
		</table>
	</div>
</body>
<!-- #############################  Add Footer ###############################-->
<%@ include file="/WEB-INF/jsp/Footer.jsp" %>