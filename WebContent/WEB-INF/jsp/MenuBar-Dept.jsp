<!-- ###################### Menu Bar - Department #################################-->
<div id="deptmenu" class="container">
	<ul>
		<li><a href="DeptManagerPage.htm" id="Home" accesskey="1"
			title="">Home</a></li>
		<li><a href="RequestPII.htm" id="RequestPII" accesskey="2"
			title="">RequestPII</a></li>

		<li><a href="DeptManagerPage.htm" id="SendRequest" accesskey="3"
			title="">Send Request</a> <!-- ***----- for dept manager it should do below operations for users only in his department***/
		/***---- for corp manager it can be done for all employees***/
		/***---- same header for corp n dep level managers***/
		 -->
			<ul>
				<li><a href="AddEmployee.htm">Add Employee</a></li>
				<li><a href="DeleteEmployee.htm">Delete Employee</a></li>
				<li><a href="TransferEmp.htm">Transfer</a></li>
				<li><a href="ViewAccInfo.htm">View Acc Info</a></li>
			</ul></li>
		<li><a href="<c:url value="/j_spring_security_logout" />"
			id="Logout" accesskey="4" title="">Logout</a></li>

	</ul>
</div>
<div id="banner" class="container"></div>
