<!-- ###################### Menu Bar - Regular Employee #################################-->
<div id="depttmenu" class="container">
	<ul>
		<li><a href="DeptManagerPage.htm" id="Home" accesskey="1"
			title="">Home</a></li>
		<li><a href="Requestuser.htm" id="Requestuser" accesskey="2"
			title="">RequestUser</a></li>
		<li><a href="SendRequest.htm" id="SendRequest" accesskey="3"
			title="">Send Request</a></li>
		<li><a href="RegEmpPage.htm" id="Transaction" accesskey="4"
			title="">Transactions</a>
			<ul>
				<li><a href="#">View</a></li>
				<li><a href="#">Create</a></li>
				<li><a href="#">Delete</a></li>
			</ul></li>
		<li><a href="<c:url value="/j_spring_security_logout" />"
			id="Logout" accesskey="5" title="">Logout</a></li>
	</ul>
</div>
<div id="banner" class="container"></div>
