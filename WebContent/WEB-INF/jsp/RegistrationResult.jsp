<%@ include file="/WEB-INF/jsp/Header.jsp"%>
<%@ include file="/WEB-INF/jsp/MenuBar-Login.jsp"%>
<!-- ##################  Add Header and MenuBar ###############################-->

<body id="Register">
	<table>
		<tr>
			<td>Registration Successful.</td>
		</tr>
		<tr>
			<td>First Name</td>
			<td>${FirstName}</td>
		</tr>
		<tr>
			<td>Last Name</td>
			<td>${LastName}</td>
		</tr>
	</table>
</body>


<!-- #############################  Add Footer ###############################-->
<%@ include file="/WEB-INF/jsp/Footer.jsp"%>