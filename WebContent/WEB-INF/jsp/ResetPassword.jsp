<%@ include file="/WEB-INF/jsp/expHeader.jsp"%>
<%@ include file="/WEB-INF/jsp/MenuBar-Login.jsp"%>

<!-- ##################  Add Header and MenuBar ###############################-->
<div class="jumbotron">
<div class="container">

<c:if test="${not empty Error}">
	<div class="errorblock">
	${Error}
	</div>
</c:if> 
 
<c:if test="${not empty Success and empty Error}">
	<div class="errorblock">
	Please check your email for a onetime password.
	</div>
</c:if>

<c:if test="${empty Success and empty Error}">
<form:form class="form-horizontal" method="POST" modelAttribute="loginFormData"	commandname="loginFormData"
				action="ResetPassword.htm">
				
		<!-- Form Name -->
		<legend> Reset Password</legend>
		<fieldset>
		<div class="form-group">
            <form:label class="col-md-4 control-label" path="user">username:</form:label>
            <div class="col-md-4"><form:input path="user" class="form-control input-md" required="true"/></div>
            <form:errors path="user" />
        </div>
        
        <div class="form-group">
        	<label class="col-md-4 control-label" for="ResetPassword"></label>
  			<div class="col-md-4"><button id="ResetPassword" type="submit" name="ResetPassword" class="btn btn-default">Reset</button></div>
  		</div>
        	
		</fieldset>		
</form:form>
</c:if>
 
 
</div>
</div>
</body>
</html>