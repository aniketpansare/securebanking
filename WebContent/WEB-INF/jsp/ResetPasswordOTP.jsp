<%@ include file="/WEB-INF/jsp/expHeader.jsp"%>
<%@ include file="/WEB-INF/jsp/MenuBar-Login.jsp"%>

<!-- ##################  Add Header and MenuBar ###############################-->
<div class="jumbotron">
<div class="container">

<c:if test="${not empty Error}">
	<div class="errorblock">
	${Error}
	</div>
</c:if> 
 
<c:if test="${not empty Success and empty Error}">
	<div class="errorblock">
	${Success} 
	</div>
</c:if>

<c:if test="${empty Success and empty Error}">
<form:form class="form-horizontal" method="POST" modelAttribute="addInternalUserForm"	commandname="addInternalUserForm"
				action="ResetPasswordOTP.htm">
				
		<!-- Form Name -->
		<legend>Reset Password via OTP</legend>
		<fieldset>
		<div class="form-group">
            <form:label class="col-md-4 control-label" path="username">username:</form:label>
            <div class="col-md-4"><form:input path="username" class="form-control input-md" required="true"/></div>
            <form:errors path="username" />
        </div>
        <div class="form-group">
            <form:label class="col-md-4 control-label" path="tokenOTP">OTP Token:</form:label>
            <div class="col-md-4"><form:input path="tokenOTP" class="form-control input-md" required="true"/></div>
            <form:errors path="tokenOTP" />
        </div>
        <div class="form-group">
            <form:label class="col-md-4 control-label" path="password">New Password:</form:label>
            <div class="col-md-4"><form:input path="password" type="password" class="form-control input-md" required="true"/></div>
            <form:errors path="password" />
        </div>

		<div class="form-group">
            <form:label class="col-md-4 control-label" path="repeatPassword">Repeat Password:</form:label>
            <div class="col-md-4"><form:input path="repeatPassword" type="Password" class="form-control input-md" required="true"/></div>
            <form:errors path="repeatPassword" />
        </div>
        <div class="form-group">
        	<label class="col-md-4 control-label" for="ResetPassword"></label>
  			<div class="col-md-4"><button id="ResetPassword" type="submit" name="ResetPassword" class="btn btn-default">Reset</button></div>
  		</div>
        	
		</fieldset>		
</form:form>
</c:if>
 
 
</div>
</div>
</body>
</html>