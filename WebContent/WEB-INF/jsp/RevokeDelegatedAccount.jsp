<%@ include file="/WEB-INF/jsp/expHeader.jsp"%>
<%@ include file="/WEB-INF/jsp/DeptManagerNavBar.jsp"%>
<!-- /***************************************************************/ -->
<div class="jumbotron">
<div class="container">

<c:if test="${not empty Error}">
	<div class="errorblock">
	${Error}
	</div>
</c:if> 	
<c:if test="${not empty Success and empty Error}">
	<div class="errorblock">
	${Success}
	</div>
</c:if>

<c:if test="${empty Success and empty Error}">
<form:form class="form-horizontal" method="POST" modelAttribute="addInternalUserForm"	commandname="addInternalUserForm"
				action="RevokeDelegatedAccount.htm">
				
		<!-- Form Name -->
		<legend> Revoke Delegated Account</legend>
		<fieldset>
		<div class="form-group">
        <form:label class="col-md-4 control-label" path="selectedAccountNo">Delgations:</form:label>
			<div class="col-md-4">
				<form:select class="form-control" path="selectedAccountNo" required="true">
					<c:forEach items="${listOfDelegations}" var="delegation">
      					<option value="${delegation.getId()}">${delegation.getPrivilege()} : ${delegation.getDelegatedTo().getUsername()}: ${delegation.getAccount().getMaskedAccountNo()}</option>
				   </c:forEach>
				</form:select>
			</div>
        </div>
        <div class="form-group">
        	<label class="col-md-4 control-label" for="RevokeButton"></label>
  			<div class="col-md-4"><button id="RevokeButton" type="submit" name="RevokeButton" class="btn btn-default">Revoke</button></div>
  		</div>
        	
		</fieldset>		
</form:form>
</c:if>


</div>
</div>
</body>
</html>