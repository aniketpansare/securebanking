<%@ include file="/WEB-INF/jsp/expHeader.jsp"%>
<%@ include file="/WEB-INF/jsp/navCorporateLvlMngr-bar.jsp"%>

<!-- /***************************************************************/ -->
<div class="jumbotron">
<div class="container">
  <div class="panel panel-default">
  <!-- Default panel contents -->
  <div class="panel-heading">Pending Requests</div>
  <div class="panel-body">
    <p>Please look into the below internal user requests</p>
  </div>

  <!-- Table -->
<form:form>
				<table>
					<tbody>
						<tr>
							<th>Employee Name:</th>
							<td><input type="text" name="transferInternalEmpName" id="transferInternalEmpName"/></td>
						</tr>
						<tr>
							<th>Employee Id:</th>
							<td><input type="text" name="transferInternalEmpId" id="transferInternalEmpId"/></td>
						</tr>
						<tr>
							<th>From Department:</th>
							<td><input type="text" name="transferInternalUsrFromDept" id="transferInternalUsrFromDept"/></td>
						</tr>
						<tr>
							<th>Employee Id:</th>
							<td><input type="text" name="transferInternalUsrToDept" id="transferInternalUsrToDept"/></td>
						</tr>
						
						
						<tr>
							<td><input type="submit" value="Transfer Internal User" /></td>
						</tr>
						
					</tbody>
				</table>
			</form:form></div>
</div>
</body>
</html>