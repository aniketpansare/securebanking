<%@ include file="/WEB-INF/jsp/expHeader.jsp"%>
<%@ include file="/WEB-INF/jsp/navCorporateLvlMngr-bar.jsp"%>

<!-- /***************************************************************/ -->
<div class="jumbotron">
<div class="container">
 <form class="form-horizontal" method='POST' action="/SecureBanking/AdminHomePage.htm">
<fieldset>
<!-- Form Name -->
<legend>Transfer an Internal User</legend>
<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="FirstName">Employee Name*</label>  
  <div class="col-md-4">
  <input id="empName" name="empName" type="text" placeholder="" class="form-control input-md" required="">
    
  </div>
</div>
<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="LastName">Employee Id*</label>  
  <div class="col-md-4">
  <input id="empId" name="empId" type="text" placeholder="" class="form-control input-md" required="">
    
  </div>
</div>
<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="fromDept">From Department*</label>  
  <div class="col-md-4">
    <select id="FromDeptDropdown" name="FromDeptDropdown" class="form-control">
      <option value="0">None</option>
      <option value="1">Human Resources</option>
      <option value="2">IT and Technical Support</option>
      <option value="3">Transaction Monitoring</option>
      <option value="4">Sales</option>
    </select>
  </div>
</div>
<div class="form-group">
  <label class="col-md-4 control-label" for="DeptDropdown">To Department*</label>
  <div class="col-md-4">
    <select id="ToDeptDropdown" name="ToDeptDropdown" class="form-control">
      <option value="0">None</option>
      <option value="1">Human Resources</option>
      <option value="2">IT and Technical Support</option>
      <option value="3">Transaction Monitoring</option>
      <option value="4">Sales</option>
    </select>
  </div>
</div>
<!-- Select Basic -->
<div class="form-group">
  <label class="col-md-4 control-label" for="Role">Role*</label>
  <div class="col-md-4">
    <select id="Role" name="Role" class="form-control">
      <option value="0">None</option>
      <option value="1">Regular Employee</option>
      
    </select>
  </div>
</div>
<div class="form-group">
  <label class="col-md-4 control-label" for="TransferButton"></label>
  <div class="col-md-4">
    <button id="TransferButton" name="TransferButton" class="btn btn-default">Transfer</button>
  </div>
  
</div>
</fieldset>
</form>
</div>
</div>
</body>
</html>