
<%@ include file="/WEB-INF/jsp/expHeader.jsp"%>
<%@ include file="/WEB-INF/jsp/MenuBar.jsp"%>
<!-- /***************************************************************/ -->
<div class="jumbotron">
<div class="container">


	<div id="Content" class="SecureBanking">

		<div id="selectUserAccounts" align="center">
			<form:form method="POST" modelAttribute="transactionFormData"
				commandname="transactionFormData"
				action="ViewTransactions.htm">
				<table>
					<tbody>
						<tr>
							<td>
								<ul>
									<label>Select Account:</label>
									<form:select path="selectedAccountNo" onchange="submit()">
										<form:option value="${curAccountNo}">				<!--  label="--Select selectedAccountNo"-->
											<c:forEach items="${accounts}" var="account">
      											<option value="${account.getAccountNo()}"  <c:if test="${account.getAccountNo() == curAccountNo}">selected="selected"</c:if>>${account.getAccountName()}: ${account.getMaskedAccountNo()}</option>
										   </c:forEach>
										</form:option>
									</form:select>
								</ul>
							</td>
						</tr>
					</tbody>
				</table>
			</form:form>
		</div>



		<table align="center" cellpadding="15" cellspacing="15">
			<tr>
				<th>Date</th>
				<th>From</th>
				<th>To</th>
				<th>Status</th>
				<th>Amount</th>
			</tr>
			<c:if test="${not empty transactions}">
				<c:forEach items="${transactions.iterator()}" var="transaction">
					<tr>
						<!-- <td><c:out value="${element.id}" /></td> -->
						<td><c:out value="${transaction.getTransactionDate()}" /></td>
						<td><c:out value="${transaction.getFromAccount().getMaskedAccountNo()}" /></td>
						<td><c:out value="${transaction.getToAccount().getMaskedAccountNo()}" /></td>
						<td><c:out value="${transaction.getTransactionStatus()}" /></td>
						<td><fmt:formatNumber value="${transaction.getTransactionAmount()}" type="CURRENCY" /></td>
					</tr>
				</c:forEach>
			</c:if>
		</table>
	</div>


</div>
</div>
</body>
</html>