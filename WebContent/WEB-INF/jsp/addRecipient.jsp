<%@ include file="/WEB-INF/jsp/expHeader.jsp"%>
<%@ include file="/WEB-INF/jsp/MenuBar.jsp"%>
<!-- /***************************************************************/ -->
<div class="jumbotron">
<div class="container">



<c:if test="${not empty Error}">
	<div class="errorblock">
	${Error}
	</div>
</c:if> 
<c:if test="${not empty Success and empty Error}">
	<div>
	${Success}
	</div>
</c:if> 	

<c:if test="${empty Success and empty transaction and empty Error}">
<form:form class="form-horizontal" method="POST" modelAttribute="addInternalUserForm"	commandname="addInternalUserForm"
				action="addRecipient.htm">
				
		<!-- Form Name -->
		<legend>Add Recipient</legend>
		<fieldset>
		
        <div class="form-group">
            <form:label class="col-md-4 control-label" path="recipientName">Recipient Name:</form:label>
            <div class="col-md-4"><form:input path="recipientName" class="form-control input-md" required="true"/></div>
            <form:errors path="recipientName" />
        </div>
        
        <div class="form-group">
            <form:label class="col-md-4 control-label" path="selectedAccountNo">Recipient Account No:</form:label>
            <div class="col-md-4"><form:input path="selectedAccountNo" class="form-control input-md" required="true"/></div>
            <form:errors path="selectedAccountNo" />
        </div>
        
        <div class="form-group">
        	<label class="col-md-4 control-label" for="AddRecipientButton"></label>
  			<div class="col-md-4"><button id="AddRecipientButton" type="submit" name="AddRecipientButton" class="btn btn-default">Add Recipient</button></div>
  		</div>
  		
		</fieldset>		
</form:form>
</c:if>

</div>
</div>
</body>
</html>