<%@ include file="/WEB-INF/jsp/expHeader.jsp"%>
<%@ include file="/WEB-INF/jsp/MenuBar.jsp"%>
<!-- /***************************************************************/ -->
<div class="jumbotron">
<div class="container">



<c:if test="${not empty Error}">
	<div class="errorblock">
	${Error}
	</div>
</c:if> 
<c:if test="${not empty Success and empty Error}">
	<div>
	${Success}
	</div>
</c:if> 	

<c:if test="${empty Success and not empty recipients and empty Error}">
<form:form class="form-horizontal" method="POST" modelAttribute="addInternalUserForm"	commandname="addInternalUserForm"
				action="deleteRecipient.htm">
				
		<!-- Form Name -->
		<legend>Delete Recipient</legend>
		<fieldset>
		
		<div class="form-group">
        <form:label class="col-md-4 control-label" path="recipientName">Account No:</form:label>
			<div class="col-md-4">
				<form:select class="form-control" path="recipientName" required="true">
					<c:forEach items="${recipients}" var="recipient">
      					<form:option value="${recipient.getName()}">${recipient.getName()}</form:option>
				     </c:forEach>
				</form:select>
			</div>
        </div>
        

        
        <div class="form-group">
        	<label class="col-md-4 control-label" for="DeleteRecipientButton"></label>
  			<div class="col-md-4"><button id="DeleteRecipientButton" type="submit" name="DeleteRecipientButton" class="btn btn-default">Delete Recipient</button></div>
  		</div>
  		
		</fieldset>		
</form:form>
</c:if>

</div>
</div>
</body>
</html>