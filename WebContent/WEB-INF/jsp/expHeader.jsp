<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib tagdir='/WEB-INF/tags' prefix='sc'%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<!-- ##########################  HEADER  ###################################-->

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "/SecureBanking/xhtml1-strict.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />


<!-- Prevent page caching -->
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="expires" content="0" />


<title></title>
<meta name="keywords" content="" />
<meta name="description" content="" />
<c:choose>
	<c:when test="${request.isSecure()}" >
		<link
			href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:200,300,400,600,700,900"
			rel="stylesheet" />
	</c:when>
	
	<c:otherwise>
		<link
			href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:200,300,400,600,700,900"
			rel="stylesheet" />
	</c:otherwise>
</c:choose>
<link href="/SecureBanking/resources/css/mycss.css" rel="stylesheet" />

<script>
function displayDate()
{
document.getElementById("demo").innerHTML=Date();
}
</script>
<script src="/SecureBanking/resources/scripts/htmlDatePicker.js"
	type="text/javascript"></script>
<title>SunDevil Bank</title>
  </head>

  <body>
	<div id="head">
 <span style="top:0px;
    vertical-align:middle;
    display: table-cell;">
		<img style="height:100px;width:100px; vertical-align: middle;display: table-cell;margin-right: 1em;" src="/SecureBanking/resources/images/SS_Logo_2_by_Rocul.jpg" />
	</span>
 <span style="top:0px;
    vertical-align:middle;
    display: table-cell;">
    <h1 style="margin:0;padding:0;top:0px;list-style:none;float:left;line-height:normal;"><h1>Bank Of SunDevils</h1></span>
 </div>