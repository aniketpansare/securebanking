<%@ include file="/WEB-INF/jsp/expHeader.jsp"%>
<%@ include file="/WEB-INF/jsp/MenuBar-Login.jsp"%>

<!-- /***************************************************************/ -->
<div class="jumbotron">
<div class="container">	
		<c:if test="${not empty error}">
					<div class="form-group" align="center">
						<label class="col-md-4 control-label" for="error">Invalid session.. Please try again.</label>
					</div>
		</c:if>

	<%-- <form class="form-horizontal" name='f' action="<c:url value='j_spring_security_check'/>" method='POST'>	 --%>
	<form autocomplete="off" class="form-horizontal" name='f'  style="margin: 0 auto;width:50%;text-align:center;" action="<c:url value='j_spring_security_check'/>" method='POST'>
		<fieldset>
			<!-- Form Name -->
			<legend>Sign in</legend>
			
			<!-- Text input-->
			<div class="form-group">
			  <label class="col-md-4 control-label" for="username">Username</label>  
			  <div class="col-md-5">
			  <input id="username" name="j_username" type="text" placeholder="Enter your username" class="form-control input-md" required=""/>
			  </div>
			</div>
	
			<!-- Password input-->
			<div class="form-group">
			  <label class="col-md-4 control-label" for="password">Password</label>
			  <div class="col-md-5">
			    <input id="password" name="j_password" type="password" placeholder="Enter your password" class="form-control input-md" required=""/>
			    
			  </div>
			</div>
			
			
			<div class="form-group">
				<div class="col-md-5">
					<!--Table -->
					<table class="table" style=" text-align:center; 
    vertical-align:middle;">
					<tr> <!-- Add Captcha -->
						<td colspan="2" style="text-align:center;margin:0 65%;padding-left:100px "><sc:captcha/></td>
					</tr>
					</table>
				</div>
			</div>
			
			<!-- Button -->
			<div class="form-group">
			  <label class="col-md-4 control-label" for="loginbutton"></label>
			  <div class="col-md-5">
			    <button id="loginbutton" type="submit"  name="loginbutton" class="btn btn-default">Login</button>
			  </div>
			</div>
			
			
		</fieldset>
		<div class="form-group" style=" display: inline-block;">
				<table style="text-align:center;"><tr>
				<td style="align:center;text-align:center;vertical-align:middle;"><a href="Register.htm">Sign Up</a></td><td  style="align:center;text-align:center;vertical-align:middle;"> or <a href="ResetPassword.htm">Reset Your Password</a></td>
				</tr></table>
	    </div>
		
	</form>
</div>
</div>

</html>

		
	