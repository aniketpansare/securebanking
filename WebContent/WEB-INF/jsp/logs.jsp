<%@ include file="/WEB-INF/jsp/include.jsp"%>
<%@ include file="/WEB-INF/jsp/expHeader.jsp"%>
<%@ include file="/WEB-INF/jsp/AdminNavBar.jsp"%>

<!-- /***************************************************************/ -->
<div class="jumbotron">
	<div class="container">

		<c:if test="${not empty Error}">
			<div class="errorblock">${Error}</div>
		</c:if>

		<c:if test="${empty Error}">
			<table border="1px" bordercolor="black" width=80% align="center">
				<form:form class="form-horizontal" method="POST"
					modelAttribute="logFilterData" commandname="logFilterData" action="logs.htm">

					<tr>

						<th><form:input path="loggerFilter"
								class="form-control input-md" placeholder="Logger Class Filter"/></th>
						<th><form:input path="priorityFilter"
								class="form-control input-md" placeholder="Priority Filter"/></th>
						<th><form:input path="messageFilter"
								class="form-control input-md" placeholder="Message Filter"/></th>
						<th><button id="logFilter" type="submit"
								name="logFilter" class="btn btn-default">View Logs</button></th>
					</tr>
				</form:form>
		</table>
		<br>
		<table border="1px" bordercolor="black" width=80% align="center">
			<tr>
				<th>Id</th>
				<th>Time</th>
				<th>Logger</th>
				<th>Priority</th>
				<th>Message</th>
				<th>Stacktrace</th>
			</tr>	
			<c:forEach items="${logEntries}" var="element">

			<tr>
				<td><c:out value="${element.id}" /></td>
				<td><c:out value="${element.time}" /></td>
				<td><c:out value="${element.logger}" /></td>
				<td><c:out value="${element.priority}" /></td>
				<td><c:out value="${element.message}" /></td>
				<td><c:out value="${element.stacktrace}" /></td>
			</tr>			
		</c:forEach>
			</table>
		</c:if>
		
	</div>
</div>

</body>
</html>