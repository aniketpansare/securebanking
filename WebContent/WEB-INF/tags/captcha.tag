<%@ tag import="net.tanesha.recaptcha.ReCaptcha" %>
<%@ tag import="net.tanesha.recaptcha.ReCaptchaImpl" %>
<%@ tag import="net.tanesha.recaptcha.ReCaptchaFactory" %>
 
<script type="text/javascript">var RecaptchaOptions = {theme : 'clean'};</script> 
<%
	if (request.isSecure()) {
    	ReCaptcha reCaptcha = ReCaptchaFactory.newSecureReCaptcha("6LeCIOkSAAAAAHPqdU9xNtUpSQNa3bnZLAB-0d2B", "6LeCIOkSAAAAABxZbikwqaz84qRhqGv_LJ4DbJTO", true);
		((ReCaptchaImpl)reCaptcha).setRecaptchaServer("https://www.google.com/recaptcha/api");
    	out.print(reCaptcha.createRecaptchaHtml(null, null));
    } else {
    	ReCaptcha reCaptcha = ReCaptchaFactory.newReCaptcha("6LeCIOkSAAAAAHPqdU9xNtUpSQNa3bnZLAB-0d2B", "6LeCIOkSAAAAABxZbikwqaz84qRhqGv_LJ4DbJTO", true);
    	out.print(reCaptcha.createRecaptchaHtml(null, null));
    }
%>