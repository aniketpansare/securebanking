/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package main.java;

/**
 * Group of system wide constant values to be enforced.
 * @author jbabb
 */
public class GlobalConstants {
     
    /// Minimum length of a username.
    public static final int USERNAME_MIN_LENGTH = 4;
    
    /// Maximum length of a username.
    public static final int USERNAME_MAX_LENGTH = 64;
     
    
    /// Minimum length of a password.
    public static final int PASSWORD_MIN_LENGTH = 8;
    
    
    /// Maximum length of a password
    public static final int PASSWORD_MAX_LENGTH = 512;
    
    /// Maximum length of an email address.
    public static final int EMAIL_MAX_LENGTH = 128;
    
    /// Maximum length of an amount entry.
    public static final int AMOUNT_MAX_LENTH = 32;
     
}
