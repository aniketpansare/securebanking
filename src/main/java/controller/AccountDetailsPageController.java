package main.java.controller;

import java.util.Set;

import main.java.domain.Account;
import main.java.domain.User;
import main.java.logging.LoggingManager;
import main.java.logging.LoggingManager.Priority;
import main.java.service.UserManager;
import main.java.util.TransactionWrapper;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class AccountDetailsPageController {

	@RequestMapping(value = "/user/Accounts", method = RequestMethod.GET)
	public ModelAndView accounts() {
		final LoggingManager logger = LoggingManager.create("/user/Accounts");
		ModelAndView modelAndView = new ModelAndView();

		/*************************************************************************/
		// Get Logged in username.
		/**************************************************************************/

		Authentication auth = SecurityContextHolder.getContext()
				.getAuthentication();
		final String username = auth.getName(); // get logged in username
		modelAndView.addObject("username", username);

		/*************************************************************************/
		// Fetch user from username.
		/**************************************************************************/
		final UserManager usermanager = UserManager.create();
		User user = null;
		try {
			user = (new TransactionWrapper<User>() {

				@Override
				public User operation(Session s) throws HibernateException {
					try {
						User u = usermanager.getUser(username);

						// Query for accounts
						u.loadAccounts();

						return u;
					} catch (NullPointerException e) {
						logger.catching(Priority.ERROR, e);
						throw e;
					}
				}
			}).execute();
		} catch (HibernateException e) {
			logger.catching(Priority.ERROR, e);
			throw e;
		}

		/*************************************************************************/
		// Fetch accounts from User.
		/**************************************************************************/

		Set<? extends Account> accountListForUser = null;
		accountListForUser = user.getAccounts();

		// fetch all accounts for User.
		if (accountListForUser != null && !accountListForUser.isEmpty()) {
			modelAndView.addObject("accountListForUser", accountListForUser);
		}

		// add model attributes.
		modelAndView.setViewName("Accounts");
		return modelAndView;
	}

}
