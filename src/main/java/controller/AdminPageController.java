package main.java.controller;

import java.nio.charset.CharacterCodingException;

import main.java.domain.Account;
import main.java.domain.Department;
import main.java.domain.Request;
import main.java.domain.Request.Decision;
import main.java.domain.User;
import main.java.domain.User.Status;
import main.java.domain.User.Type;
import main.java.domain.UserData;
import main.java.input.AddInternalUserForm;
import main.java.logging.LoggingManager;
import main.java.logging.LoggingManager.Priority;
import main.java.security.SecurityManager;
import main.java.service.AccountManager;
import main.java.service.DepartmentManager;
import main.java.service.RequestManager;
import main.java.service.UserManager;
import main.java.util.MailService;
import main.java.util.TransactionWrapper;

import org.bouncycastle.crypto.CryptoException;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.BeanFactoryAware;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class AdminPageController implements BeanFactoryAware{
	
	private BeanFactory context;

	@Override
	public void setBeanFactory(BeanFactory arg0) throws BeansException {
		context = arg0;
	}
	
	/**
	 * Returns the Admin home Page.
	 * 
	 * @return
	 */
	@RequestMapping(value = "/admin/AdminHomePage", method = RequestMethod.GET)
	public ModelAndView AdminHomePage() {
		return new ModelAndView("AdminHomePage", "command", null);

	}

	/**
	 * Get Method for Adding Internal User.
	 * 
	 * @return
	 */
	@RequestMapping(value = "/admin/AdminAddInternalUser", method = RequestMethod.GET)
	public ModelAndView AdminAddInternalUser() {
		final LoggingManager logger = LoggingManager
				.create("/admin/AdminAddInternalUser");
		ModelAndView modelAndView = new ModelAndView();

		/* get Session User Name */
		Authentication auth = SecurityContextHolder.getContext()
				.getAuthentication();
		final String sessionUserName = auth.getName(); // get logged in username
		modelAndView.addObject("sessionUserName", sessionUserName);

		/************************************************************/
		// Get List of department to which user can be added
		/************************************************************/
		final DepartmentManager deptmanager = DepartmentManager.create();
		Iterable<? extends Department> deptListForUser = null;
		try {
			deptListForUser = (new TransactionWrapper<Iterable<? extends Department>>() {

				@Override
				public Iterable<? extends Department> operation(Session s)
						throws HibernateException {
					try {
						return deptmanager.getDepartmentsLike("%");

					} catch (NullPointerException e) {
						logger.catching(Priority.ERROR, e);
						throw e;
					}
				}
			}).execute();
		} catch (HibernateException e) {
			logger.catching(Priority.ERROR, e);
			throw e;
		}

		// fetch all accounts for User.
		if (deptListForUser != null && deptListForUser.iterator().hasNext()) {
			modelAndView.addObject("deptListForUser", deptListForUser);
		}

		/* Add Object to contain form data */
		modelAndView
				.addObject("addInternalUserForm", new AddInternalUserForm());
		/* Add List of userRoles that user can have */
		modelAndView.addObject("userRoleList", User.Type.values());
		modelAndView.setViewName("AdminAddInternalUser");
		return modelAndView;
	}

	/**
	 * Post Method for adding internal User.
	 * 
	 * @param AddInternalUserForm
	 * @return
	 */
	@RequestMapping(value = "/admin/AdminAddInternalUser", method = RequestMethod.POST)
	public ModelAndView AdminAddInternalUserPost(
			@ModelAttribute AddInternalUserForm addInternalUserForm) {

		final LoggingManager logger = LoggingManager
				.create("/admin/AdminAddInternalUser");
		final ModelAndView modelAndView = new ModelAndView();

		/* get Session User Name */
		Authentication auth = SecurityContextHolder.getContext()
				.getAuthentication();
		final String sessionUserName = auth.getName(); // get logged in username
		modelAndView.addObject("sessionUserName", sessionUserName);

		/* Get user data from input form */
		final SecurityManager securityManager = SecurityManager.get();

		final String username = addInternalUserForm.getUsername();
		final String password = securityManager.randomString(64); //addInternalUserForm.getPassword();
		final String name = addInternalUserForm.getName();
		final String number = addInternalUserForm.getNumber();
		final String addr = addInternalUserForm.getAddr();
		final String email = addInternalUserForm.getEmail();
		final String social = addInternalUserForm.getSocial();

		// final String accountDescription = "Internal Account";
		final double startingBalance = 0.0;

		final Type userRole = addInternalUserForm.getUserRole();

		final String departmentName = addInternalUserForm.getDepartmentName();

		/**************************************************************/
		// Need to validate input Data and return if error.
		/***************************************************************/
		

		try {
			boolean flag = false;
			if (!securityManager.isUsername(username)) {
				flag = true;
				modelAndView.addObject("Error", "Invalid Username!!");
			} else if (!securityManager.isAlpha(name) && name.length() > 50) {
				flag = true;
				modelAndView.addObject("Error", "Invalid Name");
			} else if (!securityManager.isPhoneNumber(number)) {
				flag = true;
				modelAndView.addObject("Error", "Invalid Phone No..!!");
			} else if (!securityManager.isASCII(addr) && addr.length() > 100) {
				flag = true;
				modelAndView.addObject("Error", "Invalid Address!!");
			} else if (!securityManager.isEmail(email)) {
				flag = true;
				modelAndView.addObject("Error", "Invalid Email.!!");
			}
			else if (userRole.equals(Type.UNVALIDATED)) {
				flag = true;
				modelAndView.addObject("Error", "Cannot add a unvalidated User.!!");
			}
			else if (!securityManager.isPhoneNumber(social)) {
				flag = true;
				modelAndView.addObject("Error", "Invalid Social Security Number.!!");
			}
			
			if (flag) {
				modelAndView.setViewName("AdminAddInternalUser");
				return modelAndView;
			}

		} catch (Exception e) {
			modelAndView.addObject("Error", "Invalid Input Data..!!");
			modelAndView.setViewName("AdminAddInternalUser");
			return modelAndView;
		}

		/**************************************************************/
		// create User object to be added to database;
		/***************************************************************/

		final UserManager um = UserManager.create();
		final AccountManager accountmanager = AccountManager.create();
		final DepartmentManager departmentManager = DepartmentManager.create();

		try {
			(new TransactionWrapper<Void>() {

				@Override
				public Void operation(Session s) throws HibernateException,
						IllegalArgumentException {
					try {

						User sessionUser = um.getUser(sessionUserName);
						/******************************************************
						 * Check if username already exist.
						 *****************************************************/
						final User checkUser = um.getUser(username);
						if (checkUser != null) {
							modelAndView.addObject("Error",
									"User name already taken..!!");
							return null;
						}
						/*****************************************************/

						// Create User
						final User user = um.createUser(username,
								password, UserData.create(name, number, addr,
										email, social));

						if (user != null) {

							/********** Specific settings ************/
							user.setStatus(Status.ACTIVE);
							user.setUserType(userRole);

							if (userRole.equals(Type.CUSTOMER)
									|| userRole.equals(Type.MERCHANT)) {
								final Account a = accountmanager.createAccount(
										user, startingBalance, name,
										"External Account");
								
								a.setAccountStatus(Account.Status.ACTIVE);
								accountmanager.saveAccount(a);

							}
							else
							{
								/* Get the department to which user is to be added */
								Department department = departmentManager
										.getDepartment(departmentName);
	
								// make sure department exists.
								if (department == null) {
									throw new IllegalArgumentException();
								}
	
								department.loadEmployees();
								// Add user to the department
								department.addEmployee(user);
	
								// save department object after adding employee
								departmentManager.saveDepartment(department);
							}
							
							
							try {
								um.sendWelcomePackage(context, user);
							} catch (CryptoException e) {
								logger.catching(Priority.ERROR, e);
								throw new RuntimeException(e);
							}
							
							// Save user
							um.saveUser(user);

						}

					} catch (NullPointerException e) {
						logger.catching(Priority.ERROR, e);
						throw e;
					} catch (CharacterCodingException e) {
						logger.catching(Priority.ERROR, e);
					}
					return null;
				}
			}).execute();
		} catch (HibernateException e) {
			logger.catching(Priority.ERROR, e);
			throw e;
		}

		modelAndView.addObject("Success",
				"The User was successfully Added to database.. Password reset email sent to the user.");
		modelAndView.setViewName("AdminAddInternalUser");
		return modelAndView;
	}

	/**
	 * Get request to delete the User.
	 * 
	 * @return
	 */
	@RequestMapping(value = "/admin/AdminDeleteInternalUser", method = RequestMethod.GET)
	public ModelAndView AdminDeleteInternalUser() {
		ModelAndView modelAndView = new ModelAndView();

		/* get Session User Name */
		Authentication auth = SecurityContextHolder.getContext()
				.getAuthentication();
		final String sessionUserName = auth.getName(); // get logged in username
		modelAndView.addObject("sessionUserName", sessionUserName);

		/* Add Object to contain username to be deleted */
		modelAndView
				.addObject("addInternalUserForm", new AddInternalUserForm());
		modelAndView.setViewName("AdminDeleteInternalUser");
		return modelAndView;
	}

	/**
	 * Post Request to delete internal user.
	 * 
	 * @param addInternalUserForm
	 * @return
	 */
	@RequestMapping(value = "/admin/AdminDeleteInternalUser", method = RequestMethod.POST)
	public ModelAndView AdminDeleteInternalUserPost(
			@ModelAttribute AddInternalUserForm addInternalUserForm) {

		final LoggingManager logger = LoggingManager
				.create("/admin/AdminDeleteInternalUser");
		final ModelAndView modelAndView = new ModelAndView();

		/* get Session User Name */
		Authentication auth = SecurityContextHolder.getContext()
				.getAuthentication();
		final String sessionUserName = auth.getName(); // get logged in username
		modelAndView.addObject("sessionUserName", sessionUserName);

		/*****************************************************/
		/* Get username to delete the user */
		/***************************************************/
		final String username = addInternalUserForm.getUsername();

		/**************************************************************/
		// Need to validate input Data and return error.
		/***************************************************************/

		/* Get user data from input form */
		final SecurityManager securityManager = SecurityManager.get();

		try {
			boolean flag = false;
			if (!securityManager.isUsername(username)) {
				flag = true;
				modelAndView.addObject("Error", "Invalid Username Format!!");
			}
			if (flag) {
				modelAndView.setViewName("AdminDeleteInternalUser");
				return modelAndView;
			}

		} catch (Exception e) {
			modelAndView.addObject("Error", "Invalid Input Data..!!");
			modelAndView.setViewName("AdminDeleteInternalUser");
			return modelAndView;
		}

		/*****************************************************/
		/* Fetch user object to be marked as deleted in database */
		/***************************************************/
		// fetch user from username.

		if (!username.contentEquals(sessionUserName)) // cannot delete himself
		{
			final UserManager s_usermanager = UserManager.create();

			try {
				(new TransactionWrapper<Void>() {

					@Override
					public Void operation(Session s) throws HibernateException,
							IllegalArgumentException {
						try {
							User sessionUser = s_usermanager.getUser(sessionUserName);
							User user = s_usermanager.getUser(username);

							if (user != null
									&& !user.getStatus().equals(Status.DELETED)) {
								user.setStatus(Status.DELETED);
								s_usermanager.saveUser(user);
								
								MailService mailer = (MailService)context.getBean("mailService");
								mailer.sendNotifyMail(user,"You have been deleted from database.\n Modifier : " + sessionUser.getUserId()+".");

							} else {
								modelAndView
										.addObject("Error",
												"The User doesn't exist or is deleted.");
								return null;
							}

						} catch (NullPointerException e) {
							logger.catching(Priority.ERROR, e);
							throw e;
						}
						return null;
					}
				}).execute();
			} catch (HibernateException e) {
				logger.catching(Priority.ERROR, e);
				throw e;
			}

			modelAndView.addObject("Success", "The User is marked as deleted");
		} else {
			modelAndView.addObject("Error", "The User cannot delete itself");
		}

		modelAndView.setViewName("AdminDeleteInternalUser");
		return modelAndView;

	}

	/**
	 * Get Request to Modify Internal User.
	 * 
	 * @return
	 */
	@RequestMapping(value = "/admin/AdminModifyInternalUser", method = RequestMethod.GET)
	public ModelAndView ModifyInternalUser() {
		ModelAndView modelAndView = new ModelAndView();

		/* get Session User Name */
		Authentication auth = SecurityContextHolder.getContext()
				.getAuthentication();
		final String sessionUserName = auth.getName(); // get logged in username
		modelAndView.addObject("sessionUserName", sessionUserName);

		/* Add Object to contain user data to be modified */
		modelAndView
				.addObject("addInternalUserForm", new AddInternalUserForm());
		modelAndView.setViewName("AdminModifyInternalUser");
		return modelAndView;
	}

	/**
	 * Post Request to fetch Internal USer to modify.
	 * 
	 * @return
	 */
	@RequestMapping(value = "/admin/AdminModifyInternalUser", method = RequestMethod.POST)
	public ModelAndView ModifyInternalUserPost(
			@ModelAttribute AddInternalUserForm addInternalUserForm) {

		final LoggingManager logger = LoggingManager
				.create("/admin/AdminModifyInternalUser");
		final ModelAndView modelAndView = new ModelAndView();

		/* get Session User Name */
		Authentication auth = SecurityContextHolder.getContext()
				.getAuthentication();
		final String sessionUserName = auth.getName(); // get logged in username
		modelAndView.addObject("sessionUserName", sessionUserName);

		/*****************************************************/
		/* Get username to modify the user */
		/***************************************************/
		final String username = addInternalUserForm.getUsername();

		/**************************************************************/
		// Need to validate input Data and return error.
		/***************************************************************/

		/* Get user data from input form */
		final SecurityManager securityManager = SecurityManager.get();

		try {
			boolean flag = false;
			if (!securityManager.isUsername(username)) {
				flag = true;
				modelAndView.addObject("Error", "Invalid Username Format!!");
			}
			if (flag) {
				modelAndView.setViewName("AdminModifyInternalUser");
				return modelAndView;
			}

		} catch (Exception e) {
			modelAndView.addObject("Error", "Invalid Input Data..!!");
			modelAndView.setViewName("AdminModifyInternalUser");
			return modelAndView;
		}

		/*****************************************************/
		/* Fetch user object to be modified */
		/***************************************************/
		// fetch user from username.
		final UserManager s_usermanager = UserManager.create();
		final DepartmentManager departmentManager = DepartmentManager.create();
		User user = null;

		try {
			user = (new TransactionWrapper<User>() {

				@Override
				public User operation(Session s) throws HibernateException,
						IllegalArgumentException {
					try {
						User u = s_usermanager.getUser(username);

						if (u == null) {
							modelAndView.addObject("Error",
									"Username Doesn't exist in the database");
							return null;
						}

						u.getData().getName();

						Iterable<? extends Department> allDepartments = departmentManager
								.getDepartmentsLike("%");
						modelAndView.addObject("deptListForUser",
								allDepartments);

						return u;

					} catch (NullPointerException e) {
						logger.catching(Priority.ERROR, e);
						throw e;
					}
				}
			}).execute();
		} catch (HibernateException e) {
			logger.catching(Priority.ERROR, e);
			throw e;
		}

		if (user != null) {
			modelAndView.addObject("userToModify", user);
			modelAndView.addObject("uname", user.getData().getName());

			/* Add Object to contain user data to be modified */
			modelAndView.addObject("addInternalUserForm",
					new AddInternalUserForm());
			modelAndView.addObject("userRoleList", User.Type.values());
			modelAndView.addObject("userStatusList", User.Status.values());

			modelAndView.addObject("Success", "Loaded user data to modify");
		} else {
			modelAndView.addObject("Error",
					"Username Doesn't exist in the database");
		}

		modelAndView.setViewName("AdminModifyInternalUser");
		return modelAndView;
	}

	/**
	 * Post Request to modify Internal USer Data.
	 * 
	 * @return
	 */
	@RequestMapping(value = "/admin/AdminModifyInternalUserData", method = RequestMethod.POST)
	public ModelAndView ModifyInternalUserPostData(
			@ModelAttribute AddInternalUserForm addInternalUserForm) {

		final LoggingManager logger = LoggingManager
				.create("/admin/AdminModifyInternalUser");
		final ModelAndView modelAndView = new ModelAndView();

		/* get Session User Name */
		Authentication auth = SecurityContextHolder.getContext()
				.getAuthentication();
		final String sessionUserName = auth.getName(); // get logged in username
		modelAndView.addObject("sessionUserName", sessionUserName);

		/*****************************************************/
		/* Get modified user data from input form */
		/***************************************************/
		//final String password = addInternalUserForm.getPassword();
		final String name = addInternalUserForm.getName();
		final String number = addInternalUserForm.getNumber();
		final String addr = addInternalUserForm.getAddr();
		final String social = addInternalUserForm.getSocial();
		final String email = addInternalUserForm.getEmail();
		final Status userStatus = addInternalUserForm.getUserStatus();

		/* user name cannot be modified will be a select only field */
		final String username = addInternalUserForm.getUsername();

		// final String departmentName =
		// addInternalUserForm.getDepartmentName();

		/**************************************************************/
		// Need to validate input Data and return if error.
		/***************************************************************/
		/* Get user data from input form */
		final SecurityManager securityManager = SecurityManager.get();

		try {
			boolean flag = false;
			if (!securityManager.isUsername(username)) {
				flag = true;
				modelAndView.addObject("Error", "Invalid Username!!");
			} /*else if (!securityManager.isPassword(password)) {
				flag = true;
				modelAndView.addObject("Error",
						"Invalid Password!! (min 8 chars)");
			} */else if (!securityManager.isAlpha(name) && name.length() > 50) {
				flag = true;
				modelAndView.addObject("Error", "Invalid Name");
			} else if (!securityManager.isPhoneNumber(number)) {
				flag = true;
				modelAndView.addObject("Error", "Invalid Phone No..!!");
			} else if (!securityManager.isASCII(addr) && addr.length() > 100) {
				flag = true;
				modelAndView.addObject("Error", "Invalid Address!!");
			} else if (!securityManager.isEmail(email)) {
				flag = true;
				modelAndView.addObject("Error", "Invalid Email.!!");
			}
			else if (!securityManager.isPhoneNumber(social)) {
				flag = true;
				modelAndView.addObject("Error", "Invalid Social Security Number.!!");
			}else if (sessionUserName.equals(username))
			{
				modelAndView.addObject("Error", "User cannot modify his own details.. Please as another admin.!!");
			}

			if (flag) {
				modelAndView.setViewName("AdminModifyInternalUser");
				return modelAndView;
			}

		} catch (Exception e) {
			modelAndView.addObject("Error", "Invalid Input Data..!!");
			modelAndView.setViewName("AdminModifyInternalUser");
			return modelAndView;
		}

		/*****************************************************/
		/* Fetch user object to be modified from database */
		/***************************************************/
		// fetch user from username.
		final UserManager s_usermanager = UserManager.create();
		final DepartmentManager departmentManager = DepartmentManager.create();

		try {
			(new TransactionWrapper<Void>() {

				@Override
				public Void operation(Session s) throws HibernateException,
						IllegalArgumentException {
					try {
						User sessionUser = s_usermanager.getUser(sessionUserName);
						User u = s_usermanager.getUser(username);

						if (u == null) {
							modelAndView.addObject("Error",
									"Username doesn't exist in the database;");
							return null;
						}

						u.getData().setName(name);
						u.getData().setContactNumber(number);
						u.getData().setAddress(addr);
						u.getData().setSsn(social);
						u.getData().setEmail(email);
						u.setStatus(userStatus);

						// check if password is in correct format.
						/*
						try {
						u.getPasswordContainer().setPassword(password);
						} catch (CharacterCodingException e) {

						}*/

						s_usermanager.saveUser(u);
						
						MailService mailer = (MailService)context.getBean("mailService");
						mailer.sendNotifyMail(u,"Your personal information has been modified as per your request.\n Modifier : " + sessionUser.getUserId()+".");

						/* Get the department to which user is to be added */

						/*
						 * Department department =
						 * departmentManager.getDepartment(departmentName);
						 * 
						 * // make sure department exists.
						 * 
						 * if (department != null) {
						 * 
						 * department.loadEmployees(); // Add user to the
						 * department department.addEmployee(u);
						 * 
						 * // save department object after adding employee
						 * departmentManager.saveDepartment(department); }
						 */

					} catch (NullPointerException e) {
						logger.catching(Priority.ERROR, e);
						throw e;
					}
					return null;
				}
			}).execute();
		} catch (HibernateException e) {
			logger.catching(Priority.ERROR, e);
			throw e;
		}

		modelAndView.addObject("ModifySuccess",
				"The User data was modified successfully.. User has been Notified.");
		modelAndView.setViewName("AdminModifyInternalUser");
		return modelAndView;
	}

	@RequestMapping(value = "/admin/AdminAddRequests", method = RequestMethod.GET)
	public ModelAndView AdminAddRequests() {
		final LoggingManager logger = LoggingManager
				.create("/admin/AdminAddRequests");
		final ModelAndView modelAndView = new ModelAndView();

		/* get Session User Name */
		Authentication auth = SecurityContextHolder.getContext()
				.getAuthentication();
		final String sessionUserName = auth.getName(); // get logged in username
		modelAndView.addObject("sessionUserName", sessionUserName);

		/*****************************************************/
		/* Fetch requests from Request Table */
		/***************************************************/
		final RequestManager requestManager = RequestManager.create();

		try {
			(new TransactionWrapper<Void>() {

				@Override
				public Void operation(Session s) throws HibernateException,
						IllegalArgumentException {
					try {

						Iterable<? extends Request> requests = requestManager
								.getRequests(Request.Status.PENDING,
										Request.RequestType.ADD_USER, 0, 20);

						if (requests != null && requests.iterator().hasNext()) {
							modelAndView.addObject("requests", requests);
						} else {
							modelAndView.addObject("Error",
									"No Pending Requests");
							return null;
						}

					} catch (NullPointerException e) {
						logger.catching(Priority.ERROR, e);
						throw e;
					}
					return null;
				}
			}).execute();
		} catch (HibernateException e) {
			logger.catching(Priority.ERROR, e);
			throw e;
		}

		modelAndView
				.addObject("addInternalUserForm", new AddInternalUserForm());
		modelAndView.setViewName("AdminAddRequests");
		return modelAndView;
	}

	@RequestMapping(value = "/admin/AdminAddRequests", method = RequestMethod.POST)
	public ModelAndView AdminAddRequestsPost(
			@ModelAttribute AddInternalUserForm addInternalUserForm,
			@RequestParam(value = "approveParam") final String approveOrDeny,
			@RequestParam(value = "requestId") String requestId) {
		final LoggingManager logger = LoggingManager
				.create("/admin/AdminAddRequests");
		final ModelAndView modelAndView = new ModelAndView();

		/* get Session User Name */
		Authentication auth = SecurityContextHolder.getContext()
				.getAuthentication();
		final String sessionUserName = auth.getName(); // get logged in username
		modelAndView.addObject("sessionUserName", sessionUserName);

		long tmpId = 0l;
		try {
			tmpId = Long.parseLong(requestId);
		} catch (NullPointerException | NumberFormatException e) {
			logger.catching(Priority.ERROR, e);
			modelAndView.addObject("Error", "Invalid request ID...!!");
			modelAndView.setViewName("MoneyTransfer");
			return modelAndView;
		}

		final long reqId = tmpId;

		final UserManager userManager = UserManager.create();
		final RequestManager requestManager = RequestManager.create();

		try {
			(new TransactionWrapper<Void>() {

				@Override
				public Void operation(Session s) throws HibernateException,
						IllegalArgumentException {
					try {
						User user = userManager.getUser(sessionUserName);

						Request request = requestManager.getRequest(reqId);

						if (request != null
								&& request.getRequestStatus().equals(
										Request.Status.PENDING)) {
							if (approveOrDeny.equals("Approve")) {

								User userToAdd = request.getSubject();
								if (userToAdd != null) {
									userToAdd.setStatus(Status.ACTIVE);
									userManager.saveUser(userToAdd);
								}

								request.decide(user, Decision.APPROVE);
								requestManager.saveRequest(request);

								modelAndView.addObject("Success", "Request ("
										+ reqId + ") is Approved");
							} else if (approveOrDeny.equals("Deny") && request!=null) {
								
								User userToAdd = request.getSubject();
								if (userToAdd != null) {
									userToAdd.setStatus(Status.DELETED);
									userManager.saveUser(userToAdd);
								}

								request.decide(user, Decision.DENY);
								requestManager.saveRequest(request);
								modelAndView.addObject("Success", "Request ("
										+ reqId + ") is Denied");
							}

						} else {
							modelAndView.addObject("Error",
									"No Pending Request with ID:" + reqId);
							return null;
						}

					} catch (NullPointerException e) {
						logger.catching(Priority.ERROR, e);
						throw e;
					}
					return null;
				}
			}).execute();
		} catch (HibernateException e) {
			logger.catching(Priority.ERROR, e);
			throw e;
		}

		modelAndView.setViewName("AdminAddRequests");
		return modelAndView;
	}

	@RequestMapping(value = "/admin/AdminDeleteRequests", method = RequestMethod.GET)
	public ModelAndView AdminDeleteRequests() {
		final LoggingManager logger = LoggingManager
				.create("/admin/AdminDeleteRequests");
		final ModelAndView modelAndView = new ModelAndView();

		/* get Session User Name */
		Authentication auth = SecurityContextHolder.getContext()
				.getAuthentication();
		final String sessionUserName = auth.getName(); // get logged in username
		modelAndView.addObject("sessionUserName", sessionUserName);

		/*****************************************************/
		/* Fetch requests from Request Table */
		/***************************************************/
		final RequestManager requestManager = RequestManager.create();

		try {
			(new TransactionWrapper<Void>() {

				@Override
				public Void operation(Session s) throws HibernateException,
						IllegalArgumentException {
					try {

						Iterable<? extends Request> requests = requestManager
								.getRequests(Request.Status.PENDING,
										Request.RequestType.DELETE_USER, 0, 20);

						if (requests != null && requests.iterator().hasNext()) {
							modelAndView.addObject("requests", requests);
						} else {
							modelAndView.addObject("Error",
									"No Pending Requests");
							return null;
						}

					} catch (NullPointerException e) {
						logger.catching(Priority.ERROR, e);
						throw e;
					}
					return null;
				}
			}).execute();
		} catch (HibernateException e) {
			logger.catching(Priority.ERROR, e);
			throw e;
		}

		modelAndView
				.addObject("addInternalUserForm", new AddInternalUserForm());
		modelAndView.setViewName("AdminDeleteRequests");
		return modelAndView;
	}

	@RequestMapping(value = "/admin/AdminDeleteRequests", method = RequestMethod.POST)
	public ModelAndView AdminDeleteRequestsPost(
			@ModelAttribute AddInternalUserForm addInternalUserForm,
			@RequestParam(value = "approveParam") final String approveOrDeny,
			@RequestParam(value = "requestId") String requestId) {
		final LoggingManager logger = LoggingManager
				.create("/admin/AdminDeleteRequests");
		final ModelAndView modelAndView = new ModelAndView();

		/* get Session User Name */
		Authentication auth = SecurityContextHolder.getContext()
				.getAuthentication();
		final String sessionUserName = auth.getName(); // get logged in username
		modelAndView.addObject("sessionUserName", sessionUserName);

		long tmpId = 0l;
		try {
			tmpId = Long.parseLong(requestId);
		} catch (NullPointerException | NumberFormatException e) {
			logger.catching(Priority.ERROR, e);
			modelAndView.addObject("Error", "Invalid request ID...!!");
			modelAndView.setViewName("MoneyTransfer");
			return modelAndView;
		}

		final long reqId = tmpId;

		final UserManager userManager = UserManager.create();
		final RequestManager requestManager = RequestManager.create();

		try {
			(new TransactionWrapper<Void>() {

				@Override
				public Void operation(Session s) throws HibernateException,
						IllegalArgumentException {
					try {
						User user = userManager.getUser(sessionUserName);

						Request request = requestManager.getRequest(reqId);

						if (request != null
								&& request.getRequestStatus().equals(
										Request.Status.PENDING)) {
							if (approveOrDeny.equals("Approve")) {

								User userToDelete = request.getSubject();
								if (userToDelete != null) {
									userToDelete.setStatus(Status.DELETED);
									userManager.saveUser(userToDelete);
								}

								request.decide(user, Decision.APPROVE);
								requestManager.saveRequest(request);

								modelAndView.addObject("Success", "Request ("
										+ reqId + ") is Approved");
							} else if (approveOrDeny.equals("Deny") && request!=null) {
								
								User userToDelete = request.getSubject();
								if (userToDelete != null) {
									userToDelete.setStatus(Status.ACTIVE);
									userManager.saveUser(userToDelete);
								}

								request.decide(user, Decision.DENY);
								requestManager.saveRequest(request);
								modelAndView.addObject("Success", "Request ("
										+ reqId + ") is Denied");
							}

						} else {
							modelAndView.addObject("Error",
									"No Pending Request with ID:" + reqId);
							return null;
						}

					} catch (NullPointerException e) {
						logger.catching(Priority.ERROR, e);
						throw e;
					}
					return null;
				}
			}).execute();
		} catch (HibernateException e) {
			logger.catching(Priority.ERROR, e);
			throw e;
		}

		modelAndView.setViewName("AdminDeleteRequests");
		return modelAndView;
	}

	@RequestMapping(value = "/admin/AdminTransferRequests", method = RequestMethod.GET)
	public ModelAndView AdminTransferRequests() {
		final LoggingManager logger = LoggingManager
				.create("/admin/AdminTransferRequests");
		final ModelAndView modelAndView = new ModelAndView();

		/* get Session User Name */
		Authentication auth = SecurityContextHolder.getContext()
				.getAuthentication();
		final String sessionUserName = auth.getName(); // get logged in username
		modelAndView.addObject("sessionUserName", sessionUserName);

		/*****************************************************/
		/* Fetch requests from Request Table */
		/***************************************************/
		final RequestManager requestManager = RequestManager.create();

		try {
			(new TransactionWrapper<Void>() {

				@Override
				public Void operation(Session s) throws HibernateException,
						IllegalArgumentException {
					try {

						Iterable<? extends Request> requests = requestManager
								.getRequests(Request.Status.PENDING,
										Request.RequestType.TRANSFER_USER, 0,
										20);

						if (requests != null && requests.iterator().hasNext()) {
							modelAndView.addObject("requests", requests);
						} else {
							modelAndView.addObject("Error",
									"No Pending Requests");
							return null;
						}

					} catch (NullPointerException e) {
						logger.catching(Priority.ERROR, e);
						throw e;
					}
					return null;
				}
			}).execute();
		} catch (HibernateException e) {
			logger.catching(Priority.ERROR, e);
			throw e;
		}

		modelAndView
				.addObject("addInternalUserForm", new AddInternalUserForm());
		modelAndView.setViewName("AdminTransferRequests");
		return modelAndView;
	}

	@RequestMapping(value = "/admin/AdminTransferRequests", method = RequestMethod.POST)
	public ModelAndView AdminTransferRequestsPost(
			@ModelAttribute AddInternalUserForm addInternalUserForm,
			@RequestParam(value = "approveParam") final String approveOrDeny,
			@RequestParam(value = "requestId") String requestId) {
		final LoggingManager logger = LoggingManager
				.create("/admin/AdminTransferRequests");
		final ModelAndView modelAndView = new ModelAndView();

		/* get Session User Name */
		Authentication auth = SecurityContextHolder.getContext()
				.getAuthentication();
		final String sessionUserName = auth.getName(); // get logged in username
		modelAndView.addObject("sessionUserName", sessionUserName);

		long tmpId = 0l;
		try {
			tmpId = Long.parseLong(requestId);
		} catch (NullPointerException | NumberFormatException e) {
			logger.catching(Priority.ERROR, e);
			modelAndView.addObject("Error", "Invalid request ID...!!");
			modelAndView.setViewName("MoneyTransfer");
			return modelAndView;
		}

		final long reqId = tmpId;

		final UserManager userManager = UserManager.create();
		final RequestManager requestManager = RequestManager.create();
		final DepartmentManager departmentManager = DepartmentManager.create();

		try {
			(new TransactionWrapper<Void>() {

				@Override
				public Void operation(Session s) throws HibernateException,
						IllegalArgumentException {
					try {
						User user = userManager.getUser(sessionUserName);

						Request request = requestManager.getRequest(reqId);

						if (request != null
								&& request.getRequestStatus().equals(
										Request.Status.PENDING)) {
							if (approveOrDeny.equals("Approve")) {

								User userToTransfer = request.getSubject();
								String fromName = null;
								String toName = null;

								if (userToTransfer != null) {
									String desc = request.getDescription();
									String[] splitWords = desc.split(" ");
									boolean flag = false;

									Iterable<? extends Department> allDepartments = departmentManager
											.getDepartmentsLike("%");
									for (String word : splitWords) {
										for (Department dept : allDepartments) {
											if (word.contentEquals(dept
													.getDepartmentName())
													&& flag == false) {
												fromName = word;
												flag = true;
											} else if (word.contentEquals(dept
													.getDepartmentName())) {
												toName = word;
											}
										}
									}

									if (fromName != null && toName != null
											&& !fromName.contentEquals(toName)) {
										Department from = departmentManager
												.getDepartment(fromName);
										Department to = departmentManager
												.getDepartment(toName);

										from.loadEmployees();
										from.removeEmployee(userToTransfer);
										departmentManager.saveDepartment(from);

										to.loadEmployees();
										to.addEmployee(userToTransfer);
										departmentManager.saveDepartment(to);
										
										userToTransfer.setStatus(Status.ACTIVE);
										userManager.saveUser(userToTransfer);
										
										modelAndView.addObject("Success", "Request ("+ reqId + ") is Approved. Employee transfered from: "+fromName+" To: "+ toName);
										request.decide(user, Decision.APPROVE);
										requestManager.saveRequest(request);

									} else {
										modelAndView.addObject("Error",
												"In-appropriate description.");
										return null;
									}
								} else {
									modelAndView.addObject("Error",
											"Invalid Subject.");
									return null;
								}



								
							} else if (approveOrDeny.equals("Deny") && request!=null) {
								
								User userToTransfer = request.getSubject();
								userToTransfer.setStatus(Status.ACTIVE);
								userManager.saveUser(userToTransfer);

								request.decide(user, Decision.DENY);
								requestManager.saveRequest(request);
								modelAndView.addObject("Success", "Request ("
										+ reqId + ") is Denied");
							}

						} else {
							modelAndView.addObject("Error",
									"No Pending Request with ID:" + reqId);
							return null;
						}

					} catch (NullPointerException e) {
						logger.catching(Priority.ERROR, e);
						throw e;
					}
					return null;
				}
			}).execute();
		} catch (HibernateException e) {
			logger.catching(Priority.ERROR, e);
			throw e;
		}

		modelAndView.setViewName("AdminTransferRequests");
		return modelAndView;
	}

	@RequestMapping(value = "/admin/AdminAddAccountToUser", method = RequestMethod.GET)
	public ModelAndView AdminAddAccountToUser() {
		ModelAndView modelAndView = new ModelAndView();

		/* get Session User Name */
		Authentication auth = SecurityContextHolder.getContext()
				.getAuthentication();
		final String sessionUserName = auth.getName(); // get logged in username
		modelAndView.addObject("sessionUserName", sessionUserName);

		/* Add Object to contain username to be deleted */
		modelAndView
				.addObject("addInternalUserForm", new AddInternalUserForm());
		modelAndView.setViewName("AdminAddAccountToUser");
		return modelAndView;
	}

	/**
	 * Post Request to Add Account to user.
	 * 
	 * @param addInternalUserForm
	 * @return
	 */
	@RequestMapping(value = "/admin/AdminAddAccountToUser", method = RequestMethod.POST)
	public ModelAndView AdminAddAccountToUserPost(
			@ModelAttribute AddInternalUserForm addInternalUserForm) {

		final LoggingManager logger = LoggingManager
				.create("/admin/AdminAddAccountToUser");
		final ModelAndView modelAndView = new ModelAndView();

		/* get Session User Name */
		Authentication auth = SecurityContextHolder.getContext()
				.getAuthentication();
		final String sessionUserName = auth.getName(); // get logged in username
		modelAndView.addObject("sessionUserName", sessionUserName);

		/*****************************************************/
		/* Get username to delete the user */
		/***************************************************/
		final String username = addInternalUserForm.getUsername();
		final String accountDescription = addInternalUserForm.getDescription();
		final String accountName = addInternalUserForm.getName();

		/**************************************************************/
		// Need to validate input Data and return error.
		/***************************************************************/

		/* Get user data from input form */
		final SecurityManager securityManager = SecurityManager.get();

		try {
			boolean flag = false;
			if (!securityManager.isUsername(username)) {
				flag = true;
				modelAndView.addObject("Error", "Invalid Username Format!!");
			} else if (!securityManager.isASCII(accountName)
					&& accountName.length() > 50) {
				flag = true;
				modelAndView
						.addObject("Error", "Invalid Account name format!!");
			} else if (!securityManager.isASCII(accountDescription)
					&& accountDescription.length() > 50) {
				flag = true;
				modelAndView.addObject("Error", "Invalid Description format!!");
			}
			if (flag) {
				modelAndView.setViewName("AdminAddAccountToUser");
				return modelAndView;
			}

		} catch (Exception e) {
			modelAndView.addObject("Error", "Invalid Input Data..!!");
			modelAndView.setViewName("AdminAddAccountToUser");
			return modelAndView;
		}

		/*****************************************************/
		/* Fetch user object to be marked as deleted in database */
		/***************************************************/
		// fetch user from username.

		if (!username.contentEquals(sessionUserName)) // cannot delete himself
		{
			final UserManager s_usermanager = UserManager.create();
			final AccountManager accountManager = AccountManager.create();

			try {
				(new TransactionWrapper<Void>() {

					@Override
					public Void operation(Session s) throws HibernateException,
							IllegalArgumentException {
						try {
							User user = s_usermanager.getUser(username);

							if (user != null
									&& !user.getStatus().equals(Status.DELETED)) {

								Account a=accountManager.createAccount(user, 0.0,
										accountName, accountDescription);
	
								a.setAccountStatus(Account.Status.ACTIVE);
								accountManager.saveAccount(a);
								
								MailService mailer = (MailService)context.getBean("mailService");
								mailer.sendNewAccountMail(user,a.getAccountNo());

								modelAndView
										.addObject("Success",
												"Account was successfully added to user.");
							} else {
								modelAndView
										.addObject("Error",
												"The User doesn't exist or is deleted.");
								return null;
							}

						} catch (NullPointerException e) {
							logger.catching(Priority.ERROR, e);
							throw e;
						}
						return null;
					}
				}).execute();
			} catch (HibernateException e) {
				logger.catching(Priority.ERROR, e);
				throw e;
			}

		} else {
			modelAndView.addObject("Error",
					"Account Could not be added to the user");
		}

		modelAndView.setViewName("AdminAddAccountToUser");
		return modelAndView;

	}
	
	
	
	
	
	
	@RequestMapping(value = "/admin/AdminAddDepartmentToUser", method = RequestMethod.GET)
	public ModelAndView AdminAddDepartmentToUser() {
		ModelAndView modelAndView = new ModelAndView();

		final LoggingManager logger = LoggingManager
				.create("/admin/AdminAddDepartmentToUser");
		/* get Session User Name */
		Authentication auth = SecurityContextHolder.getContext()
				.getAuthentication();
		final String sessionUserName = auth.getName(); // get logged in username
		modelAndView.addObject("sessionUserName", sessionUserName);
		
		/************************************************************/
		// Get List of department to which user can be added
		/************************************************************/
		final DepartmentManager deptmanager = DepartmentManager.create();
		Iterable<? extends Department> deptListForUser = null;
		try {
			deptListForUser = (new TransactionWrapper<Iterable<? extends Department>>() {

				@Override
				public Iterable<? extends Department> operation(Session s)
						throws HibernateException {
					try {
						return deptmanager.getDepartmentsLike("%");

					} catch (NullPointerException e) {
						logger.catching(Priority.ERROR, e);
						throw e;
					}
				}
			}).execute();
		} catch (HibernateException e) {
			logger.catching(Priority.ERROR, e);
			throw e;
		}
		
		if (deptListForUser != null && deptListForUser.iterator().hasNext()) {
			modelAndView.addObject("deptListForUser", deptListForUser);
		}


		/* Add Object to contain username to be deleted */
		modelAndView
				.addObject("addInternalUserForm", new AddInternalUserForm());
		modelAndView.setViewName("AdminAddDepartmentToUser");
		return modelAndView;
	}

	/**
	 * Post Request to Add Account to user.
	 * 
	 * @param addInternalUserForm
	 * @return
	 */
	@RequestMapping(value = "/admin/AdminAddDepartmentToUser", method = RequestMethod.POST)
	public ModelAndView AdminAddDepartmentToUserPost(
			@ModelAttribute AddInternalUserForm addInternalUserForm) {

		final LoggingManager logger = LoggingManager
				.create("/admin/AdminAddDepartmentToUser");
		final ModelAndView modelAndView = new ModelAndView();

		/* get Session User Name */
		Authentication auth = SecurityContextHolder.getContext()
				.getAuthentication();
		final String sessionUserName = auth.getName(); // get logged in username
		modelAndView.addObject("sessionUserName", sessionUserName);

		/*****************************************************/
		/* Get username to delete the user */
		/***************************************************/
		final String username = addInternalUserForm.getUsername();
		final String departmentName = addInternalUserForm.getDepartmentName();

		/**************************************************************/
		// Need to validate input Data and return error.
		/***************************************************************/

		/* Get user data from input form */
		final SecurityManager securityManager = SecurityManager.get();

		try {
			boolean flag = false;
			if (!securityManager.isUsername(username)) {
				flag = true;
				modelAndView.addObject("Error", "Invalid Username Format!!");
			} 
			if (flag) {
				modelAndView.setViewName("AdminAddDepartmentToUser");
				return modelAndView;
			}

		} catch (Exception e) {
			modelAndView.addObject("Error", "Invalid Input Data..!!");
			modelAndView.setViewName("AdminAddDepartmentToUser");
			return modelAndView;
		}

		/*****************************************************/
		/* Fetch user object to be marked as deleted in database */
		/***************************************************/
		// fetch user from username.

		if (!username.contentEquals(sessionUserName)) // cannot Add himself to a department
		{
			final UserManager s_usermanager = UserManager.create();
			final DepartmentManager departmentManager = DepartmentManager.create();

			try {
				(new TransactionWrapper<Void>() {

					@Override
					public Void operation(Session s) throws HibernateException,
							IllegalArgumentException {
						try {
							User user = s_usermanager.getUser(username);

							if (user != null
									&& !user.getStatus().equals(Status.DELETED)) {
								
								if(user.getUserType().equals(Type.CUSTOMER) || user.getUserType().equals(Type.MERCHANT))
								{
									modelAndView.addObject("Error", "User of Type Customer / Merchant cannot be added to the department");
									return null;
								}

								/* Get the department to which user is to be added */
								Department department = departmentManager
										.getDepartment(departmentName);

								// make sure department exists.
								if (department == null) {
									throw new IllegalArgumentException();
								}

								department.loadEmployees();
								// Add user to the department
								department.addEmployee(user);

								// save department object after adding employee
								departmentManager.saveDepartment(department);
								
								MailService mailer = (MailService)context.getBean("mailService");
								mailer.sendNotifyMail(user, "You have been addded to "+departmentName+" Department.\n: ");

								modelAndView
										.addObject("Success",
												"User was successfully added to the Department.");
							} else {
								modelAndView
										.addObject("Error",
												"The User doesn't exist or is deleted.");
								return null;
							}

						} catch (NullPointerException e) {
							logger.catching(Priority.ERROR, e);
							throw e;
						}
						return null;
					}
				}).execute();
			} catch (HibernateException e) {
				logger.catching(Priority.ERROR, e);
				throw e;
			}

		} else {
			modelAndView.addObject("Error",
					"Department Could not be added to the logged in User");
		}

		modelAndView.setViewName("AdminAddDepartmentToUser");
		return modelAndView;

	}
	
	
	
	
	
	
	
	
	

	@RequestMapping(value = "/admin/InternalUserRequest", method = RequestMethod.GET)
	public ModelAndView InternalUserRequest() {
		return new ModelAndView("InternalUserRequest", "command", null);

	}

	@RequestMapping(value = "/admin/ExternalUserRequest", method = RequestMethod.GET)
	public ModelAndView ExternalUserRequest() {
		return new ModelAndView("InternalUserRequest", "command", null);

	}

	@RequestMapping(value = "/admin/AddInternalUser", method = RequestMethod.GET)
	public ModelAndView AddInternalUser() {
		return new ModelAndView("AddInternalUser", "command", null);

	}

	@RequestMapping(value = "/admin/DeleteInternalUser", method = RequestMethod.GET)
	public ModelAndView DeleteInternalUser() {
		return new ModelAndView("DeleteInternalUser", "command", null);

	}
}
