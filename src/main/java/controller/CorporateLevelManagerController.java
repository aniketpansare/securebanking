
package main.java.controller;

import java.nio.charset.CharacterCodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import main.java.domain.Department;
import main.java.domain.Request;
import main.java.domain.Request.RequestType;
import main.java.domain.User;
import main.java.domain.User.Status;
import main.java.domain.User.Type;
import main.java.domain.UserData;
import main.java.input.AddInternalUserForm;
import main.java.logging.LoggingManager;
import main.java.logging.LoggingManager.Priority;
import main.java.security.SecurityManager;
import main.java.service.AccountManager;
import main.java.service.DepartmentManager;
import main.java.service.RequestManager;
import main.java.service.UserManager;
import main.java.util.MailService;
import main.java.util.TransactionWrapper;

import org.bouncycastle.crypto.CryptoException;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.BeanFactoryAware;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;


@Controller
public class CorporateLevelManagerController implements BeanFactoryAware{
	private BeanFactory context;

	@Override
	public void setBeanFactory(BeanFactory arg0) throws BeansException {
		context = arg0;
	}
	
	/**
	 * Home Page for Department Manager.
	 * 
	 * @return
	 */
	@RequestMapping(value = "/corp/CorpManagerHomePage", method = RequestMethod.GET)
	public ModelAndView CorpManagerPage() {
		ModelAndView modelAndView = new ModelAndView();

		modelAndView.setViewName("CorpManagerHomePage");
		return modelAndView;
	}

	/**
	 * Get Method for Adding (Internal user: Type Manger/Employee) by Department
	 * Manager.
	 * 
	 * @return
	 */
	@RequestMapping(value = "/corp/CorpManagerAddEmployee", method = RequestMethod.GET)
	public ModelAndView CorpManagerAddEmployee() {
		final LoggingManager logger = LoggingManager
				.create("/corp/ManagerAddEmployee");
		final ModelAndView modelAndView = new ModelAndView();

		/* get Session User Name */
		Authentication auth = SecurityContextHolder.getContext()
				.getAuthentication();
		final String sessionUserName = auth.getName(); // get logged in username
		modelAndView.addObject("sessionUserName", sessionUserName);

		/************************************************************/
		// Get List of department to which manager belongs
		/************************************************************/
		final UserManager usermanager = UserManager.create();
		Iterable<? extends Department> deptListForUser = null;
		try {
			deptListForUser = (new TransactionWrapper<Iterable<? extends Department>>() {

				@Override
				public Iterable<? extends Department> operation(Session s)
						throws HibernateException {
					try {
						User u = usermanager.getUser(sessionUserName);

						if (u == null) {
							modelAndView.addObject("Error",
									"Failed to retrieve Session User");
							return null;
						}
						// load Departments
						u.loadDepartments();

						return u.getDepartments();

					} catch (NullPointerException e) {
						logger.catching(Priority.ERROR, e);
						throw e;
					}
				}
			}).execute();
		} catch (HibernateException e) {
			logger.catching(Priority.ERROR, e);
			throw e;
		}

		// Add the list of departments to which manager belongs to a model
		// attribute
		if (deptListForUser != null && deptListForUser.iterator().hasNext()) {
			modelAndView.addObject("deptListForUser", deptListForUser);
		} else {
			modelAndView.addObject("Error",
					"Corporate Manager Does not belong to any Department");
		}

		/* Add Object to contain form data */
		modelAndView
				.addObject("addInternalUserForm", new AddInternalUserForm());
		/* Manager can only add users of type Employee */
		List<Type> userRoleList = new ArrayList<Type>();
		userRoleList.add(User.Type.MANAGER);
		userRoleList.add(User.Type.EMPLOYEE);
		modelAndView.addObject("userRoleList", userRoleList);
		modelAndView.setViewName("CorpManagerAddEmployee");
		return modelAndView;
	}

	/**
	 * Post Method for Adding (Internal user: Type Employee/MANAGER) by Department
	 * Manager.
	 * 
	 * @param AddInternalUserForm
	 * @return
	 */
	@RequestMapping(value = "/corp/CorpManagerAddEmployee", method = RequestMethod.POST)
	public ModelAndView CorpManagerAddEmployeePost(
			@ModelAttribute AddInternalUserForm addInternalUserForm) {

		final LoggingManager logger = LoggingManager
				.create("/corp/CorpManagerAddEmployee");
		final ModelAndView modelAndView = new ModelAndView();

		/* get Session User Name */
		Authentication auth = SecurityContextHolder.getContext()
				.getAuthentication();
		final String sessionUserName = auth.getName(); // get logged in username
		modelAndView.addObject("sessionUserName", sessionUserName);

		/* Get user data from input form */
		final SecurityManager securityManager = SecurityManager.get();

		final String username = addInternalUserForm.getUsername();
		final String password = securityManager.randomString(64);//addInternalUserForm.getPassword();
		final String name = addInternalUserForm.getName();
		final String number = addInternalUserForm.getNumber();
		final String addr = addInternalUserForm.getAddr();
		final String email = addInternalUserForm.getEmail();
		final String social = addInternalUserForm.getSocial();

		// final String accountDescription = "Internal Account";
		final double startingBalance = 0.0;

		// Always of type EMPLOYEE / manager
		final Type userRole = addInternalUserForm.getUserRole();

		final String departmentName = addInternalUserForm.getDepartmentName();

		/**************************************************************/
		// Need to validate input Data and return if error.
		/***************************************************************/
		

		try {
			boolean flag = false;
			if (!securityManager.isUsername(username)) {
				flag = true;
				modelAndView.addObject("Error", "Invalid Username!!");
			} /*else if (!securityManager.isPassword(password)) {
				flag = true;
				modelAndView.addObject("Error", "Invalid Password.. (min 8 chars)!!");
			}*/ else if (!securityManager.isAlpha(name) && name.length() > 50) {
				flag = true;
				modelAndView.addObject("Error", "Invalid Name");
			} else if (!securityManager.isPhoneNumber(number)) {
				flag = true;
				modelAndView.addObject("Error", "Invalid Phone No..!!");
			} else if (!securityManager.isASCII(addr) && addr.length() > 100) {
				flag = true;
				modelAndView.addObject("Error", "Invalid Address!!");
			} else if (!securityManager.isEmail(email)) {
				flag = true;
				modelAndView.addObject("Error", "Invalid Email.!!");
			}
			else if (!securityManager.isPhoneNumber(social)) {
				flag = true;
				modelAndView.addObject("Error", "Invalid Social Security Number.!!");
			}

			if (flag) {
				modelAndView.setViewName("CorpManagerAddEmployee");
				return modelAndView;
			}

		} catch (Exception e) {
			modelAndView.addObject("Error", "Invalid Input Data..!!");
			modelAndView.setViewName("CorpManagerAddEmployee");
			return modelAndView;
		}

		/**************************************************************/
		// create User object to be added to database;
		/***************************************************************/

		final UserManager s_usermanager = UserManager.create();
		final AccountManager accountmanager = AccountManager.create();
		final DepartmentManager departmentManager = DepartmentManager.create();
		final RequestManager requestManager = RequestManager.create();

		try {
			(new TransactionWrapper<Void>() {

				@Override
				public Void operation(Session s) throws HibernateException,
						IllegalArgumentException {
					try {
						
						/******************************************************
						 * Check if username already exist.
						 *****************************************************/
						final User checkUser = s_usermanager.getUser(username);
						if (checkUser != null) {
							modelAndView.addObject("Error",
									"User name already taken..!!");
							return null;
						}
						/*****************************************************/
						
						// Create User
						final User user = s_usermanager.createUser(username,
								password, UserData.create(name, number, addr,
										email, social));

						if (user != null) {

							/********** Specific settings ************/
							// Only Admin can activate this user
							user.setStatus(Status.DISABLED);
							user.setUserType(userRole);

							// Save user
							s_usermanager.saveUser(user);

							/** If we decide to add sample account for each user **/
							// final Account a =
							// accountmanager.createAccount(user,
							// startingBalance, name , accountDescription);

							/* Get the department to which user is to be added */
							Department department = departmentManager
									.getDepartment(departmentName);

							// make sure department exists.
							if (department == null) {
								throw new IllegalArgumentException();
							}

							// load employees in the department
							department.loadEmployees();

							// Add user to the department
							department.addEmployee(user);

							// save department object after adding employee
							departmentManager.saveDepartment(department);

							// Generate request for Admin approval
							User sessionUser = s_usermanager
									.getUser(sessionUserName);
							requestManager.createRequest(sessionUser, user,
									Request.RequestType.ADD_USER, "Add User.");
							
							try {
								s_usermanager.sendWelcomePackage(context, user);
							} catch (CryptoException e) {
								logger.catching(Priority.ERROR, e);
								throw new RuntimeException("", e);
							}
							
							s_usermanager.saveUser(user);

						}

					} catch (NullPointerException e) {
						logger.catching(Priority.ERROR, e);
						throw e;
					} catch (CharacterCodingException e) {
						logger.catching(Priority.ERROR, e);
					}
					return null;
				}
			}).execute();
		} catch (HibernateException e) {
			logger.catching(Priority.ERROR, e);
			throw e;
		}

		modelAndView
				.addObject("Success",
						"The User is added and sent for Admin approval to Activate.");
		modelAndView.setViewName("CorpManagerAddEmployee");
		return modelAndView;
	}

	/**
	 * Get request to delete the User.
	 * 
	 * @return
	 */
	@RequestMapping(value = "/corp/CorpManagerDeleteEmployee", method = RequestMethod.GET)
	public ModelAndView CorpManagerDeleteEmployee() {
		final LoggingManager logger = LoggingManager
				.create("/corp/CorpManagerDeleteEmployee");
		final ModelAndView modelAndView = new ModelAndView();

		/* get Session User Name */
		Authentication auth = SecurityContextHolder.getContext()
				.getAuthentication();
		final String sessionUserName = auth.getName(); // get logged in username
		modelAndView.addObject("sessionUserName", sessionUserName);

		final List<User> listOfUsers = new ArrayList<User>();

		/************************************************************/
		// get List of Active User's from department to which manager belongs.
		/************************************************************/
		final UserManager usermanager = UserManager.create();
		Iterable<? extends Department> deptListForUser = null;
		try {
			deptListForUser = (new TransactionWrapper<Iterable<? extends Department>>() {

				@Override
				public Iterable<? extends Department> operation(Session s)
						throws HibernateException {
					try {
						User u = usermanager.getUser(sessionUserName);

						if (u == null) {
							modelAndView
									.addObject("Error",
											"Failed to retrieve session user from database");
							return null;
						}

						// load Departments
						u.loadDepartments();

						Set<? extends Department> departments = u
								.getDepartments();

						for (Department dept : departments) {
							dept.loadEmployees();
							Set<? extends User> ulist = dept.getEmployees();

							for (User usr : ulist) {
								if (usr.getStatus().equals(Status.ACTIVE)
										&& usr.getUserId() != u.getUserId() && (usr.getUserType().equals(Type.MANAGER) || usr.getUserType().equals(Type.EMPLOYEE)))
									listOfUsers.add(usr);
							}
						}

						return departments;

					} catch (NullPointerException e) {
						logger.catching(Priority.ERROR, e);
						throw e;
					}
				}
			}).execute();
		} catch (HibernateException e) {
			logger.catching(Priority.ERROR, e);
			throw e;
		}

		// This list of Users are the only users department manager can mark for
		// delete.
		modelAndView.addObject("listOfUsers", listOfUsers);
		// This list of Users are the only users department manager can mark for
		// delete.
		modelAndView.addObject("deptListForUser", deptListForUser);
		/* Add Object to contain username to be deleted */
		modelAndView
				.addObject("addInternalUserForm", new AddInternalUserForm());
		modelAndView.setViewName("CorpManagerDeleteEmployee");
		return modelAndView;
	}

	/**
	 * Post Request to delete internal user.
	 * 
	 * @param addInternalUserForm
	 * @return
	 */
	@RequestMapping(value = "/corp/CorpManagerDeleteEmployee", method = RequestMethod.POST)
	public ModelAndView CorpManagerDeleteEmployeePost(
			@ModelAttribute AddInternalUserForm addInternalUserForm) {

		final LoggingManager logger = LoggingManager
				.create("/corp/CorpManagerDeleteEmployee");
		final ModelAndView modelAndView = new ModelAndView();

		/* get Session User Name */
		Authentication auth = SecurityContextHolder.getContext()
				.getAuthentication();
		final String sessionUserName = auth.getName(); // get logged in username
		modelAndView.addObject("sessionUserName", sessionUserName);

		/**************************************************************/
		// Need to validate input Data and return error.
		/***************************************************************/

		/*****************************************************/
		/* Get username to delete the user */
		/***************************************************/
		final String username = addInternalUserForm.getUsername();
		
		final SecurityManager securityManager = SecurityManager.get();

		try {
			boolean flag = false;
			if (!securityManager.isUsername(username)) {
				flag = true;
				modelAndView.addObject("Error", "Invalid Username!!");
			}
			if (flag) {
				modelAndView.setViewName("CorpManagerDeleteEmployee");
				return modelAndView;
			}

		} catch (Exception e) {
			modelAndView.addObject("Error", "Invalid Input Data..!!");
			modelAndView.setViewName("CorpManagerDeleteEmployee");
			return modelAndView;
		}
		

		/*****************************************************/
		/* Fetch user object to be marked as deleted in database */
		/***************************************************/
		// fetch user from username.
		final UserManager s_usermanager = UserManager.create();
		final RequestManager requestManager = RequestManager.create();

		try {
			(new TransactionWrapper<Void>() {

				@Override
				public Void operation(Session s) throws HibernateException,
						IllegalArgumentException {
					try {
						User user = s_usermanager.getUser(username);

						if (user != null) {
							user.setStatus(Status.DISABLED);
							s_usermanager.saveUser(user);

							// Generate request for Admin approval
							User sessionUser = s_usermanager
									.getUser(sessionUserName);
							requestManager.createRequest(sessionUser, user,
									Request.RequestType.DELETE_USER,
									"Delete this User.");
							
							MailService mailer = (MailService)context.getBean("mailService");
							mailer.sendNotifyMail(user,"You have been marked as deleted.\n Modifier : " + sessionUser.getUserId()+".");

						} else {
							modelAndView.addObject("Error",
									"User doesn't exist or is disabled");
							return null;
						}

					} catch (NullPointerException e) {
						logger.catching(Priority.ERROR, e);
						throw e;
					}
					return null;
				}
			}).execute();
		} catch (HibernateException e) {
			logger.catching(Priority.ERROR, e);
			throw e;
		}

		modelAndView
				.addObject("Success",
						"The Employee is marked as DISABLED and sent for Admin approval to Delete.");
		modelAndView.setViewName("CorpManagerDeleteEmployee");
		return modelAndView;
	}

	@RequestMapping(value = "/corp/CorpManagerTransferEmployee", method = RequestMethod.GET)
	public ModelAndView CorpManagerTransferEmployee() {
		final LoggingManager logger = LoggingManager
				.create("/corp/CorpManagerTransferEmployee");
		final ModelAndView modelAndView = new ModelAndView();

		/* get Session User Name */
		Authentication auth = SecurityContextHolder.getContext()
				.getAuthentication();
		final String sessionUserName = auth.getName(); // get logged in username
		modelAndView.addObject("sessionUserName", sessionUserName);

		final List<User> listOfUsers = new ArrayList<User>();

		/************************************************************/
		// get List of Active User's from department to which manager belongs.
		/************************************************************/
		final UserManager usermanager = UserManager.create();
		final DepartmentManager deptmanager = DepartmentManager.create();
		Iterable<? extends Department> allDeptList;

		try {
			allDeptList = (new TransactionWrapper<Iterable<? extends Department>>() {

				@Override
				public Iterable<? extends Department> operation(Session s)
						throws HibernateException {
					try {
						User u = usermanager.getUser(sessionUserName);

						if (u == null) {
							modelAndView
									.addObject("Error",
											"Failed to retrieve session user from database");
							return null;
						}

						// load Departments
						u.loadDepartments();

						Set<? extends Department> departments = u
								.getDepartments();

						Iterable<? extends Department> allDepartments = deptmanager
								.getDepartmentsLike("%");

						for (Department dept : departments) {
							dept.loadEmployees();
							Set<? extends User> ulist = dept.getEmployees();

							for (User usr : ulist) {
								if (usr.getStatus().equals(Status.ACTIVE)
										&& usr.getUserId() != u.getUserId() && (usr.getUserType().equals(Type.MANAGER) || usr.getUserType().equals(Type.EMPLOYEE)))
									listOfUsers.add(usr);
							}
						}

						return allDepartments;

					} catch (NullPointerException e) {
						logger.catching(Priority.ERROR, e);
						throw e;
					}
				}
			}).execute();
		} catch (HibernateException e) {
			logger.catching(Priority.ERROR, e);
			throw e;
		}

		// This list of Users are the only users department manager can mark for
		// delete.
		modelAndView.addObject("listOfUsers", listOfUsers);
		// This list of Users are the only users department manager can mark for
		// delete.
		modelAndView.addObject("allDeptList", allDeptList);
		/* Add Object to contain username to be deleted */
		modelAndView
				.addObject("addInternalUserForm", new AddInternalUserForm());
		modelAndView.setViewName("CorpManagerTransferEmployee");
		return modelAndView;
	}

	@RequestMapping(value = "/corp/CorpManagerTransferEmployee", method = RequestMethod.POST)
	public ModelAndView CorpManagerTransferEmployeePost(
			@ModelAttribute AddInternalUserForm addInternalUserForm) {

		final LoggingManager logger = LoggingManager
				.create("/corp/CorpManagerDeleteEmployee");
		final ModelAndView modelAndView = new ModelAndView();

		/* get Session User Name */
		Authentication auth = SecurityContextHolder.getContext()
				.getAuthentication();
		final String sessionUserName = auth.getName(); // get logged in username
		modelAndView.addObject("sessionUserName", sessionUserName);

		/**************************************************************/
		// Need to validate input Data and return error.
		/***************************************************************/

		/*****************************************************/
		/* Get username of user to Transfer */
		/***************************************************/
		final String username = addInternalUserForm.getUsername();
		final String fromDepartmentname = addInternalUserForm
				.getFromDepartmentName();
		final String toDepartmentName = addInternalUserForm
				.getToDepartmentName();

		final String transferDescription = "Transfer From: "
				+ fromDepartmentname + " To: " + toDepartmentName;

		/*****************************************************/
		/* Fetch user object to be Transfered */
		/***************************************************/
		// fetch user from username.
		final UserManager s_usermanager = UserManager.create();
		final RequestManager requestManager = RequestManager.create();
		User transferUser = null;

		try {
			transferUser = (new TransactionWrapper<User>() {

				@Override
				public User operation(Session s) throws HibernateException,
						IllegalArgumentException {
					try {
						User sessionUser = s_usermanager
								.getUser(sessionUserName);
						User user = s_usermanager.getUser(username);

						if (user != null
								&& !user.getStatus().equals(Status.DISABLED)) {
							user.setStatus(Status.DISABLED);
							s_usermanager.saveUser(user);

							Request request = requestManager.createRequest(
									sessionUser, user,
									RequestType.TRANSFER_USER,
									transferDescription);

							MailService mailService = (MailService)context.getBean("mailService");
							mailService.sendNotifyMail(user,"You are being transfered to "+ toDepartmentName+" department.\n Modifier : " + sessionUser.getUserId()+".");
							return user;
						} else {
							modelAndView.addObject("Error",
									"User doesn't exist or is in transfer.");
							return null;
						}

					} catch (NullPointerException e) {
						logger.catching(Priority.ERROR, e);
						throw e;
					}
				}
			}).execute();
		} catch (HibernateException e) {
			logger.catching(Priority.ERROR, e);
			throw e;
		}

		modelAndView.addObject("Success",
				"Transfer Request Sent for Admin Approval");
		modelAndView.setViewName("CorpManagerTransferEmployee");
		return modelAndView;
	}

	@RequestMapping(value = "/corp/RequestPII", method = RequestMethod.GET)
	public ModelAndView RequestpII() {
		return new ModelAndView("RequestPII", "command", null);
	}

	@RequestMapping(value = "/corp/ViewAccInfo", method = RequestMethod.GET)
	public ModelAndView ViewAccInfo() {
		return new ModelAndView("ViewAccInfo", "command", null);
	}

	@RequestMapping(value = "/corp/TransferEmp", method = RequestMethod.GET)
	public ModelAndView TransferEmp() {
		return new ModelAndView("TransferEmp", "command", null);
	}

	@RequestMapping(value = "/corp/RegEmpPage", method = RequestMethod.GET)
	public ModelAndView RegEmpPage() {
		ModelAndView modelAndView = new ModelAndView();

		modelAndView.addObject("dummy", 223);
		modelAndView.setViewName("RegEmpPage");
		return modelAndView;
	}

	@RequestMapping(value = "/corp/Merchant-Org", method = RequestMethod.GET)
	public ModelAndView Merchantorg() {
		ModelAndView modelAndView = new ModelAndView();

		modelAndView.addObject("dummy", 223);
		modelAndView.setViewName("Merchant-Org");
		return modelAndView;
	}
}
