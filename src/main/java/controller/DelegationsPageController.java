package main.java.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import main.java.domain.Account;
import main.java.domain.Delegation;
import main.java.domain.Delegation.PrivilegeLevel;
import main.java.domain.Department;
import main.java.domain.User;
import main.java.domain.User.Status;
import main.java.input.AddInternalUserForm;
import main.java.logging.LoggingManager;
import main.java.logging.LoggingManager.Priority;
import main.java.security.SecurityManager;
import main.java.service.AccountManager;
import main.java.service.DelegationManager;
import main.java.service.UserManager;
import main.java.util.TransactionWrapper;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class DelegationsPageController {

	@RequestMapping(value = "/dept/DeptManagerDelegateAccount", method = RequestMethod.GET)
	public ModelAndView DeptManagerDeleteEmployee() {
		final LoggingManager logger = LoggingManager
				.create("/dept/DeptManagerDelegateAccount");
		final ModelAndView modelAndView = new ModelAndView();

		/* get Session User Name */
		Authentication auth = SecurityContextHolder.getContext()
				.getAuthentication();
		final String sessionUserName = auth.getName(); // get logged in username
		modelAndView.addObject("sessionUserName", sessionUserName);

		final List<User> listOfUsers = new ArrayList<User>();

		/************************************************************/
		// get List of Active User's from department to which manager belongs.
		/************************************************************/
		final UserManager usermanager = UserManager.create();
		Iterable<? extends Department> deptListForUser = null;
		try {
			deptListForUser = (new TransactionWrapper<Iterable<? extends Department>>() {

				@Override
				public Iterable<? extends Department> operation(Session s)
						throws HibernateException {
					try {
						User u = usermanager.getUser(sessionUserName);

						if (u == null) {
							modelAndView
									.addObject("Error",
											"Failed to retrieve session user from database");
							return null;
						}

						/******************************************************************/
						// get Accounts owned by manager
						/*****************************************************************/

						u.loadAccounts();

						List<Account>listOfAccounts = new ArrayList<Account>();
						
						listOfAccounts.addAll(u.getAccounts());

						// load Departments
						u.loadDepartments();

						Set<? extends Department> departments = u
								.getDepartments();

						for (Department dept : departments) {
							
							listOfAccounts.addAll(dept.getAccounts());
							
							dept.loadEmployees();
							Set<? extends User> ulist = dept.getEmployees();

							for (User usr : ulist) {
								if (usr.getStatus().equals(Status.ACTIVE)
										&& usr.getUserId() != u.getUserId())
									listOfUsers.add(usr);
							}
						}
						
						if (listOfAccounts == null
								|| !listOfAccounts.iterator().hasNext()) {
							modelAndView
									.addObject("Error",
											"Manager doesn't has any accounts to delegate");
							return null;
						}

						modelAndView
								.addObject("listOfAccounts", listOfAccounts);


						return departments;

					} catch (NullPointerException e) {
						logger.catching(Priority.ERROR, e);
						throw e;
					}
				}
			}).execute();
		} catch (HibernateException e) {
			logger.catching(Priority.ERROR, e);
			throw e;
		}

		// This list of Users are the only users department manager can mark for
		// delete.
		modelAndView.addObject("listOfUsers", listOfUsers);
		// This list of Users are the only users department manager can mark for
		// delete.
		modelAndView.addObject("deptListForUser", deptListForUser);
		modelAndView.addObject("privilegeList",
				Delegation.PrivilegeLevel.values());
		/* Add Object to contain username to be deleted */
		modelAndView
				.addObject("addInternalUserForm", new AddInternalUserForm());
		modelAndView.setViewName("DeptManagerDelegateAccount");
		return modelAndView;
	}

	@RequestMapping(value = "/dept/DeptManagerDelegateAccount", method = RequestMethod.POST)
	public ModelAndView DeptManagerDelegateAccountPost(
			@ModelAttribute AddInternalUserForm addInternalUserForm) {
		final LoggingManager logger = LoggingManager
				.create("/dept/DeptManagerDelegateAccount");
		final ModelAndView modelAndView = new ModelAndView();

		/* get Session User Name */
		Authentication auth = SecurityContextHolder.getContext()
				.getAuthentication();
		final String sessionUserName = auth.getName(); // get logged in username
		modelAndView.addObject("sessionUserName", sessionUserName);

		long accnt = 0l;
		final String employee_name = addInternalUserForm.getUsername();
		final String privilegeLevel = addInternalUserForm.getPrivilegeLevel();
		
		final PrivilegeLevel privilege;
		try {
			accnt = Long.parseLong(addInternalUserForm.getSelectedAccountNo());
			
			if(PrivilegeLevel.MODIFY.toString().equals(privilegeLevel))
			{
				privilege= PrivilegeLevel.MODIFY;
			}
			else if (PrivilegeLevel.ACCESS.toString().equals(privilegeLevel))
			{
				privilege= PrivilegeLevel.ACCESS;
			}
			else
			{
				modelAndView.addObject("Error", "Invalid Privilege Level...!!");
				modelAndView.setViewName("DeptManagerDelegateAccount");
				return modelAndView;
			}

		} catch (NullPointerException | NumberFormatException e) {
			logger.catching(Priority.ERROR, e);
			modelAndView.addObject("Error", "Invalid Attempt...!!");
			modelAndView.setViewName("DeptManagerDelegateAccount");
			return modelAndView;
		}
		final long selectedAccountNo = accnt;

		final UserManager userManager = UserManager.create();
		final AccountManager accountManager = AccountManager.create();
		final SecurityManager secmanager = SecurityManager.get();
		final DelegationManager delmanager = DelegationManager.create();

		/*****************************************************************/
		// dummy delegations
		/*****************************************************************/
		try {
			(new TransactionWrapper<Void>() {

				@Override
				public Void operation(Session s) throws HibernateException {
					try {

						User fromUser = userManager.getUser(sessionUserName);
						User toUser = userManager.getUser(employee_name);
						Account accountToDelegate = accountManager
								.getAccount(selectedAccountNo);

						if (toUser == null || fromUser==null) {
							modelAndView.addObject("Error", "Invalid User..");
							return null;
						}
						if (accountToDelegate == null) {
							modelAndView.addObject("Error",
									"Invalid Account Number..");
							return null;
						}

						delmanager.createDelegation(
								fromUser,
								accountToDelegate, privilege,
								toUser);

						return null;
					} catch (NullPointerException e) {
						e.printStackTrace();

					} catch (IllegalArgumentException e) {
						e.printStackTrace();

					}
					return null;
				}
			}).execute();
		} catch (HibernateException e) {
			e.printStackTrace();

		}

		modelAndView.addObject("Success", "Account was successfully Delegated");
		modelAndView.setViewName("DeptManagerDelegateAccount");
		return modelAndView;
	}

	
	
	
	
	
	
	@RequestMapping(value = "/user/ExtUserDelegateAccount", method = RequestMethod.GET)
	public ModelAndView ExtUserDeleteEmployee() {
		final LoggingManager logger = LoggingManager
				.create("/user/ExtUserDelegateAccount");
		final ModelAndView modelAndView = new ModelAndView();

		/* get Session User Name */
		Authentication auth = SecurityContextHolder.getContext()
				.getAuthentication();
		final String sessionUserName = auth.getName(); // get logged in username
		modelAndView.addObject("sessionUserName", sessionUserName);

		
		final UserManager usermanager = UserManager.create();
		try {
			(new TransactionWrapper<Void>() {

				@Override
				public Void operation(Session s) throws HibernateException {
					try {
						User u = usermanager.getUser(sessionUserName);

						// Query for accounts
						u.loadAccounts();
						if (u.getAccounts().size() > 0)
							modelAndView.addObject("listOfAccounts", u.getAccounts());
					} catch (NullPointerException e) {
						logger.catching(Priority.ERROR, e);
						throw e;
					}
					return null;
				}
			}).execute();
		} catch (HibernateException e) {
			logger.catching(Priority.ERROR, e);
			throw e;
		}
		
		
		
		modelAndView.addObject("privilegeList",
				Delegation.PrivilegeLevel.values());
		/* Add Object to contain username to be deleted */
		modelAndView
				.addObject("addInternalUserForm", new AddInternalUserForm());
		modelAndView.setViewName("ExtUserDelegateAccount");
		return modelAndView;
	}

	@RequestMapping(value = "/user/ExtUserDelegateAccount", method = RequestMethod.POST)
	public ModelAndView ExtUserDelegateAccountPost(
			@ModelAttribute AddInternalUserForm addInternalUserForm) {
		final LoggingManager logger = LoggingManager
				.create("/user/ExtUserDelegateAccount");
		final ModelAndView modelAndView = new ModelAndView();

		/* get Session User Name */
		Authentication auth = SecurityContextHolder.getContext()
				.getAuthentication();
		final String sessionUserName = auth.getName(); // get logged in username
		modelAndView.addObject("sessionUserName", sessionUserName);

		long accnt = 0l;
		final String user_to_delgate = addInternalUserForm.getUsername();
		final String privilegeLevel = addInternalUserForm.getPrivilegeLevel();
		final PrivilegeLevel privilege;
		try {
			accnt = Long.parseLong(addInternalUserForm.getSelectedAccountNo());
			if(PrivilegeLevel.MODIFY.toString().equals(privilegeLevel))
			{
				privilege= PrivilegeLevel.MODIFY;
			}
			else if (PrivilegeLevel.ACCESS.toString().equals(privilegeLevel))
			{
				privilege= PrivilegeLevel.ACCESS;
			}
			else
			{
				modelAndView.addObject("Error", "Invalid Privilege Level...!!");
				modelAndView.setViewName("ExtUserDelegateAccount");
				return modelAndView;
			}

		} catch (NullPointerException | NumberFormatException e) {
			logger.catching(Priority.ERROR, e);
			modelAndView.addObject("Error", "Invalid Attempt...!!");
			modelAndView.setViewName("ExtUserDelegateAccount");
			return modelAndView;
		}
		final long selectedAccountNo = accnt;

		final UserManager userManager = UserManager.create();
		final AccountManager accountManager = AccountManager.create();
		final SecurityManager secmanager = SecurityManager.get();
		final DelegationManager delmanager = DelegationManager.create();

		/*****************************************************************/
		// dummy delegations
		/*****************************************************************/
		try {
			(new TransactionWrapper<Void>() {

				@Override
				public Void operation(Session s) throws HibernateException {
					try {

						User fromUser = userManager.getUser(sessionUserName);
						User toUser = userManager.getUser(user_to_delgate);
						Account accountToDelegate = accountManager
								.getAccount(selectedAccountNo);

						if (toUser == null || fromUser==null) {
							modelAndView.addObject("Error", "Invalid User..");
							return null;
						}
						if (accountToDelegate == null) {
							modelAndView.addObject("Error",
									"Invalid Account Number..");
							return null;
						}

						delmanager.createDelegation(
								fromUser,
								accountToDelegate, privilege,
								toUser);

						return null;
					} catch (NullPointerException e) {
						e.printStackTrace();

					} catch (IllegalArgumentException e) {
						e.printStackTrace();

					}
					return null;
				}
			}).execute();
		} catch (HibernateException e) {
			e.printStackTrace();

		}

		modelAndView.addObject("Success", "Account was successfully Delegated");
		modelAndView.setViewName("ExtUserDelegateAccount");
		return modelAndView;
	}

	
	
		
	@RequestMapping(value = "/dept/RevokeDelegatedAccount", method = RequestMethod.GET)
	public ModelAndView RevokeDelegatedAccount() {
		final LoggingManager logger = LoggingManager
				.create("/dept/RevokeDelegatedAccount");
		final ModelAndView modelAndView = new ModelAndView();

		/* get Session User Name */
		Authentication auth = SecurityContextHolder.getContext()
				.getAuthentication();
		final String sessionUserName = auth.getName(); // get logged in username
		modelAndView.addObject("sessionUserName", sessionUserName);

		/************************************************************/
		// get List of Active User's from department to which manager belongs.
		/************************************************************/
		final UserManager usermanager = UserManager.create();

		try {
			(new TransactionWrapper<Void>() {

				@Override
				public Void operation(Session s)
						throws HibernateException {
					try {
						User u = usermanager.getUser(sessionUserName);

						if (u == null) {
							modelAndView
									.addObject("Error",
											"Failed to retrieve session user from database");
							return null;
						}

						/******************************************************************/
						// get Accounts owned by manager
						/*****************************************************************/

						u.loadOutgoingDelegations();

						List<Delegation>listOfDelegations = new ArrayList<Delegation>();
						
						listOfDelegations.addAll(u.getOutgoingDelegations());

						if (listOfDelegations == null
								|| !listOfDelegations.iterator().hasNext()) {
							modelAndView
									.addObject("Error",
											"User has not delegated any account..!");
							return null;
						}

						modelAndView
								.addObject("listOfDelegations", listOfDelegations);


						return null;

					} catch (NullPointerException e) {
						logger.catching(Priority.ERROR, e);
						throw e;
					}
				}
			}).execute();
		} catch (HibernateException e) {
			logger.catching(Priority.ERROR, e);
			throw e;
		}

		modelAndView
				.addObject("addInternalUserForm", new AddInternalUserForm());
		modelAndView.setViewName("RevokeDelegatedAccount");
		return modelAndView;
	}

	@RequestMapping(value = "/dept/RevokeDelegatedAccount", method = RequestMethod.POST)
	public ModelAndView RevokeDelegatedAccountPost(
			@ModelAttribute AddInternalUserForm addInternalUserForm) {
		final LoggingManager logger = LoggingManager
				.create("/dept/RevokeDelegatedAccount");
		final ModelAndView modelAndView = new ModelAndView();

		/* get Session User Name */
		Authentication auth = SecurityContextHolder.getContext()
				.getAuthentication();
		final String sessionUserName = auth.getName(); // get logged in username
		modelAndView.addObject("sessionUserName", sessionUserName);


		final String DelegationNo = addInternalUserForm.getSelectedAccountNo();
		final long delegationNo;
		
		
		final PrivilegeLevel privilege;
		try {
			delegationNo = Long.parseLong(addInternalUserForm.getSelectedAccountNo());
			
		} catch (NullPointerException | NumberFormatException e) {
			logger.catching(Priority.ERROR, e);
			modelAndView.addObject("Error", "Invalid delegation Number...!!");
			modelAndView.setViewName("RevokeDelegatedAccount");
			return modelAndView;
		}


		final UserManager userManager = UserManager.create();
		final DelegationManager delmanager = DelegationManager.create();

		/*****************************************************************/
		// dummy delegations
		/*****************************************************************/
		try {
			(new TransactionWrapper<Void>() {

				@Override
				public Void operation(Session s) throws HibernateException {
					try {

						User user = userManager.getUser(sessionUserName);
						
						Delegation delegation = delmanager.getDelegation(delegationNo);
						
						if(delegation!=null)
							{
							delmanager.removeDelegation(delegation);
							}
						else{
							modelAndView.addObject("Error", "Delegation Doesn't Exist");
							return null;
						}


						return null;
					} catch (NullPointerException e) {
						e.printStackTrace();
						modelAndView.addObject("Error", "Exception Occurred during delete.. check with admin");

					} catch (IllegalArgumentException e) {
						e.printStackTrace();
						modelAndView.addObject("Error", "Exception Occurred during delete.. check with admin");
						

					}
					return null;
				}
			}).execute();
		} catch (HibernateException e) {
			e.printStackTrace();
			modelAndView.addObject("Error", "Exception Occurred during delete.. check with admin");
			throw e;
		}

		modelAndView.addObject("Success", "Account privileges were revoked successfully.");
		modelAndView.setViewName("RevokeDelegatedAccount");
		return modelAndView;
	}
	
	
	
	
	
	
	
}
