
package main.java.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.stereotype.Controller;


@Controller
public class DeptLevelManagerController 
{
	@RequestMapping(value = "/user/DeptLvlMngrHomePage", method = RequestMethod.GET)
	   public ModelAndView DeptManagerHomeGET() 
	{

	      return new ModelAndView("DeptLvlMngrHomePage", "command", null);
	}
	@RequestMapping(value = "/user/DeptLvlMngrHomePage", method = RequestMethod.POST)
	   public ModelAndView DeptManagerHomePOST() 
	{
	      return new ModelAndView("DeptLvlMngrHomePage", "command", null);
	}
	@RequestMapping(value = "/user/AddIntrnlUsrByDeptLvlMngr", method = RequestMethod.GET)
	   public ModelAndView AddIntrnlUsrByDeptLvlMngrGET() 
	{
	      return new ModelAndView("AddIntrnlUsrByDeptLvlMngr", "command", null);
	}
	@RequestMapping(value = "/user/AddIntrnlUsrByDeptLvlMngr", method = RequestMethod.POST)
	   public ModelAndView AddIntrnlUsrByDeptLvlMngrPOST() 
	{
	      return new ModelAndView("AddIntrnlUsrByDeptLvlMngr", "command", null);
	}
	@RequestMapping(value = "/user/DeleteIntrnlUsrByDeptLvlMngr", method = RequestMethod.GET)
	   public ModelAndView DeleteIntrnlUsrByDeptLvlMngrGET() 
	{
	      return new ModelAndView("DeleteIntrnlUsrByDeptLvlMngr", "command", null);
	}
	@RequestMapping(value = "/user/DeleteIntrnlUsrByDeptLvlMngr", method = RequestMethod.POST)
	   public ModelAndView DeleteIntrnlUsrByDeptLvlMngrPOST() 
	{
	      return new ModelAndView("DeleteIntrnlUsrByDeptLvlMngr", "command", null);
	}
	@RequestMapping(value = "/user/TransferIntrnlUsrByDeptLvlMngr", method = RequestMethod.GET)
	   public ModelAndView TransferIntrnlUsrByDeptLvlMngrGET() 
	{
	      return new ModelAndView("TransferIntrnlUsrByDeptLvlMngr", "command", null);
	}
	@RequestMapping(value = "/user/TransferIntrnlUsrByDeptLvlMngr", method = RequestMethod.POST)
	   public ModelAndView TransferIntrnlUsrByDeptLvlMngrPOST() 
	{
	      return new ModelAndView("TransferIntrnlUsrByDeptLvlMngr", "command", null);
	}
}
