package main.java.controller;

import java.util.ArrayList;
import java.util.List;

import main.java.domain.LogEntry;
import main.java.input.LogFilterData;
import main.java.logging.LoggingManager;
import main.java.logging.LoggingManager.Priority;
import main.java.util.TransactionWrapper;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class LogController {

	// / Logger for the class

	@RequestMapping(value = "/admin/logs", method = RequestMethod.GET)
	public ModelAndView display() {

		ModelAndView modelAndView = new ModelAndView();
		// Display raw log screen.

		modelAndView.addObject("logFilterData", new LogFilterData());
		modelAndView.setViewName("logs");
		return modelAndView;
	}

	@RequestMapping(value = "/admin/logs", method = RequestMethod.POST)
	public ModelAndView onSubmit(@ModelAttribute final LogFilterData logFilterData) {
		final LoggingManager logger = LoggingManager.create("/admin/logs");
		final ModelAndView modelAndView = new ModelAndView();

		try {
			(new TransactionWrapper<Void>() {

				@Override
				public Void operation(Session s) throws HibernateException,
						IllegalArgumentException {
					try {

						List<LogEntry> logEntries = new ArrayList<LogEntry>();
						logEntries.addAll(logger.query(
								logFilterData.getLoggerFilter(),
								logFilterData.getPriorityFilter(),
								logFilterData.getMessageFilter(), null, null,
								0, 1000));
						modelAndView.addObject("logEntries", logEntries);

					} catch (NullPointerException e) {
						logger.catching(Priority.ERROR, e);
						throw e;
					} 
					return null;
				}
			}).execute();
		} catch (HibernateException e) {
			logger.catching(Priority.ERROR, e);
			throw e;
		}

		modelAndView.addObject("logFilterData", new LogFilterData());
		modelAndView.setViewName("logs");
		return modelAndView;
	}

}
