package main.java.controller;

import java.nio.charset.CharacterCodingException;
import java.security.NoSuchAlgorithmException;

import javax.servlet.http.HttpSession;

import main.java.domain.User;
import main.java.input.AddInternalUserForm;
import main.java.input.LoginFormData;
import main.java.input.RegistrationForm;
import main.java.logging.LoggingManager;
import main.java.logging.LoggingManager.Priority;
import main.java.security.SecurityManager;
import main.java.service.UserManager;
import main.java.util.TransactionWrapper;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.BeanFactoryAware;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;

@Controller
@SessionAttributes("sessionObj")
public class LoginPageController implements BeanFactoryAware {
	private LoggingManager log = LoggingManager.create("/login");
	private BeanFactory context;

	@Override
	public void setBeanFactory(BeanFactory arg0) throws BeansException {
		context = arg0;
	}

	@RequestMapping(value = "/login", method = RequestMethod.GET)
	public ModelAndView login() {
		log.info("THE LOGGER IS WORKING!");
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.setViewName("login");
		return modelAndView;
	}

	@RequestMapping(value = "/ResetPassword", method = RequestMethod.GET)
	public ModelAndView resetPassword() {

		final ModelAndView modelAndView = new ModelAndView();

		modelAndView.addObject("loginFormData", new LoginFormData());
		modelAndView.setViewName("ResetPassword");
		return modelAndView;

	}

	@RequestMapping(value = "/ResetPassword", method = RequestMethod.POST)
	public ModelAndView resetPasswordPOST(
			@ModelAttribute LoginFormData loginFormData) {

		final LoggingManager logger = LoggingManager.create("/ResetPassword");
		final ModelAndView modelAndView = new ModelAndView();

		final String username = loginFormData.getUser();

		/**************************************************************/
		// Need to validate input Data and return if error.
		/***************************************************************/

		final SecurityManager securityManager = SecurityManager.get();

		try {
			boolean flag = false;
			if (!securityManager.isUsername(username)) {
				flag = true;
				modelAndView.addObject("Error", "Invalid Username!!");
			}
			if (flag) {
				modelAndView.setViewName("ResetPassword");
				return modelAndView;
			}

		} catch (Exception e) {
			modelAndView.addObject("Error", "Invalid Input Data..!!");
			modelAndView.setViewName("ResetPassword");
			return modelAndView;
		}

		final UserManager um = UserManager.create();
		try {

			(new TransactionWrapper<Void>() {

				@Override
				public Void operation(Session s) throws HibernateException,
						RuntimeException {

					User user = um.getUser(username);
					if (user == null) {
						modelAndView.addObject("Error", "Invalid Username");
						return null;
					}
					um.generatePasswordResetRequest(context, user);

					return null;
				}
			}).execute();

		} catch (Exception e) {
			throw e;
		}

		modelAndView.addObject("Success",
				"Please check your email for one time password.");
		modelAndView.setViewName("ResetPassword");
		return modelAndView;
	}

	@RequestMapping(value = "/ResetPasswordOTP", method = RequestMethod.GET)
	public ModelAndView resetPasswordOTP() {

		final ModelAndView modelAndView = new ModelAndView();

		modelAndView
				.addObject("addInternalUserForm", new AddInternalUserForm());
		modelAndView.setViewName("ResetPasswordOTP");
		return modelAndView;

	}

	@RequestMapping(value = "/ResetPasswordOTP", method = RequestMethod.POST)
	public ModelAndView resetPasswordOTPPost(
			@ModelAttribute AddInternalUserForm addInternalUserForm) {

		final ModelAndView modelAndView = new ModelAndView();
		final LoggingManager logger = LoggingManager.create("/ResetPasswordOTP");
		final String username = addInternalUserForm.getUsername();
		final String password = addInternalUserForm.getPassword();
		final String repeatPassword = addInternalUserForm.getRepeatPassword();
		final String tokenOTP = addInternalUserForm.getTokenOTP();

		/**************************************************************/
		// Need to validate input Data and return if error.
		/***************************************************************/

		final SecurityManager securityManager = SecurityManager.get();
		
		try {
			boolean flag = false;
			if (!securityManager.isUsername(username)) {
				flag = true;
				modelAndView.addObject("Error", "Invalid Username!!");
			} else if (tokenOTP.length() > 50) {
				flag = true;
				modelAndView.addObject("Error", "Invalid OTP token!!");
			} else if (!securityManager.isPassword(password)) {
				flag = true;
				modelAndView.addObject("Error",
						"Invalid Password!!(min 8 chars)");
			} else if (!password.contentEquals(repeatPassword)) {
				flag = true;
				modelAndView.addObject("Error",
						"Repeat password does not Match");
			}

			if (flag) {
				modelAndView.setViewName("ResetPasswordOTP");
				return modelAndView;
			}

		} catch (Exception e) {
			modelAndView.addObject("Error", "Invalid Input Data..!!");
			modelAndView.setViewName("ResetPasswordOTP");
			return modelAndView;
		}

		final UserManager um = UserManager.create();
		try {

			(new TransactionWrapper<Void>() {

				@Override
				public Void operation(Session s) throws HibernateException,
						RuntimeException {

					User user = um.getUser(username);
					if (user == null) {
						modelAndView.addObject("Error", "Invalid Username");
						return null;
					}

					if (!user.validateOTP(tokenOTP)) {
						modelAndView.addObject("Error",
								"Invalid OTP.... Please Reset Passwod Again!!");
						return null;
					}

					try {
						user.getPasswordContainer().setPassword(password);
					} catch (CharacterCodingException e) {
						// TODO Auto-generated catch block
						modelAndView.addObject("Error",
								"Error in resetting the Password!!");
						return null;
					}

					user.invalidateOTP();
					um.saveUser(user);

					return null;
				}
			}).execute();

		} catch (Exception e) {
			throw e;
		}

		modelAndView.addObject("Success",
				"Password has been reset successfully.");
		modelAndView.setViewName("ResetPasswordOTP");
		return modelAndView;
	}

	@RequestMapping(value = "/loginFailed", method = RequestMethod.GET)
	public ModelAndView loginerror() {

		ModelAndView modelAndView = new ModelAndView();
		modelAndView.addObject("error", "true");
		modelAndView.setViewName("login");
		return modelAndView;
	}

	@RequestMapping(value = "/InvalidSession", method = RequestMethod.GET)
	public ModelAndView invalidSession(HttpSession session) {

		session.invalidate(); // sessionDestroyed() is executed

		ModelAndView modelAndView = new ModelAndView();
		modelAndView.addObject("error", "true");
		modelAndView.setViewName("login");
		return modelAndView;
	}

	@RequestMapping(value = "/logout", method = RequestMethod.GET)
	public ModelAndView logout(HttpSession session) {
		session.invalidate(); // sessionDestroyed() is executed

		ModelAndView modelAndView = new ModelAndView();
		modelAndView.setViewName("login");
		return modelAndView;
	}

	@RequestMapping(value = "/SecureBanking/authenticate", method = RequestMethod.POST)
	public ModelAndView onSubmit(@ModelAttribute LoginFormData loginFormData,
			HttpSession session) {
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.addObject("sessionId", session.getId());
		modelAndView.addObject("creationTime", session.getCreationTime());
		modelAndView
				.addObject("lastAccesedTime", session.getLastAccessedTime());
		modelAndView.addObject("maxInactiveInterval",
				session.getMaxInactiveInterval());

		modelAndView.addObject("loginFormData", loginFormData);
		modelAndView.setViewName("homepage");
		return modelAndView;
	}

	@RequestMapping(value = "/nextpage", method = RequestMethod.POST)
	public String onClickk(
			@ModelAttribute("Spring") final RegistrationForm RegistrationForm,
			ModelMap model) {

		if (RegistrationForm == null) {
			throw new IllegalArgumentException("A user/pass is required");
		}
		model.addAttribute("FirstName", RegistrationForm.getFirstName());
		model.addAttribute("LastName", RegistrationForm.getLastName());

		return "RegistrationResult";
	}

}
