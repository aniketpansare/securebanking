package main.java.controller;

import java.nio.charset.CharacterCodingException;
import java.util.ArrayList;
import java.util.List;

import main.java.domain.Account;
import main.java.domain.Request;
import main.java.domain.User;
import main.java.domain.User.Status;
import main.java.domain.User.Type;
import main.java.domain.UserData;
import main.java.input.AddInternalUserForm;
import main.java.logging.LoggingManager;
import main.java.logging.LoggingManager.Priority;
import main.java.security.SecurityManager;
import main.java.service.AccountManager;
import main.java.service.DepartmentManager;
import main.java.service.RequestManager;
import main.java.service.UserManager;
import main.java.util.TransactionWrapper;

import org.bouncycastle.crypto.CryptoException;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.BeanFactoryAware;
import org.springframework.mail.MailException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class RegisterUserController implements BeanFactoryAware{
	
	private BeanFactory context;

	@Override
	public void setBeanFactory(BeanFactory arg0) throws BeansException {
		context = arg0;
	}

	@RequestMapping(value = "/Register", method = RequestMethod.GET)
	public ModelAndView register() {
		final ModelAndView modelAndView = new ModelAndView();

		/* get Session User Name */
		Authentication auth = SecurityContextHolder.getContext()
				.getAuthentication();
		final String sessionUserName = auth.getName(); // get logged in username
		modelAndView.addObject("sessionUserName", sessionUserName);

		List<String> userRoleList = new ArrayList<String>();
		userRoleList.add(User.Type.CUSTOMER.toString());
		userRoleList.add(User.Type.MERCHANT.toString());

		modelAndView
				.addObject("addInternalUserForm", new AddInternalUserForm());
		modelAndView.addObject("userRoleList", userRoleList);
		modelAndView.setViewName("Register");
		return modelAndView;
	}

	/**
	 * Post Method for adding internal User.
	 * 
	 * @param AddInternalUserForm
	 * @return
	 */
	@RequestMapping(value = "/Register", method = RequestMethod.POST)
	public ModelAndView AdminAddInternalUserPost(
			@ModelAttribute AddInternalUserForm addInternalUserForm) {

		final LoggingManager logger = LoggingManager.create("/Register");
		final ModelAndView modelAndView = new ModelAndView();

		/* get Session User Name */
		Authentication auth = SecurityContextHolder.getContext()
				.getAuthentication();
		final String sessionUserName = auth.getName(); // get logged in username
		modelAndView.addObject("sessionUserName", sessionUserName);

		/* Get user data from input form */

		final String username = addInternalUserForm.getUsername();
		final String password = "123!@#123!@#";//addInternalUserForm.getPassword();
		//final String repeatPassword = addInternalUserForm.getRepeatPassword();
		final String name = addInternalUserForm.getName();
		final String number = addInternalUserForm.getNumber();
		final String addr = addInternalUserForm.getAddr();
		final String email = addInternalUserForm.getEmail();
		final String social = addInternalUserForm.getSocial();
		final Type userRole = addInternalUserForm.getUserRole();

		final String accountDescription = "External Account";
		final double startingBalance = 0.0;

		/**************************************************************/
		// Need to validate input Data and return if error.
		/***************************************************************/
		final SecurityManager securityManager = SecurityManager.get();

		try {
			boolean flag = false;
			if (!securityManager.isUsername(username)) {
				flag = true;
				modelAndView.addObject("Error", "Invalid Username!!");
			} /*else if (!securityManager.isPassword(password)) {
				flag = true;
				modelAndView.addObject("Error", "Invalid Password!! (min 8 chars)");
			}*/ else if (!securityManager.isAlpha(name) && name.length() > 50) {
				flag = true;
				modelAndView.addObject("Error", "Invalid Name");
			} else if (!securityManager.isPhoneNumber(number)) {
				flag = true;
				modelAndView.addObject("Error", "Invalid Phone No..!!");
			} else if (!securityManager.isASCII(addr) && addr.length() > 100) {
				flag = true;
				modelAndView.addObject("Error", "Invalid Address!!");
			} else if (!securityManager.isEmail(email)) {
				flag = true;
				modelAndView.addObject("Error", "Invalid Email.!!");
			} else if (!securityManager.isPhoneNumber(social)) {
				flag = true;
				modelAndView.addObject("Error", "Invalid Social Security Number.!!");
			}
			/*else if (!password.contentEquals(repeatPassword)) {
				flag = true;
				modelAndView.addObject("Error",
						"Repeat password does not Match");
			}*/

			if (flag) {
				modelAndView.setViewName("Register");
				return modelAndView;
			}

		} catch (Exception e) {
			modelAndView.addObject("Error", "Invalid Input Data..!!");
			modelAndView.setViewName("Register");
			return modelAndView;
		}

		/**************************************************************/
		// create User object to be added to database;
		/***************************************************************/

		final UserManager s_usermanager = UserManager.create();
		final AccountManager accountmanager = AccountManager.create();
		final DepartmentManager departmentManager = DepartmentManager.create();
		final RequestManager requestManager = RequestManager.create();

		try {
			(new TransactionWrapper<Void>() {

				@Override
				public Void operation(Session s) throws HibernateException,
						IllegalArgumentException {
					try {

						/******************************************************
						 * Check if username already exist.
						 *****************************************************/
						final User checkUser = s_usermanager.getUser(username);
						if (checkUser != null) {
							modelAndView.addObject("Error",
									"User name already taken..!!");
							return null;
						}
						/*****************************************************/

						// Create User
						final User user = s_usermanager.createUser(username,
								password, UserData.create(name, number, addr,
										email, social));

						if (user != null) {

							/********** Specific settings ************/
							user.setStatus(Status.DISABLED);
							user.setUserType(userRole);
							// user.getPassword().setEncodedString(password);
							
	
							/** If we decide to add sample account for each user **/
							final Account a = accountmanager.createAccount(
									user, startingBalance, name,
									accountDescription);
							a.setAccountStatus(Account.Status.ACTIVE);
							accountmanager.saveAccount(a);
							
							
							requestManager.createRequest(user, user,
									Request.RequestType.ADD_USER, "Add User.");
							
							// Save user
							s_usermanager.saveUser(user);
							
							// Send welcome package
							try {
								s_usermanager.sendWelcomePackage(context, user);
							} catch (MailException | CryptoException e) {
								logger.catching(Priority.ERROR, e);
								throw new RuntimeException(e);
							} 
						}

					} catch (NullPointerException e) {
						logger.catching(Priority.ERROR, e);
						throw e;
					} catch (CharacterCodingException e) {
						logger.catching(Priority.ERROR, e);
					}
					return null;
				}
			}).execute();
		} catch (HibernateException e) {
			logger.catching(Priority.ERROR, e);
			throw e;
		}

		modelAndView.addObject("Success",
				"The User has been registered and sent for Admin approval.. Please check your mail for Password Reset option.");
		modelAndView.setViewName("Register");
		return modelAndView;
	}

}
