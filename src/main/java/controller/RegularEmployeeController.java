package main.java.controller;

import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import main.java.domain.Account;
import main.java.domain.BadTransactionException;
import main.java.domain.Delegation;
import main.java.domain.Delegation.PrivilegeLevel;
import main.java.domain.Transaction;
import main.java.domain.Transaction.Status;
import main.java.domain.User;
import main.java.input.AddInternalUserForm;
import main.java.input.TransactionFormData;
import main.java.logging.LoggingManager;
import main.java.logging.LoggingManager.Priority;
import main.java.security.AuthorizationException;
import main.java.security.SecurityManager;
import main.java.security.impl.UserCredentials;
import main.java.service.AccountManager;
import main.java.service.DelegationManager;
import main.java.service.DepartmentManager;
import main.java.service.TransactionManager;
import main.java.service.UserManager;
import main.java.util.TransactionWrapper;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class RegularEmployeeController {
	@RequestMapping(value = "/employee/EmployeeHomePage", method = RequestMethod.GET)
	public ModelAndView RegularEmpHomePage() {
		return new ModelAndView("EmployeeHomePage", "command", null);

	}

	@RequestMapping(value = "/employee/EmployeeViewUserTransactions", method = RequestMethod.GET)
	public ModelAndView employeeViewTransaction() {
		final LoggingManager logger = LoggingManager
				.create("/employee/EmployeeViewUserTransactions");
		final ModelAndView modelAndView = new ModelAndView();

		/*************************************************************************/
		// Get Logged in username.
		/**************************************************************************/
		Authentication auth = SecurityContextHolder.getContext()
				.getAuthentication();
		final String username = auth.getName(); // get logged in username

		/*************************************************************************/
		// Get User from username.
		/**************************************************************************/
		final UserManager usermanager = UserManager.create();
		final DelegationManager delegationManager = DelegationManager.create();
		try {
			(new TransactionWrapper<Void>() {

				@Override
				public Void operation(Session s) throws HibernateException {
					try {
						User u = usermanager.getUser(username);

						// Query for accounts
						u.loadAccounts();
						
						Set<? extends Account> personalAccounts = u.getAccounts();
						
						Set<? extends Delegation> delegations = u.getIncomingDelegations();
						
						List<Account> listOfAccounts= new ArrayList<Account>();

						
						for(Account a: personalAccounts)
						{
							listOfAccounts.add(a);
						}
			
						for(Delegation del : delegations)
						{
							listOfAccounts.add(del.getAccount());
						}
												
						if (listOfAccounts.size() > 0) {
							modelAndView.addObject("accounts", listOfAccounts);
						} else {
							modelAndView.addObject("Error",
									"This User doesn't have any account or delegated account.");
						}
					} catch (NullPointerException e) {
						logger.catching(Priority.ERROR, e);
						throw e;
					}
					return null;
				}
			}).execute();
		} catch (HibernateException e) {
			logger.catching(Priority.ERROR, e);
			throw e;
		}

		/*************************************************************************/
		// Fetch Accounts for User.
		/**************************************************************************/

		// add additional model attributes.
		modelAndView.addObject("username", username);
		modelAndView.addObject("curAccountNo", null);
		modelAndView
				.addObject("transactionFormData", new TransactionFormData());

		modelAndView.setViewName("EmployeeViewUserTransactions");
		return modelAndView;
	}

	@RequestMapping(value = "/employee/EmployeeViewUserTransactions", method = RequestMethod.POST)
	public ModelAndView EmployeeViewUserTransactionsPost(
			@ModelAttribute TransactionFormData transactionFormData) {
		final LoggingManager logger = LoggingManager
				.create("/employee/EmployeeViewUserTransactions");
		final ModelAndView modelAndView = new ModelAndView();

		/*************************************************************************/
		// Get Logged in username.
		/**************************************************************************/

		// Get context / input data
		Authentication auth = SecurityContextHolder.getContext()
				.getAuthentication();
		final String username = auth.getName(); // get logged in username

		long tmp = 0l;
		try {
			tmp = Long.parseLong(transactionFormData.getSelectedAccountNo());
		} catch (NullPointerException | NumberFormatException e) {
			logger.catching(Priority.ERROR, e);

			modelAndView.addObject("Error", "Invalid Account No.");
			modelAndView.setViewName("EmployeeViewUserTransactions");
			return modelAndView;
		}
		final long selectedAccountNo = tmp;

		/*************************************************************************/
		// Get Data from database
		/**************************************************************************/
		final UserManager usermanager = UserManager.create();
		try {
			(new TransactionWrapper<Void>() {

				@Override
				public Void operation(Session s) throws HibernateException {
					try {
						// Load user and his/her accounts
						User u = usermanager.getUser(username);
						u.loadAccounts();
						
						Set<? extends Account> personalAccounts = u.getAccounts();
						
						Set<? extends Delegation> delegations = u.getIncomingDelegations();
						
						List<Account> listOfAccounts= new ArrayList<Account>();
						
						for(Account a: personalAccounts)
						{
							listOfAccounts.add(a);
						}
						for(Delegation del : delegations)
						{
							listOfAccounts.add(del.getAccount());
						}

						modelAndView.addObject("accounts", listOfAccounts);
						// Find the selected account
						Account selected = null;
						for (Account a : listOfAccounts) {
							if (a.getAccountNo() == selectedAccountNo)
								selected = a;
						}

						// make sure account exists.
						if (selected == null) {
							modelAndView.addObject("Error",
									"Account does not belong to this user.");
							return null;
						}

						// load transactions
						// NOTE: JSP Requires the getIterator() method instead
						// of the iterator() method
						// provided by Iteratable. To get around this we wrap it
						// into a JSP compatible wrapper.
						selected.loadTransactions();
						if (selected.getTransactions().iterator().hasNext())
							modelAndView.addObject("transactions",
									selected.getTransactions());

					} catch (NullPointerException e) {
						logger.catching(Priority.ERROR, e);
						throw e;
					}
					return null;
				}
			}).execute();
		} catch (HibernateException e) {
			logger.catching(Priority.ERROR, e);
			throw e;
		}

		// add model attributes.
		modelAndView.addObject("username", username);
		modelAndView.addObject("curAccountNo",
				transactionFormData.getSelectedAccountNo());
		modelAndView.addObject("transactionFormData", transactionFormData);
		modelAndView.setViewName("EmployeeViewUserTransactions");

		return modelAndView;
	}
	
	
	
	
	
	
	@RequestMapping(value = "/employee/EmployeeAddUserTransaction", method = RequestMethod.GET)
	public ModelAndView EmployeeAddUserTransaction() {
		final LoggingManager logger = LoggingManager
				.create("/employee/EmployeeAddUserTransactions");
		final ModelAndView modelAndView = new ModelAndView();

		/* get Session User Name */
		Authentication auth = SecurityContextHolder.getContext()
				.getAuthentication();
		final String sessionUserName = auth.getName(); // get logged in username
		modelAndView.addObject("sessionUserName", sessionUserName);

		
		final UserManager usermanager = UserManager.create();
		final DelegationManager delegationManager = DelegationManager.create();
		try {
			(new TransactionWrapper<Void>() {

				@Override
				public Void operation(Session s) throws HibernateException {
					try {
						User u = usermanager.getUser(sessionUserName);

						// Query for accounts
						u.loadAccounts();
						
						Set<? extends Account> personalAccounts = u.getAccounts();
						
						Set<? extends Delegation> delegations = u.getIncomingDelegations();
						
						List<Account> listOfAccounts= new ArrayList<Account>();

						
						for(Account a: personalAccounts)
						{
							listOfAccounts.add(a);
						}
			
						for(Delegation del : delegations)
						{
							if(del.getPrivilege().equals(PrivilegeLevel.MODIFY))
										{
											listOfAccounts.add(del.getAccount());
										}
						}
												
						if (listOfAccounts.size() > 0) {
							modelAndView.addObject("accounts", listOfAccounts);
						} else {
							modelAndView.addObject("Error",
									"This User doesn't have any account or delegated account with modify privilege.");
						}
					} catch (NullPointerException e) {
						logger.catching(Priority.ERROR, e);
						throw e;
					}
					return null;
				}
			}).execute();
		} catch (HibernateException e) {
			logger.catching(Priority.ERROR, e);
			throw e;
		}

		
		
		/* Add Object to contain username to view transactions */
		modelAndView
				.addObject("addInternalUserForm", new AddInternalUserForm());
		modelAndView.setViewName("EmployeeAddUserTransaction");
		return modelAndView;
	}

	@RequestMapping(value = "/employee/EmployeeAddUserTransaction", method = RequestMethod.POST)
	public ModelAndView EmployeeAddUserTransactionPost(
			@ModelAttribute AddInternalUserForm addInternalUserForm) {
		final LoggingManager logger = LoggingManager
				.create("/user/EmployeeAddUserTransaction");
		final ModelAndView modelAndView = new ModelAndView();

		/* get Session User Name */
		Authentication auth = SecurityContextHolder.getContext()
				.getAuthentication();
		final String sessionUserName = auth.getName(); // get logged in username
		final UserCredentials creds = (UserCredentials)auth.getCredentials();
		modelAndView.addObject("sessionUserName", sessionUserName);

		/*****************************************************/
		/* Get username to delete the user */
		/***************************************************/
		/*
		final String username = addInternalUserForm.getUsername();
		*/
		/**************************************************************/
		// Need to validate input Data and return if error.
		/***************************************************************/
		final SecurityManager securityManager = SecurityManager.get();

		/*
		try {
			boolean flag = false;
			if (!securityManager.isUsername(username)) {
				flag = true;
				modelAndView.addObject("Error", "Invalid Username!!");
			}
			if (flag) {
				modelAndView.setViewName("EmployeeAddUserTransaction");
				return modelAndView;
			}

		} catch (Exception e) {
			modelAndView.addObject("Error", "Invalid Input Data..!!");
			modelAndView.setViewName("EmployeeAddUserTransaction");
			return modelAndView;
		}
		*/

		long from = 0l;
		long to = 0l;
		double amt = 0.0;
		try {
			from = Long.parseLong(addInternalUserForm.getFromAccountNo());
			to = Long.parseLong(addInternalUserForm.getToAccountNo());
			amt = Double.parseDouble(addInternalUserForm.getAmount());
		} catch (NullPointerException | NumberFormatException e) {
			logger.catching(Priority.ERROR, e);
			modelAndView.addObject("Error",
					"Invalid data format for Account no / Amount.");
			modelAndView.setViewName("EmployeeAddUserTransaction");
			return modelAndView;
		}
		final long fromAccountNo = from;
		final long toAccountNo = to;
		final double transferAmount = amt;

		if (fromAccountNo == toAccountNo) {
			modelAndView.addObject("Error",
					"Both from and to are same accounts.");
			modelAndView.setViewName("EmployeeAddUserTransaction");
			return modelAndView;
		}

		if (amt > 10000 || amt <= 0) {
			modelAndView.addObject("Error",
					"Cannot transfer more than $10000 or a negative/zero amount.");
			modelAndView.setViewName("EmployeeAddUserTransaction");
			return modelAndView;
		}
		
		
		final UserManager usermanager = UserManager.create();
		final AccountManager accountmanager = AccountManager.create();
		final DepartmentManager departmentManager = DepartmentManager.create();
		final TransactionManager transactionManager = TransactionManager
				.create();
		Transaction transaction = null;
		try {
			transaction = (new TransactionWrapper<Transaction>() {

				@Override
				public Transaction operation(Session s)
						throws HibernateException {
					try {
						User user = usermanager.getUser(sessionUserName);
						Account fromAccount = accountmanager
								.getAccount(fromAccountNo);
						Account toAccount = accountmanager
								.getAccount(toAccountNo);
						
						if (fromAccount == null) {
							modelAndView.addObject("Error",
									"From Account No does not exist.");
							return null;
						}
						if (toAccount == null) {
							modelAndView.addObject("Error",
									"To Account No does not exist.");
							return null;
						}
						
						if (fromAccount.getAccountBalance() >= transferAmount) {
							Transaction trans = transactionManager
									.createTransaction(user, creds, fromAccount, toAccount,
											transferAmount);
							// transaction.setTransactionStatus(Status.APPROVAL_REQUIRED);
							// transactionManager.saveTransaction(transaction);
							return trans;

						} else {
							modelAndView.addObject("Error",
									"Insufficient balance.");
							return null;
						}

					} catch (NullPointerException e) {
						logger.catching(Priority.ERROR, e);
						throw e;
					} catch (IllegalArgumentException e) {
						logger.catching(Priority.ERROR, e);
						throw e;
					} catch (BadTransactionException e) {
						modelAndView.addObject("Error",
								(e.getMessage() != null) ? e.getMessage() : "An error occurred processing the transaction.");
						return null;

					}  catch (AuthorizationException e) {
						modelAndView.addObject("Error",
								(e.getMessage() != null) ? e.getMessage() : "Unable to verify whether you are authorized to perform this action.");
						return null;
					}
				}
			}).execute();
		} catch (HibernateException e) {
			logger.catching(Priority.ERROR, e);
			throw e;
		}

		if (transaction != null) {
			modelAndView.addObject("transaction", transaction);
			modelAndView.addObject("Success",
					"Transaction Was Successful. (Transaction No: "
							+ transaction.getTransactionNo() + ")");
		}

		modelAndView.setViewName("EmployeeAddUserTransaction");
		return modelAndView;
	}

	@RequestMapping(value = "/employee/EmployeeDeleteUserTransaction", method = RequestMethod.GET)
	public ModelAndView DeleteUserTransactionByRegEmpGET() {
		return new ModelAndView("EmployeeDeleteUserTransaction", "command",
				null);
	}

	@RequestMapping(value = "/employee/EmployeeDeleteUserTransaction", method = RequestMethod.POST)
	public ModelAndView DeleteUserTransactionByRegEmpPOST() {
		return new ModelAndView("EmployeeDeleteUserTransaction", "command",
				null);
	}

	@RequestMapping(value = "/employee/EmployeeModifyUserTransaction", method = RequestMethod.GET)
	public ModelAndView ModifyUserTransactionByRegEmp() {
		return new ModelAndView("EmployeeModifyUserTransaction", "command",
				null);
	}

	@RequestMapping(value = "/employee/EmployeeAuthorizeUserTransaction", method = RequestMethod.GET)
	public ModelAndView AuthorizeUserTransactionByRegEmp() {
		return new ModelAndView("EmployeeAuthorizeUserTransaction", "command",
				null);
	}
}
