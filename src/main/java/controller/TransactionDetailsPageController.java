package main.java.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import main.java.domain.Account;
import main.java.domain.Delegation;
import main.java.domain.User;
import main.java.input.TransactionFormData;
import main.java.logging.LoggingManager;
import main.java.logging.LoggingManager.Priority;
import main.java.service.DelegationManager;
import main.java.service.UserManager;
import main.java.util.TransactionWrapper;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class TransactionDetailsPageController {

	@RequestMapping(value = "/user/ViewTransactions", method = RequestMethod.GET)
	public ModelAndView view() {
		final LoggingManager logger = LoggingManager
				.create("/user/ViewTransactions");
		final ModelAndView modelAndView = new ModelAndView();

		/*************************************************************************/
		// Get Logged in username.
		/**************************************************************************/
		Authentication auth = SecurityContextHolder.getContext()
				.getAuthentication();
		final String username = auth.getName(); // get logged in username

		/*************************************************************************/
		// Get User from username.
		/**************************************************************************/
		final UserManager usermanager = UserManager.create();
		try {
			(new TransactionWrapper<Void>() {

				@Override
				public Void operation(Session s) throws HibernateException {
					try {
						User u = usermanager.getUser(username);

						// Query for accounts
						u.loadAccounts();
						
						List<Account> listOfAccounts =  new ArrayList<Account>();
						
						Set<? extends Account> personalAccounts = u.getAccounts();
						Set<? extends Delegation>delegations = u.getIncomingDelegations();
						
						for(Account a: personalAccounts)
						{
							listOfAccounts.add(a);
						}
						for(Delegation del : delegations)
						{
							listOfAccounts.add(del.getAccount());
						}
						
						modelAndView.addObject("accounts", listOfAccounts);
						
					} catch (NullPointerException e) {
						logger.catching(Priority.ERROR, e);
						throw e;
					}
					return null;
				}
			}).execute();
		} catch (HibernateException e) {
			logger.catching(Priority.ERROR, e);
			throw e;
		}

		/*************************************************************************/
		// Fetch Accounts for User.
		/**************************************************************************/

		// add additional model attributes.
		modelAndView.addObject("username", username);
		modelAndView.addObject("curAccountNo", null);
		modelAndView
				.addObject("transactionFormData", new TransactionFormData());

		modelAndView.setViewName("ViewTransactions");
		return modelAndView;
	}

	@RequestMapping(value = "/user/ViewTransactions", method = RequestMethod.POST)
	public ModelAndView viewTransactionsForSelectedAccount(
			@ModelAttribute TransactionFormData transactionFormData) {
		final LoggingManager logger = LoggingManager
				.create("/user/ViewTransactions");
		final ModelAndView modelAndView = new ModelAndView();

		/*************************************************************************/
		// Get Logged in username.
		/**************************************************************************/

		// Get context / input data
		Authentication auth = SecurityContextHolder.getContext()
				.getAuthentication();
		final String username = auth.getName(); // get logged in username

		long tmp = 0l;
		try {
			tmp = Long.parseLong(transactionFormData.getSelectedAccountNo());
		} catch (NullPointerException | NumberFormatException e) {
			logger.catching(Priority.ERROR, e);

			modelAndView.addObject("Error", "Invalid Account No.");
			modelAndView.setViewName("ViewTransactions");
			return modelAndView;
		}
		final long selectedAccountNo = tmp;

		/*************************************************************************/
		// Get Data from database
		/**************************************************************************/
		final UserManager usermanager = UserManager.create();
		try {
			(new TransactionWrapper<Void>() {

				@Override
				public Void operation(Session s) throws HibernateException {
					try {
						// Load user and his/her accounts
						User u = usermanager.getUser(username);
						u.loadAccounts();
						
						Set<? extends Account> personalAccounts = u.getAccounts();
						
						Set<? extends Delegation> delegations = u.getIncomingDelegations();
						
						List<Account> listOfAccounts= new ArrayList<Account>();
						
						for(Account a: personalAccounts)
						{
							listOfAccounts.add(a);
						}
						for(Delegation del : delegations)
						{
							listOfAccounts.add(del.getAccount());
						}

						modelAndView.addObject("accounts", listOfAccounts);
						// Find the selected account
						Account selected = null;
						for (Account a : listOfAccounts) {
							if (a.getAccountNo() == selectedAccountNo)
								selected = a;
						}

						// make sure account exists.
						if (selected == null) {
							modelAndView.addObject("Error",
									"Account does not belong to this user.");
							return null;
						}

						// load transactions
						// NOTE: JSP Requires the getIterator() method instead
						// of the iterator() method
						// provided by Iteratable. To get around this we wrap it
						// into a JSP compatible wrapper.
						selected.loadTransactions();
						if (selected.getTransactions().iterator().hasNext())
							modelAndView.addObject("transactions",
									selected.getTransactions());

					} catch (NullPointerException e) {
						logger.catching(Priority.ERROR, e);
						throw e;
					}
					return null;
				}
			}).execute();
		} catch (HibernateException e) {
			logger.catching(Priority.ERROR, e);
			throw e;
		}

		// add model attributes.
		modelAndView.addObject("username", username);
		modelAndView.addObject("curAccountNo",
				transactionFormData.getSelectedAccountNo());
		modelAndView.addObject("transactionFormData", transactionFormData);
		modelAndView.setViewName("ViewTransactions");

		return modelAndView;
	}

	@RequestMapping(value = "/employee/EmployeeViewTransactions", method = RequestMethod.GET)
	public ModelAndView employeeViewTransaction() {
		final LoggingManager logger = LoggingManager
				.create("/employee/EmployeeViewTransactions");
		final ModelAndView modelAndView = new ModelAndView();

		/*************************************************************************/
		// Get Logged in username.
		/**************************************************************************/
		Authentication auth = SecurityContextHolder.getContext()
				.getAuthentication();
		final String username = auth.getName(); // get logged in username

		/*************************************************************************/
		// Get User from username.
		/**************************************************************************/
		final UserManager usermanager = UserManager.create();
		final DelegationManager delegationManager = DelegationManager.create();
		try {
			(new TransactionWrapper<Void>() {

				@Override
				public Void operation(Session s) throws HibernateException {
					try {
						User u = usermanager.getUser(username);

						// Query for accounts
						u.loadAccounts();
						
						Set<? extends Account> personalAccounts = u.getAccounts();
						
						Set<? extends Delegation> delegations = u.getIncomingDelegations();
						
						List<Account> listOfAccounts= new ArrayList<Account>();
						
						for(Account a: personalAccounts)
						{
							listOfAccounts.add(a);
						}
						for(Delegation del : delegations)
						{
							listOfAccounts.add(del.getAccount());
						}
												
						if (listOfAccounts.size() > 0) {
							modelAndView.addObject("accounts", listOfAccounts);
						} else {
							modelAndView.addObject("Error",
									"This User doesn't have any account or delegated account.");
						}
					} catch (NullPointerException e) {
						logger.catching(Priority.ERROR, e);
						throw e;
					}
					return null;
				}
			}).execute();
		} catch (HibernateException e) {
			logger.catching(Priority.ERROR, e);
			throw e;
		}

		/*************************************************************************/
		// Fetch Accounts for User.
		/**************************************************************************/

		// add additional model attributes.
		modelAndView.addObject("username", username);
		modelAndView.addObject("curAccountNo", null);
		modelAndView
				.addObject("transactionFormData", new TransactionFormData());

		modelAndView.setViewName("EmployeeViewTransactions");
		return modelAndView;
	}

	@RequestMapping(value = "/employee/EmployeeViewTransactions", method = RequestMethod.POST)
	public ModelAndView EmployeeViewTransactionsPost(
			@ModelAttribute TransactionFormData transactionFormData) {
		final LoggingManager logger = LoggingManager
				.create("/employee/EmployeeViewTransactions");
		final ModelAndView modelAndView = new ModelAndView();

		/*************************************************************************/
		// Get Logged in username.
		/**************************************************************************/

		// Get context / input data
		Authentication auth = SecurityContextHolder.getContext()
				.getAuthentication();
		final String username = auth.getName(); // get logged in username

		long tmp = 0l;
		try {
			tmp = Long.parseLong(transactionFormData.getSelectedAccountNo());
		} catch (NullPointerException | NumberFormatException e) {
			logger.catching(Priority.ERROR, e);

			modelAndView.addObject("Error", "Invalid Account No.");
			modelAndView.setViewName("EmployeeViewTransactions");
			return modelAndView;
		}
		final long selectedAccountNo = tmp;

		/*************************************************************************/
		// Get Data from database
		/**************************************************************************/
		final UserManager usermanager = UserManager.create();
		try {
			(new TransactionWrapper<Void>() {

				@Override
				public Void operation(Session s) throws HibernateException {
					try {
						// Load user and his/her accounts
						User u = usermanager.getUser(username);
						u.loadAccounts();
						
						Set<? extends Account> personalAccounts = u.getAccounts();
						
						Set<? extends Delegation> delegations = u.getIncomingDelegations();
						
						List<Account> listOfAccounts= new ArrayList<Account>();
						
						for(Account a: personalAccounts)
						{
							listOfAccounts.add(a);
						}
						for(Delegation del : delegations)
						{
							listOfAccounts.add(del.getAccount());
						}

						modelAndView.addObject("accounts", listOfAccounts);
						// Find the selected account
						Account selected = null;
						for (Account a : listOfAccounts) {
							if (a.getAccountNo() == selectedAccountNo)
								selected = a;
						}

						// make sure account exists.
						if (selected == null) {
							modelAndView.addObject("Error",
									"Account does not belong to this user.");
							return null;
						}

						// load transactions
						// NOTE: JSP Requires the getIterator() method instead
						// of the iterator() method
						// provided by Iteratable. To get around this we wrap it
						// into a JSP compatible wrapper.
						selected.loadTransactions();
						if (selected.getTransactions().iterator().hasNext())
							modelAndView.addObject("transactions",
									selected.getTransactions());

					} catch (NullPointerException e) {
						logger.catching(Priority.ERROR, e);
						throw e;
					}
					return null;
				}
			}).execute();
		} catch (HibernateException e) {
			logger.catching(Priority.ERROR, e);
			throw e;
		}

		// add model attributes.
		modelAndView.addObject("username", username);
		modelAndView.addObject("curAccountNo",
				transactionFormData.getSelectedAccountNo());
		modelAndView.addObject("transactionFormData", transactionFormData);
		modelAndView.setViewName("EmployeeViewTransactions");

		return modelAndView;
	}

}
