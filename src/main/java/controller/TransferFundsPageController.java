package main.java.controller;

import java.util.Set;

import main.java.domain.Account;
import main.java.domain.BadTransactionException;
import main.java.domain.Recipient;
import main.java.domain.Transaction;
import main.java.domain.User;
import main.java.input.AddInternalUserForm;
import main.java.logging.LoggingManager;
import main.java.logging.LoggingManager.Priority;
import main.java.security.AuthorizationException;
import main.java.security.SecurityManager;
import main.java.security.impl.UserCredentials;
import main.java.service.AccountManager;
import main.java.service.RecipientManager;
import main.java.service.TransactionManager;
import main.java.service.UserManager;
import main.java.util.TransactionWrapper;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class TransferFundsPageController {
	@RequestMapping(value = "/user/Transfer", method = RequestMethod.GET)
	public ModelAndView transfer() {
		return new ModelAndView("Transfer", "command", null);
	}

	@RequestMapping(value = "/user/MoneyTransfer", method = RequestMethod.GET)
	public ModelAndView MoneyTransfer() {

		final LoggingManager logger = LoggingManager
				.create("/admin/MoneyTransfer");
		final ModelAndView modelAndView = new ModelAndView();

		/* get Session User Name */
		Authentication auth = SecurityContextHolder.getContext()
				.getAuthentication();
		final String sessionUserName = auth.getName(); // get logged in username
		modelAndView.addObject("sessionUserName", sessionUserName);

		final UserManager s_usermanager = UserManager.create();
		//final AccountManager accountmanager = AccountManager.create();
		//final RecipientManager recmanager = RecipientManager.create();

		try {
			(new TransactionWrapper<Void>() {

				@Override
				public Void operation(Session s) throws HibernateException,
						IllegalArgumentException {
					try {
						User user = s_usermanager.getUser(sessionUserName);

						// Query for accounts
						user.loadAccounts();
						if (user.getAccounts().size() > 0)
							modelAndView.addObject("accounts",
									user.getAccounts());

						user.loadRecipients();

						Set<? extends Recipient> recipients = user
								.getRecipients();

						if (recipients.iterator().hasNext()) {
							modelAndView.addObject("recipients", recipients);

						}

						return null;

					} catch (NullPointerException e) {
						logger.catching(Priority.ERROR, e);
						throw e;
					}
				}
			}).execute();
		} catch (HibernateException e) {
			logger.catching(Priority.ERROR, e);
			throw e;
		}

		/* Add Object to contain username to be deleted */
		modelAndView
				.addObject("addInternalUserForm", new AddInternalUserForm());
		modelAndView.setViewName("MoneyTransfer");
		return modelAndView;
	}

	@RequestMapping(value = "/user/MoneyTransfer", method = RequestMethod.POST)
	public ModelAndView MoneyTransferPost(
			@ModelAttribute AddInternalUserForm addInternalUserForm) {
		final LoggingManager logger = LoggingManager
				.create("/user/MoneyTransfer");
		final ModelAndView modelAndView = new ModelAndView();

		/* get Session User Name */
		final Authentication auth = SecurityContextHolder.getContext()
				.getAuthentication();
		final UserCredentials creds = (UserCredentials)auth.getCredentials();
		final String sessionUserName = auth.getName(); // get logged in username
		modelAndView.addObject("sessionUserName", sessionUserName);

		/*****************************************************/
		/* Get username to MoneyTransfer */
		/***************************************************/
		//final String username = addInternalUserForm.getUsername();

		/**************************************************************/
		// Need to validate input Data and return if error.
		/***************************************************************/
		final SecurityManager securityManager = SecurityManager.get();

		try {
			boolean flag = false;
			if (!securityManager.isUsername(sessionUserName)) {
				flag = true;
				modelAndView.addObject("Error", "Invalid Username!!");
			}
			if (flag) {
				modelAndView.setViewName("MoneyTransfer");
				return modelAndView;
			}

		} catch (Exception e) {
			modelAndView.addObject("Error", "Invalid Input Data..!!");
			modelAndView.setViewName("MoneyTransfer");
			return modelAndView;
		}

		long from = 0l;
		long to = 0l;
		double amt = 0.0;
		try {
			to = Long.parseLong(addInternalUserForm.getToAccountNo());
			from = Long.parseLong(addInternalUserForm.getFromAccountNo());
			amt = Double.parseDouble(addInternalUserForm.getAmount());

		} catch (NullPointerException | NumberFormatException e) {
			logger.catching(Priority.ERROR, e);
			modelAndView
					.addObject("Error", "Invalid Account No or Amount...!!");
			modelAndView.setViewName("MoneyTransfer");
			return modelAndView;
		}
		final long fromAccountNo = from;
		final long toAccountNo = to;
		final double transferAmount = amt;

		if (fromAccountNo == toAccountNo) {
			modelAndView.addObject("Error",
					"Both from and to are same accounts.");
			modelAndView.setViewName("MoneyTransfer");
			return modelAndView;
		}

		if (amt > 1000 || amt <= 0) {
			modelAndView
					.addObject("Error",
							"Cannot credit or debit more than $1000 or a negative/zero amount.");
			modelAndView.setViewName("MoneyTransfer");
			return modelAndView;
		}

		final UserManager usermanager = UserManager.create();
		final AccountManager accountmanager = AccountManager.create();
		//final DepartmentManager departmentManager = DepartmentManager.create();
		final TransactionManager transactionManager = TransactionManager
				.create();
		Transaction transaction = null;
		try {
			transaction = (new TransactionWrapper<Transaction>() {

				@Override
				public Transaction operation(Session s)
						throws HibernateException {
					try {
						User user = usermanager.getUser(sessionUserName);
						Account fromAccount = accountmanager
								.getAccount(fromAccountNo);
						Account toAccount = accountmanager
								.getAccount(toAccountNo);

						if (fromAccount == null) {
							modelAndView.addObject("Error",
									"From Account No does not exist.");
							return null;
						}
						if (toAccount == null) {
							modelAndView.addObject("Error",
									"To Account No does not exist.");
							return null;
						}

						if (fromAccount.getAccountBalance() >= transferAmount) {
							Transaction trans = transactionManager
									.createTransaction(user, creds, fromAccount, toAccount,
											transferAmount);
							// transaction.setTransactionStatus(Status.APPROVAL_REQUIRED);
							// transactionManager.saveTransaction(transaction);
							return trans;

						} else {
							modelAndView.addObject("Error",
									"Insufficient balance.");
							return null;
						}

					} catch (NullPointerException e) {
						logger.catching(Priority.ERROR, e);
						throw e;
					} catch (IllegalArgumentException e) {
						logger.catching(Priority.ERROR, e);
						throw e;
					} catch (BadTransactionException e) {
						modelAndView.addObject("Error",
								(e.getMessage() != null) ? e.getMessage() : "An error occurred processing the transaction.");
						return null;
					} catch (AuthorizationException e) {
						modelAndView.addObject("Error",
								(e.getMessage() != null) ? e.getMessage() : "Unable to verify whether you are authorized to perform this action.");
						return null;
					}
				}
			}).execute();
		} catch (HibernateException e) {
			logger.catching(Priority.ERROR, e);
			throw e;
		}

		if (transaction != null) {
			modelAndView.addObject("transaction", transaction);
			modelAndView.addObject("Success",
					"Transaction Was Successful. (Transaction No: "
							+ transaction.getTransactionNo() + ")");
		}

		modelAndView.setViewName("MoneyTransfer");
		return modelAndView;
	}

	@RequestMapping(value = "/user/addRecipient", method = RequestMethod.GET)
	public ModelAndView addRecipients() {
		ModelAndView modelAndView = new ModelAndView();

		/* get Session User Name */
		Authentication auth = SecurityContextHolder.getContext()
				.getAuthentication();
		final String sessionUserName = auth.getName(); // get logged in username
		modelAndView.addObject("sessionUserName", sessionUserName);

		/* Add Object to contain username to be deleted */
		modelAndView
				.addObject("addInternalUserForm", new AddInternalUserForm());
		modelAndView.setViewName("addRecipient");
		return modelAndView;
	}

	@RequestMapping(value = "/user/addRecipient", method = RequestMethod.POST)
	public ModelAndView addRecipientPost(
			@ModelAttribute AddInternalUserForm addInternalUserForm) {

		final LoggingManager logger = LoggingManager
				.create("/admin/addRecipient");
		final ModelAndView modelAndView = new ModelAndView();

		/* get Session User Name */
		Authentication auth = SecurityContextHolder.getContext()
				.getAuthentication();
		final String sessionUserName = auth.getName(); // get logged in username
		modelAndView.addObject("sessionUserName", sessionUserName);

		/**************************************************************/
		// Need to validate input Data and return error.
		/***************************************************************/

		final String recipientName = addInternalUserForm.getRecipientName();

		/**************************************************************/
		// Need to validate input Data and return if error.
		/***************************************************************/
		final SecurityManager securityManager = SecurityManager.get();

		try {
			boolean flag = false;
			if (!securityManager.isAlpha(recipientName)
					&& recipientName.length() > 50) {
				flag = true;
				modelAndView.addObject("Error", "Invalid Recipient Name!!");
			}
			if (flag) {
				modelAndView.setViewName("addRecipient");
				return modelAndView;
			}

		} catch (Exception e) {
			modelAndView.addObject("Error", "Invalid Input Data..!!");
			modelAndView.setViewName("addRecipient");
			return modelAndView;
		}

		/*****************************************************/
		/* Get username to delete the user */
		/***************************************************/
		long tmp = 0l;
		try {

			tmp = Long.parseLong(addInternalUserForm.getSelectedAccountNo());

		} catch (NullPointerException | NumberFormatException e) {
			logger.catching(Priority.ERROR, e);
			modelAndView.addObject("Error", "Invalid recipient Account No.");
			modelAndView.setViewName("addRecipient");
			return modelAndView;
		}
		final long recipAccountNo = tmp;

		final UserManager s_usermanager = UserManager.create();
		final AccountManager accountmanager = AccountManager.create();
		final RecipientManager recmanager = RecipientManager.create();
		Recipient recip = null;

		try {
			recip = (new TransactionWrapper<Recipient>() {

				@Override
				public Recipient operation(Session s)
						throws HibernateException, IllegalArgumentException {
					try {
						User user = s_usermanager.getUser(sessionUserName);

						Account recipAccount = accountmanager
								.getAccount(recipAccountNo);

						if (recipAccount == null) {
							modelAndView
									.addObject("Error",
											"The recipient Account Number doesn't exist.");
							return null;
						}

						Recipient recipient = recmanager.createRecipient(user,
								recipientName, recipAccount);

						return recipient;

					} catch (NullPointerException e) {
						logger.catching(Priority.ERROR, e);
						throw e;
					}
				}
			}).execute();
		} catch (HibernateException e) {
			logger.catching(Priority.ERROR, e);
			throw e;
		}

		if (recip != null) {
			modelAndView.addObject("Success",
					"Recipient was added successfully");
		} else {
			modelAndView.addObject("Error",
					"Invalid Attempt.. Recipient could not be added");
		}

		modelAndView.setViewName("addRecipient");
		return modelAndView;
	}

	@RequestMapping(value = "/user/deleteRecipient", method = RequestMethod.GET)
	public ModelAndView deleteRecipient() {

		final LoggingManager logger = LoggingManager
				.create("/admin/deleteRecipient");
		final ModelAndView modelAndView = new ModelAndView();

		/* get Session User Name */
		Authentication auth = SecurityContextHolder.getContext()
				.getAuthentication();
		final String sessionUserName = auth.getName(); // get logged in username
		modelAndView.addObject("sessionUserName", sessionUserName);

		final UserManager s_usermanager = UserManager.create();
		//final AccountManager accountmanager = AccountManager.create();
		//final RecipientManager recmanager = RecipientManager.create();

		try {
			(new TransactionWrapper<Void>() {

				@Override
				public Void operation(Session s) throws HibernateException,
						IllegalArgumentException {
					try {
						User user = s_usermanager.getUser(sessionUserName);

						user.loadRecipients();

						Set<? extends Recipient> recipients = user
								.getRecipients();

						if (recipients.iterator().hasNext()) {
							modelAndView.addObject("recipients", recipients);
						}
						else
						{
							modelAndView.addObject("Error", "User doesn't have any recipients to delete!!");
							return null;
						}
						

						return null;

					} catch (NullPointerException e) {
						logger.catching(Priority.ERROR, e);
						throw e;
					}
				}
			}).execute();
		} catch (HibernateException e) {
			logger.catching(Priority.ERROR, e);
			throw e;
		}

		/* Add Object to contain username to be deleted */
		modelAndView
				.addObject("addInternalUserForm", new AddInternalUserForm());
		modelAndView.setViewName("deleteRecipient");
		return modelAndView;
	}

	@RequestMapping(value = "/user/deleteRecipient", method = RequestMethod.POST)
	public ModelAndView deleteRecipientPost(
			@ModelAttribute AddInternalUserForm addInternalUserForm) {

		
		final LoggingManager logger = LoggingManager
				.create("/admin/deleteRecipient");
		final ModelAndView modelAndView = new ModelAndView();

		/* get Session User Name */
		final Authentication auth = SecurityContextHolder.getContext()
				.getAuthentication();
		final String sessionUserName = auth.getName(); // get logged in username
		modelAndView.addObject("sessionUserName", sessionUserName);

		/**************************************************************/
		// Need to validate input Data and return error.
		/***************************************************************/

		final String recipientName = addInternalUserForm.getRecipientName();
		// final Recipient recipientToDelete =
		// addInternalUserForm.getRecipient();

		/*****************************************************/
		/* Get recipient object to delete */
		/***************************************************/

		final UserManager s_usermanager = UserManager.create();
		//final AccountManager accountmanager = AccountManager.create();
		final RecipientManager recmanager = RecipientManager.create();

		try {
			(new TransactionWrapper<Void>() {

				@Override
				public Void operation(Session s) throws HibernateException,
						IllegalArgumentException {
					try {
						User user = s_usermanager.getUser(sessionUserName);
						user.loadRecipients();

						Iterable<? extends Recipient> listToDelete = user
								.getFilteredRecipients(recipientName);

						if (!listToDelete.iterator().hasNext()) {
							modelAndView
									.addObject("Error",
											"Invalid Attempt.. Recipient does not exist");
							return null;
						}

						for (Recipient recipientToDelete : listToDelete) {
							recmanager.deleteRecipient(recipientToDelete);
						}

					} catch (NullPointerException e) {
						logger.catching(Priority.ERROR, e);
						throw e;
					}
					return null;
				}
			}).execute();
		} catch (HibernateException e) {
			logger.catching(Priority.ERROR, e);
			throw e;
		}

		modelAndView.addObject("Success", "Recipient was deleted successfully");

		modelAndView.setViewName("deleteRecipient");
		return modelAndView;
	}

	@RequestMapping(value = "/user/CreditDebit", method = RequestMethod.GET)
	public ModelAndView CreditDebit() {
		final LoggingManager logger = LoggingManager
				.create("/user/CreditDebit");
		final ModelAndView modelAndView = new ModelAndView();

		/* get Session User Name */
		Authentication auth = SecurityContextHolder.getContext()
				.getAuthentication();
		final String sessionUserName = auth.getName(); // get logged in username
		modelAndView.addObject("sessionUserName", sessionUserName);

		/*************************************************************************/
		// Get Accounts from username.
		/**************************************************************************/
		final UserManager usermanager = UserManager.create();
		try {
			(new TransactionWrapper<Void>() {

				@Override
				public Void operation(Session s) throws HibernateException {
					try {
						User u = usermanager.getUser(sessionUserName);

						// Query for accounts
						u.loadAccounts();
						if (u.getAccounts().size() > 0)
							modelAndView.addObject("accounts", u.getAccounts());
					} catch (NullPointerException e) {
						logger.catching(Priority.ERROR, e);
						throw e;
					}
					return null;
				}
			}).execute();
		} catch (HibernateException e) {
			logger.catching(Priority.ERROR, e);
			throw e;
		}

		/* Add Object to contain username to view transactions */
		modelAndView
				.addObject("addInternalUserForm", new AddInternalUserForm());
		modelAndView.setViewName("CreditDebit");
		return modelAndView;
	}

	@RequestMapping(value = "/user/CreditDebit", method = RequestMethod.POST)
	public ModelAndView CreditDebitPost(
			@ModelAttribute AddInternalUserForm addInternalUserForm) {
		final LoggingManager logger = LoggingManager
				.create("/user/CreditDebit");
		final Authentication auth = SecurityContextHolder.getContext()
				.getAuthentication();
		final ModelAndView modelAndView = new ModelAndView();

		/* get Session User Name */
		final String sessionUserName = auth.getName(); // get logged in username
		final UserCredentials creds = (UserCredentials)auth.getCredentials();
		modelAndView.addObject("sessionUserName", sessionUserName);

		/**************************************************************/
		// Need to validate input Data and return error.
		/***************************************************************/

		/*****************************************************/
		/* Get username to delete the user */
		/***************************************************/
		//final String username = addInternalUserForm.getUsername();

		/**************************************************************/
		// Need to validate input Data and return if error.
		/***************************************************************/
		final SecurityManager securityManager = SecurityManager.get();

		try {
			boolean flag = false;
			if (!securityManager.isUsername(sessionUserName)) {
				flag = true;
				modelAndView.addObject("Error", "Invalid Username!!");
			}
			if (flag) {
				modelAndView.setViewName("CreditDebit");
				return modelAndView;
			}

		} catch (Exception e) {
			modelAndView.addObject("Error", "Invalid Input Data..!!");
			modelAndView.setViewName("CreditDebit");
			return modelAndView;
		}

		long from = 0l;
		long to = 0l;
		double amt = 0.0;
		final String option=addInternalUserForm.getCreditDebitOption();
		try {
			if (option.contentEquals(
					"CREDIT")) {
				from = 0l;
				to = Long.parseLong(addInternalUserForm.getSelectedAccountNo());
			}
			else if (option.contentEquals(
					"DEBIT")) {
				from = Long.parseLong(addInternalUserForm
						.getSelectedAccountNo());
				to = 0l;
			}
			amt = Double.parseDouble(addInternalUserForm.getAmount());
		} catch (NullPointerException | NumberFormatException e) {
			logger.catching(Priority.ERROR, e);
			modelAndView.addObject("Error", "Invalid Attempt.");
			modelAndView.setViewName("CreditDebit");
			return modelAndView;
		}
		final long fromAccountNo = from;
		final long toAccountNo = to;
		final double transferAmount = amt;

		if (fromAccountNo == toAccountNo) {
			modelAndView.addObject("Error",
					"Both from and to are same accounts.");
			modelAndView.setViewName("CreditDebit");
			return modelAndView;
		}

		if (amt > 1000 || amt <= 0) {
			modelAndView
					.addObject("Error",
							"Cannot credit or debit more than $1000 or a negative/zero amount.");
			modelAndView.setViewName("CreditDebit");
			return modelAndView;
		}

		final UserManager usermanager = UserManager.create();
		final AccountManager accountmanager = AccountManager.create();
		//final DepartmentManager departmentManager = DepartmentManager.create();
		final TransactionManager transactionManager = TransactionManager
				.create();
		Transaction transaction = null;
		try {
			transaction = (new TransactionWrapper<Transaction>() {

				@Override
				public Transaction operation(Session s)
						throws HibernateException {
					try {
						User sessionUser = usermanager.getUser(sessionUserName);
						User extuser = usermanager.getUser("external");

						if (extuser == null) {
							modelAndView
									.addObject("Error",
											"External User Account doesn't exist to credit or debit.");
							return null;
						}

						extuser.loadAccounts();
						Set<? extends Account> extaccounts = extuser
								.getAccounts();

						Account fromAccount = null;
						Account toAccount = null;

						if (extaccounts.iterator().hasNext()
								&& fromAccountNo == 0l) {
							fromAccount = extaccounts.iterator().next();
							toAccount = accountmanager.getAccount(toAccountNo);
						} else if (extaccounts.iterator().hasNext()
								&& toAccountNo == 0l) {
							toAccount = extaccounts.iterator().next();
							fromAccount = accountmanager
									.getAccount(fromAccountNo);
						} else {
							modelAndView
									.addObject("Error",
											"External Account to credit debit doesnt exist.");
							return null;
						}

						if (fromAccount == null || toAccount == null) {
							modelAndView.addObject("Error",
									"User Account No does not exist.");
							return null;
						}
						
						if (creds == null) {
							logger.debug("User '{}' has null credentials.", sessionUser);
						} else if (!creds.hasCertificate()) {
							logger.debug("User '{}' has no valid certificate.", sessionUser);
						}
						
						Transaction trans = null;
						if (fromAccount.getAccountBalance() >= transferAmount && option.contentEquals("CREDIT")) {
							 trans = transactionManager
									.createTransaction(extuser, null, fromAccount, toAccount,
											transferAmount);
							

						} 
						else if(fromAccount.getAccountBalance() >= transferAmount && option.contentEquals("DEBIT"))
						{
							 trans = transactionManager
									.createTransaction(sessionUser, creds, fromAccount, toAccount,
											transferAmount);
							
						}
						else {
							modelAndView.addObject("Error",
									"Insufficient balance.");
							return null;
						}
						
						return trans;

					} catch (NullPointerException e) {
						logger.catching(Priority.ERROR, e);
						throw e;
					} catch (IllegalArgumentException e) {
						logger.catching(Priority.ERROR, e);
						throw e;
					} catch (BadTransactionException e) {
						modelAndView.addObject("Error",
								(e.getMessage() != null) ? e.getMessage() : "An error occurred processing the transaction.");
						return null;
					} catch (AuthorizationException e) {
						modelAndView.addObject("Error",
								(e.getMessage() != null) ? e.getMessage() : "Unable to verify whether you are authorized to perform this action.");
						return null;
					}
				}
			}).execute();
		} catch (HibernateException e) {
			logger.catching(Priority.ERROR, e);
			throw e;
		}

		if (transaction != null) {
			modelAndView.addObject("transaction", transaction);
			modelAndView.addObject("Success",
					addInternalUserForm.getCreditDebitOption()
							+ " Transaction Was Successful. (Transaction No: "
							+ transaction.getTransactionNo() + ")");
		}

		modelAndView.setViewName("CreditDebit");
		return modelAndView;
	}

}
