package main.java.domain;

import java.util.Date;
import java.util.Set;

import main.java.domain.impl.AccountImpl;
import main.java.domain.impl.TransactionImpl;


public abstract class Account {

	/**********************************************************************************/
	/* Types */
	/**********************************************************************************/
	/**
	 * Account status flag.
	 * @author jbabb
	 */
	public enum Status {
		ACTIVE,
		DISABLED,
		DELETED
	}
	
	/**
	 * The type of account that this is
	 * @author jbabb
	 */
	public enum Type {
		PERSONAL,
		DEPARTMENT
	}
	
	/**
	 * Filter values for account status.
	 * @author jbabb
	 */
	public enum StatusFilter {
		ALL(null, true),
		ACTIVE(Status.ACTIVE, false),
		DISABLED(Status.DISABLED, false),
		DELETED(Status.DELETED, false),
		NOT_DELETED(Status.DELETED, true);
		
		public final Status status; 	///< The status corresponding to the filter.
		public final boolean negate;	///< Whether to inver the entries matching the filter.
		
		private StatusFilter(Status status, boolean negate) {
			this.status = status;
			this.negate = negate;
		}
		
	}
	
	/**********************************************************************************/
	/* Static Factory Methods */
	/**********************************************************************************/
	
	/**
	 * Initialize the account with an owner
	 * @param owner The owner of the account.
	 * @param balance The starting balance of the account.
	 * @throws NullPointerException Thrown if owner is null.
	 * @throws IllegalArgumentException Thrown if owner is an unsupported user container or if balance < 0.
	 */
	@SuppressWarnings("deprecation")
	public static Account create(User owner, double balance) throws NullPointerException, IllegalArgumentException {
		return new AccountImpl(owner, balance);
	}
	
	/**
	 * Initialize the account with an owner
	 * @param owner The owner of the account.
	 * @param balance The starting balance of the account.
	 * @throws NullPointerException Thrown if owner is null.
	 * @throws IllegalArgumentException Thrown if owner is an unsupported user container or if balance < 0.
	 */
	@SuppressWarnings("deprecation")
	public static Account create(Department owner, double balance) throws NullPointerException, IllegalArgumentException {
		return new AccountImpl(owner, balance);
	}
	
	/**
	 * Initialize the account with an owner, name, and description.
	 * @param owner The owner of the account.
	 * @param balance The starting account balance. 
	 * @param name The name of the account.
	 * @param description The description of the account.
	 * @throws NullPointerException Thrown if any of the arguments are null.
	 * @throws IllegalArgumentException Thrown if owner is an unsupported user container or if balance < 0.
	 */
	@SuppressWarnings("deprecation")
	public static Account create(User owner, double balance, String name, String description) throws NullPointerException, IllegalArgumentException {
		return new AccountImpl(owner, balance, name, description);
	}
	
	/**
	 * Initialize the account with an owner, name, and description.
	 * @param owner The department which owns the account.
	 * @param balance The starting account balance. 
	 * @param name The name of the account.
	 * @param description The description of the account.
	 * @throws NullPointerException Thrown if any of the arguments are null.
	 * @throws IllegalArgumentException Thrown if owner is an unsupported user container or if balance < 0.
	 */
	@SuppressWarnings("deprecation")
	public static Account create(Department owner, double balance, String name, String description) throws NullPointerException, IllegalArgumentException {
		return new AccountImpl(owner, balance, name, description);
	}
	
	/**********************************************************************************/
	/* Interface */
	/**********************************************************************************/
	
	/**
	 * @return the accountNo
	 */
	public abstract long getAccountNo();
	
	/**
	 * @return Gets the account number in the form XXXXXX...XXXX2342.
	 */
	public abstract String getMaskedAccountNo();
	
	/**
	 * @return the owner of the account.
	 * This is either a User or Department depending on the account type.
	 */
	public abstract Object getAccountOwner();

	/**
	 * @return The type of this account (i.e. whether it's owned by an individual or department).
	 */
	public abstract Type getAccountType();
	
	/**
	 * @return the accountName
	 */
	public abstract String getAccountName();

	/**
	 * @param accountName the accountName to set
	 * @throws NullPointerException Thrown if accountName is null.
	 */
	public abstract void setAccountName(String accountName) throws NullPointerException;

	/**
	 * @return the accountBalance
	 */
	public abstract double getAccountBalance();

	/**
	 * @return the accountDescription
	 */
	public abstract String getAccountDescription();

	/**
	 * @param accountDescription the accountDescription to set
	 * @throws NullPointerException if accountDescription is null.
	 */
	public abstract void setAccountDescription(String accountDescription) throws NullPointerException;

	/**
	 * @return the accountActiveFlag
	 */
	public abstract Status getAccountStatus();

	/**
	 * @param accountActiveFlag the accountActiveFlag to set
	 */
	public abstract void setAccountStatus(Status accountStatus) throws NullPointerException;

	/**
	 * @return the accountActivationDate
	 */
	public abstract Date getAccountActivationDate();

	/**
	 * @return the accountTerminationDate
	 */
	public abstract Date getAccountTerminationDate();

	/**
	 * @return the accountLastUpdateDate
	 */
	public abstract Date getAccountLastUpdateDate();
	
	/**
	 * Loads all credit transactions from the database.
	 * Should be called within a TransactionWrapper.
	 */
	public abstract void loadCredits();
	
	/**
	 * Determines if the list of credi transactions have been loaded from the database.
	 */
	public abstract boolean isCreditsLoaded();
	
	/**
	 * @return the credit transactions. Credits should already be loaded.
	 */
	public abstract Iterable<? extends Transaction> getCredits();
	
	/**
	 * Loads all debit transactions from the database.
	 * Should be called within a TransactionWrapper.
	 */
	public abstract void loadDebits();
	
	/**
	 * Determines if the list of debit transactions have been loaded from the database.
	 */
	public abstract boolean isDebitsLoaded();
	
	/**
	 * @return the debit transactions. Debits should already be loaded.
	 */
	public abstract Iterable<TransactionImpl> getDebits();
	
	/**
	 * Loads all transactions from the database.
	 * Should be called within a TransactionWrapper.
	 */
	public abstract void loadTransactions();
	
	/**
	 * Determines if the list of all transactions have been loaded from the database.
	 */
	public abstract boolean isTransactionsLoaded();
	
	/**
	 * @return all transactions. Credits + debits should already be loaded.
	 */
	public abstract Iterable<? extends Transaction> getTransactions();
	
	/**
	 * Determines if the list of delegations have been loaded from the database.
	 */
	public abstract boolean isDelegationsLoaded();
	
	/**
	 * Loads all delegation from the database.
	 * Should be called within a TransactionWrapper.
	 */
	public abstract void loadDelegations();
	
	/**
	 * @return All privilege delegations associated with the account.
	 */
	public abstract Set<? extends Delegation> getDelegations();
	
}