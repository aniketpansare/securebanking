package main.java.domain;

/**
 * Exception thrown if the transaction isn't valid.
 * @author jbabb
 *
 */
public class BadTransactionException extends Exception {
	private static final long serialVersionUID = 1L;

	public BadTransactionException() {
		super();
	}

	public BadTransactionException(String message, Throwable cause,
			boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public BadTransactionException(String message, Throwable cause) {
		super(message, cause);
	}

	public BadTransactionException(String message) {
		super(message);
	}

	public BadTransactionException(Throwable cause) {
		super(cause);
	}

}
