package main.java.domain;

import java.io.Serializable;

import main.java.domain.Account;
import main.java.domain.impl.DelegationImpl;
import main.java.domain.User;
import main.java.security.AuthorizationException;


public abstract class Delegation implements Serializable {
	private static final long serialVersionUID = 1L;

	/************************************************************************/
	/* Types */
	/************************************************************************/

	/**
	 * Descriptor for the privilege level being delegated.
	 * @author jbabb
	 *
	 */
	public enum PrivilegeLevel {
		ACCESS,
		MODIFY
	}
	
	/************************************************************************/
	/* Static Factory Methods */
	/************************************************************************/
	
	/**
	 * Initializes a new delegation instance.
	 * @param delegator The user whose requested the delegation.
	 * @param account The account whose privileges are being delegated.
	 * @param delegatedPrivileges The privilege level being delegated.
	 * @param delegatee The individual whose being delegated the privileges.
	 * @throws IllegalArgumentException Throw if delegator, account, or delegatee aren't valid user/account containers.
	 * @throws NullPointerException Thrown if any of the arguments are null.
	 */
	@SuppressWarnings("deprecation")
	public static Delegation create(User delegator, Account account, PrivilegeLevel delegatedPrivileges, User delegatee) 
		throws IllegalArgumentException, NullPointerException {
		return new DelegationImpl(delegator, account, delegatedPrivileges, delegatee);
	}
	
	/************************************************************************/
	/* Interface Sepcification */
	/************************************************************************/
	
	/**
	 * @return The  ID of the delegation
	 */
	public abstract long getId(); 
	
	/**
	 * @return the createdBy
	 */
	public abstract User getCreatedBy();

	/**
	 * @return the account
	 */
	public abstract Account getAccount();

	/**
	 * @return the privilege
	 */
	public abstract PrivilegeLevel getPrivilege();

	/**
	 * @return the delegatedTo
	 */
	public abstract User getDelegatedTo();

}