package main.java.domain;

import java.util.Set;

import main.java.domain.impl.DepartmentImpl;

public abstract class Department {

	/******************************************************************************************/
	/* Static Factory Methods */
	/******************************************************************************************/
	/**
	 * Initialize a new department with the given name.
	 * @param name The name for the department.
	 * @throws NullPointerException Thrown if name is null.
	 */
	@SuppressWarnings("deprecation")
	public static Department create(String name) throws NullPointerException {
		return new DepartmentImpl(name);
		
	}
	
	
	/******************************************************************************************/
	/* Interface Specification */
	/******************************************************************************************/
	
	
	/**
	 * @return the id
	 */
	public abstract Long getId();

	/**
	 * @return the departmentName
	 */
	public abstract String getDepartmentName();

	/**
	 * @param departmentName the departmentName to set
	 */
	public abstract void setDepartmentName(String departmentName);
	
	/**
	 * Loads the department's accounts from the database.
	 * Must be called within a TransactionWrapper.
	 */
	public abstract void loadAccounts();
	
	/**
	 * Checks if the department's accounts have been loaded from the database.
	 */
	public abstract boolean isAccountsLoaded();
	
	/**
	 * @return the department's accounts. Must be already loaded in.
	 */
	public abstract Set<? extends Account> getAccounts();
	
	/**
	 * Filters the department's accounts by their status.
	 * @param filter The status to filter by.
	 * @return An iterable of filtered accounts.
	 * @throws NullPointerException Thrown if filter is null.
	 */
	public abstract Iterable<? extends Account> getFilteredAccounts(Account.StatusFilter filter) throws NullPointerException;
	
	/**
	 * @return the parent.
	 */
	public abstract Department getParent();

	/**
	 * @param parent the parent to set
	 * @throws IllegalArgumentException Thrown if department is not a supported department container.
	 */
	public abstract void setParent(Department parent) throws IllegalArgumentException;

	/**
	 * Determines if the department's children have been loaded from the database.
	 */
	public abstract boolean isChildrenLoaded();
	
	/**
	 * Loads child departments from the database.
	 * Should be called within a TransactionWrapper.
	 */
	public abstract void loadChildren();
	
	/**
	 * @return the children. Must have already been initialized.
	 */
	public abstract Set<? extends Department> getChildren();

	/**
	 * Determines if the department's employees have been loaded from the database.
	 */
	public abstract boolean isEmployeesLoaded();
	
	/**
	 * Loads employees from the database.
	 * Should be called within a TransactionWrapper.
	 */
	public abstract void loadEmployees();
	
	/**
	 * @return the employees. Must have been loaded.
	 */
	public abstract Set<? extends User> getEmployees();
	
	/**
	 * Returns a filtered list of all employees in the department.
	 * @param username The username substring to filter by.
	 * @param name The name substring to filter by.
	 * @return An iterable of the filtered employees.
	 */
	public abstract Iterable<? extends User> getFilteredEmployees(String username, String name);
	
	/**
	 * Removes an employee from the department.
	 * @param employee the employee to remove.
	 * @throws NullPointerException Thrown if employee is null.
	 * @throws IllegalArgumentException Thrown if employee is not a supported user container.
	 * @throws IllegalStateException Thrown if the employees list has not been properly loaded.
	 */
	public abstract void removeEmployee(User employee) throws NullPointerException, IllegalArgumentException, IllegalStateException;

	/**
	 * Adds an employee to the department.
	 * @param employee the employee to add.
	 * @throws NullPointerException Thrown if employee is null.
	 * @throws IllegalArgumentException Thrown if employee is not a supported user container.
	 * @throws IllegalStateException Thrown if the employees list has not been properly loaded.
	 */
	public abstract void addEmployee(User employee) throws NullPointerException, IllegalArgumentException, IllegalStateException;

}