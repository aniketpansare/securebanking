package main.java.domain;

import main.java.domain.impl.IdentifierImpl;

/**
 * Safe external interface for an identifier.
 * @author jbabb
 */
public abstract class Identifier {

	/***********************************************************************************************/
	/* Static Factory Methods */
	/***********************************************************************************************/
	
	
	@SuppressWarnings("deprecation")
	public static Identifier create(long id, String table) {
		return new IdentifierImpl(id, table);
	}
	
	
	
	/***********************************************************************************************/
	/* Interface Definition */
	/***********************************************************************************************/
	
	/**
	 * @return the id
	 */
	public abstract long getIdentifier();

	/**
	 * @return the usedIn
	 */
	public abstract String getTable();

}