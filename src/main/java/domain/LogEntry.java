package main.java.domain;

import java.util.Date;

/**
 * Interface for a log in the database.
 * @author jbabb
 *
 */
public abstract class LogEntry {

	/**
	 * Gets the name of the logger used to create the statement.
	 * @return the logger used to log the statement
	 */
	public abstract String getLogger();

	/**
	 * Gets the event log priority.
	 * @return the event priority
	 */
	public abstract String getPriority();

	/**
	 * Gets the event message.
	 * @return the event message
	 */
	public abstract String getMessage();

	/**
	 * Gets the stacktrace of the event.
	 * @return The stacktrace of the event (or NULL if not available).
	 */
	public abstract String getStacktrace();
	/**
	 * Gets the time that the event was created.
	 * @return the time the event was logged at.
	 */
	public abstract Date getTime();

}