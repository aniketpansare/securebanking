package main.java.domain;

import java.nio.charset.CharacterCodingException;

import main.java.domain.impl.PasswordImpl;

/**
 * Safe interface for password objects.
 * @author jbabb
 *
 */
public abstract class Password {

	
	/***********************************************************************************************/
	/* Static Factory methods */
	/***********************************************************************************************/
	
	/**
	 * Set up the password object given the cleartext password.
	 * @param password the cleartext password.
	 * @throws CharacterCodingException Thrown if password isn't an ASCII string.
	 * @throws NullPointerException Thrown if password is null.
	 */
	@SuppressWarnings("deprecation")
	public static Password create(CharSequence password) throws NullPointerException, CharacterCodingException {
		return new PasswordImpl(password);
	}
	
	
	/**
	 * Setup the password object with the provided input.
	 * @param password The cleartext password.
	 * @param salt The salt used to hash the password.
	 * @throws NullPointerException Thrown if password or salt is null.
	 * @throws CharacterCodingException Thrown if password is not an ASCII string.
	 */
	@SuppressWarnings("deprecation")
	public static Password create(CharSequence password, byte[] salt) throws NullPointerException, CharacterCodingException {
		return new PasswordImpl(password, salt);
	}
	
	
	/**
	 * Setup the password object with the provided input.
	 * @param input Whether the password has already been hashed or not.
	 * @param password The hashed password.
	 * @param salt The salt used to hash the password.
	 * @throws NullPointerException Thrown if password or salt is null.
	 * @throws CharacterCodingException Thrown if password is not an ASCII string.
	 */
	@SuppressWarnings("deprecation")
	public static Password create(byte[] password, byte[] salt) throws NullPointerException, CharacterCodingException {
		return new PasswordImpl(password, salt);
	}
	
	/**
	 * Creates a new password instance to match the encoded string.
	 * @param encoded A string in the format hash{salt}.
	 * @throws NullPointerException Thrown if encoded is null. 
	 * @throws IllegalArgumentException Thrown if the string is not in the correct format or contains non base64 characters.
	 */
	@SuppressWarnings("deprecation")
	public static Password fromEncodedString(String encoded) throws NullPointerException, IllegalArgumentException {
		Password ret = new PasswordImpl();
		ret.setEncodedString(encoded);
		return ret;
	}
	
	
	/***********************************************************************************************/
	/* Interface Definition */
	/***********************************************************************************************/
	
	/**
	 * @return the passwordHash
	 */
	public abstract byte[] getPasswordHash();

	/**
	 * @return the salt
	 */
	public abstract byte[] getSalt();
	
	
	/**
	 * Setup the password object with the provided input.
	 * @param password The cleartext password.
	 * @throws NullPointerException Thrown if password or salt is null.
	 * @throws CharacterCodingException Thrown if password is not an ASCII string.
	 */
	public abstract void setPassword(CharSequence password) throws NullPointerException, CharacterCodingException;
	
	/**
	 * Setup the password object with the provided input.
	 * @param password The cleartext password.
	 * @param salt The salt used to hash the password.
	 * @throws NullPointerException Thrown if password or salt is null.
	 * @throws CharacterCodingException Thrown if password is not an ASCII string.
	 */
	public abstract void setPassword(CharSequence password, byte[] salt) throws NullPointerException, CharacterCodingException;

	/**
	 * Sets the password to be a random value.
	 * @param bytes The number of bytes to use as a seed (determining length)
	 * @return The new random password (unhashed).
	 * @throws IllegalArgumentException Thrown if bytes <= 0.
	 */
	public abstract String setRandom(int bytes) throws IllegalArgumentException;
	
	
	/**
	 * Gets the password instance encoded as a string.
	 * @return a string of the format hash{salt}.
	 */
	public abstract String getEncodedString();
	
	/**
	 * Sets the password instance to match the encoded string.
	 * @param encoded A string in the format hash{salt}.
	 * @throws NullPointerException Thrown if encoded is null. 
	 * @throws IllegalArgumentException Thrown if the string is not in the correct format or contains non base64 characters.
	 */
	public abstract void setEncodedString(String encoded) throws NullPointerException, IllegalArgumentException;
	
	/**
	 * Tests if the provided cleartext password matches the password that generated the stored hash.
	 * @param password The password to test
	 * @return True if the password matches the hashed value. False otherwise.
	 * @thrown NullPointerException Thrown if password is null.
	 */
	public abstract boolean validate(CharSequence password)
			throws NullPointerException, CharacterCodingException;

}