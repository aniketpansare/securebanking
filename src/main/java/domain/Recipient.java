package main.java.domain;

import java.io.Serializable;

import main.java.domain.impl.RecipientImpl;

public abstract class Recipient implements Serializable {

	private static final long serialVersionUID = 1L;
	
	/***********************************************************************************************/
	/* Static Factory Methods */
	/***********************************************************************************************/
	/**
	 * Initializes a new recipient.
	 * @param user The user creating the recipient entry.
	 * @param name The nickname of the recipient entry.
	 * @param account The account the entry is pointing to.
	 * @throws NullPointerException Thrown if any of the arguments are null.
	 * @throws IllegalArgumentException Thrown if user or account isn't a valid user/account container.
	 */
	public static Recipient create(User user, String name, Account account) 
		throws NullPointerException, IllegalArgumentException {
		return new RecipientImpl(user, name, account);
	}
	
	/***********************************************************************************************/
	/* Interface Specification */
	/***********************************************************************************************/
	
	/**
	 * @return the user
	 */
	public abstract User getUser();

	/**
	 * @return the name
	 */
	public abstract String getName();

	/**
	 * @return the account
	 */
	public abstract Account getAccount();

}