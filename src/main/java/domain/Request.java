package main.java.domain;

import java.io.Serializable;
import java.util.Date;

import main.java.domain.impl.RequestImpl;
import main.java.domain.impl.UserImpl;
import main.java.security.AuthorizationException;

public abstract class Request implements Serializable {
	private static final long serialVersionUID = 1L;

	/***********************************************************************************************/
	/* Types */
	/***********************************************************************************************/
	
	/**
	 * The possible statuses for a request.
	 * @author jbabb
	 */
	public enum Status {
		APPROVED,
		PENDING,
		DENIED,
		WITHDRAWN
	}
	
	
	
	/**
	 * The possible request types.
	 * @author jbabb
	 */
	public enum RequestType {
		ADD_USER,
		DELETE_USER,
		MODIFY_USER,
		TRANSFER_USER
	}
	
	/**
	 * The type of user that made the decision.
	 * @author jbabb
	 */
	public enum DecisionAuthority {
		UNDECIDED,
		ADMIN,
		CORPORATE
	}
	
	/**
	 * Decisions that can be made.
	 * @author jbabb
	 */
	public enum Decision {
		APPROVE,
		DENY
	}
	
	/***********************************************************************************************/
	/* Static Factory Methods */
	/***********************************************************************************************/
	
	/**
	 * Create a new pending request.
	 * @param requestor The user submitting the request.
	 * @param subject The use whose the subject of the request.
	 * @param type The type of action requested.
	 * @param description A description of the request.
	 * @throws NullPointerException Thrown if any of the arguments are null.
	 * @throws IllegalArgumentException Thrown if requestor or subject aren't supported user containers.
	 */
	@SuppressWarnings("deprecation")
	public static Request create(User requestor, User subject, RequestType type, String description) throws NullPointerException, IllegalArgumentException {
		return new RequestImpl(requestor, subject, type, description);
	}
	
	
	
	/***********************************************************************************************/
	/* Interface Methods */
	/***********************************************************************************************/
	/**
	 * Marks a decision for the request.
	 * @param decider The user whose making the decision.
	 * @param decision The decision that's been made.
	 * @throws NullPointerException Thrown if either argument is null.
	 * @throws IllegalArgumentException Thrown if decider isn't a supported user container.
	 * @throws AuthorizationException Thrown if user doesn't have sufficient privileges.
	 */
	public abstract void decide(User decider, Decision decision)
			throws NullPointerException, IllegalArgumentException,
			AuthorizationException;

	/**
	 * Withdraws the request from consideration.
	 * @throws IllegalStateException Thrown if the request is not currently pending.
	 */
	public abstract void withdraw() throws IllegalStateException;

	/**
	 * @return the requestor
	 */
	public abstract UserImpl getRequestor();

	/**
	 * @return the subject
	 */
	public abstract UserImpl getSubject();

	/**
	 * @return the requestType
	 */
	public abstract RequestType getRequestType();

	/**
	 * @return the description
	 */
	public abstract String getDescription();

	/**
	 * @return the requestDate
	 */
	public abstract Date getRequestDate();

	/**
	 * @return the requestStatus
	 */
	public abstract Status getRequestStatus();

	/**
	 * @return the decisionAuthority
	 */
	public abstract DecisionAuthority getDecisionAuthority();

	/**
	 * @return the decidedBy
	 */
	public abstract UserImpl getDecidedBy();

	/**
	 * @return the decisionDate
	 */
	public abstract Date getDecisionDate();

}