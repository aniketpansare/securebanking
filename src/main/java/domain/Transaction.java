package main.java.domain;

import java.util.Date;

import main.java.domain.impl.AccountImpl;
import main.java.domain.impl.TransactionImpl;
import main.java.domain.impl.UserImpl;
import main.java.security.AuthorizationException;

public abstract class Transaction {

	/**********************************************************************************/
	/* Types */
	/**********************************************************************************/
	
	/**
	 * Status flag of the transaction.
	 * @author jbabb
	 *
	 */
	public enum Status {
		PENDING,
		COMPLETE,
		APPROVAL_REQUIRED
	}
	
	/**
	 * Filter flags for working with transactions.
	 * @author jbabb
	 */
	public enum StatusFilter {
		ALL(null, true),
		PENDING(Status.PENDING, false),
		COMPLETE(Status.COMPLETE, false),
		APPROVAL_REQUIRED(Status.APPROVAL_REQUIRED, false),
		NOT_COMPLETE(Status.COMPLETE, true);
		
		public final Status status; 	///< The status corresponding to the filter.
		public final boolean negate;	///< Whether to negate the filter match.
		
		private StatusFilter(Status status, boolean negate) {
			this.status = status;
			this.negate = negate;
		}
	}
	
	/**********************************************************************************/
	/* Static Factory Methods */
	/**********************************************************************************/
	
	/**
	 * constructs a new transaction.
	 * @param from The originating account.
	 * @param to The destination account.
	 * @param amount The amount of the transaction.
	 * @throws NullPointerException Thrown if any argument is null.
	 * @throws IllegalArgumentException Thrown if user, from, or to are not supported user/account container or if amount is <= 0.
	 */
	@SuppressWarnings("deprecation")
	public static Transaction create(User user, Account from, Account to, double amount) throws NullPointerException, IllegalArgumentException {
		return new TransactionImpl(user, from, to, amount);
	}
	
	
	/**********************************************************************************/
	/* Interface Specification */
	/**********************************************************************************/
	
	
	/**
	 * @return the transactionNo
	 */
	public abstract long getTransactionNo();

	/**
	 * @return the user
	 */
	public abstract UserImpl getUser();
	
	/**
	 * @return the fromAccount
	 */
	public abstract AccountImpl getFromAccount();

	/**
	 * @return the toAccountNo
	 */
	public abstract AccountImpl getToAccount();

	/**
	 * @return the transactionAmount
	 */
	public abstract double getTransactionAmount();

	/**
	 * @return the transactionStatus
	 */
	public abstract Status getTransactionStatus();

	/**
	 * @param transactionStatus the transactionStatus to set
	 * @throws NullPointerException Thrown if transactionStatus is null.
	 */
	public abstract void setTransactionStatus(Status transactionStatus)
			throws NullPointerException;

	/**
	 * @return the transactionDate
	 */
	public abstract Date getTransactionDate();

	/**
	 * @param transactionDate the transactionDate to set
	 * @throws NullPointerException Thrown if transaction date is null.
	 */
	public abstract void setTransactionDate(Date transactionDate)
			throws NullPointerException;

}