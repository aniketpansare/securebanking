package main.java.domain;

import java.nio.charset.CharacterCodingException;
import java.util.Collection;
import java.util.Set;

import main.java.domain.impl.RequestImpl;
import main.java.domain.impl.UserImpl;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

public abstract class User implements UserDetails {
	private static final long serialVersionUID = 1L;

	/**********************************************************************************/
	/* Types */
	/**********************************************************************************/

	/**
	 * The user type.
	 * @author jbabb
	 *
	 */
	public enum Type {
		UNVALIDATED,
		CUSTOMER,
		MERCHANT,
		EMPLOYEE,
		ADMIN,
		MANAGER,
		CORPORATE
	};
	
	/**
	 * The level of certificate validation required by the user.
	 * @author jbabb
	 *
	 */
	public enum CertReqLevel {
		NONE,
		REQ_ON_LOGIN,
		REQ_ON_TRANSACTION
	};
	
	
	/**
	 * Filter values for user type.
	 * @author jbabb
	 *
	 */
	public enum TypeFilter {
		ALL(null, true),
		UNVALIDATED(Type.UNVALIDATED, false),
		CUSTOMER(Type.CUSTOMER, false),
		MERCHANT(Type.MERCHANT, false),
		EMPLOYEE(Type.EMPLOYEE, false),
		ADMIN(Type.ADMIN, false),
		MANAGER(Type.MANAGER, false),
		CORPORATE(Type.CORPORATE, false),
		VALIDATED(Type.UNVALIDATED, true);
		
		public final Type type;			///< The corresponding type.
		public final boolean negate;	///< Whether to invert the match.
		
		private TypeFilter(Type type, boolean negate) {
			this.type = type;
			this.negate = negate;
		}
	}
	
	
	/**
	 * The status of the user account.
	 * @author jbabb
	 *
	 */
	public enum Status {
		ACTIVE,
		DISABLED,
		DELETED
	}
	
	/**
	 * Filter values for user status.
	 * @author jbabb
	 */
	public enum StatusFilter {
		ALL(null, true),
		ACTIVE(Status.ACTIVE, false),
		DISABLED(Status.DISABLED, false),
		DELETED(Status.DELETED, false),
		NOT_DELETED(Status.DELETED, true);
		
		public final Status status;	///< The corresponding status
		public final boolean negate;	///< Whether to invert the match.
		
		private StatusFilter(Status status, boolean negate) {
			this.status = status;
			this.negate = negate;
		}
		
	}
	
		
	/**********************************************************************************/
	/* Static Factory Methods */
	/**********************************************************************************/
	
	/**
	 * Initializes a user with the given information
	 * @param username The username used for logging in.
	 * @param usertype The user's class.
	 * @param status The user's account status.
	 * @param password The password used for logging in.
	 * @throws NullPointerException Thrown if any of the arguments are null. 
	 * @throws CharacterCodingException Thrown if password is not an ASCII string.
	 */
	@SuppressWarnings("deprecation")
	public static User create(String username, Type usertype, Status status, String password) throws NullPointerException, CharacterCodingException {
		return new UserImpl(username, usertype, status, password);
	}
	
	/**
	 * Initializes a user with the given information
	 * @param username The username used for logging in.
	 * @param usertype The user's class.
	 * @param status The user's account status.
	 * @param password The password used for logging in.
	 * @param data The users sensitive data container.
	 * @throws NullPointerException Thrown if any of the arguments are null. 
	 * @throws CharacterCodingException Thrown if password is not an ASCII string.
	 * @throws IllegalArgumentException Thrown if data is not a supported container type.
	 */
	@SuppressWarnings("deprecation")
	public static User create(String username, Type usertype, Status status, String password, UserData data) throws NullPointerException, CharacterCodingException, IllegalArgumentException {
		return new UserImpl(username, usertype, status, password, data);
	}
	
	/**********************************************************************************/
	/* Interface Specification */
	/**********************************************************************************/
	
	/**
	 * @return the userId
	 */
	public abstract long getUserId();

	/**
	 * @return the userName
	 */
	@Override
	public abstract String getUsername();

	/**
	 * @return the userType
	 */
	public abstract Type getUserType();

	/**
	 * @param userType the userType to set
	 * @throws NullPointerException Thrown if userType is null.
	 */
	public abstract void setUserType(Type userType) throws NullPointerException;
	
	/**
	 * @return the userType
	 */
	public abstract CertReqLevel getCertAuthenticationLevel();
	
	
	/**
	 * @return the status of the user account.
	 */
	public abstract Status getStatus();

	/**
	 * @param status The new status of the user account.
	 * @throws NullPointerException Thrown if status is null.
	 */
	public abstract void setStatus(Status status);
	
	/**
	 * Ensures that the user is not currently expired.
	 */
	@Override
	public abstract boolean isAccountNonExpired();
	
	/**
	 * Ensures that the user is not currently locked.
	 */
	@Override
	public abstract boolean isAccountNonLocked();
	
	/**
	 * Ensures that the user is currently enabled.
	 */
	@Override
	public abstract boolean isEnabled();
	
	/**
	 * Returns the list of spring security authorities granted to the user.
	 */
	@Override
	public abstract Collection<GrantedAuthority> getAuthorities();
	
	/**
	 * @return the password hash.
	 */
	public abstract Password getPasswordContainer();

	/**
	 * Gets the encoded password for the user.
	 */
	@Override
	public abstract String getPassword();
	
	/**
	 * Ensures that the account password has not expired.
	 */
	@Override
	public abstract boolean isCredentialsNonExpired();
	
	/**
	 * @return the data
	 */
	public abstract UserData getData();

	
	/**
	 * Generates a new (time-sensitive) one-time-password for 
	 * the user and registers it (invalidating previous OTPs).
	 * @return The generated one-time-password token.
	 */
	public abstract String generateOTP();
	
	/**
	 * Attempts to validate the provided one-time-password token.
	 * @param OTP the one-time-password token to attempt to validate.
	 * @return True if the OTP matches the user's OTP AND if it isn't expired.
	 * @throws NullPointerException Thrown if OTP is null.
	 * @throws IllegalArgumentException Thrown if OTP isn't an ASCII string.
	 */
	public abstract boolean validateOTP(String OTP)
		throws NullPointerException, IllegalArgumentException;
	
	
	/**
	 * Invalidates the user's current OTP.
	 */
	public abstract void invalidateOTP();
	
	/**
	 * Loads the user's accounts from the database.
	 * Must be called within a TransactionWrapper.
	 */
	public abstract void loadAccounts();
	
	/**
	 * Checks if the user's accounts have been loaded from the database.
	 */
	public abstract boolean isAccountsLoaded();
	
	/**
	 * @return the user's accounts. Must be already loaded in.
	 */
	public abstract Set<? extends Account> getAccounts();
	
	/**
	 * Filters the user's accounts by their status.
	 * @param filter The status to filter by.
	 * @return An iterable of filtered accounts.
	 * @throws NullPointerException Thrown if filter is null.
	 */
	public abstract Iterable<? extends Account> getFilteredAccounts(Account.StatusFilter filter) throws NullPointerException;
		
	/**
	 * Loads the user's departments from the database.
	 * Must be called within a TransactionWrapper.
	 */
	public abstract void loadDepartments();
	
	/**
	 * Checks if the user's departments have been loaded from the database.
	 */
	public abstract boolean isDepartmentsLoaded();
	
	/**
	 * @return the departments the user belongs to.
	 */
	public abstract Set<? extends Department> getDepartments();
	
	/**
	 * Loads all delegations created by the user from the database.
	 * Must be called within a TransactionWrapper.
	 */
	public abstract void loadOutgoingDelegations();
	
	/**
	 * Checks if the outgoing delegations have been loaded.
	 */
	public abstract boolean isOutgoingDelegationsLoaded();
	
	/**
	 * Gets the set of delegations created by the user.
	 * @return The set of delegations created by the user.
	 * Should have been loaded from the database prior to this call.
	 */
	public abstract Set<? extends Delegation> getOutgoingDelegations();
	
	/**
	 * Loads all delegations provided to the user from the database.
	 * Must be called within a TransactionWrapper.
	 */
	public abstract void loadIncomingDelegations();
	
	/**
	 * Checks if the incoming delegations have been loaded.
	 */
	public abstract boolean isIncomingDelegationsLoaded();
	
	/**
	 * Gets the set of delegations provided to the user.
	 * @return The set of delegations provided to the user.
	 * Should have been loaded from the database prior to this call.
	 */
	public abstract Set<? extends Delegation> getIncomingDelegations();
	
	/**
	 * Loads the user's recipients from the database.
	 * Must be called within a TransactionWrapper.
	 */
	public abstract void loadRecipients();
	
	/**
	 * Determines if the user's recipients have been loaded from the database.
	 */
	public abstract boolean isRecipientsLoaded();
	
	/**
	 * @return the user's recipients.
	 */
	public abstract Set<? extends Recipient> getRecipients();
	
	/**
	 * Filters the user's recipients and returns an iterator for the recipients whose name contains match.
	 * @param match The string to match recipient nicknames against.
	 * @return A filtered iterable of the user's recipients matching match.
	 * @throws NullPointerException thrown if match is null.
	 */
	public abstract Iterable<? extends Recipient> getFilteredRecipients(String match) throws NullPointerException;
	
	/**
	 * Loads the requests generated by the user.
	 * Should be called within a TransactionWrapper.
	 */
	public abstract void loadRequests();
	
	/**
	 * Checks if the requests have been loaded.
	 */
	public abstract boolean isRequestsLoaded();
	
	/**
	 * Get all requests generated by the user.
	 */
	public abstract Iterable<RequestImpl> getRequests();
	
	/**
	 * Gets an iterable of the requests generated by the user matching the provided filter.
	 * @param statusFilter The status to filter by.
	 * @return an iterable of the requests generated by the user matching the provided filter.
	 */
	public abstract Iterable<? extends Request> getFilteredRequests(Request.Status statusFilter);

	
	
	
}

