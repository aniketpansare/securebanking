package main.java.domain;

import javax.crypto.BadPaddingException;

import main.java.domain.impl.UserDataImpl;

/**
 * Safe interface for userdata.
 * @author jbabb
 *
 */
public abstract class UserData {

	/******************************************************************/
	/* Static Factory Methods */
	/******************************************************************/
	/**
	 * Default constructor.
	 */
	@SuppressWarnings("deprecation")
	public static UserData create() {
		return new UserDataImpl();
	}
	
	
	/**
	 * Initializes this object using the provided encrypted data.
	 * @param cipherdata The encrypted container to set the object with.
	 * @throws IllegalArgumentException Thrown if cipherdata is not a base64 encoded data string or if its not an encrypted user data object.
	 * @throws BadPaddingException Thrown if the cipher data is corrupted or our key is wrong.
	 * @throws NullPointerException Thrown if cipherdata is null.
	 */
	@SuppressWarnings("deprecation")
	public static UserData create(byte[] cipherdata) throws IllegalArgumentException, NullPointerException, BadPaddingException {
		return new UserDataImpl(cipherdata);
	}
	
	/**
	 * Full constructor.
	 * @param name		The name of the user.
	 * @param number	The contact phone # for the user.
	 * @param addr		The address of the user.
	 * @param email		The email address of the user.
	 * @param social	The social security number for the user.
	 * @throws NullPointerException Thrown if any of the arguments are null.
	 */
	@SuppressWarnings("deprecation")
	public static UserData create(String name, String number, String addr, String email, String social) {
		return new UserDataImpl(name, number, addr, email, social);
	}
	
	
	/*******************************************************************/
	/* Interface Definition */
	/*******************************************************************/
	
	/**
	 * @return the name
	 */
	public abstract String getName();

	/**
	 * @param name the name to set
	 * @throws NullPointerException Thrown if name is null.
	 */
	public abstract void setName(String name);

	/**
	 * @return the contactNumber
	 */
	public abstract String getContactNumber();

	/**
	 * @param contactNumber the contactNumber to set
	 * @throws NullPointerException Thrown if contactNumber is null.
	 */
	public abstract void setContactNumber(String contactNumber);

	/**
	 * @return the address
	 */
	public abstract String getAddress();

	/**
	 * @param address the address to set
	 * @throws NullPointerException Thrown if address is null.
	 */
	public abstract void setAddress(String address);
	
	/**
	 * @return the address
	 */
	public abstract String getEmail();

	/**
	 * @param address the address to set
	 * @throws NullPointerException Thrown if email is null.
	 */
	public abstract void setEmail(String email);

	/**
	 * @return the ssn
	 */
	public abstract String getSsn();

	/**
	 * @param ssn the ssn to set
	 * @throws NullPointerException Thrown if ssn is null.
	 */
	public abstract void setSsn(String ssn);

}