package main.java.domain.impl;

import java.io.Serializable;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;
import javax.persistence.Transient;

import main.java.domain.Account;
import main.java.domain.BadTransactionException;
import main.java.domain.Delegation;
import main.java.domain.Department;
import main.java.domain.Transaction;
import main.java.domain.User;
import main.java.logging.LoggingManager;
import main.java.logging.LoggingManager.Priority;

import org.hibernate.Hibernate;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.NamedQueries;
import org.hibernate.annotations.NamedQuery;
import org.hibernate.annotations.Parameter;

import com.google.common.collect.Iterables;


/**
 * Account Class is a persistent object to store in database. 
 * @author AP
 *
 */
@NamedQueries({
	@NamedQuery(
		name = AccountImpl.QUERY.BY_ID,
		query = "SELECT acct FROM AccountImpl acct WHERE " + AccountImpl.COLUMN.ID + " = :id"
	),
	@NamedQuery(
		name = AccountImpl.QUERY.ALL,
		query = "SELECT acct FROM AccountImpl acct"
	),
	@NamedQuery(
		name = AccountImpl.QUERY.BY_STATUS,
		query = "SELECT acct FROM AccountImpl acct WHERE "
				+ "(:status IS NULL) OR (" + AccountImpl.COLUMN.STATUS + " = :status)"
	),
	@NamedQuery(
		name =  AccountImpl.QUERY.BY_NOT_STATUS,
		query = "SELECT acct FROM AccountImpl acct WHERE "
				+ "(:status IS NULL) OR NOT (" + AccountImpl.COLUMN.STATUS + " = :status)"
	)
})
@Entity
@Table(name = AccountImpl.TABLE_NAME)
public class AccountImpl extends Account implements Serializable {
	public static final String TABLE_NAME = "ACCOUNT";
	
	
	private static final long serialVersionUID = 1L;
	
	@Transient
	private final LoggingManager log = LoggingManager.create(AccountImpl.class.getName());
	

	/**********************************************************************************/
	/* Types */
	/**********************************************************************************/
	/**
	 * A list of all database queries for this table.
	 * @author jbabb
	 */
	public static class QUERY {
		public static final String BY_ID = 				"ACCOUNT_BY_ID";
		public static final String ALL = 				"ACCOUNT_ALL_ACCOUNTS";
		public static final String BY_STATUS = 			"ACCOUNT_ACCOUNTS_BY_STATUS";
		public static final String BY_NOT_STATUS =		"ACCOUNT_ACCOUNTS_BY_NOT_STATUS";
	}
	
	/**
	 * A list of all database columns for this table.
	 * @author jbabb
	 */
	public static class COLUMN {

		public static final String ID = 				"ID";
		public static final String TYPE = 				"TYPE";
		public static final String USER_OWNER = 		"USER_OWNER";
		public static final String DEPARTMENT_OWNER =	"DEPARTMENT_OWNER";
		public static final String NAME = 				"NAME";
		public static final String BALANCE = 			"BALANCE";
		public static final String DESCRIPTION = 		"DESCRIPTION";
		public static final String STATUS = 			"STATUS";
		public static final String ACTIVATION_DATE = 	"ACTIVATION_DATE";
		public static final String TERMINATION_DATE = 	"TERMINATION_DATE";
		public static final String LAST_UPDATE = 		"LAST_UPDATE";
		
	}
	
	/**********************************************************************************/
	/* Members */
	/**********************************************************************************/
	
	/**
	 * Account #.
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "RandomIdentifierGenerator")
	@GenericGenerator(name ="RandomIdentifierGenerator",
	strategy="main.java.util.RandomIdentifierGenerator",
	parameters= {
			@Parameter(name = "table", value = TABLE_NAME)
	})
	@Column(name = COLUMN.ID, unique = true, nullable = false)
	private long accountNo;

	/**
	 * Account's user-defined name.
	 */
	@Column(name = COLUMN.NAME, nullable = false)
	private String accountName;
	
	/**
	 * Account's balance.
	 */
	@Column(name = COLUMN.BALANCE, nullable = false)
	private double accountBalance;
	
	/**
	 * Account's description
	 */
	@Column(name = COLUMN.DESCRIPTION, nullable = false)
	private String accountDescription;

	/**
	 * The status of the account (whether it is active)
	 */
	@Enumerated(EnumType.STRING)
	@Column(name = COLUMN.STATUS, nullable=false)
	private Status accountStatus;
	
	/**
	 * The date tehe account was activated.
	 */
	@Column(name = COLUMN.ACTIVATION_DATE, nullable = false)
	private Date accountActivationDate;
	
	/**
	 * The date the account was terminated (if applicable)
	 */
	@Column(name = COLUMN.TERMINATION_DATE)
	private Date accountTerminationDate;
	
	/**
	 * The date of the last account change.
	 */
	@Column(name = COLUMN.LAST_UPDATE)
	private Date accountLastUpdateDate;
	
	/**
	 * The user the account belongs to (if applicable).
	 */
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = COLUMN.USER_OWNER)
	private UserImpl accountUserOwner;
	
	/**
	 * The user the account belongs to (if applicable).
	 */
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = COLUMN.DEPARTMENT_OWNER)
	private DepartmentImpl accountDepartmentOwner;
	
	@Enumerated(EnumType.STRING)
	@Column(name = COLUMN.TYPE, nullable = false)
	private Type accountType;
	
	
	/**
	 * The debit transactions associated with the account.
	 */
	@OneToMany(mappedBy = "fromAccount", fetch = FetchType.LAZY)
	@Cascade(value = {CascadeType.ALL})
	@OrderBy(TransactionImpl.COLUMN.DATE + " DESC")
	private List<TransactionImpl> debits;
	
	/**
	 * The credit transactions associated with the account.
	 */
	@OneToMany(mappedBy = "toAccount", fetch = FetchType.LAZY)
	@Cascade(value = {CascadeType.ALL})
	@OrderBy(TransactionImpl.COLUMN.DATE + " DESC")
	private List<TransactionImpl> credits;
	
	/**
	 * The delegation's relating to this account.
	 */
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "account")
	@Cascade(value = {CascadeType.ALL})
	private Set<DelegationImpl> delegations;
	
	/**********************************************************************************/
	/* Constructors */
	/**********************************************************************************/
	
	/**
	 * Default constructor
	 */
	@Deprecated
	public AccountImpl() {
		accountNo = 0L;
		accountName = null;
		accountBalance = 0L;
		accountDescription = null;
		accountStatus = Status.DISABLED;
		accountLastUpdateDate = new Date();
		accountActivationDate = new Date();
		accountTerminationDate = null;
		accountUserOwner = null;
		accountDepartmentOwner = null;
		accountType = Type.PERSONAL;
		debits = new LinkedList<TransactionImpl>();
		credits = new LinkedList<TransactionImpl>();
		delegations = new HashSet<DelegationImpl>(0);
	}
	
	/**
	 * Initialize the account with an owner
	 * @param owner The owner of the account.
	 * @param balance The starting balance for the account.
	 * @throws NullPointerException Thrown if owner is null.
	 * @throws IllegalArgumentException Thrown if owner is an unsupported user container or if balance < 0.
	 */
	@Deprecated
	public AccountImpl(User owner, double balance) throws NullPointerException, IllegalArgumentException {
		
		if (owner == null) {
			NullPointerException e = new NullPointerException();
			log.catching(Priority.WARN, e);
			throw e;
		}
		
		if (balance < 0.0) {
			IllegalArgumentException e = new IllegalArgumentException();
			log.catching(Priority.WARN, e);
			throw e;
		}
		
		accountNo = 0L;
		accountName = null;
		accountBalance = balance;
		accountDescription = null;
		accountStatus = Status.DISABLED;
		accountLastUpdateDate = new Date();
		accountActivationDate = new Date();
		accountTerminationDate = null;
		
		if (owner.getClass() == UserImpl.class) {
			accountUserOwner = (UserImpl)owner;
			accountDepartmentOwner = null;
			accountType = Type.PERSONAL;
		} else {
			IllegalArgumentException e = new IllegalArgumentException();
			log.catching(Priority.WARN, e);
			throw e;
		}
		
		debits = new LinkedList<TransactionImpl>();
		credits = new LinkedList<TransactionImpl>();
		delegations = new HashSet<DelegationImpl>(0);
	}
	
	/**
	 * Initialize the account with an owner
	 * @param owner The owner of the account.
	 * @param balance The starting balance for the account.
	 * @throws NullPointerException Thrown if owner is null.
	 * @throws IllegalArgumentException Thrown if owner is an unsupported user container or if balance < 0.
	 */
	@Deprecated
	public AccountImpl(Department owner, double balance) throws NullPointerException, IllegalArgumentException {
		
		if (owner == null) {
			NullPointerException e = new NullPointerException();
			log.catching(Priority.WARN, e);
			throw e;
		}
		
		if (balance < 0.0) {
			IllegalArgumentException e = new IllegalArgumentException();
			log.catching(Priority.WARN, e);
			throw e;
		}
		
		accountNo = 0L;
		accountName = null;
		accountBalance = balance;
		accountDescription = null;
		accountStatus = Status.DISABLED;
		accountLastUpdateDate = new Date();
		accountActivationDate = new Date();
		accountTerminationDate = null;
		
		if (owner.getClass() == DepartmentImpl.class) {
			accountDepartmentOwner = (DepartmentImpl)owner;
			accountUserOwner = null;
			accountType = Type.DEPARTMENT;
		} else {
			IllegalArgumentException e = new IllegalArgumentException();
			log.catching(Priority.WARN, e);
			throw e;
		}
		
		debits = new LinkedList<TransactionImpl>();
		credits = new LinkedList<TransactionImpl>();
		delegations = new HashSet<DelegationImpl>(0);
	}
	
	/**
	 * Initialize the account with an owner, name, and description.
	 * @param owner The owner of the account.
	 * @param balance The starting balance for the account.
	 * @param name The name of the account.
	 * @param description The description of the account.
	 * @throws NullPointerException Thrown if any of the arguments are null.
	 * @throws IllegalArgumentException Thrown if owner is an unsupported user container or if balance < 0.
	 */
	@Deprecated
	public AccountImpl(User owner, double balance, String name, String description) throws NullPointerException, IllegalArgumentException {
		
		if (owner == null || name == null || description == null) {
			NullPointerException e = new NullPointerException();
			log.catching(Priority.WARN, e);
			throw e;
		}
		
		if (balance < 0.0) {
			IllegalArgumentException e = new IllegalArgumentException();
			log.catching(Priority.WARN, e);
			throw e;
		}
		
		accountNo = 0L;
		accountName = name;
		accountBalance = balance;
		accountDescription = description;
		accountStatus = Status.DISABLED;
		accountLastUpdateDate = new Date();
		accountActivationDate = new Date();
		accountTerminationDate = null;
		
		if (owner.getClass() == UserImpl.class) {
			accountUserOwner = (UserImpl)owner;
			accountDepartmentOwner = null;
			accountType = Type.PERSONAL;
		} else {
			IllegalArgumentException e = new IllegalArgumentException();
			log.catching(Priority.WARN, e);
			throw e;
		}
		
		debits = new LinkedList<TransactionImpl>();
		credits = new LinkedList<TransactionImpl>();
		delegations = new HashSet<DelegationImpl>(0);
	}
	
	/**
	 * Initialize the account with an owner, name, and description.
	 * @param owner The owner of the account.
	 * @param balance The starting balance for the account.
	 * @param name The name of the account.
	 * @param description The description of the account.
	 * @throws NullPointerException Thrown if any of the arguments are null.
	 * @throws IllegalArgumentException Thrown if owner is an unsupported user container or if balance < 0.
	 */
	@Deprecated
	public AccountImpl(Department owner, double balance, String name, String description) throws NullPointerException, IllegalArgumentException {
		
		if (owner == null || name == null || description == null) {
			NullPointerException e = new NullPointerException();
			log.catching(Priority.WARN, e);
			throw e;
		}
		
		if (balance < 0.0) {
			IllegalArgumentException e = new IllegalArgumentException();
			log.catching(Priority.WARN, e);
			throw e;
		}
		
		accountNo = 0L;
		accountName = name;
		accountBalance = balance;
		accountDescription = description;
		accountStatus = Status.DISABLED;
		accountLastUpdateDate = new Date();
		accountActivationDate = new Date();
		accountTerminationDate = null;
		
		if (owner.getClass() == DepartmentImpl.class) {
			accountDepartmentOwner = (DepartmentImpl)owner;
			accountUserOwner = null;
			accountType = Type.DEPARTMENT;
		} else {
			IllegalArgumentException e = new IllegalArgumentException();
			log.catching(Priority.WARN, e);
			throw e;
		}
		
		debits = new LinkedList<TransactionImpl>();
		credits = new LinkedList<TransactionImpl>();
		delegations = new HashSet<DelegationImpl>(0);
	}
	
	/**********************************************************************************/
	/* Interface methods */
	/**********************************************************************************/
	
	/* (non-Javadoc)
	 * @see main.java.domain.Account#getAccountNo()
	 */
	@Override
	public long getAccountNo() {
		return accountNo;
	}

	@Override
	public String getMaskedAccountNo() {
		String num = String.format("%20d", accountNo);
		return num.replaceFirst(".{16}", "****************");
	}
	
	
	/* (non-Javadoc)
	 * @see main.java.domain.Account#getAccountOwner()
	 */
	@Override
	public Object getAccountOwner() {
		return (accountType == Type.PERSONAL) ? accountUserOwner : accountDepartmentOwner;
	}
	
	/* (non-Javadoc)
	 * @see main.java.domain.Account#getAccountName()
	 */
	@Override
	public String getAccountName() {
		return accountName;
	}
	
	/* (non-Javadoc)
	 * @see main.java.domain.Account#setAccountName(java.lang.String)
	 */
	@Override
	public void setAccountName(String accountName) throws NullPointerException {
		if (accountName == null) {
			NullPointerException e = new NullPointerException();
			log.catching(Priority.WARN, e);
			throw e;
		}
		
		if (!accountName.equals(this.accountName)) {
			setAccountLastUpdateDate(new Date());
			this.accountName = accountName;
		}
	}
	
	/* (non-Javadoc)
	 * @see main.java.domain.Account#getAccountBalance()
	 */
	@Override
	public double getAccountBalance() {
		return accountBalance;
	}
	
	/* (non-Javadoc)
	 * @see main.java.domain.Account#getAccountDescription()
	 */
	@Override
	public String getAccountDescription() {
		return accountDescription;
	}



	/* (non-Javadoc)
	 * @see main.java.domain.Account#setAccountDescription(java.lang.String)
	 */
	@Override
	public void setAccountDescription(String accountDescription) throws NullPointerException  {
		if (accountDescription == null) {
			NullPointerException e = new NullPointerException();
			log.catching(Priority.WARN, e);
			throw e;
		}
		
		this.accountDescription = accountDescription;
	}



	/* (non-Javadoc)
	 * @see main.java.domain.Account#getAccountStatus()
	 */
	@Override
	public Status getAccountStatus() {
		return accountStatus;
	}



	/* (non-Javadoc)
	 * @see main.java.domain.Account#setAccountStatus(main.java.domain.impl.AccountImpl.Status)
	 */
	@Override
	public void setAccountStatus(Status accountStatus) {
		if (accountStatus == this.accountStatus) return;
		if (this.accountStatus == Status.DELETED)
			setAccountTerminationDate(null);
		else if (accountStatus == Status.DELETED && accountTerminationDate == null)
			setAccountTerminationDate(new Date());
		
		this.accountStatus = accountStatus;
	}



	/* (non-Javadoc)
	 * @see main.java.domain.Account#getAccountActivationDate()
	 */
	@Override
	public Date getAccountActivationDate() {
		return accountActivationDate;
	}
	
	/* (non-Javadoc)
	 * @see main.java.domain.Account#getAccountTerminationDate()
	 */
	@Override
	public Date getAccountTerminationDate() {
		return accountTerminationDate;
	}
	
	/* (non-Javadoc)
	 * @see main.java.domain.Account#getAccountLastUpdateDate()
	 */
	@Override
	public Date getAccountLastUpdateDate() {
		return accountLastUpdateDate;
	}

	
	/* (non-Javadoc)
	 * @see main.java.domain.Account#loadDebits()
	 */
	@Override
	public void loadDebits() {
		Hibernate.initialize(debits);
	}
	
	@Override
	public boolean isDebitsLoaded() {
		return Hibernate.isInitialized(debits);
	}
	
	/* (non-Javadoc)
	 * @see main.java.domain.Account#getDebits()
	 */
	@Override
	public Iterable<TransactionImpl> getDebits() {
		return Iterables.unmodifiableIterable(debits);
	}
	
	/* (non-Javadoc)
	 * @see main.java.domain.Account#loadCredits()
	 */
	@Override
	public void loadCredits() {
		Hibernate.initialize(credits);
	}
	
	
	@Override
	public boolean isCreditsLoaded() {
		return Hibernate.isInitialized(credits);
	}
	
	/* (non-Javadoc)
	 * @see main.java.domain.Account#getCredits()
	 */
	@Override
	public Iterable<TransactionImpl> getCredits() {
		return Iterables.unmodifiableIterable(credits);
	}
	
	/* (non-Javadoc)
	 * @see main.java.domain.Account#loadTransactions()
	 */
	@Override
	public void loadTransactions() {
		loadCredits();
		loadDebits();
	}
	
	@Override
	public boolean isTransactionsLoaded() {
		return isCreditsLoaded() && isDebitsLoaded();
	}
	
	/* (non-Javadoc)
	 * @see main.java.domain.Account#getTransactions()
	 */
	@Override
	public Iterable<TransactionImpl> getTransactions() {
		return new InterleavingIterable<TransactionImpl>(getCredits(), getDebits(), new Comparator<TransactionImpl>() {
			@Override
			public int compare(TransactionImpl o1, TransactionImpl o2) {
				return o1.getTransactionDate().compareTo(o2.getTransactionDate());
			}
		});
	}
	
	@Override
	public void loadDelegations() {
		Hibernate.initialize(delegations);
		
	}
	
	@Override
	public boolean isDelegationsLoaded() {
		return Hibernate.isInitialized(delegations);
	}

	@Override
	public Set<DelegationImpl> getDelegations() {
		return Collections.unmodifiableSet(delegations);
	}
	
	/**********************************************************************************/
	/* Additional methods */
	/**********************************************************************************/

	/**
	 * Sets the account owner
	 * @param accountOwner The new account owner.
	 */
	public void setAccountOwner(User accountOwner) throws IllegalArgumentException, NullPointerException {
		if (accountOwner == null) {
			NullPointerException e = new NullPointerException();
			log.catching(Priority.WARN, e);
			throw e;
		}
		if (accountOwner.getClass() == UserImpl.class) {
			this.accountUserOwner = (UserImpl)accountOwner;
			this.accountDepartmentOwner = null;
			accountType = Type.PERSONAL;
		} else {
			IllegalArgumentException e = new IllegalArgumentException();
			log.catching(Priority.WARN, e);
			throw e;
		}
	}
	
	/**
	 * Sets the account owner
	 * @param accountOwner The new account owner.
	 */
	public void setAccountOwner(Department accountOwner) throws IllegalArgumentException, NullPointerException {
		if (accountOwner == null) {
			NullPointerException e = new NullPointerException();
			log.catching(Priority.WARN, e);
			throw e;
		}
		if (accountOwner.getClass() == DepartmentImpl.class) {
			this.accountDepartmentOwner = (DepartmentImpl)accountOwner;
			this.accountUserOwner = null;
			accountType = Type.DEPARTMENT;
		} else {
			IllegalArgumentException e = new IllegalArgumentException();
			log.catching(Priority.WARN, e);
			throw e;
		}
	}
	

	@Override
	public Type getAccountType() {
		return accountType;
	}
	
	/**
	 * @param transactions the transactions to set
	 */
	public void setDebits(List<TransactionImpl> debits) {
		this.debits = debits;
	}
	
	/**
	 * @param transactions the transactions to set
	 */
	public void setCredits(List<TransactionImpl> credits) {
		this.credits = credits;
	}
	
	/**
	 * Adds a transaction to the account. Returns the new balance.
	 * @param transaction The transaction to add.
	 * @return The new balance of the account.
	 * @throws NullPointerException Thrown if the transaction is null.
	 * @throws IllegalArgumentException If the transaction isn't a supported transaction container.
	 * @throws BadTransactionException if the transaction isn't related to this account, if the transaction value is <= 0, if the transaction would overdraw the account, or if the account is disabled.
	 */
	public double addTransaction(Transaction transaction) throws IllegalArgumentException, BadTransactionException  {
		if (transaction == null) {
			NullPointerException e = new NullPointerException();
			log.catching(Priority.WARN, e);
			throw e;
		}
		
		if (transaction.getClass() != TransactionImpl.class) {
			IllegalArgumentException e = new IllegalArgumentException();
			log.catching(Priority.WARN, e);
			throw e;
		}
		
		TransactionImpl t = (TransactionImpl)transaction;
		
		if (t.getTransactionAmount() <= 0) {
			BadTransactionException e = new BadTransactionException();
			log.catching(Priority.WARN, e);
			throw e;
		}
		
		if (getAccountStatus() != Account.Status.ACTIVE) {
			BadTransactionException e = new BadTransactionException();
			log.catching(Priority.WARN, e);
			throw e;
		}
		
		
		if (t.getFromAccount().equals(this)) {
			// Debit
			
			// Make sure this won't overdraw the account.
			if (t.getTransactionAmount() > getAccountBalance()) {
				BadTransactionException e = new BadTransactionException();
				log.catching(Priority.WARN, e);
				throw e;
			}
			
			// Update the account
			setAccountLastUpdateDate(new Date());
			if (isDebitsLoaded())
				debits.add(t);
			setAccountBalance(getAccountBalance() - t.getTransactionAmount());
			return getAccountBalance();
			
			
		} else if (t.getToAccount().equals(this)) {
			// Credit
			// Update the account
			setAccountLastUpdateDate(new Date());
			if (isCreditsLoaded())
				credits.add(t);
			setAccountBalance(getAccountBalance() + t.getTransactionAmount());
			return getAccountBalance();
			
		} else {
			// Neither. Bad transaction.
			IllegalArgumentException e = new IllegalArgumentException();
			log.catching(Priority.WARN, e);
			throw e;
		}

	}
	
	/**
	 * @param delegations The new delegation set.
	 */
	public void setDelegations(Set<DelegationImpl> delegations) {
		this.delegations = delegations;
	}
	

	/**
	 * Adds a delegation that is associated with the account.
	 * @param delegation The delegation to add. 
	 * @throws NullPointerException Thrown if delegation is null. 
	 * @throws IllegalArgumentException Thrown if delegation is not a supported delegation container OR if delegation doesn't refer to this account.
	 */
	public void addDelegation(Delegation delegation)
			throws NullPointerException, IllegalArgumentException {
		
		if (delegation == null) {
			NullPointerException e = new NullPointerException();
			log.catching(Priority.WARN, e);
			throw e;
		}
		
		if (delegation.getClass() != DelegationImpl.class || !delegation.getAccount().equals(this)) {
			IllegalArgumentException e = new IllegalArgumentException();
			log.catching(Priority.WARN, e);
			throw e;
		}
		
		if (isDelegationsLoaded())
			this.delegations.add((DelegationImpl)delegation);	
	}

	/**
	 * Removes a delegation that is associated with the account.
	 * @param delegation the delegation to remove.
	 * @throws NullPointerException Thrown if delegation is null.
	 */
	public void removeDelegation(Delegation delegation)
			throws NullPointerException {
		if (delegation == null) {
			NullPointerException e = new NullPointerException();
			log.catching(Priority.WARN, e);
			throw e;
		}
		
		if (isDelegationsLoaded())
			this.delegations.remove((DelegationImpl)delegation);
	}
	
	/**********************************************************************************/
	/* Private Methods */
	/**********************************************************************************/
	
	/**
	 * @param accountBalance the accountBalance to set
	 */
	private void setAccountBalance(double accountBalance) {
		this.accountBalance = accountBalance;
	}

	/**
	 * @param accountActivationDate the accountActivationDate to set
	 */
	private void setAccountActivationDate(Date accountActivationDate) {
		this.accountActivationDate = accountActivationDate;
	}

	/**
	 * @param accountTerminationDate the accountTerminationDate to set
	 */
	private void setAccountTerminationDate(Date accountTerminationDate) {
		this.accountTerminationDate = accountTerminationDate;
	}

	/**
	 * @param accountLastUpdateDate the accountLastUpdateDate to set
	 */
	private void setAccountLastUpdateDate(Date accountLastUpdateDate) {
		this.accountLastUpdateDate = accountLastUpdateDate;
	}
	
	/**********************************************************************************/
	/* Etc */
	/**********************************************************************************/
	
	@Override
	public String toString() {
        StringBuffer buffer = new StringBuffer();
        buffer.append("Account Number: " + accountNo);
        buffer.append("Description: " + accountDescription + ";");
        return buffer.toString();
    }

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (accountNo ^ (accountNo >>> 32));
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AccountImpl other = (AccountImpl) obj;
		if (accountNo != other.accountNo)
			return false;
		return true;
	}

}
