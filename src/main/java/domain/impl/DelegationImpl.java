package main.java.domain.impl;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import main.java.domain.Account;
import main.java.domain.Delegation;
import main.java.domain.User;
import main.java.logging.LoggingManager;
import main.java.logging.LoggingManager.Priority;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;
import org.hibernate.annotations.NamedQueries;
import org.hibernate.annotations.NamedQuery;

/**
 * A privilege delegation between users.
 * @author jbabb
 *
 */
@NamedQueries({
	@NamedQuery(
		name = DelegationImpl.QUERY.BY_ID,
		query = "SELECT d FROM DelegationImpl d WHERE " + DelegationImpl.COLUMN.ID + " = :id"
	)
})
@Entity
@Table(name = DelegationImpl.TABLE_NAME)
public class DelegationImpl extends Delegation {
	public static final String TABLE_NAME = "DELEGATION";
	
	private static final long serialVersionUID = 1L;
	
	@Transient
	private LoggingManager log = LoggingManager.create(DelegationImpl.class.getName());
	
	/************************************************************************/
	/* Types */
	/************************************************************************/

	/**
	 * A list of all database queries for this table.
	 * @author jbabb
	 */
	public static class QUERY {
		public static final String BY_ID = 			"DELEGATION_BY_ID";
	}
	
	/**
	 * A list of all database columns for this table.
	 * @author jbabb
	 */
	static class COLUMN {
		public static final String ID = 			"ID";
		public static final String CREATED_BY = 	"CREATOR";
		public static final String ACCOUNT =		"ACCOUNT";
		public static final String PRIVILEGE = 		"PRIVILEGE";
		public static final String DELEGATED_TO =	"DELEGATED_TO";
	}
	
	/************************************************************************/
	/* Members */
	/************************************************************************/
	/**
	 * The delegation ID#.
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name=COLUMN.ID)
	private long id;
	
	/**
	 * The user which has created the delegation.
	 */
	@ManyToOne(fetch=FetchType.EAGER)
	@Cascade({CascadeType.PERSIST, CascadeType.REFRESH})
	@JoinColumn(name=COLUMN.CREATED_BY, nullable = false)
	private UserImpl createdBy;
	
	/**
	 * The account the delegation partains to.
	 */
	@ManyToOne(fetch=FetchType.EAGER)
	@Cascade({CascadeType.PERSIST, CascadeType.REFRESH})
	@JoinColumn(name=COLUMN.ACCOUNT, nullable = false)
	private AccountImpl account;
	
	/**
	 * The privilege being delegated.
	 */
	@Column(name=COLUMN.PRIVILEGE, nullable = false)
	private PrivilegeLevel privilege;
	
	/**
	 * The user that's being delegated the privileges.
	 */
	@ManyToOne(fetch=FetchType.EAGER)
	@Cascade({CascadeType.PERSIST, CascadeType.REFRESH})
	@JoinColumn(name=COLUMN.DELEGATED_TO, nullable = false)
	private UserImpl delegatedTo;
	
	/************************************************************************/
	/* Constructors */
	/************************************************************************/

	/**
	 * Default constructor.
	 */
	@Deprecated
	public DelegationImpl() {
		// Intentionally left blank
		createdBy = null;
		account = null;
		privilege = PrivilegeLevel.ACCESS;
		delegatedTo = null;
	}
	
	/**
	 * Initializes a new delegation instance.
	 * @param delegator The user whose requested the delegation.
	 * @param account The account whose privileges are being delegated.
	 * @param delegatedPrivileges The privilege level being delegated.
	 * @param delegatee The individual whose being delegated the privileges.
	 * @throws IllegalArgumentException Throw if delegator, account, or delegatee aren't valid user/account containers.
	 * @throws NullPointerException Thrown if any of the arguments are null.
	 */
	@Deprecated
	public DelegationImpl(User delegator, Account account, PrivilegeLevel delegatedPrivileges, User delegatee) 
		throws IllegalArgumentException, NullPointerException {
		if (delegator == null || account == null || delegatedPrivileges == null || delegatee == null) {
			NullPointerException e = new NullPointerException();
			log.catching(Priority.WARN, e);
			throw e;
		}
		
		if (delegator.getClass() != UserImpl.class || account.getClass() != AccountImpl.class || delegatee.getClass() != UserImpl.class) {
			IllegalArgumentException e = new IllegalArgumentException();
			log.catching(Priority.WARN, e);
			throw e;
		}
		
		createdBy = (UserImpl)delegator;
		this.account = (AccountImpl)account;
		this.privilege = delegatedPrivileges;
		this.delegatedTo = (UserImpl)delegatee;
		
	}
	
	/************************************************************************/
	/* Interface Methods */
	/************************************************************************/
	
	@Override
	public long getId() {
		return id;
	}
	
	/* (non-Javadoc)
	 * @see main.java.domain.Delegation#getCreatedBy()
	 */
	@Override
	public UserImpl getCreatedBy() {
		return createdBy;
	}
	
	/* (non-Javadoc)
	 * @see main.java.domain.Delegation#getAccount()
	 */
	@Override
	public AccountImpl getAccount() {
		return account;
	}
	
	/* (non-Javadoc)
	 * @see main.java.domain.Delegation#getPrivilege()
	 */
	@Override
	public PrivilegeLevel getPrivilege() {
		return privilege;
	}
	
	/* (non-Javadoc)
	 * @see main.java.domain.Delegation#getDelegatedTo()
	 */
	@Override
	public UserImpl getDelegatedTo() {
		return delegatedTo;
	}
	
	/************************************************************************/
	/* Internal methods */
	/************************************************************************/
	

	
	/************************************************************************/
	/* Etc */
	/************************************************************************/
	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (id ^ (id >>> 32));
		return result;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DelegationImpl other = (DelegationImpl) obj;
		if (id != other.id)
			return false;
		return true;
	}
	
	
}
