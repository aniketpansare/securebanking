package main.java.domain.impl;

import java.io.Serializable;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import main.java.domain.Account;
import main.java.domain.Department;
import main.java.domain.User;
import main.java.logging.LoggingManager;
import main.java.logging.LoggingManager.Priority;

import org.hibernate.Hibernate;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

import com.google.common.base.Predicate;
import com.google.common.collect.Iterables;

/**
 * A department in the bank.
 * @author jbabb
 *
 */
@NamedQueries({
	@NamedQuery(
		name = DepartmentImpl.QUERY.BY_ID,
		query = "SELECT d FROM DepartmentImpl d WHERE " + DepartmentImpl.COLUMN.ID + " LIKE :id"
	),
	@NamedQuery(
		name = DepartmentImpl.QUERY.ALL,
		query = "SELECT d FROM DepartmentImpl d"
	),
	@NamedQuery(
		name =  DepartmentImpl.QUERY.BY_NAME,
		query = "SELECT d FROM DepartmentImpl d WHERE " + DepartmentImpl.COLUMN.NAME + " LIKE :name"
	)
})
@Entity
@Table(name = DepartmentImpl.TABLE_NAME)
public class DepartmentImpl extends Department implements Serializable {
	public static final String TABLE_NAME = "DEPARTMENT";
	
	private static final long serialVersionUID = 1L;
	
	@Transient
	private LoggingManager log = LoggingManager.create(DepartmentImpl.class.getName());
	
	/**********************************************************************************/
	/* Types */
	/**********************************************************************************/
	
	/**
	 * A list of all database queries for this table.
	 * @author jbabb
	 */
	public static class QUERY {
		public static final String BY_ID = 				"DEPT_BY_ID";
		public static final String ALL = 				"DEPT_ALL_DEPTS";
		public static final String BY_NAME = 			"DEPT_BY_NAME";
	}
	
	/**
	 * A list of all database columns for this table.
	 * @author jbabb
	 */
	public static class COLUMN {

		public static final String ID = 				"ID";
		public static final String NAME = 				"NAME";
		public static final String PARENT = 			"PARENT";
		public static final String MANAGER = 			"MANAGER";
		
	}
	
	/**********************************************************************************/
	/* Members */
	/**********************************************************************************/
	
	
	/**
	 * The department ID key.
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "RandomIdentifierGenerator")
	@GenericGenerator(name ="RandomIdentifierGenerator",
	strategy="main.java.util.RandomIdentifierGenerator",
	parameters= {
			@Parameter(name = "table", value = TABLE_NAME)
	})
	@Column(name = COLUMN.ID, unique = true, nullable = false)
	private Long id;

	/**
	 * The name of the department.
	 */
	@Column(name = COLUMN.NAME, nullable = false, unique = true)
	private String departmentName;
	
	/**
	 * The department's accounts.
	 */
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "accountDepartmentOwner")
	@Cascade({CascadeType.PERSIST, CascadeType.SAVE_UPDATE, CascadeType.REFRESH})
	private  Set<AccountImpl> accounts;
	
	/**
	 * The superior department.
	 */
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = COLUMN.PARENT)
	private DepartmentImpl parent;
	
	/**
	 * The inferior departments 
	 */
	@OneToMany(fetch = FetchType.LAZY, mappedBy="parent")
	@Cascade(value = {CascadeType.ALL})
	private Set<DepartmentImpl> children;
	
	/**
	 * The employees within the department.
	 */
	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(name = "DEPARTMENT_EMPLOYEE", 
		joinColumns = {	@JoinColumn(name = "DEPT_ID", nullable=false) },
		inverseJoinColumns = { @JoinColumn(name = "EMPLOYEE_ID", nullable=false) }
	)
	private Set<UserImpl> employees;

	/**********************************************************************************/
	/* Constructors */
	/**********************************************************************************/
	
	/**
	 * Default constructor.
	 */
	@Deprecated
	public DepartmentImpl() {
		
		id = 0L;
		departmentName = null;
		employees = new HashSet<UserImpl>(0);
		children = new HashSet<DepartmentImpl>(0);
		accounts = new HashSet<AccountImpl>(0);
	}
	
	/**
	 * Initialize with name.
	 * @param name The name for the department.
	 * @throws NullPointerException Thrown if name is null.
	 */
	@Deprecated
	public DepartmentImpl(String name) throws NullPointerException {
		if (name == null) {
			NullPointerException e = new NullPointerException();
			log.catching(Priority.WARN, e);
			throw e;
		}
		
		id = 0L;
		departmentName = name;
		employees = new HashSet<UserImpl>(0);
		children = new HashSet<DepartmentImpl>(0);
		accounts = new HashSet<AccountImpl>(0);
	}
	
	/**********************************************************************************/
	/* Interface Methods */
	/**********************************************************************************/
	
	@Override
	public void loadAccounts() {
		Hibernate.initialize(accounts);
	}
	
	
	@Override
	public boolean isAccountsLoaded() {
		return Hibernate.isInitialized(accounts);
	}
	
	/* (non-Javadoc)
	 * @see main.java.domain.impl.User#getAccounts()
	 */
	@Override
	public Set<AccountImpl> getAccounts() {
		return Collections.unmodifiableSet(accounts);
	}
	
	@Override
	public Iterable<? extends Account> getFilteredAccounts(Account.StatusFilter filter) throws NullPointerException {
		
		if (filter == null) {
			NullPointerException e = new NullPointerException();
			log.catching(Priority.WARN, e);
			throw e;
		}
		
		switch (filter) {
		case ACTIVE:
			return Iterables.unmodifiableIterable(Iterables.filter(getAccounts(), new Predicate<Account>() {
				@Override
				public boolean apply(Account arg0) {
					return ((Account)arg0).getAccountStatus() == Account.Status.ACTIVE;
				}
			}));
			
		case DISABLED:
			return Iterables.unmodifiableIterable(Iterables.filter(getAccounts(), new Predicate<Account>() {
				@Override
				public boolean apply(Account arg0) {
					return ((Account)arg0).getAccountStatus() == Account.Status.DISABLED;
				}
			}));
			
		case DELETED:
			return Iterables.unmodifiableIterable(Iterables.filter(getAccounts(), new Predicate<Account>() {
				@Override
				public boolean apply(Account arg0) {
					return ((Account)arg0).getAccountStatus() == Account.Status.DELETED;
				}
			}));
		case NOT_DELETED:
			return Iterables.unmodifiableIterable(Iterables.filter(getAccounts(), new Predicate<Account>() {
				@Override
				public boolean apply(Account arg0) {
					return ((Account)arg0).getAccountStatus() != Account.Status.DELETED;
				}
			}));
		case ALL:
		default:
			return getAccounts();
		}
	}
	
	/* (non-Javadoc)
	 * @see main.java.domain.impl.Department#getId()
	 */
	@Override
	public Long getId() {
		return id;
	}
	
	/* (non-Javadoc)
	 * @see main.java.domain.impl.Department#getDepartmentName()
	 */
	@Override
	public String getDepartmentName() {
		return departmentName;
	}

	/* (non-Javadoc)
	 * @see main.java.domain.impl.Department#setDepartmentName(java.lang.String)
	 */
	@Override
	public void setDepartmentName(String departmentName) {
		this.departmentName = departmentName;
	}

	/* (non-Javadoc)
	 * @see main.java.domain.impl.Department#getParent()
	 */
	@Override
	public DepartmentImpl getParent() {
		return parent;
	}

	/* (non-Javadoc)
	 * @see main.java.domain.impl.Department#setParent(main.java.domain.impl.Department)
	 */
	@Override
	public void setParent(Department parent) throws IllegalArgumentException {		
	
		if (parent != null && parent.getClass() != DepartmentImpl.class) {
			IllegalArgumentException e = new IllegalArgumentException();
			log.catching(Priority.WARN, e);
			throw e;
		}
		
		if (this.parent != null && this.parent.isChildrenLoaded()) {
			this.parent.children.remove(this);
		}

		this.parent = (DepartmentImpl)parent;
		
	}

	@Override
	public void loadChildren() {
		Hibernate.initialize(children);
		
	}
	
	@Override
	public boolean isChildrenLoaded() {
		return Hibernate.isInitialized(children);
	}
	
	/* (non-Javadoc)
	 * @see main.java.domain.impl.Department#getChildren()
	 */
	@Override
	public Set<DepartmentImpl> getChildren() {
		return Collections.unmodifiableSet(children);
	}

	@Override
	public void loadEmployees() {
		Hibernate.initialize(employees);
	}
	
	@Override
	public boolean isEmployeesLoaded() {
		return Hibernate.isInitialized(employees);
	}

	/* (non-Javadoc)
	 * @see main.java.domain.impl.Department#getEmployees()
	 */
	@Override
	public Set<UserImpl> getEmployees() {
		return Collections.unmodifiableSet(employees);
	}
	
	@Override
	public Iterable<? extends User> getFilteredEmployees(final String username,
			final String name) {
		return Iterables.unmodifiableIterable(Iterables.filter(getEmployees(), new Predicate<UserImpl>() {

			@Override
			public boolean apply(UserImpl arg0) {
				return (username == null || arg0.getUsername().contains(username)) 
						&& (name == null || arg0.getData().getName().contains(name));
			}
			
		}));
	}
	

	@Override
	public void addEmployee(User employee) throws NullPointerException, IllegalArgumentException, IllegalStateException {
		if (employee == null) {
			NullPointerException e = new NullPointerException();
			log.catching(Priority.WARN, e);
			throw e;
		}
		
		if (employee.getClass() != UserImpl.class) {
			IllegalArgumentException e = new IllegalArgumentException();
			log.catching(Priority.WARN, e);
			throw e;
		}
		
		if (!isEmployeesLoaded()) {
			IllegalStateException e = new IllegalStateException();
			log.catching(Priority.WARN, e);
			throw e;
		}
		
		UserImpl emp = (UserImpl)employee;
		
		employees.add(emp);
		
		emp.addDepartment(this);		
	}


	@Override
	public void removeEmployee(User employee) throws NullPointerException, IllegalArgumentException, IllegalStateException {
		if (employee == null) {
			NullPointerException e = new NullPointerException();
			log.catching(Priority.WARN, e);
			throw e;
		}
		
		if (employee.getClass() != UserImpl.class) {
			IllegalArgumentException e = new IllegalArgumentException();
			log.catching(Priority.WARN, e);
			throw e;
		}
		
		if (!isEmployeesLoaded()) {
			IllegalStateException e = new IllegalStateException();
			log.catching(Priority.WARN, e);
			throw e;
		}
		
		UserImpl emp = (UserImpl)employee;
		employees.remove(emp);
		
		emp.removeDepartment(this);
	}
	
	/**********************************************************************************/
	/* Internal Methods */
	/**********************************************************************************/
	
	/**
	 * @param account The account to add to the user.
	 * @throws NullPointerException Thrown if account is null.
	 * @throws IllegalArgumentException Thrown if account isn't a supported account container OR if account isn't owned by this user.
	 */
	public void addAccount(Account account) throws NullPointerException, IllegalArgumentException {
		if (account == null) {
			NullPointerException e = new NullPointerException();
			log.catching(Priority.WARN, e);
			throw e;
		}
		
		if (!this.equals(account.getAccountOwner())) {
			IllegalArgumentException e = new IllegalArgumentException();
			log.catching(Priority.WARN, e);
			throw e;
			
		}
		
		if (account.getClass() != AccountImpl.class) {
			IllegalArgumentException e = new IllegalArgumentException();
			log.catching(Priority.WARN, e);
			throw e;
		}
		
		if (isAccountsLoaded()) {
			accounts.add((AccountImpl)account);
		}
		
	}
	
	/**********************************************************************************/
	/* Other */
	/**********************************************************************************/
	
	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DepartmentImpl other = (DepartmentImpl) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	
}
