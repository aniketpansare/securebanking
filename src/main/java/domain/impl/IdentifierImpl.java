package main.java.domain.impl;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import main.java.domain.Identifier;

@Entity
@Table(name = IdentifierImpl.TABLE_NAME)
public class IdentifierImpl extends Identifier implements Serializable  {
	private static final long serialVersionUID = 1L;

	/**
	 * Associated hibernate table.
	 */
	public static final String TABLE_NAME ="IDENTIFIER";
	
	/**********************************************************************************/
	/* Types */
	/**********************************************************************************/
	
	/**
	 * A list of all database queries for this table.
	 * @author jbabb
	 */
	public static class QUERY {

	}
	
	/**
	 * A list of all database columns for this table.
	 * @author jbabb
	 */
	public static class COLUMN {

		public static final String ID = "ID";
		public static final String IDENTIFIER = "IDENTIFIER";
		public static final String TABLE = "TABLE_NAME";
		
	}
	
	
	/**********************************************************************************/
	/* Members */
	/**********************************************************************************/
	
	/**
	 * The internal table identifier number.
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name=COLUMN.ID)
	private long id;
	
	/**
	 * The identifier that this entry corresponds to.
	 */
	@Column(name=COLUMN.IDENTIFIER)
	private long identifier;
	
	/**
	 * The table the identifier is used in.
	 */
	@Column(name=COLUMN.TABLE)
	private String table;
	
	
	/**********************************************************************************/
	/* Constructors */
	/**********************************************************************************/
	/**
	 * Default constructor.
	 */
	@Deprecated
	public IdentifierImpl() {
		id = 0L;
		identifier = 0L;
		table = null;
	}
	
	/**
	 * Full constructor.
	 * @param identifier The identifier to register.
	 * @param table The table the identifier is associated with.
	 */
	@Deprecated
	public IdentifierImpl(long identifier, String table) {
		id = 0L;
		this.identifier = identifier;
		this.table = table;
	}
	
	/**********************************************************************************/
	/* Interface Methods */
	/**********************************************************************************/
	/* (non-Javadoc)
	 * @see main.java.domain.impl.Identifier#getId()
	 */
	@Override
	public long getIdentifier() {
		return identifier;
	}

	/* (non-Javadoc)
	 * @see main.java.domain.impl.Identifier#getTable()
	 */
	@Override
	public String getTable() {
		return table;
	}
	
	/**********************************************************************************/
	/* Internal Methods */
	/**********************************************************************************/
	
	/**********************************************************************************/
	/* Others */
	/**********************************************************************************/

	

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (id ^ (id >>> 32));
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		IdentifierImpl other = (IdentifierImpl) obj;
		if (id != other.id)
			return false;
		return true;
	}
	
	
	
	
	
}
