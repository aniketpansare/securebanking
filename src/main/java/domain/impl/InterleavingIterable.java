package main.java.domain.impl;

import java.util.Comparator;
import java.util.Iterator;

import com.google.common.collect.AbstractIterator;

class InterleavingIterable<T> implements Iterable<T>
{
	final Iterable<T> first;
	final Iterable<T> second;
	final Comparator<T> comparison;
	
	public InterleavingIterable(Iterable<T> first, Iterable<T> second, Comparator<T> comparison) {
		this.first = first;
		this.second = second;
		this.comparison = comparison;
	}

	@Override
	public Iterator<T> iterator() {
		return new AbstractIterator<T>() {

			final Iterator<T> itfirst = first.iterator();
			final Iterator<T> itsecond = second.iterator();
			
			boolean got_next_first = false;
			boolean got_next_second = false;
			T next_first = null;
			T next_second = null;
			
			
			@Override
			protected T computeNext() {
				
				if (!got_next_first && itfirst.hasNext()) {
					got_next_first = true;
					next_first = itfirst.next();
				}
				
				if (!got_next_second && itsecond.hasNext()) {
					got_next_second = true;
					next_second = itsecond.next();
				}
				
				if (got_next_first && got_next_second) {
					if (comparison.compare(next_first, next_second) >= 0) {
						got_next_first = false;
						return next_first;
					} else {
						got_next_second = false;
						return next_second;
					}
				} else if (got_next_first) {
					got_next_first = false;
					return next_first;
				} else if (got_next_second) {
					got_next_second = false;
					return next_second;
				} else {
					return this.endOfData();
				}
			}
			
		};
	}

}