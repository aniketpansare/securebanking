package main.java.domain.impl;
import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.NamedQueries;
import org.hibernate.annotations.NamedQuery;

import main.java.domain.LogEntry;
import main.java.logging.LoggingManager;



/**
 * Log entry container class.
 * @author jbabb
 *
 */
@NamedQueries({
	@NamedQuery(
			name= LogEntryImpl.QUERY.ALL,
			query="SELECT l FROM LogEntryImpl l "
				+ "ORDER BY " + LogEntryImpl.COLUMN.ID + " DESC"
	),
	@NamedQuery(
			name= LogEntryImpl.QUERY.MULTIFILTER,
			query="SELECT l FROM LogEntryImpl l WHERE ("
				+ "(:priority IS NULL) OR (" 	+ LogEntryImpl.COLUMN.PRIORITY + " LIKE :priority)) AND ("
				+ "(:message IS NULL) OR (" 	+ LogEntryImpl.COLUMN.MESSAGE + " LIKE :message)) AND (" 
				+ "(:logger IS NULL) OR (" 		+ LogEntryImpl.COLUMN.LOGGER + " LIKE :logger)) AND ("
				+ "(:begintime IS NULL) OR (" 	+ LogEntryImpl.COLUMN.TIME + " >= :begintime)) AND ("
				+ "(:endtime IS NULL) OR (" 	+ LogEntryImpl.COLUMN.TIME + " <= :endtime)) "
				+ "ORDER BY " 					+ LogEntryImpl.COLUMN.ID + " DESC"
	)
})
@Entity
@Table(name = LogEntryImpl.TABLE_NAME)
public class LogEntryImpl extends LogEntry implements Serializable {
	public static final String TABLE_NAME = "LOGS";
	private static final long serialVersionUID = 1L;

	
	/**********************************************************************************/
	/* Types */
	/**********************************************************************************/
	/**
	 * A list of all database queries for this table.
	 * @author jbabb
	 */
	public static class QUERY {
		public static final String ALL = 				"LOGENTRY_ALL_ENTRIES";
		public static final String MULTIFILTER =		"LOGENTRY_MULTIFILTER";
	}
	
	/**
	 * A list of all database columns for this table.
	 * @author jbabb
	 */
	public static class COLUMN {

		public static final String ID = 			"ID";
		public static final String LOGGER = 		"LOGGER";
		public static final String PRIORITY = 		"PRIORITY";
		public static final String MESSAGE = 		"MESSAGE";
		public static final String STACKTRACE = 	"STACKTRACE";
		public static final String TIME = 			"EVENT_TIME";
		
	}
	
	/**********************************************************************************/
	/* Members */
	/**********************************************************************************/
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = COLUMN.ID)
	private long id;

	@Column(name = COLUMN.TIME)
	private Date time;

	@Column(name = COLUMN.STACKTRACE, columnDefinition = "TEXT")
	private String stacktrace;

	@Column(name = COLUMN.MESSAGE)
	private String message;

	@Column(name = COLUMN.PRIORITY)
	private String priority;

	@Column(name = COLUMN.LOGGER)
	private String logger;

	
	/**********************************************************************************/
	/* Constructors */
	/**********************************************************************************/
	
	/**
	 * Default constructor. 
	 */
	LogEntryImpl() {
		id = 0;
		logger = "";
		priority = "";
		message = "";
		stacktrace = null;
		time = null;
	}
	
	/**
	 * Partial constructor for an event occurring now.
	 * @param logPriority The logging priority of the event.
	 * @param message The log message.
	 */
	LogEntryImpl(LoggingManager.Priority logPriority, String message) {
		id = 0;
		logger = "";
		priority = logPriority.toString();
		this.message = message;
		stacktrace = null;
		time = null;
	}
	
	/**
	 * Partial constructor
	 * @param logPriority The logging priority of the event.
	 * @param message The log message.
	 * @param time The time the event occurred at.
	 */
	LogEntryImpl(LoggingManager.Priority logPriority, String message, Date time) {
		id = 0;
		logger = "";
		priority = logPriority.toString();
		this.message = message;
		stacktrace = null;
		this.time = time;
	}
	
	/**
	 * Partial constructor for an event occurring right now.
	 * @param logger The logger used to log the event.
	 * @param logPriority The logging priority of the event.
	 * @param message The log message.
	 */
	LogEntryImpl(String logger, LoggingManager.Priority logPriority, String message) {
		this.logger = logger;
		priority = logPriority.toString();
		this.message = message;
		stacktrace = null;
		time = null;
	}
	
	/**
	 * Partial constructor.
	 * @param logger The logger used to log the event.
	 * @param logPriority The logging priority of the event.
	 * @param message The log message.
	 * @param time The time the event occurred at.
	 */
	LogEntryImpl(String logger, LoggingManager.Priority logPriority, String message, Date time) {
		this.logger = logger;
		priority = logPriority.toString();
		this.message = message;
		stacktrace = null;
		this.time = null;
	}
	
	
	/**********************************************************************************/
	/* Interface Methods */
	/**********************************************************************************/



	/* (non-Javadoc)
	 * @see main.java.domain.impl.LogEntry#getLogger()
	 */
	@Override
	public String getLogger() {
		return logger;
	}

	/* (non-Javadoc)
	 * @see main.java.domain.impl.LogEntry#getPriority()
	 */
	@Override
	public String getPriority() {
		return priority;
	}


	/* (non-Javadoc)
	 * @see main.java.domain.impl.LogEntry#getMessage()
	 */
	@Override
	public String getMessage() {
		return message;
	}

	/* (non-Javadoc)
	 * @see main.java.domain.impl.LogEntry#getStacktrace()
	 */
	@Override
	public String getStacktrace() {
		return stacktrace;
	}

	/* (non-Javadoc)
	 * @see main.java.domain.impl.LogEntry#getTime()
	 */
	@Override
	public Date getTime() {
		return time;
	}
	
	/**********************************************************************************/
	/* Internal Methods */
	/**********************************************************************************/

    /**
     * Gets the event's unique ID.
	 * @return the id
	 */
	public long getId() {
		return id;
	}

	
	/**********************************************************************************/
	/* Etc */
	/**********************************************************************************/
	
	public String toString() {
        StringBuffer buffer = new StringBuffer();
        buffer.append(Long.toString(getId()));
        buffer.append(": ");
        buffer.append(getPriority().toString());
        buffer.append("[ ");
        buffer.append(getTime().toString());
        buffer.append(" ]: ");
        buffer.append(getMessage());
        return buffer.toString();
    }

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (id ^ (id >>> 32));
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		LogEntryImpl other = (LogEntryImpl) obj;
		if (id != other.id)
			return false;
		return true;
	}
    
	
	
}
