package main.java.domain.impl;

import java.io.Serializable;
import java.nio.charset.CharacterCodingException;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Transient;

import main.java.domain.Password;
import main.java.logging.LoggingManager;
import main.java.logging.LoggingManager.Priority;
import main.java.security.SecurityManager;



/**
 * Secure Password container object
 * @author jbabb
 */
@Embeddable
@Access(value=AccessType.PROPERTY)
public class PasswordImpl extends Password implements Serializable {

	private static final long serialVersionUID = 1L;
	
	/**
	 * The security manager to be used for creating password hashes.
	 */
	@Transient
	private final SecurityManager security = SecurityManager.get();
	
	@Transient
	private final LoggingManager log = LoggingManager.create(PasswordImpl.class.getName());
	
	/**
	 * The size (in bytes) of the password salt to use.
	 */
	private static final int SALT_SZ = 32;

	
	/**********************************************************************************/
	/* Types */
	/**********************************************************************************/
	
	public static class COLUMN {
		public static final String ENCODED_PASSWORD = "ENCODED_PASSWORD";
	}
	
	/**********************************************************************************/
	/* Members */
	/**********************************************************************************/
	
	/**
	 * A hash of the user's password.
	 */
	@Transient
	private byte[] passwordHash;
	
	/**
	 * The salt used to generate the password.
	 */
	@Transient
	private byte[] salt;
	
	/**********************************************************************************/
	/* Static Factor Methods */
	/**********************************************************************************/
	
	/**********************************************************************************/
	/* Constructors */
	/**********************************************************************************/
	
	/**
	 * Default constructor.
	 */
	@Deprecated
	public PasswordImpl() {
		passwordHash = null;
		salt = null;
	}
	
	/**
	 * Set up the password object given the cleartext password.
	 * @param password the cleartext password.
	 * @throws CharacterCodingException Thrown if password isn't an ASCII string.
	 * @throws NullPointerException Thrown if password is null.
	 */
	@Deprecated
	public PasswordImpl(CharSequence password)  throws NullPointerException, CharacterCodingException {
		setPassword(password);
	}
	
	
	/**
	 * Setup the password object with the provided input.
	 * @param password The cleartext password.
	 * @param salt The salt used to hash the password.
	 * @throws NullPointerException Thrown if password or salt is null.
	 * @throws CharacterCodingException Thrown if password is not an ASCII string.
	 */
	@Deprecated
	public PasswordImpl(CharSequence password, byte[] salt) throws NullPointerException, CharacterCodingException {
		setPassword(password, salt);
	}
	
	
	/**
	 * Setup the password object with the provided input.
	 * @param input Whether the password has already been hashed or not.
	 * @param password The hashed password.
	 * @param salt The salt used to hash the password.
	 * @throws NullPointerException Thrown if password is null.
	 * @throws CharacterCodingException Thrown if password is not an ASCII string.
	 */
	@Deprecated
	public PasswordImpl(byte[] password, byte[] salt) throws NullPointerException, CharacterCodingException {
		if (password == null) {
			NullPointerException e = new NullPointerException();
			log.catching(Priority.WARN, e);
			throw e;
		}
			
		if (salt != null) {
			this.salt = new byte[salt.length];
			System.arraycopy(salt, 0, this.salt, 0, salt.length);
		} else salt = null;
				
		this.passwordHash = new byte[password.length];
		System.arraycopy(password, 0, this.passwordHash, 0, password.length);
	}
	
	
	/**********************************************************************************/
	/* Getters/setters */
	/**********************************************************************************/
	
	/* (non-Javadoc)
	 * @see main.java.domain.Password#setPassword(java.lang.String)
	 */
	@Override
	@Transient
	public void setPassword(CharSequence password)  throws NullPointerException, CharacterCodingException {
		if (password == null) {
			NullPointerException e = new NullPointerException();
			log.catching(Priority.WARN, e);
			throw e;
		}
		
		if (this.salt == null) {
			byte[] salt = new byte[SALT_SZ];
			security.randomBytes(salt);
			this.salt = salt;
		}
		passwordHash = security.hash(security.encodeASCII(password), this.salt);
	}
	
	/* (non-Javadoc)
	 * @see main.java.domain.Password#setPassword(java.lang.String, byte[])
	 */
	@Override
	@Transient
	public void setPassword(CharSequence password, byte[] salt) throws NullPointerException, CharacterCodingException {
		if (password == null) {
			NullPointerException e = new NullPointerException();
			log.catching(Priority.WARN, e);
			throw e;
		}
			
		if (salt != null) {
			this.salt = new byte[salt.length];
			System.arraycopy(salt, 0, this.salt, 0, salt.length);
		} else this.salt = null;
		
		passwordHash = security.hash(security.encodeASCII(password), salt);
	}
	
	@Override
	@Transient
	public String setRandom(int bytes) throws IllegalArgumentException{
		String val;
		val = security.randomString(bytes);
		try {
			setPassword(val);
		} catch (NullPointerException | CharacterCodingException e) {
			// This will never happens
			log.catching(Priority.ERROR, e);
		}
		return val;
	}
	
	
	/* (non-Javadoc)
	 * @see main.java.domain.Password#getPasswordHash()
	 */
	@Override
	@Transient
	public byte[] getPasswordHash() {
		return passwordHash;
	}


	/* (non-Javadoc)
	 * @see main.java.domain.Password#getSalt()
	 */
	@Override
	@Transient
	public byte[] getSalt() {
		return salt;
	}
	
	@Override
	@Column(name = COLUMN.ENCODED_PASSWORD, nullable = false)
	public String getEncodedString() {
		return security.encodeBase64(passwordHash) + "{" + security.encodeBase64(salt) + "}";
	}
	
	@Override
	public void setEncodedString(String encoded) throws NullPointerException, IllegalArgumentException {
		if (encoded == null) {
			NullPointerException e = new NullPointerException();
			log.catching(Priority.WARN, e);
			throw e;
		}
		
		String[] split = encoded.split("[{}]");
		
		if (split.length > 2 || split.length < 1) {
			IllegalArgumentException e = new IllegalArgumentException();
			log.catching(Priority.WARN, e);
			throw e;
		}
		
		byte[] hash = null;
		byte[] salt = null;
		
		hash = security.decodeBase64(split[0]);
		
		if (split.length == 2) {
			salt = security.decodeBase64(split[1]);
		}
		
		passwordHash = hash;
		this.salt = salt;
	}
	
	
	/**********************************************************************************/
	/* Utilities */
	/**********************************************************************************/
	
	/* (non-Javadoc)
	 * @see main.java.domain.Password#validate(java.lang.String)
	 */
	@Override
	@Transient
	public boolean validate(CharSequence password) throws NullPointerException, CharacterCodingException {
		Password other = null;
		try {
			other = new PasswordImpl(password, getSalt());
		} catch (IllegalArgumentException e) {
			// This should never happen.
			log.catching(Priority.ERROR, e);
			return false;
		}
		return this.equals(other);
	}

	/**********************************************************************************/
	/* Other */
	/**********************************************************************************/

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + java.util.Arrays.hashCode(passwordHash);
		result = prime * result + java.util.Arrays.hashCode(salt);
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PasswordImpl other = (PasswordImpl) obj;
		if (!java.util.Arrays.equals(passwordHash, other.passwordHash))
			return false;
		if (!java.util.Arrays.equals(salt, other.salt))
			return false;
		return true;
	}

}
