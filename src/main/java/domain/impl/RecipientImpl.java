package main.java.domain.impl;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import main.java.domain.Account;
import main.java.domain.Recipient;
import main.java.domain.User;
import main.java.logging.LoggingManager;
import main.java.logging.LoggingManager.Priority;

@Entity
@Table(name = RecipientImpl.TABLE_NAME)
public class RecipientImpl extends Recipient  {
	static public final String TABLE_NAME = "RECIPIENT" ;
	private static final long serialVersionUID = 1L;
	
	@Transient
	private final LoggingManager log = LoggingManager.create(RecipientImpl.class.getName());

	/***********************************************************************************************/
	/* Types */
	/***********************************************************************************************/
	/**
	 * A list of all database queries for this table.
	 * @author jbabb
	 */
	public static class QUERY {

	}
	
	/**
	 * A list of all database columns for this table.
	 * @author jbabb
	 */
	public static class COLUMN {
		public static final String ID = "ID";
		public static final String USER = "USER";
		public static final String NAME = "NAME";
		public static final String ACCOUNT = "ACCOUNT";
	}
	
	/***********************************************************************************************/
	/* Members */
	/***********************************************************************************************/
	
	/**
	 * The ID of the recipient entry.
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = COLUMN.ID)
	private long id;
	
	/**
	 * The user that created the recipient entry.
	 */
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = COLUMN.USER, nullable=false)
	private UserImpl user;
	
	/**
	 * The nickname for the recipient entry.
	 */
	@Column(name = COLUMN.NAME, nullable=false)
	private String name;
	
	/**
	 * The account the recipient entry points to.
	 */
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = COLUMN.ACCOUNT, nullable=false)
	private AccountImpl account;
	
	/***********************************************************************************************/
	/* Constructors */
	/***********************************************************************************************/
	/**
	 * Default constructor.
	 */
	@Deprecated
	public RecipientImpl() {
		id = 0L;
		user = null;
		name = null;
		account = null;
	}
	
	/**
	 * Initializes a new recipient.
	 * @param user The user creating the recipient entry.
	 * @param name The nickname of the recipient entry.
	 * @param account The account the entry is pointing to.
	 * @throws NullPointerException Thrown if any of the arguments are null.
	 * @throws IllegalArgumentException Thrown if user or account isn't a valid user/account container.
	 */
	public RecipientImpl(User user, String name, Account account) 
		throws NullPointerException, IllegalArgumentException {

		if (user == null || name == null || account == null) {
			NullPointerException e = new NullPointerException();
			log.catching(Priority.WARN, e);
			throw e;
		}
		
		if (user.getClass() != UserImpl.class || account.getClass() != AccountImpl.class) {
			IllegalArgumentException e = new IllegalArgumentException();
			log.catching(Priority.WARN, e);
			throw e;
		}
		
		this.user = (UserImpl)user;
		this.name = name;
		this.account = (AccountImpl)account;
	}


	 
	/***********************************************************************************************/
	/* Interface methods */
	/***********************************************************************************************/

	/* (non-Javadoc)
	 * @see main.java.domain.Recipient#getUser()
	 */
	@Override
	public UserImpl getUser() {
		return user;
	}

	/* (non-Javadoc)
	 * @see main.java.domain.Recipient#getName()
	 */
	@Override
	public String getName() {
		return name;
	}

	/* (non-Javadoc)
	 * @see main.java.domain.Recipient#getAccount()
	 */
	@Override
	public AccountImpl getAccount() {
		return account;
	}
	
	/***********************************************************************************************/
	/* Internal methods */
	/***********************************************************************************************/
	
	/**
	 * @return the id
	 */
	public long getId() {
		return id;
	}


	/***********************************************************************************************/
	/* Others */
	/***********************************************************************************************/
	
	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (id ^ (id >>> 32));
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		RecipientImpl other = (RecipientImpl) obj;
		if (id != other.id)
			return false;
		return true;
	}
	
	
}
