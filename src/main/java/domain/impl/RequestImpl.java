package main.java.domain.impl;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import main.java.domain.Request;
import main.java.domain.User;
import main.java.logging.LoggingManager;
import main.java.logging.LoggingManager.Priority;
import main.java.security.AuthorizationException;

import org.hibernate.annotations.NamedQueries;
import org.hibernate.annotations.NamedQuery;

/**
 * A request put into the system.
 * @author jbabb
 */
@NamedQueries({
	@NamedQuery(
		name = RequestImpl.QUERY.BY_ID,
		query = "SELECT req FROM RequestImpl req WHERE " + RequestImpl.COLUMN.ID + " = :id"
	),
	@NamedQuery(
		name = RequestImpl.QUERY.ALL,
		query = "SELECT req FROM RequestImpl req"
				+ " ORDER BY " + RequestImpl.COLUMN.ID + " DESC"
	),
	@NamedQuery(
		name = RequestImpl.QUERY.BY_TYPE_STATUS,
		query = "SELECT req FROM RequestImpl req WHERE "
				+ "((:status IS NULL) OR (" + RequestImpl.COLUMN.STATUS + " = :status))"
				+ " AND ((:type IS NULL) OR (" + RequestImpl.COLUMN.REQ_TYPE + " = :type))"
				+ " ORDER BY " + RequestImpl.COLUMN.ID + " DESC"
	)
})
@Entity
@Table(name = RequestImpl.TABLE_NAME)
public class RequestImpl extends Request {
	public static final String TABLE_NAME = "REQUEST";
	private static final long serialVersionUID = 1L;
	
	@Transient
	private final LoggingManager log = LoggingManager.create(RequestImpl.class.getName());
			
	/***********************************************************************************************/
	/* Types */
	/***********************************************************************************************/

	/**
	 * A list of all database queries for this table.
	 * @author jbabb
	 */
	public static class QUERY {
		public static final String BY_ID = 				"REQUEST_BY_ID";
		public static final String ALL = 				"REQUEST_ALL_REQUESTS";
		public static final String BY_TYPE_STATUS = 	"REQUEST_REQUESTS_BY_TYPE_STATUS";
	}
	
	/**
	 * A list of all database columns for this table.
	 * @author jbabb
	 */
	public static class COLUMN {
		public static final String ID = "ID";
		public static final String REQUESTOR = "REQUESTOR";
		public static final String SUBJECT = "SUBJECT";
		public static final String REQ_TYPE = "REQ_TYPE";
		public static final String REQ_DATE = "REQ_DATE";
		public static final String DESCRIPTION = "DESCRIPTION";
		public static final String STATUS = "STATUS";
		public static final String DECISION_AUTHORITY = "DECISION_AUTHORITY";
		public static final String DECISION_BY = "DECISION_BY";
		public static final String DECISION_DATE = "DECISION_DATE";
	}
	
	
	/***********************************************************************************************/
	/* Members */
	/***********************************************************************************************/
	/**
	 * Request ID #
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name=COLUMN.ID)
	private long id;
			
	/**
	 * The user whose submitted the request.
	 */
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name=COLUMN.REQUESTOR, nullable = false)
	private UserImpl requestor;
	
	/**
	 * The user the request is about.
	 */
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name=COLUMN.SUBJECT, nullable = false)
	private UserImpl subject;
	
	/**
	 * The type of action requested.
	 */
	@Enumerated(EnumType.STRING)
	@Column(name=COLUMN.REQ_TYPE, nullable = false)
	private RequestType requestType;
	
	/**
	 * A description of the request.
	 */
	@Column(name = COLUMN.DESCRIPTION, columnDefinition="TEXT", nullable  = false)
	private String description;
	
	/**
	 * The date/time that the request was made on.
	 */
	@Column(name = COLUMN.REQ_DATE, nullable = false)
	private Date requestDate;
	
	/**
	 * The status of this request.
	 */
	@Enumerated(EnumType.STRING)
	@Column(name=COLUMN.STATUS, nullable = false)
	private Status requestStatus;
	
	
	/**
	 * The authority of the user that made the decision.
	 */
	@Enumerated(EnumType.STRING)
	@Column(name=COLUMN.DECISION_AUTHORITY, nullable = false)
	private DecisionAuthority decisionAuthority;
	
	/**
	 * The user who made the decision (or null)
	 */
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name=COLUMN.DECISION_BY)
	private UserImpl decidedBy;
	
	
	/**
	 * The date/time that the decision was made on (or null).
	 */
	@Column(name = COLUMN.DECISION_DATE)
	private Date decisionDate;
			
	/***********************************************************************************************/
	/* Constructors */
	/***********************************************************************************************/
	/**
	 * Default constructor.
	 */
	@Deprecated
	public RequestImpl() {
		id = 0L;
		requestor = null;
		subject = null;
		requestType = null;
		description = null;
		requestDate = new Date();
		requestStatus = Status.PENDING;
		decisionAuthority = DecisionAuthority.UNDECIDED;
		decidedBy = null;
		decisionDate = null;
	}
	
	/**
	 * Create a new pending request.
	 * @param requestor The user submitting the request.
	 * @param subject The use whose the subject of the request.
	 * @param type The type of action requested.
	 * @param description A description of the request.
	 * @throws NullPointerException Thrown if any of the arguments are null.
	 * @throws IllegalArgumentException Thrown if requestor or subject aren't supported user containers.
	 */
	@Deprecated
	public RequestImpl(User requestor, User subject, RequestType type, String description) throws NullPointerException, IllegalArgumentException {
		
		if (requestor == null || subject == null || type == null || description == null) {
			NullPointerException e = new NullPointerException();
			log.catching(Priority.WARN, e);
			throw e;
		}
		
		if (requestor.getClass() != UserImpl.class || subject.getClass() != UserImpl.class) {
			IllegalArgumentException e = new IllegalArgumentException();
			log.catching(Priority.WARN, e);
			throw e;
		}
		
		
		id = 0L;
		this.requestor = (UserImpl)requestor;
		this.subject = (UserImpl)subject;
		requestType = type;
		this.description = description;
		requestDate = new Date();
		requestStatus = Status.PENDING;
		decisionAuthority = DecisionAuthority.UNDECIDED;
		decidedBy = null;
		decisionDate = null;
	}
	
	/***********************************************************************************************/
	/* Interface Methods */
	/***********************************************************************************************/
	/* (non-Javadoc)
	 * @see main.java.domain.Request#decide(main.java.domain.User, main.java.domain.RequestImpl.Decision)
	 */
	@Override
	public void decide(User decider, Decision decision) throws NullPointerException, IllegalArgumentException, AuthorizationException {
		if (decider == null || decision == null) {
			NullPointerException e = new NullPointerException();
			log.catching(Priority.WARN, e);
			throw e;
		}
		
		if (decider.getClass() != UserImpl.class) {
			IllegalArgumentException e = new IllegalArgumentException();
			log.catching(Priority.WARN, e);
			throw e;
		}
		
		switch (decider.getUserType()) {
		case ADMIN:
			decisionAuthority = DecisionAuthority.ADMIN;
			break;
		case CORPORATE:
			decisionAuthority = DecisionAuthority.CORPORATE;
			break;
		default:
			
			AuthorizationException e = new AuthorizationException();
			log.catching(Priority.WARN, e);
			throw e;
		}
		
		
		switch (decision) {
		case APPROVE:
			requestStatus = Status.APPROVED;
			break;
		case DENY:
			requestStatus = Status.DENIED;
			break;
		default:
		
			IllegalArgumentException e = new IllegalArgumentException("Unknown decision");
			log.catching(Priority.WARN, e);
			throw e;
		}
		
		decidedBy = (UserImpl)decider;
		decisionDate = new Date();
	}
	
	/* (non-Javadoc)
	 * @see main.java.domain.Request#withdraw()
	 */
	@Override
	public void withdraw() throws IllegalStateException {
		if (requestStatus != Status.PENDING) {
			IllegalStateException e = new IllegalStateException();
			log.catching(Priority.WARN, e);
			throw e;
		}
		requestStatus = Status.WITHDRAWN;
	}
	
	
	/* (non-Javadoc)
	 * @see main.java.domain.Request#getRequestor()
	 */
	@Override
	public UserImpl getRequestor() {
		return requestor;
	}

	/* (non-Javadoc)
	 * @see main.java.domain.Request#getSubject()
	 */
	@Override
	public UserImpl getSubject() {
		return subject;
	}

	/* (non-Javadoc)
	 * @see main.java.domain.Request#getRequestType()
	 */
	@Override
	public RequestType getRequestType() {
		return requestType;
	}

	/* (non-Javadoc)
	 * @see main.java.domain.Request#getDescription()
	 */
	@Override
	public String getDescription() {
		return description;
	}

	/* (non-Javadoc)
	 * @see main.java.domain.Request#getRequestDate()
	 */
	@Override
	public Date getRequestDate() {
		return requestDate;
	}

	/* (non-Javadoc)
	 * @see main.java.domain.Request#getRequestStatus()
	 */
	@Override
	public Status getRequestStatus() {
		return requestStatus;
	}

	/* (non-Javadoc)
	 * @see main.java.domain.Request#getDecisionAuthority()
	 */
	@Override
	public DecisionAuthority getDecisionAuthority() {
		return decisionAuthority;
	}

	/* (non-Javadoc)
	 * @see main.java.domain.Request#getDecidedBy()
	 */
	@Override
	public UserImpl getDecidedBy() {
		return decidedBy;
	}

	/* (non-Javadoc)
	 * @see main.java.domain.Request#getDecisionDate()
	 */
	@Override
	public Date getDecisionDate() {
		return decisionDate;
	}
	
	/***********************************************************************************************/
	/* Internal Methods */
	/***********************************************************************************************/
	
	/**
	 * @return the id
	 */
	public long getId() {
		return id;
	}
	
	/***********************************************************************************************/
	/* Etc */
	/***********************************************************************************************/
	
	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (id ^ (id >>> 32));
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		RequestImpl other = (RequestImpl) obj;
		if (id != other.id)
			return false;
		return true;
	}
	
}
