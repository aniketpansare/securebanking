package main.java.domain.impl;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import main.java.domain.Account;
import main.java.domain.Transaction;
import main.java.domain.User;
import main.java.logging.LoggingManager;
import main.java.logging.LoggingManager.Priority;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.NamedQueries;
import org.hibernate.annotations.NamedQuery;
import org.hibernate.annotations.Parameter;

@NamedQueries({
	@NamedQuery(
		name = "TRANSACTION_BY_ID",
		query = "SELECT t FROM TransactionImpl t WHERE " + TransactionImpl.COLUMN.ID + " = :id"
	),
	@NamedQuery(
		name = "TRANSACTION_ALL_TRANSACTIONS",
		query = "SELECT t FROM TransactionImpl t"
	),
	@NamedQuery(
		name = "TRANSACTION_TRANSACTIONS_BY_STATUS",
		query = "SELECT t FROM TransactionImpl t WHERE "
				+ "(:status IS NULL) OR (" + TransactionImpl.COLUMN.STATUS + " = :status)"
	),
	@NamedQuery(
		name = "TRANSACTION_TRANSACTIONS_BY_NOT_STATUS",
		query = "SELECT t FROM TransactionImpl t WHERE "
				+ "(:status IS NULL) OR NOT (" + TransactionImpl.COLUMN.STATUS + " = :status)"
	)
})
@Entity
@Table(name = TransactionImpl.TABLE_NAME)
public class TransactionImpl extends Transaction implements Serializable {
	public static final String TABLE_NAME = "TRANSACTION";
	
	private static final long serialVersionUID = 1L;
	
	@Transient
	private final LoggingManager log = LoggingManager.create(TransactionImpl.class.getName());
	
	/**********************************************************************************/
	/* Types */
	/**********************************************************************************/
	
	/**
	 * A list of all database queries for this table.
	 * @author jbabb
	 */
	public static class QUERY {
		public static final String BY_ID = "TRANSACTION_BY_ID";
		public static final String ALL = "TRANSACTION_ALL_TRANSACTIONS";
		public static final String BY_STATUS = "TRANSACTION_TRANSACTIONS_BY_STATUS";
		public static final String BY_NOT_STATUS = "TRANSACTION_TRANSACTIONS_BY_NOT_STATUS";
	}
	
	/**
	 * A list of all database columns for this table.
	 * @author jbabb
	 */
	public static class COLUMN {
		public static final String ID = "ID";
		public static final String USER = "USER";
		public static final String FROM = "FROM_ACCT";
		public static final String AMOUNT = "AMOUNT";
		public static final String TO = "TO_ACCT";
		public static final String STATUS = "STATUS";
		public static final String DATE = "DATE";
		
	}
	
	/**********************************************************************************/
	/* Members */
	/**********************************************************************************/
	/**
	 * The unique transaction identification number.
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "RandomIdentifierGenerator")
	@GenericGenerator(name ="RandomIdentifierGenerator",
	strategy="main.java.util.RandomIdentifierGenerator",
	parameters= {
			@Parameter(name = "table", value = TABLE_NAME)
	})
	@Column(name = COLUMN.ID, unique = true, nullable = false)
	private long transactionNo;
	
	/**
	 * The user that created the transaction.
	 */
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = COLUMN.USER, nullable = false)
	private UserImpl user;
	
	/**
	 * The account the transaction is originating from.
	 */
	@ManyToOne(fetch = FetchType.EAGER)
	@Cascade({CascadeType.PERSIST, CascadeType.SAVE_UPDATE, CascadeType.REFRESH})
	@JoinColumn(name = COLUMN.FROM, nullable = false)
	private AccountImpl fromAccount;
	
	/**
	 * The account the transaction is terminating at.
	 */
	@ManyToOne(fetch = FetchType.EAGER)
	@Cascade({CascadeType.PERSIST, CascadeType.SAVE_UPDATE, CascadeType.REFRESH})
	@JoinColumn(name = COLUMN.TO, nullable = false)
	private AccountImpl toAccount;
	
	/**
	 * The amount of the transaction.
	 */
	@Column(name = COLUMN.AMOUNT, nullable = false)
	private double transactionAmount;
	
	/**
	 * The status of the transaction.
	 */
	@Enumerated(EnumType.STRING)
	@Column(name = COLUMN.STATUS, nullable = false)
	private Status transactionStatus;
	
	/**
	 * The date the transaction was initiated.
	 */
	@Column(name = COLUMN.DATE, nullable = false)
	private Date transactionDate;	
	
	/**********************************************************************************/
	/* Constructors */
	/**********************************************************************************/
	
	/**
	 * Default constructor.
	 */
	@Deprecated
	public TransactionImpl() {
		transactionNo = 0L;
		user = null;
		fromAccount = null;
		toAccount = null;
		transactionAmount = 0.0;
		transactionStatus = Status.PENDING;
		transactionDate = new Date();
	}
	
	/**
	 * Full constructor for a new transaction.
	 * @param user The user that created the transaction.
	 * @param from The originating account.
	 * @param to The destination account.
	 * @param amount The amount of the transaction.
	 * @throws NullPointerException Thrown if any argument is null.
	 * @throws IllegalArgumentException Thrown if user, from , or to are not supported user/account containers OR if amount <= 0.
	 */
	@Deprecated
	public TransactionImpl(User user, Account from, Account to, double amount) throws NullPointerException, IllegalArgumentException {
		if (user == null || from == null || to == null) {
			NullPointerException e = new NullPointerException();
			log.catching(Priority.WARN, e);
			throw e;
		}
		
		if (user.getClass() != UserImpl.class || from.getClass() != AccountImpl.class || to.getClass() != AccountImpl.class) {
			IllegalArgumentException e = new IllegalArgumentException();
			log.catching(Priority.WARN, e);
			throw e;
		}
		
		if (amount <= 0.0) {
			IllegalArgumentException e = new IllegalArgumentException();
			log.catching(Priority.WARN, e);
			throw e;
		}
		
		transactionNo = 0L;
		this.user = (UserImpl)user;
		fromAccount = (AccountImpl)from;
		toAccount = (AccountImpl)to;
		transactionAmount = amount;
		transactionStatus = Status.COMPLETE;
		transactionDate = new Date();
	}

	/**********************************************************************************/
	/* Interface Methods */
	/**********************************************************************************/
	
	/* (non-Javadoc)
	 * @see main.java.domain.impl.Transaction#getTransactionNo()
	 */
	@Override
	public long getTransactionNo() {
		return transactionNo;
	}
	
	/* (non-Javadoc)
	 * @see main.java.domain.impl.Transaction#getUser()
	 */
	@Override
	public UserImpl getUser() {
		return user;
	}
	
	/* (non-Javadoc)
	 * @see main.java.domain.impl.Transaction#getFromAccount()
	 */
	@Override
	public AccountImpl getFromAccount() {
		return fromAccount;
	}
	
	/* (non-Javadoc)
	 * @see main.java.domain.impl.Transaction#getToAccount()
	 */
	@Override
	public AccountImpl getToAccount() {
		return toAccount;
	}
	
	/* (non-Javadoc)
	 * @see main.java.domain.impl.Transaction#getTransactionAmount()
	 */
	@Override
	public double getTransactionAmount() {
		return transactionAmount;
	}
	
	/* (non-Javadoc)
	 * @see main.java.domain.impl.Transaction#getTransactionStatus()
	 */
	@Override
	public Status getTransactionStatus() {
		return transactionStatus;
	}
	
	/* (non-Javadoc)
	 * @see main.java.domain.impl.Transaction#setTransactionStatus(main.java.domain.impl.TransactionImpl.Status)
	 */
	@Override
	public void setTransactionStatus(Status transactionStatus) throws NullPointerException {
		this.transactionStatus = transactionStatus;
	}
	
	/* (non-Javadoc)
	 * @see main.java.domain.impl.Transaction#setTransactionDate(java.util.Date)
	 */
	@Override
	public void setTransactionDate(Date transactionDate) throws NullPointerException {
		this.transactionDate = transactionDate;
	}
	
	/* (non-Javadoc)
	 * @see main.java.domain.impl.Transaction#getTransactionDate()
	 */
	@Override
	public Date getTransactionDate() {
		return transactionDate;
	}
	
	
	/**********************************************************************************/
	/* Internal Methods */
	/**********************************************************************************/

	
	/**********************************************************************************/
	/* Other */
	/**********************************************************************************/
	
	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ (int) (transactionNo ^ (transactionNo >>> 32));
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TransactionImpl other = (TransactionImpl) obj;
		if (transactionNo != other.transactionNo)
			return false;
		return true;
	}

	
}
