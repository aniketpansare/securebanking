package main.java.domain.impl;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.Serializable;
import java.security.InvalidKeyException;

import javax.crypto.BadPaddingException;
import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Transient;

import main.java.domain.UserData;
import main.java.logging.LoggingManager;
import main.java.logging.LoggingManager.Priority;
import main.java.security.SecurityManager;

/**
 * Secure user data container object.
 * @author jbabb
 *
 */
@Embeddable
@Access(value=AccessType.PROPERTY)
public class UserDataImpl extends UserData implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Transient
	private final LoggingManager log = LoggingManager.create(UserDataImpl.class.getName());
	
	@Transient
	private final SecurityManager security = SecurityManager.get();	
			
	/**********************************************************************************/
	/* Types */
	/**********************************************************************************/
	
	public static class COLUMN {
		public static final String DATA = "ENCRYPTED_USER_DATA";
	}
	
	
	/**********************************************************************************/
	/* Members */
	/**********************************************************************************/
	
	/**
	 * The name of the user.
	 */
	private String name;
	
	/**
	 * The user's phone number.
	 */
	private String contactNumber;
	
	/**
	 * The user's address.
	 */
	private String address;
	
	/**
	 * The user's email address.
	 */
	private String email;
	
	/**
	 * The user's social security number.
	 */
	private String ssn;
	
	
	/**********************************************************************************/
	/* Constructors */
	/**********************************************************************************/
	
	/**
	 * Default constructor.
	 */
	@Deprecated
	public UserDataImpl() {
		name = "";
		contactNumber = "";
		address = "";
		email = "";
		ssn = "";
	}
	
	/**
	 * Initializes this object using the provided encrypted data.
	 * @param cipherdata The encrypted container to set the object with.
	 * @throws IllegalArgumentException Thrown if cipherdata is not a base64 encoded data string or if its not an encrypted user data object.
	 * @throws BadPaddingException Thrown if the cipher data is corrupted or our key is wrong.
	 * @throws NullPointerException Thrown if cipherdata is null.
	 */
	@Deprecated
	public UserDataImpl(byte[] cipherdata) throws IllegalArgumentException, NullPointerException, BadPaddingException {
		setEncryptedData(cipherdata);
	}
	
	/**
	 * Full constructor.
	 * @param name		The name of the user.
	 * @param number	The contact phone # for the user.
	 * @param addr		The address of the user.
	 * @param email		The email address of the user.
	 * @param social	The social security number for the user.
	 * @throws NullPointerException Thrown if any of the arguments are null.
	 */
	@Deprecated
	public UserDataImpl(String name, String number, String addr, String email, String social) {
		if (name == null || number == null || addr == null || email == null || social == null) {
			throw new NullPointerException();
		}
		this.name = name;
		contactNumber = number;
		address = addr;
		this.email = email;
		ssn = social;
	}
	
	/**********************************************************************************/
	/* Getters / Setters */
	/**********************************************************************************/

	/* (non-Javadoc)
	 * @see main.java.domain.impl.UserData#getName()
	 */
	@Override
	@Transient
	public String getName() {
		return name;
	}

	/* (non-Javadoc)
	 * @see main.java.domain.impl.UserData#setName(java.lang.String)
	 */
	@Override
	public void setName(String name) throws NullPointerException {
		if (name == null) {
			NullPointerException e = new NullPointerException();
			log.catching(Priority.WARN, e);
			throw e;
		}
		
		this.name = name;
	}

	/* (non-Javadoc)
	 * @see main.java.domain.impl.UserData#getContactNumber()
	 */
	@Override
	@Transient
	public String getContactNumber() {
		return contactNumber;
	}

	/* (non-Javadoc)
	 * @see main.java.domain.impl.UserData#setContactNumber(java.lang.String)
	 */
	@Override
	public void setContactNumber(String contactNumber) throws NullPointerException {
		if (contactNumber == null) {
			NullPointerException e = new NullPointerException();
			log.catching(Priority.WARN, e);
			throw e;
		}
		
		this.contactNumber = contactNumber;
	}

	/* (non-Javadoc)
	 * @see main.java.domain.impl.UserData#getAddress()
	 */
	@Override
	@Transient
	public String getAddress() {
		return address;
	}

	/* (non-Javadoc)
	 * @see main.java.domain.impl.UserData#setAddress(java.lang.String)
	 */
	@Override
	public void setAddress(String address) throws NullPointerException {
		if (address == null) {
			NullPointerException e = new NullPointerException();
			log.catching(Priority.WARN, e);
			throw e;
		}
		
		this.address = address;
	}

	/* (non-Javadoc)
	 * @see main.java.domain.impl.UserData#getEmail()
	 */
	@Override
	@Transient
	public String getEmail() {
		return email;
	}

	/* (non-Javadoc)
	 * @see main.java.domain.impl.UserData#setEmail(java.lang.String)
	 */
	@Override
	public void setEmail(String email) throws NullPointerException {
		if (email == null) {
			NullPointerException e = new NullPointerException();
			log.catching(Priority.WARN, e);
			throw e;
		}
		
		this.email = email;
	}
	
	/* (non-Javadoc)
	 * @see main.java.domain.impl.UserData#getSsn()
	 */
	@Override
	@Transient
	public String getSsn() {
		return ssn;
	}

	/* (non-Javadoc)
	 * @see main.java.domain.impl.UserData#setSsn(java.lang.String)
	 */
	@Override
	public void setSsn(String ssn) throws NullPointerException {
		if (ssn == null) {
			NullPointerException e = new NullPointerException();
			log.catching(Priority.WARN, e);
			throw e;
		}
		
		this.ssn = ssn;
	}
	
	/**********************************************************************************/
	/* Other */
	/**********************************************************************************/
	
	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((ssn == null) ? 0 : ssn.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		UserDataImpl other = (UserDataImpl) obj;
		if (ssn == null) {
			if (other.ssn != null)
				return false;
		} else if (!ssn.equals(other.ssn))
			return false;
		return true;
	}

	
	/**
	 * Get the encrypted container for this object.
	 * @return The encrypted container for this object.
	 */
	@Column(name = COLUMN.DATA, columnDefinition="BLOB", nullable = false)
	public byte[] getEncryptedData() {
		// serialize to byte array
		ByteArrayOutputStream stream = new ByteArrayOutputStream(
				10 + 3 * (name.length() + contactNumber.length() + address.length() + email.length() + ssn.length()));
		DataOutputStream out = new DataOutputStream(stream);
		
		byte[] encryptedData = null;
		try {
			out.writeUTF(name);
			out.writeUTF(contactNumber);
			out.writeUTF(address);
			out.writeUTF(email);
			out.writeUTF(ssn);
			
			byte[] d = stream.toByteArray();
			
			// Encrypt and encode
			encryptedData = security.encrypt(d, security.getPIIKey());
			
		} catch (IOException | InvalidKeyException | NullPointerException e) {
			// This shouldn't happen.
			log.catching(Priority.ERROR, e);
			throw new RuntimeException(e);
		} finally {
			try {
				out.close();
				stream.close();
			} catch (IOException e) {
				log.catching(Priority.WARN, e);
			}
		}

		return encryptedData;
	}
	

	/**
	 * @param cipherdata The encrypted container to set the object with.
	 * @throws IllegalArgumentException Thrown if cipherdata is not an encrypted user data object.
	 * @throws BadPaddingException Thrown if the cipher data is corrupted or our key is wrong.
	 * @throws NullPointerException Thrown if cipherdata is null.
	 */
	public void setEncryptedData(byte[] cipherdata) throws IllegalArgumentException, NullPointerException, BadPaddingException {
		
		try {
			byte[] cleartextdata = security.decrypt(cipherdata, security.getPIIKey());
			ByteArrayInputStream bis = new ByteArrayInputStream(cleartextdata);
			DataInputStream in =  new DataInputStream(bis);
				
			try {
			
				setName(in.readUTF());
				setContactNumber(in.readUTF());
				setAddress(in.readUTF());
				setEmail(in.readUTF());
				setSsn(in.readUTF());
				
			} catch (IOException e) {
				// should never happen
				log.catching(Priority.ERROR, e);
				name = null;
				contactNumber = null;
				address = null;
				email = null;
				ssn = null;
				throw new RuntimeException(e);
			} finally {
				try  {
					in.close();
					bis.close();
				} catch (IOException e) {
					log.catching(Priority.ERROR, e);
				}
			}
				
				
		} catch (InvalidKeyException e) {
			// This should never happen.
			log.catching(Priority.ERROR, e);
			
			name = null;
			contactNumber = null;
			address = null;
			email = null;
			ssn = null;
			
			throw new RuntimeException(e);
		}
		
		
		
		
	}
	
	
}
