package main.java.domain.impl;

import java.io.Serializable;
import java.nio.charset.CharacterCodingException;
import java.security.NoSuchAlgorithmException;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Set;

import javax.persistence.AttributeOverride;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import main.java.domain.Account;
import main.java.domain.Delegation;
import main.java.domain.Department;
import main.java.domain.Password;
import main.java.domain.Recipient;
import main.java.domain.Request;
import main.java.domain.User;
import main.java.domain.UserData;
import main.java.logging.LoggingManager;
import main.java.logging.LoggingManager.Priority;
import main.java.security.SecurityManager;

import org.hibernate.Hibernate;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.NamedQueries;
import org.hibernate.annotations.NamedQuery;
import org.hibernate.annotations.Parameter;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import com.google.common.base.Predicate;
import com.google.common.collect.Iterables;


@NamedQueries({
	@NamedQuery(
	name = UserImpl.QUERY.BY_ID,
	query = "SELECT u FROM UserImpl u WHERE " + UserImpl.COLUMN.ID + " = :id"
	),
	@NamedQuery(
		name = UserImpl.QUERY.BY_USERNAME,
		query = "SELECT u FROM UserImpl u WHERE " + UserImpl.COLUMN.USERNAME + " = :username"
	),
	@NamedQuery(
		name = UserImpl.QUERY.ALL,
		query = "SELECT u FROM UserImpl u"
	),
	@NamedQuery(
		name = UserImpl.QUERY.BY_TYPE_STATUS,
		query = "SELECT u FROM UserImpl u WHERE ("
				+ "(:status IS NULL) OR (" + UserImpl.COLUMN.STATUS + " = :status)) AND ("
				+ "(:type IS NULL) OR (" + UserImpl.COLUMN.TYPE + " = :type))"
	),
	@NamedQuery(
		name = UserImpl.QUERY.BY_NOT_TYPE_STATUS,
		query = "SELECT u FROM UserImpl u WHERE ("
				+ "(:status IS NULL) OR (" + UserImpl.COLUMN.STATUS + " = :status)) AND ("
				+ "(:type IS NULL) OR NOT (" + UserImpl.COLUMN.TYPE + " = :type))"
	),
	@NamedQuery(
		name = UserImpl.QUERY.BY_NOT_TYPE_NOT_STATUS,
		query = "SELECT u FROM UserImpl u WHERE ("
				+ "(:status IS NULL) OR NOT (" + UserImpl.COLUMN.STATUS + " = :status)) AND ("
				+ "(:type IS NULL) OR NOT (" + UserImpl.COLUMN.TYPE + " = :type))"
	),
	@NamedQuery(
			name = UserImpl.QUERY.BY_TYPE_NOT_STATUS,
			query = "SELECT u FROM UserImpl u WHERE ("
					+ "(:status IS NULL) OR NOT (" + UserImpl.COLUMN.STATUS + " = :status)) AND ("
					+ "(:type IS NULL) OR (" + UserImpl.COLUMN.TYPE + " = :type))"
		)
})
@Entity
@Table(name = UserImpl.TABLE_NAME)
public class UserImpl extends User implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * Associated hibernate table.
	 */
	public static final String TABLE_NAME = "USER";
	
	@Transient
	private final LoggingManager logger = LoggingManager.create(UserImpl.class.getName());
	
	@Transient
	private final SecurityManager security = SecurityManager.get();
	
	/**********************************************************************************/
	/* Types */
	/**********************************************************************************/
	/**
	 * A list of all database queries for this table.
	 * @author jbabb
	 */
	public static class QUERY {
		public static final String BY_ID = "USER_BY_ID";
		public static final String BY_USERNAME = "USER_BY_USERNAME";
		public static final String ALL = "USER_ALL_USERS";
		public static final String BY_TYPE_STATUS = "USER_USERS_BY_TYPE_STATUS";
		public static final String BY_NOT_TYPE_STATUS = "USER_USERS_BY_NOT_TYPE_STATUS";
		public static final String BY_TYPE_NOT_STATUS = "USER_USERS_BY_TYPE_NOT_STATUS";
		public static final String BY_NOT_TYPE_NOT_STATUS = "USER_USERS_BY_NOT_TYPE_NOT_STATUS";
	}
	
	/**
	 * A list of all database columns for this table.
	 * @author jbabb
	 */
	public static class COLUMN {
		public static final String ID = "USER_ID";
		public static final String USERNAME = "USERNAME";
		public static final String AUTH_LEVEL = "AUTH_LEVEL";
		public static final String TYPE = "TYPE";
		public static final String STATUS = "STATUS";		
		public static final String DATA = UserDataImpl.COLUMN.DATA;
		public static final String ENCODED_PASSWORD = PasswordImpl.COLUMN.ENCODED_PASSWORD;
		public static final String ENCODED_OTP = "OTP";
		public static final String OTP_EXPIRATION_DATE = "OTP_EXPIRATION";

	}
	
	/**********************************************************************************/
	/* Members */
	/**********************************************************************************/
	
	/**
	 * External user ID.
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "RandomIdentifierGenerator")
	@GenericGenerator(name ="RandomIdentifierGenerator",
	strategy="main.java.util.RandomIdentifierGenerator",
	parameters= {
			@Parameter(name = "table", value = TABLE_NAME)
	})
	@Column(name = COLUMN.ID)
	private long userId;
	
	/**
	 * Username for online system.
	 */
	@Column(name = COLUMN.USERNAME, unique=true, nullable=false)
	private String userName;
	
	/**
	 * The type of user this account corresponds to.
	 */
	@Enumerated(EnumType.STRING)
	@Column(name = COLUMN.TYPE, nullable=false)
	private Type userType;
	
	/**
	 * The status of the account.
	 */
	@Enumerated(EnumType.STRING)
	@Column(name = COLUMN.STATUS, nullable=false)
	private Status status;
	
	/**
	 * hashed password. 
	 */
	@Embedded
	private PasswordImpl password;
	
	/**
	 * Secure user data.
	 */
	@Embedded
	private UserDataImpl data;
	
	/**
	 * One time password.
	 */
	@Embedded
	@AttributeOverride(name = "encodedString", column = @Column(name = COLUMN.ENCODED_OTP, nullable = false))
	private PasswordImpl otp;
	
	/**
	 * Expiration date for the password.
	 */
	@Column(name = COLUMN.OTP_EXPIRATION_DATE, nullable = false)
	private Date otpExpiration;
	
	/**
	 * The user's accounts.
	 */
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "accountUserOwner")
	@Cascade({CascadeType.PERSIST, CascadeType.SAVE_UPDATE, CascadeType.REFRESH})
	private  Set<AccountImpl> accounts;

	/**
	 * The departments the user belongs to.
	 */
	@ManyToMany(fetch = FetchType.LAZY, mappedBy="employees")
	private Set<DepartmentImpl> departments;
	
	/**
	 * The set of all delegations created by the user.
	 */
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "createdBy")
	@Cascade({CascadeType.ALL})
	private Set<DelegationImpl> outgoingDelegations;
	
	/**
	 * The set of all delegations received by the user.
	 */
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "delegatedTo")
	@Cascade({CascadeType.ALL})
	private Set<DelegationImpl> incomingDelegations;
	
	/**
	 * The set of user-defined recipients.
	 */
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "user")
	@Cascade({CascadeType.ALL})
	private Set<RecipientImpl> recipients;
	
	/**
	 * The requests that the user has submitted.
	 */
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "requestor")
	@Cascade({CascadeType.ALL})
	private Set<RequestImpl> requests;
	
	
	/**********************************************************************************/
	/* Constructors */
	/**********************************************************************************/
	

	/**
	 * Default constructor.
	 */
	@Deprecated
	public UserImpl() {
		userId = 0L;
		userName = null;
		password = null;
		userType = Type.CUSTOMER;
		data = null;
		otp = null;
		otpExpiration = new Date(0);
		this.accounts = new HashSet<AccountImpl>(0);
		this.departments = new HashSet<DepartmentImpl>(0);
		this.incomingDelegations = new HashSet<DelegationImpl>(0);
		this.outgoingDelegations = new HashSet<DelegationImpl>(0);
		this.recipients = new HashSet<RecipientImpl>(0);
		this.requests = new HashSet<RequestImpl>(0);
	}

	
	/**
	 * Initializes a user with the given information
	 * @param username The username used for logging in.
	 * @param usertype The user's class.
	 * @param password The password used for logging in.
	 * @throws NullPointerException Thrown if any of the arguments are null. 
	 * @throws CharacterCodingException Thrown if password is not an ASCII string.
	 */
	@Deprecated
	public UserImpl(String username, Type usertype, Status status, String password) throws NullPointerException, CharacterCodingException {
		
		if (username == null || usertype == null || status == null || password == null) {
			NullPointerException e = new NullPointerException();
			logger.catching(Priority.WARN, e);
			throw e;
		}
		
		this.userId = 0L;
		this.userName = username;
		this.userType = usertype;
		this.status = status;
		this.password = new PasswordImpl(password);
		data = new UserDataImpl();
		
		generateOTP();
		otpExpiration = new Date(0);
		this.accounts = new HashSet<AccountImpl>(0);
		this.departments = new HashSet<DepartmentImpl>(0);
		this.incomingDelegations = new HashSet<DelegationImpl>(0);
		this.outgoingDelegations = new HashSet<DelegationImpl>(0);
		this.recipients = new HashSet<RecipientImpl>(0);
		this.requests = new HashSet<RequestImpl>(0);
	}
	
	/**
	 * Initializes a user with the given information
	 * @param username The username used for logging in.
	 * @param usertype The user's class.
	 * @param status The status of the account.
	 * @param password The password used for logging in.
	 * @param data The users sensitive data container.
	 * @throws NullPointerException Thrown if any of the arguments are null. 
	 * @throws CharacterCodingException Thrown if password is not an ASCII string.
	 * @throws IllegalArgumentException Thrown if data is not a supported container type.
	 */
	@Deprecated
	public UserImpl(String username, Type usertype, Status status, String password, UserData data) throws NullPointerException, CharacterCodingException, IllegalArgumentException {
		if (username == null || usertype == null || status == null || password == null || data == null) {
			NullPointerException e = new NullPointerException();
			logger.catching(Priority.WARN, e);
			throw e;
		}
		
		this.userId = 0L;
		this.userName = username;
		this.userType = usertype;
		this.status = status;
		this.password = new PasswordImpl(password);
		if (data.getClass() == UserDataImpl.class)
			this.data = (UserDataImpl)data;
		else {
			IllegalArgumentException e = new IllegalArgumentException();
			logger.catching(Priority.WARN, e);
			throw e;
		}
		generateOTP();
		otpExpiration = new Date(0);
		this.accounts = new HashSet<AccountImpl>(0);
		this.departments = new HashSet<DepartmentImpl>(0);
		this.incomingDelegations = new HashSet<DelegationImpl>(0);
		this.outgoingDelegations = new HashSet<DelegationImpl>(0);
		this.recipients = new HashSet<RecipientImpl>(0);
		this.requests = new HashSet<RequestImpl>(0);
	}
	
	
	/**********************************************************************************/
	/* Interface Methods */
	/**********************************************************************************/

	/* (non-Javadoc)
	 * @see main.java.domain.impl.User#getUserId()
	 */
	@Override
	public long getUserId() {
		return userId;
	}

	/* (non-Javadoc)
	 * @see main.java.domain.impl.User#getUserName()
	 */

	@Override
	public String getUsername() {
		return userName;
	}

	/* (non-Javadoc)
	 * @see main.java.domain.impl.User#getUserType()
	 */
	@Override
	public Type getUserType() {
		return userType;
	}
	
	/* (non-Javadoc)
	 * @see main.java.domain.impl.User#setUserType(main.java.domain.impl.UserImpl.Type)
	 */
	@Override
	public void setUserType(Type userType) throws NullPointerException {
		if (userType == null) {
			NullPointerException e = new NullPointerException();
			logger.catching(Priority.WARN, e);
			throw e;
		}
		this.userType = userType;
	}
	
	@Override
	public CertReqLevel getCertAuthenticationLevel() {
		switch (userType) {
		case ADMIN:
		case CORPORATE:
		case MANAGER:
		case EMPLOYEE:
			return CertReqLevel.REQ_ON_LOGIN;
		case MERCHANT:
			return CertReqLevel.REQ_ON_TRANSACTION;
		case CUSTOMER:
		case UNVALIDATED:
		default:
			return CertReqLevel.NONE;
		}
	}
	
	@Override
	public Status getStatus() {
		return status;
	}

	@Override
	public void setStatus(Status status) throws NullPointerException {
		if (status == null) {
			NullPointerException e = new NullPointerException();
			logger.catching(Priority.WARN, e);
			throw e;
		}
		this.status = status;
	}
	
	/* (non-Javadoc)
	 * @see main.java.domain.impl.User#getPassword()
	 */
	@Override
	public PasswordImpl getPasswordContainer() {
		return password;
	}

	/* (non-Javadoc)
	 * @see main.java.domain.impl.User#getData()
	 */
	@Override
	public UserDataImpl getData() {
		return data;
	}

	@Override
	public String generateOTP() {
		// Generate a new random OTP
		String candidate;
		if (otp == null) {
			try {
				otp = new PasswordImpl("");
			} catch (NullPointerException | CharacterCodingException e) {
				// not going to happen.
				logger.catching(Priority.ERROR, e);
			}
		}
		candidate = otp.setRandom(16);
		
		// The OTP expires in an hour.
		Calendar cal = Calendar.getInstance();
		cal.setTime(new Date());
		cal.add(Calendar.HOUR_OF_DAY, 1);
		otpExpiration = cal.getTime();
		
		return candidate;
	}
	
	@Override
	public boolean validateOTP(String OTP)
		throws NullPointerException, IllegalArgumentException {
		
		if (OTP == null) {
			NullPointerException e = new NullPointerException();
			logger.catching(Priority.WARN, e);
			throw e;
		}
		
		if (otpExpiration == null || otp == null) return false;
		
		Date now = new Date();
		
		// Make sure the OTP hasn't expired.
		if (now.after(otpExpiration)) return false;
		
		
		try {
			return otp.validate(OTP);
		} catch (CharacterCodingException e) {
			IllegalArgumentException e1 = new IllegalArgumentException(e);
			logger.catching(Priority.WARN, e1);
			throw e1;
		}	
	}
	
	@Override
	public void invalidateOTP() {
		generateOTP();
		otpExpiration = new Date(0);
	}
	
	@Override
	public void loadAccounts() {
		Hibernate.initialize(accounts);
	}
	
	
	@Override
	public boolean isAccountsLoaded() {
		return Hibernate.isInitialized(accounts);
	}
	
	/* (non-Javadoc)
	 * @see main.java.domain.impl.User#getAccounts()
	 */
	@Override
	public Set<AccountImpl> getAccounts() {
		return Collections.unmodifiableSet(accounts);
	}
	
	@Override
	public Iterable<? extends Account> getFilteredAccounts(Account.StatusFilter filter) throws NullPointerException {
		
		if (filter == null) {
			NullPointerException e = new NullPointerException();
			logger.catching(Priority.WARN, e);
			throw e;
		}
		
		switch (filter) {
		case ACTIVE:
			return Iterables.unmodifiableIterable(Iterables.filter(getAccounts(), new Predicate<Account>() {
				@Override
				public boolean apply(Account arg0) {
					return ((Account)arg0).getAccountStatus() == Account.Status.ACTIVE;
				}
			}));
			
		case DISABLED:
			return Iterables.unmodifiableIterable(Iterables.filter(getAccounts(), new Predicate<Account>() {
				@Override
				public boolean apply(Account arg0) {
					return ((Account)arg0).getAccountStatus() == Account.Status.DISABLED;
				}
			}));
			
		case DELETED:
			return Iterables.unmodifiableIterable(Iterables.filter(getAccounts(), new Predicate<Account>() {
				@Override
				public boolean apply(Account arg0) {
					return ((Account)arg0).getAccountStatus() == Account.Status.DELETED;
				}
			}));
		case NOT_DELETED:
			return Iterables.unmodifiableIterable(Iterables.filter(getAccounts(), new Predicate<Account>() {
				@Override
				public boolean apply(Account arg0) {
					return ((Account)arg0).getAccountStatus() != Account.Status.DELETED;
				}
			}));
		case ALL:
		default:
			return getAccounts();
		}
	}
	
	@Override
	public void loadDepartments() {
		Hibernate.initialize(departments);
	}
	
	@Override
	public boolean isDepartmentsLoaded() {
		return Hibernate.isInitialized(departments);
	}
	
	
	/* (non-Javadoc)
	 * @see main.java.domain.impl.User#getDepartments()
	 */
	@Override
	public Set<DepartmentImpl> getDepartments() {
		return Collections.unmodifiableSet(departments);
	}
	
	@Override
	public void loadOutgoingDelegations() {
		Hibernate.initialize(outgoingDelegations);		
	}

	@Override
	public boolean isOutgoingDelegationsLoaded() {
		return Hibernate.isInitialized(outgoingDelegations);
	}
	

	@Override
	public Set<DelegationImpl> getOutgoingDelegations() {
		return Collections.unmodifiableSet(outgoingDelegations);
	}
	
	@Override
	public void loadIncomingDelegations() {
		Hibernate.initialize(incomingDelegations);
	}

	@Override
	public boolean isIncomingDelegationsLoaded() {
		return Hibernate.isInitialized(incomingDelegations);
	}

	@Override
	public Set<DelegationImpl> getIncomingDelegations() {
		return Collections.unmodifiableSet(incomingDelegations);
	}
	
	@Override
	public void loadRecipients() {
		Hibernate.initialize(recipients);
	}


	@Override
	public boolean isRecipientsLoaded() {
		return Hibernate.isInitialized(recipients);
	}


	@Override
	public Set<RecipientImpl> getRecipients() {
		return Collections.unmodifiableSet(recipients);
	}


	@Override
	public Iterable<RecipientImpl> getFilteredRecipients(final String match)
			throws NullPointerException {
		if (match == null) {
			NullPointerException e = new NullPointerException();
			logger.catching(Priority.WARN, e);
			throw e;
		}
		return Iterables.unmodifiableIterable(Iterables.filter(recipients, new Predicate<RecipientImpl>() {
			@Override
			public boolean apply(RecipientImpl arg0) {
				return arg0.getName().contains(match);
			}
		}));
	}
	
	@Override
	public void loadRequests() {
		Hibernate.initialize(requests);
	}

	@Override
	public boolean isRequestsLoaded() {
		return Hibernate.isInitialized(requests);
	}
	

	@Override
	public Iterable<RequestImpl> getRequests() {
		return Iterables.unmodifiableIterable(requests);
	}


	@Override
	public Iterable<? extends Request> getFilteredRequests(
			final main.java.domain.Request.Status statusFilter) {
		return Iterables.filter(getRequests(), new Predicate<RequestImpl>() {

			@Override
			public boolean apply(RequestImpl arg0) {
				return arg0.getRequestStatus() == statusFilter;
			}
			
		});
	}

	/**********************************************************************************/
	/* Internal Methods */
	/**********************************************************************************/
	
	
	
	/**
	 * @param account The account to add to the user.
	 * @throws NullPointerException Thrown if account is null.
	 * @throws IllegalArgumentException Thrown if account isn't a supported account container OR if account isn't owned by this user.
	 */
	public void addAccount(Account account) throws NullPointerException, IllegalArgumentException {
		if (account == null) {
			NullPointerException e = new NullPointerException();
			logger.catching(Priority.WARN, e);
			throw e;
		}
		
		if (!this.equals(account.getAccountOwner())) {
			IllegalArgumentException e = new IllegalArgumentException();
			logger.catching(Priority.WARN, e);
			throw e;
			
		}
		
		if (account.getClass() != AccountImpl.class) {
			IllegalArgumentException e = new IllegalArgumentException();
			logger.catching(Priority.WARN, e);
			throw e;
		}
		
		if (isAccountsLoaded()) {
			accounts.add((AccountImpl)account);
		}
		
	}
	
	/**
	 * @param department the department to add to the user.
	 * @throws NullPointerException Thrown if department is null.
	 * @throws IllegalArgumentException Thrown if department isn't a supported department container.
	 */
	@Transient
	public void addDepartment(Department department) throws NullPointerException, IllegalArgumentException {
		if (department == null) {
			NullPointerException e = new NullPointerException();
			logger.catching(Priority.WARN, e);
			throw e;
		}
		
		if (department.getClass() != DepartmentImpl.class) {
			IllegalArgumentException e = new IllegalArgumentException();
			logger.catching(Priority.WARN, e);
			throw e;
		}
		
		if (isDepartmentsLoaded())
			this.departments.add((DepartmentImpl)department);
		
	}
	
	/**
	 * @param department the department to remove from the user.
	 * @throws NullPointerException Thrown if department is null.
	 * @throws IllegalArgumentException Thrown if department is not a supported department container.
	 */
	@Transient
	public void removeDepartment(Department department) throws NullPointerException, IllegalArgumentException {
		if (department == null) {
			NullPointerException e = new NullPointerException();
			logger.catching(Priority.WARN, e);
			throw e;
		}
		
		if (department.getClass() != DepartmentImpl.class) {
			IllegalArgumentException e = new IllegalArgumentException();
			logger.catching(Priority.WARN, e);
			throw e;
		}
		
		if (isDepartmentsLoaded()) {
			this.departments.remove(department);
		}
	}
	
	/**
	 * Adds a delegation created by the user.
	 * @param delegation The delegation to add.
	 * @throws NullPointerException Thrown if delegation is null.
	 * @throws IllegalArgumentException Thrown if delegation isn't a supported delegation container OR if the delegation wasn't created by the user.
	 */
	public void addOutgoingDelegation(Delegation delegation) throws NullPointerException, IllegalArgumentException {
		if (delegation == null) {
			NullPointerException e = new NullPointerException();
			logger.catching(Priority.WARN, e);
			throw e;
		}
		
		if (delegation.getClass() != DelegationImpl.class || !delegation.getCreatedBy().equals(this)) {
			IllegalArgumentException e = new IllegalArgumentException();
			logger.catching(Priority.WARN, e);
			throw e;
		}
		
		if (isOutgoingDelegationsLoaded()) {
			this.outgoingDelegations.add((DelegationImpl)delegation);
		}
	}
	
	/**
	 * Removes a delegation that was created by the user.
	 * @param delegation The delegation to remove.
	 * @throws NullPointerException Thrown if delegation is null.
	 * @throws IllegalArgumentException Thrown if delegation isn't a supported delegation container.
	 */
	public void removeOutgoingDelegation(Delegation delegation) throws NullPointerException, IllegalArgumentException {
		if (delegation == null) {
			NullPointerException e = new NullPointerException();
			logger.catching(Priority.WARN, e);
			throw e;
		}
		
		if (delegation.getClass() != DelegationImpl.class) {
			IllegalArgumentException e = new IllegalArgumentException();
			logger.catching(Priority.WARN, e);
			throw e;
		}
		
		if (isOutgoingDelegationsLoaded()) {
			this.outgoingDelegations.remove((DelegationImpl)delegation);
		}
	}

	/**
	 * Adds a delegation that was provided to the user.
	 * @param delegation The delegation to add.
	 * @throws NullPointerException Thrown if delegation is null.
	 * @throws IllegalArgumentException Thrown if delegation isn't a supported delegation container OR if the delegation isn't for the user.
	 */
	public void addIncomingDelegation(Delegation delegation) throws NullPointerException, IllegalArgumentException {
		if (delegation == null) {
			NullPointerException e = new NullPointerException();
			logger.catching(Priority.WARN, e);
			throw e;
		}
		
		if (delegation.getClass() != DelegationImpl.class || !delegation.getDelegatedTo().equals(this)) {
			IllegalArgumentException e = new IllegalArgumentException();
			logger.catching(Priority.WARN, e);
			throw e;
		}
		
		if (isIncomingDelegationsLoaded()) {
			this.incomingDelegations.add((DelegationImpl)delegation);
		}
	}
	
	/**
	 * Removes a delegation that was created by the user.
	 * @param delegation The delegation to remove.
	 * @throws NullPointerException Thrown if delegation is null.
	 * @throws IllegalArgumentException Thrown if delegation isn't a supported delegation container OR if the delegation isn't for the user.
	 */
	public void removeIncomingDelegation(Delegation delegation) throws NullPointerException, IllegalArgumentException {
		if (delegation == null) {
			NullPointerException e = new NullPointerException();
			logger.catching(Priority.WARN, e);
			throw e;
		}
		
		if (delegation.getClass() != DelegationImpl.class || !delegation.getDelegatedTo().equals(this)) {
			IllegalArgumentException e = new IllegalArgumentException();
			logger.catching(Priority.WARN, e);
			throw e;
		}
		
		if (isIncomingDelegationsLoaded()) {
			this.incomingDelegations.remove((DelegationImpl)delegation);
		}
	}
	
	/**
	 * Adds a user-defined recipient to the user.
	 * @param recipient The recipient to add.
	 * @throws NullPointerException Thrown if delegation is null.
	 * @throws IllegalArgumentException Thrown if recipient isn't a supported recipient container OR if the recipient wasn't created by the user.
	 */
	public void addRecipient(Recipient recipient) throws IllegalArgumentException, NullPointerException {
		if (recipient == null) {
			NullPointerException e = new NullPointerException();
			logger.catching(Priority.WARN, e);
			throw e;
		}
		
		if (recipient.getClass() != RecipientImpl.class || !recipient.getUser().equals(this)) {
			IllegalArgumentException e = new IllegalArgumentException();
			logger.catching(Priority.WARN, e);
			throw e;
		}
		
		if (isRecipientsLoaded()) {
			this.recipients.add((RecipientImpl)recipient);
		}
		
	}
	
	/**
	 * removes a user-defined recipient from the user.
	 * @param recipient The recipient to add.
	 * @throws NullPointerException Thrown if delegation is null.
	 * @throws IllegalArgumentException Thrown if recipient isn't a supported recipient container.
	 */
	public void removeRecipient(Recipient recipient) throws IllegalArgumentException, NullPointerException {
		if (recipient == null) {
			NullPointerException e = new NullPointerException();
			logger.catching(Priority.WARN, e);
			throw e;
		}
		
		if (recipient.getClass() != RecipientImpl.class) {
			IllegalArgumentException e = new IllegalArgumentException();
			logger.catching(Priority.WARN, e);
			throw e;
		}
		
		if (isRecipientsLoaded()) {
			this.recipients.remove((RecipientImpl)recipient);
		}
	}
	
	/**
	 * Adds a request that the user has created.
	 * @param request The request to add.
	 * @throws IllegalArgumentException Thrown if request is not a valid request container OR if it wasn't created by this user.
	 * @throws NullPointerException Thrown if request is null.
	 */
	public void addRequest(Request request) throws IllegalArgumentException, NullPointerException {
		if (request == null) {
			NullPointerException e = new NullPointerException();
			logger.catching(Priority.WARN, e);
			throw e;
		}
		
		if (request.getClass() != RequestImpl.class || !request.getRequestor().equals(this)) {
			IllegalArgumentException e = new IllegalArgumentException();
			logger.catching(Priority.WARN, e);
			throw e;
		}
		
		if (isRequestsLoaded()) {
			requests.add((RequestImpl)request);
		}
		
	}
	
	@Override
	public boolean isAccountNonExpired() {
		return status != Status.DELETED;
	}


	@Override
	public boolean isAccountNonLocked() {
		return true;
	}


	@Override
	public boolean isEnabled() {
		return status == Status.ACTIVE; 
	}


	@Override
	public Collection<GrantedAuthority> getAuthorities() {
		LinkedList<GrantedAuthority> l = new LinkedList<GrantedAuthority>();
		l.add(new SimpleGrantedAuthority(userType.name()));
		return l;
	}


	@Override
	public String getPassword() {
		return getPasswordContainer().getEncodedString();
	}


	@Override
	public boolean isCredentialsNonExpired() {
		return true;
	}
	
	/**********************************************************************************/
	/* Other */
	/**********************************************************************************/
	

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (userId ^ (userId >>> 32));
		return result;
	}


	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		UserImpl other = (UserImpl) obj;
		if (userId != other.userId)
			return false;
		return true;
	}



	
}
