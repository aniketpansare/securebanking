package main.java.input;

import main.java.domain.Recipient;
import main.java.domain.User.Status;
import main.java.domain.User.Type;

public class AddInternalUserForm {
	
			String username = null;
			String password = null;
			String repeatPassword = null;
			String name = null;
			String number = null;
			String addr = null;
			String email = null;
			String social = null;
			String departmentName = null;
			Type userRole=null;
			Status userStatus = null;
			String fromDepartmentName = null;
			String toDepartmentName = null;
			String selectedAccountNo = null;
			String fromAccountNo = null;
			String toAccountNo = null;
			String amount = null;
			String creditDebitOption = null;
			String recipientName = null;
			Recipient recipient = null;
			String requestId = null;
			String privilegeLevel = null;
			String description = null;
			String tokenOTP = null;
			
			/**
			 * @return the username
			 */
			public String getUsername() {
				return username;
			}
			/**
			 * @param username the username to set
			 */
			public void setUsername(String username) {
				this.username = username;
			}
			/**
			 * @return the password
			 */
			public String getPassword() {
				return password;
			}
			/**
			 * @param password the password to set
			 */
			public void setPassword(String password) {
				this.password = password;
			}
			/**
			 * @return the repeatPassword
			 */
			public String getRepeatPassword() {
				return repeatPassword;
			}
			/**
			 * @param repeatPassword the repeatPassword to set
			 */
			public void setRepeatPassword(String repeatPassword) {
				this.repeatPassword = repeatPassword;
			}
			/**
			 * @return the name
			 */
			public String getName() {
				return name;
			}
			/**
			 * @param name the name to set
			 */
			public void setName(String name) {
				this.name = name;
			}
			/**
			 * @return the number
			 */
			public String getNumber() {
				return number;
			}
			/**
			 * @param number the number to set
			 */
			public void setNumber(String number) {
				this.number = number;
			}
			/**
			 * @return the addr
			 */
			public String getAddr() {
				return addr;
			}
			
			/**
			 * @param addr the addr to set
			 */
			public void setAddr(String addr) {
				this.addr = addr;
			}
			/**
			 * @return the email
			 */
			public String getEmail() {
				return email;
			}
			
			/**
			 * @param addr the addr to set
			 */
			public void setEmail(String email) {
				this.email = email;
			}
			/**
			 * @return the social
			 */
			public String getSocial() {
				return social;
			}
			/**
			 * @param social the social to set
			 */
			public void setSocial(String social) {
				this.social = social;
			}
			/**
			 * @return the departmentName
			 */
			public String getDepartmentName() {
				return departmentName;
			}
			/**
			 * @param departmentName the departmentName to set
			 */
			public void setDepartmentName(String departmentName) {
				this.departmentName = departmentName;
			}
			/**
			 * @return the userRole
			 */
			public Type getUserRole() {
				return userRole;
			}
			/**
			 * @param userRole the userRole to set
			 */
			public void setUserRole(Type userRole) {
				this.userRole = userRole;
			}
			
			/**
			 * @return the userStatus
			 */
			public Status getUserStatus() {
				return userStatus;
			}
			/**
			 * @param userStatus the userStatus to set
			 */
			public void setUserStatus(Status userStatus) {
				this.userStatus = userStatus;
			}
			/**
			 * @return the fromDepartmentName
			 */
			public String getFromDepartmentName() {
				return fromDepartmentName;
			}
			/**
			 * @param fromDepartmentName the fromDepartmentName to set
			 */
			public void setFromDepartmentName(String fromDepartmentName) {
				this.fromDepartmentName = fromDepartmentName;
			}
			/**
			 * @return the toDepartmentName
			 */
			public String getToDepartmentName() {
				return toDepartmentName;
			}
			/**
			 * @param toDepartmentName the toDepartmentName to set
			 */
			public void setToDepartmentName(String toDepartmentName) {
				this.toDepartmentName = toDepartmentName;
			}
			/**
			 * @return the selectedAccountNo
			 */
			public String getSelectedAccountNo() {
				return selectedAccountNo;
			}
			/**
			 * @param selectedAccountNo the selectedAccountNo to set
			 */
			public void setSelectedAccountNo(String selectedAccountNo) {
				this.selectedAccountNo = selectedAccountNo;
			}
			/**
			 * @return the fromAccountNo
			 */
			public String getFromAccountNo() {
				return fromAccountNo;
			}
			/**
			 * @param fromAccountNo the fromAccountNo to set
			 */
			public void setFromAccountNo(String fromAccountNo) {
				this.fromAccountNo = fromAccountNo;
			}
			/**
			 * @return the toAccountNo
			 */
			public String getToAccountNo() {
				return toAccountNo;
			}
			/**
			 * @param toAccountNo the toAccountNo to set
			 */
			public void setToAccountNo(String toAccountNo) {
				this.toAccountNo = toAccountNo;
			}
			/**
			 * @return the amount
			 */
			public String getAmount() {
				return amount;
			}
			/**
			 * @param amount the amount to set
			 */
			public void setAmount(String amount) {
				this.amount = amount;
			}
			/**
			 * @return the creditDebitOption
			 */
			public String getCreditDebitOption() {
				return creditDebitOption;
			}
			/**
			 * @param creditDebitOption the creditDebitOption to set
			 */
			public void setCreditDebitOption(String creditDebitOption) {
				this.creditDebitOption = creditDebitOption;
			}
			/**
			 * @return the recipientName
			 */
			public String getRecipientName() {
				return recipientName;
			}
			/**
			 * @param recipientName the recipientName to set
			 */
			public void setRecipientName(String recipientName) {
				this.recipientName = recipientName;
			}
			/**
			 * @return the recipient
			 */
			public Recipient getRecipient() {
				return recipient;
			}
			/**
			 * @param recipient the recipient to set
			 */
			public void setRecipient(Recipient recipient) {
				this.recipient = recipient;
			}
			/**
			 * @return the requestId
			 */
			public String getRequestId() {
				return requestId;
			}
			/**
			 * @param requestId the requestId to set
			 */
			public void setRequestId(String requestId) {
				this.requestId = requestId;
			}
			/**
			 * @return the privilegeLevel
			 */
			public String getPrivilegeLevel() {
				return privilegeLevel;
			}
			/**
			 * @param privilegeLevel the privilegeLevel to set
			 */
			public void setPrivilegeLevel(String privilegeLevel) {
				this.privilegeLevel = privilegeLevel;
			}
			/**
			 * @return the description
			 */
			public String getDescription() {
				return description;
			}
			/**
			 * @param description the description to set
			 */
			public void setDescription(String description) {
				this.description = description;
			}
			/**
			 * @return the tokenOTP
			 */
			public String getTokenOTP() {
				return tokenOTP;
			}
			/**
			 * @param tokenOTP the tokenOTP to set
			 */
			public void setTokenOTP(String tokenOTP) {
				this.tokenOTP = tokenOTP;
			}


}
