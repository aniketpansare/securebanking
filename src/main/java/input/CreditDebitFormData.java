package main.java.input;

public class CreditDebitFormData {
	private double amount;
	private String cd_option; 
	private String selectedAccountNo;

	public String getSelectedAccountNo() {
		return selectedAccountNo;
	}

	public void setSelectedAccountNo(String selectedAccountNo) {
		this.selectedAccountNo = selectedAccountNo;
	}
		
	public double getAmount() {
			return amount;
		}

	public void setAmount(double amt) {
		this.amount = amt;
	}
	public String getCd_option() {
		return cd_option;
	}

	public void setCd_option(String cd_option) {
		this.cd_option = cd_option;
	}
}
