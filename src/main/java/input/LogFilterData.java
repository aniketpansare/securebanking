package main.java.input;

import java.util.Date;
import java.util.List;

import main.java.domain.LogEntry;
import main.java.domain.impl.LogEntryImpl;

/**
 * Input data used to filter logs.
 * @author jbabb
 *
 */
public class LogFilterData {
	
	/**************************************************************************/
	/* Members */
	/**************************************************************************/
	
	private String priorityFilter;
	private String messageFilter;
	private String loggerFilter;
	private String beginTime;
	private String endTime;
	
	private List<? extends LogEntry> entries;
	
	/**************************************************************************/
	/* Methods */
	/**************************************************************************/
	
	/**
	 * @return the priorityFilter
	 */
	public String getPriorityFilter() {
		return priorityFilter;
	}
	/**
	 * @param priorityFilter the priorityFilter to set
	 */
	public void setPriorityFilter(String priorityFilter) {
		this.priorityFilter = priorityFilter;
	}
	/**
	 * @return the messageFilter
	 */
	public String getMessageFilter() {
		return messageFilter;
	}
	/**
	 * @param messageFilter the messageFilter to set
	 */
	public void setMessageFilter(String messageFilter) {
		this.messageFilter = messageFilter;
	}
	/**
	 * @return the loggerFilter
	 */
	public String getLoggerFilter() {
		return loggerFilter;
	}
	/**
	 * @param loggerFilter the loggerFilter to set
	 */
	public void setLoggerFilter(String loggerFilter) {
		this.loggerFilter = loggerFilter;
	}

	/**
	 * @return the entries
	 */
	public List<? extends LogEntry> getEntries() {
		return entries;
	}
	/**
	 * @param list the entries to set
	 */
	public void setEntries(List<? extends LogEntry> list) {
		this.entries = list;
	}
	/**
	 * @return the beginTime
	 */
	public String getBeginTime() {
		return beginTime;
	}
	/**
	 * @param beginTime the beginTime to set
	 */
	public void setBeginTime(String beginTime) {
		this.beginTime = beginTime;
	}
	/**
	 * @return the endTime
	 */
	public String getEndTime() {
		return endTime;
	}
	/**
	 * @param endTime the endTime to set
	 */
	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}
	
}
