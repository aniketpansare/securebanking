package main.java.input;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class LoginFormData {
	
	/** Logger for this class and subclasses */
    protected final Log logger = LogFactory.getLog(getClass());

    private String user;
    private String password;

    
    public void setPassword(String pass) 
    {
        password = pass;
        logger.info("password set to " + password);
    }
 
    public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public String getPassword() {
    	return password;
    }
    
    
    
}
