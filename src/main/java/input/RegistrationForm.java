package main.java.input;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class RegistrationForm 
{
	/** Logger for this class and subclasses */
    protected final Log logger = LogFactory.getLog(getClass());

    private String FirstName;
    private String LastName;
    
    public void setFirstName(String FirstName) 
	{
		this.FirstName = FirstName;
	}
    public void setLastName(String LastName) 
	{
		this.LastName = LastName;
	}
	public String getFirstName()
	{
		return FirstName;
    }
	 public String getLastName()
	{
		return LastName;
    }
    
}