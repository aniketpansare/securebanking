package main.java.input;

public class TransactionFormData {

	private String selectedAccountNo;

	public String getSelectedAccountNo() {
		return selectedAccountNo;
	}

	public void setSelectedAccountNo(String selectedAccountNo) {
		this.selectedAccountNo = selectedAccountNo;
	}
	
}
