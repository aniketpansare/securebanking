package main.java.logging;

import java.util.Date;
import java.util.List;

import main.java.domain.LogEntry;
import main.java.logging.impl.LoggingManagerImpl;

public abstract class LoggingManager {

	/********************************************************************/
	/* Static Factory methods */
	/********************************************************************/
	
	/**
	 * Creates a new logging manager object for the provided component.
	 * @param component The component to retrieve the logging manager for.
	 * @return The new logging manager object.
	 */
	@SuppressWarnings("deprecation")
	public static LoggingManager create(String component) {
		return new LoggingManagerImpl(component);
	}
	
	
	
	/********************************************************************/
	/* Types */
	/********************************************************************/
	
	/**
	 * Logging level descriptor.
	 * @author jbabb
	 */
	public enum Priority {
		TRACE(org.apache.log4j.Priority.DEBUG, 0),
		DEBUG(org.apache.log4j.Priority.DEBUG, 1),
		INFO(org.apache.log4j.Priority.INFO, 2),
		WARN(org.apache.log4j.Priority.WARN, 3),
		ERROR(org.apache.log4j.Priority.ERROR, 4),
		FATAL(org.apache.log4j.Priority.FATAL, 5);
		
		
		// The internal logging level
		final public org.apache.log4j.Priority internal;
		final public int index;
		
		private Priority(org.apache.log4j.Priority internal, int index) {
			this.internal = internal;
			this.index = index;
		}
	};
	
	/********************************************************************/
	/* Methods */
	/********************************************************************/
	
	/**
	 * Logs a message at the trace level.
	 * @param message The message to log.
	 */
	public abstract void trace(String message);

	/**
	 * Logs a message at the trace level.
	 * @param format The format of the log message ({} is replaced with the next parameter)
	 * @param params The replacement parameters.
	 */
	public abstract void trace(String format, Object... params);

	/**
	 * Logs a message at the debug level.
	 * @param message The message to log.
	 */
	public abstract void debug(String message);

	/**
	 * Logs a message at the debug level.
	 * @param format The format of the log message ({} is replaced with the next parameter)
	 * @param params The replacement parameters.
	 */
	public abstract void debug(String format, Object... params);

	/**
	 * Logs a message at the info level.
	 * @param message The message to log.
	 */
	public abstract void info(String message);

	/**
	 * Logs a message at the info level.
	 * @param format The format of the log message ({} is replaced with the next parameter)
	 * @param params The replacement parameters.
	 */
	public abstract void info(String format, Object... params);

	/**
	 * Logs a message at the warning level.
	 * @param message The message to log.
	 */
	public abstract void warn(String message);

	/**
	 * Logs a message at the warning level.
	 * @param format The format of the log message ({} is replaced with the next parameter)
	 * @param params The replacement parameters.
	 */
	public abstract void warn(String format, Object... params);

	/**
	 * Logs a message at the error level.
	 * @param message The message to log.
	 */
	public abstract void error(String message);

	/**
	 * Logs a message at the error level.
	 * @param format The format of the log message ({} is replaced with the next parameter)
	 * @param params The replacement parameters.
	 */
	public abstract void error(String format, Object... params);

	/**
	 * Logs a message at the fatal level.
	 * @param message The message to log.
	 */
	public abstract void fatal(String message);

	/**
	 * Logs a message at the fatal level.
	 * @param format The format of the log message ({} is replaced with the next parameter)
	 * @param params The replacement parameters.
	 */
	public abstract void fatal(String format, Object... params);

	/**
	 * Logs a caught exception.
	 * @param level The logging level to use.
	 * @param exception The exception to log.
	 */
	public abstract void catching(Priority level, Throwable exception);

	/**
	 * Queries the log table for events matching the provided information.
	 * @param loggerFilter A partial logger name string match against (or null).
	 * @param priorityFilter The priority to include (or null).
	 * @param messageFilter A partial message string to match against (or null).
	 * @param beginTime The first time point to include.
	 * @param endTime The last time point to include.
	 * @param first The first result index to return.
	 * @param num The number of results to return (0 for unbounded).
	 */
	public abstract List<? extends LogEntry> query(String loggerFilter,
			String priorityFilter, String messageFilter, Date beginTime,
			Date endTime, int first, int num);

}