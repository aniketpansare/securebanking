package main.java.logging.impl;

import java.util.Date;
import java.util.List;

import main.java.domain.impl.LogEntryImpl;
import main.java.logging.LoggingManager;
import main.java.util.HibernateUtil;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.Session;
import org.slf4j.helpers.MessageFormatter;

/**
 * A simple class providing a standard logging interface.
 * @author jbabb
 *
 */
public class LoggingManagerImpl extends LoggingManager {
	
	/********************************************************************/
	/* Types */
	/********************************************************************/
	
	/********************************************************************/
	/* Members */
	/********************************************************************/
	
	private final ThreadLocal<Logger> _log;
		
	/********************************************************************/
	/* Constructor */
	/********************************************************************/
	
	/**
	 * Initializes the logging manager. 
	 * @param component The name of the component that will be logging events from this instance.
	 */
	@Deprecated
	public LoggingManagerImpl(final String component) {
		_log = new ThreadLocal<Logger>() {
			@Override
			protected synchronized Logger initialValue() {
				return LogManager.getLogger(component);
			}
		};
	}
	
	/********************************************************************/
	/* Public Methods */
	/********************************************************************/
	
	static {
		// This ensures the that the mysql driver has been properly loaded prior to log4j initialization.
		try {
			Class.forName("com.mysql.jdbc.Driver");
		} catch (ClassNotFoundException e) {
			throw new ExceptionInInitializerError(e);
		}
	}
	
	
	
	/* (non-Javadoc)
	 * @see main.java.logging.LoggingManager#trace(java.lang.String)
	 */
	@Override
	public void trace(String message) {
		_log.get().trace(message);
	}
	
	/* (non-Javadoc)
	 * @see main.java.logging.LoggingManager#trace(java.lang.String, java.lang.Object)
	 */
	@Override
	public void trace(String format, Object... params) {
		_log.get().trace(MessageFormatter.arrayFormat(format, params).getMessage());
	}
	
	/* (non-Javadoc)
	 * @see main.java.logging.LoggingManager#debug(java.lang.String)
	 */
	@Override
	public void debug(String message) {
		_log.get().debug(message);
	}
	
	/* (non-Javadoc)
	 * @see main.java.logging.LoggingManager#debug(java.lang.String, java.lang.Object)
	 */
	@Override
	public void debug(String format, Object... params) {
		_log.get().debug(MessageFormatter.arrayFormat(format, params).getMessage());
	}
	
	/* (non-Javadoc)
	 * @see main.java.logging.LoggingManager#info(java.lang.String)
	 */
	@Override
	public void info(String message) {
		_log.get().info(message);
	}
	
	/* (non-Javadoc)
	 * @see main.java.logging.LoggingManager#info(java.lang.String, java.lang.Object)
	 */
	@Override
	public void info(String format, Object... params) {
		_log.get().info(MessageFormatter.arrayFormat(format, params).getMessage());
	}
	
	
	/* (non-Javadoc)
	 * @see main.java.logging.LoggingManager#warn(java.lang.String)
	 */
	@Override
	public void warn(String message) {
		_log.get().warn(message);
	}
	
	/* (non-Javadoc)
	 * @see main.java.logging.LoggingManager#warn(java.lang.String, java.lang.Object)
	 */
	@Override
	public void warn(String format, Object... params) {
		_log.get().warn(MessageFormatter.arrayFormat(format, params).getMessage());
	}
	
	/* (non-Javadoc)
	 * @see main.java.logging.LoggingManager#error(java.lang.String)
	 */
	@Override
	public void error(String message) {
		_log.get().error(message);
	}
	
	/* (non-Javadoc)
	 * @see main.java.logging.LoggingManager#error(java.lang.String, java.lang.Object)
	 */
	@Override
	public void error(String format, Object... params) {
		_log.get().error(MessageFormatter.arrayFormat(format, params).getMessage());
	}
	/* (non-Javadoc)
	 * @see main.java.logging.LoggingManager#fatal(java.lang.String)
	 */
	@Override
	public void fatal(String message) {
		_log.get().fatal(message);
	}
	
	/* (non-Javadoc)
	 * @see main.java.logging.LoggingManager#fatal(java.lang.String, java.lang.Object)
	 */
	@Override
	public void fatal(String format, Object... params) {
		_log.get().fatal(MessageFormatter.arrayFormat(format, params).getMessage());
	}
	
	
	/* (non-Javadoc)
	 * @see main.java.logging.LoggingManager#catching(main.java.logging.LoggingManagerImpl.Level, java.lang.Throwable)
	 */
	@Override
	public void catching(Priority level, Throwable exception) {
		_log.get().log(level.internal, "Catching", exception);
	}
	
	
	
	
	/* (non-Javadoc)
	 * @see main.java.logging.LoggingManager#query(java.lang.String, java.lang.String, java.lang.String, java.util.Date, java.util.Date, int, int)
	 */
	@Override
	@SuppressWarnings("unchecked")
	public List<LogEntryImpl> query(String loggerFilter, String priorityFilter, String messageFilter, Date beginTime, Date endTime, int first, int num) {
    	// construct the query
    	Session s = HibernateUtil.getSessionFactory().getCurrentSession();
		Query q = s.getNamedQuery("LOGENTRY_MULTIFILTER");
		
		// Bind query params
		q.setParameter("priority", 	"%"+priorityFilter+"%");
		q.setParameter("message", 	"%"+messageFilter+"%");
		q.setParameter("logger", 	"%"+loggerFilter+"%");
		q.setParameter("begintime", beginTime);
		q.setParameter("endtime", 	endTime);
		if (first > 0)	q.setFirstResult(first);
		if (num > 0)	q.setMaxResults(num);
		
		// Execute
		return (List<LogEntryImpl>)q.list();
	}
	
}
