package main.java.security;

/**
 * Exception which is thrown when an authentication error occurs.
 * @author jbabb
 *
 */
public class AuthenticationException extends SecurityException {

	private static final long serialVersionUID = 1L;
	
	/********************************************************************/
	/* Constructors */
	/*******************************************************************/
	
	/**
	 * Default constructor
	 */
	public AuthenticationException() {
		// Intentionally left blank.
	}
	
	/**
	 * 
	 * @param description A description of the error.
	 */
	public AuthenticationException(String description) {
		super(description);
	}
	
	/**
	 * 
	 * @param e The internal error which caused the exception.
	 */
	public AuthenticationException(Throwable e) {
		super(e);
	}

	/**
	 * 
	 * @param description A description of the error.
	 * @param e The internal error which caused the exception.
	 */
	public AuthenticationException(String description, Throwable e) {
		super(description, e);
	}
	
	
	

}
