package main.java.security;

/**
 * Exception which is thrown when an authorization error occurs.
 * @author jbabb
 *
 */
public class AuthorizationException extends SecurityException {

	private static final long serialVersionUID = 1L;
	
	/********************************************************************/
	/* Constructors */
	/*******************************************************************/
	
	/**
	 * Default constructor
	 */
	public AuthorizationException() {
		// Intentionally left blank.
	}
	
	/**
	 * 
	 * @param description A description of the error.
	 */
	public AuthorizationException(String description) {
		super(description);
	}
	
	/**
	 * 
	 * @param e The internal error which caused the exception.
	 */
	public AuthorizationException(Throwable e) {
		super(e);
	}

	/**
	 * 
	 * @param description A description of the error.
	 * @param e The internal error which caused the exception.
	 */
	public AuthorizationException(String description, Throwable e) {
		super(description, e);
	}
	
	
	

}
