package main.java.security;

import java.io.IOException;
import java.util.Set;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationSuccessHandler;

public class CustomAuthenticationHandler extends SimpleUrlAuthenticationSuccessHandler {
	 
	 @Override
	 public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication) throws ServletException, IOException {
	      String userTargetUrl = "/user/Accounts.htm";
	      String adminTargetUrl = "/admin/AdminHomePage.htm";
	      String employeeTargetUrl = "/employee/EmployeeHomePage.htm";
	      String managerTargetUrl = "/dept/DeptManagerHomePage.htm";
	      String corporateTargetUrl = "/corp/CorpManagerHomePage.htm";
	      Set<String> roles = AuthorityUtils.authorityListToSet(authentication.getAuthorities());
	      if (roles.contains("ADMIN")) {
	         getRedirectStrategy().sendRedirect(request, response, adminTargetUrl);
	      } else if (roles.contains("CUSTOMER") || roles.contains("MERCHANT")) {
	         getRedirectStrategy().sendRedirect(request, response, userTargetUrl);
	      } 
	      else if (roles.contains("EMPLOYEE")) {
		         getRedirectStrategy().sendRedirect(request, response, employeeTargetUrl);
		      } 
	      else if (roles.contains("MANAGER")) {
		         getRedirectStrategy().sendRedirect(request, response, managerTargetUrl);
		      } 
	      else if (roles.contains("CORPORATE")) {
		         getRedirectStrategy().sendRedirect(request, response, corporateTargetUrl);
		      } 
	      else {
	         super.onAuthenticationSuccess(request, response, authentication);
	         return;
	      }
	   }
	}