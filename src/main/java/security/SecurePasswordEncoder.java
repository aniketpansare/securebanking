package main.java.security;

import java.nio.charset.CharacterCodingException;

import main.java.domain.Password;

import org.springframework.security.crypto.password.PasswordEncoder;

public class SecurePasswordEncoder implements PasswordEncoder {

	@Override
	public String encode(CharSequence arg0) {
		try {
			return (Password.create(arg0)).getEncodedString();
		} catch (NullPointerException | CharacterCodingException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public boolean matches(CharSequence arg0, String arg1) {
		try {
			return Password.fromEncodedString(arg1).validate(arg0);
		} catch (NullPointerException | IllegalArgumentException | CharacterCodingException e) {
			throw new RuntimeException(e);
		} 
	}

}
