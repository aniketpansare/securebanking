/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package main.java.security;

import java.nio.charset.CharacterCodingException;
import java.security.InvalidKeyException;
import java.security.KeyStore;

import javax.crypto.BadPaddingException;
import javax.crypto.SecretKey;

import main.java.domain.User;
import main.java.security.impl.SecurityManagerImpl;

import org.bouncycastle.crypto.CryptoException;

/**
 * Security Manager interface class.
 * @author jbabb
 */
public abstract class SecurityManager {
   
	private static final ThreadLocal<SecurityManagerImpl> secman = new ThreadLocal<SecurityManagerImpl>() {
		@Override
		protected synchronized SecurityManagerImpl initialValue() {
			return new SecurityManagerImpl();
		}
	};
	
	/***************************************************************************/
	/* Factory Methods */
	/***************************************************************************/
	/**
	 * gets a security manager object.
	 */
	
	@SuppressWarnings("deprecation")
	public static SecurityManager get() {
		return secman.get();
	}
	
	
   /***************************************************************************/
   /* Input Validation Functions */
   /***************************************************************************/
    
    /**
     * Determines if the input string is a valid positive amount.
     * @param input The string to test.
     * @return True if the input is valid.
     * @throws NullPointerException Thrown if input is NULL.
     */
    public abstract boolean isAmount(String input) throws NullPointerException;
    
    
    /**
     * Determines if the input string is a valid username.
     * @param input The string to test.
     * @return True if the input is valid.
     * @throws NullPointerException Thrown if input is NULL.
     */
    public abstract boolean isUsername(String input) throws NullPointerException;
    
    /**
     * Determines if the input string is a valid email address.
     * @param input The string to test.
     * @return True if the input is valid.
     * @throws NullPointerException Thrown if input is NULL.
     */
    public abstract boolean isEmail(String input) throws NullPointerException;
    
    /**
     * Determines if the input string is a valid password.
     * @param input The string to test.
     * @return True if the input is valid.
     * @throws NullPointerException Thrown if input is NULL.
     */
    public abstract boolean isPassword(String input) throws NullPointerException;
    
    /**
     * Determines if the input string is a valid phone number.
     * @param input The string to test.
     * @return True if the input is valid.
     * @throws NullPointerException Thrown if input is NULL.
     */
    public abstract boolean isPhoneNumber(String input) throws NullPointerException;
    
            
    // ----------------------------------------------------- Primitives
            
    
    /**
     * Determines if the input string is a valid alphabetical string.
     * @param input The string to test.
     * @return True if the input is valid.
     * @throws NullPointerException Thrown if input is NULL.
     */
    public abstract boolean isAlpha(String input) throws NullPointerException;
    
    /**
     * Determines if the input string is a valid numerical string.
     * @param input The string to test.
     * @return True if the input is valid.
     * @throws NullPointerException Thrown if input is NULL.
     */
    public abstract boolean isNumeric(String input) throws NullPointerException;
    
    /**
     * Determines if the input string is a valid integral string.
     * @param input The string to test.
     * @return True if the input is valid.
     * @throws NullPointerException Thrown if input is NULL.
     */
    public abstract boolean isInteger(String input) throws NullPointerException;
    
    /**
     * Determines if the input string is a ASCII string.
     * @param input The string to test.
     * @return True if the input is valid.
     * @throws NullPointerException Thrown if input is NULL.
     */
    public abstract boolean isASCII(String input) throws NullPointerException;
    
    /**
     * Determines if the input string is a valid alphabetical/numerical string 
     * (with whitespace)
     * @param input The string to test.
     * @return True if the input is valid.
     * @throws NullPointerException Thrown if input is NULL.
     */
    public abstract boolean isAlphaNumSpace(String input) throws NullPointerException;
    
    /**
     * 
     * Determines if the input string is a valid alphabetical/numerical string 
     * (without whitespace)
     * @param input The string to test.
     * @return True if the input is valid.
     * @throws NullPointerException Thrown if input is NULL.
     */
    public abstract boolean isAlphaNum(String input) throws NullPointerException;
    
   /***************************************************************************/
   /* Cryptographic Functions */
   /***************************************************************************/
    
    /**
     * Gets the key that should be used to encrypt/decrypt user PII
     * @return The key that should be used.
     */
    public abstract SecretKey getPIIKey();
    
    /**
     * Gets a random integer.
     * @return A random integer generated with a secure random number generated.
     */
    public abstract int randomInt();
    
    /**
     * Gets a random long.
     * @return A random long generated with a secure random number generated.
     */
    public abstract long randomLong();
    
    /**
     * Gets a random boolean value.
     * @return A random boolean generated with a secure random number generated.
     */
    public abstract boolean randomBoolean();
    
    /**
     * Gets a random list of bytes.
     * @param buffer A buffer to fill with random bytes.
     * @throws NullPointerException Thrown if buffer is null.
     */
    public abstract void randomBytes(byte[] buffer) throws NullPointerException;
    
    
	/**
	 * Generates a new random string.
	 * @param bytes The number of bytes to use as a seed value (determining length).
	 * @return The new random string
	 * @throws IllegalArgumentException Thrown if bytes <= 0.
	 */
	public abstract String randomString(int bytes) throws IllegalArgumentException;
    
    /**
     * Calculate the hash of the data and salt provided.
     * @param data The data to hash.
     * @param salt The salt for the hash (or NULL).
     * @return The salted hash of the data. NULL if something went wrong.
     * @throws NullPointerException Thrown if data is NULL.
     */
    public abstract byte[] hash(byte[] data, byte[] salt) throws NullPointerException;
    
    
    /**
     * Returns a collection of the valid key lengths (in bytes). 
     * @return The valid key lengths accepted by createEncryptionKey.
     */
    public abstract int[] validKeyLengths();
    
    /**
     * Creates an encryption key object given the provided raw key material.
     * @param key The raw key material to create the key with.
     * @return A key object corresponding to the key material.
     * @throws NullPointerException Thrown if data is NULL or if key is NULL.
     * @throws IllegalArgumentException Thrown if the key is not one of the valid key lengths.
     */
    public abstract SecretKey createEncryptionKey(byte[] key) throws NullPointerException, IllegalArgumentException;
    
    
    /**
     * Encrypts the provided data using the provided key.
     * @param cleartext The data to encrypt.
     * @param key The key to use to encrypt the data.
     * @return  The encrypted data or NULL if something went wrong.
     * @throws NullPointerException Thrown if data is NULL or if key is NULL.
     * @throws InvalidKeyException Thrown if the provided key is invalid.
     */
    public abstract byte[] encrypt(byte[] cleartext, SecretKey key) throws NullPointerException, InvalidKeyException;
    
    /**
     * Decrypts the provided data using the provided key.
     * @param ciphertext The data to decrypt.
     * @param key The key to use to decrypt the data.
     * @return The decrypted data or NULL if something went wrong.
     * @throws NullPointerException Thrown if ciphertext or key are null.
     * @throws InvalidKeyException Thrown if the provided key doesn't match our algorithm.
     * @throws BadPaddingException Thrown if the ciphertext has been corrupted or our key doesn't match.
     */
    public abstract byte[] decrypt(byte[] ciphertext, SecretKey key) throws NullPointerException, InvalidKeyException, BadPaddingException;
    
    /**
     * Generates a PKCS12 keystore instance containing a new rsa keypair and signed certificate for the provided user that is encrypted with the provided password.
     * @param user The user to generate the keys for.
     * @param password The password to encrypt the keys with.
     * @return A new keystore instance with the user's RSA key pair and signed certificate.
     * @throws NullPointerException Thrown if user or password is null.
     * @throws CryptoException Thrown if something went wrong during the key generation/packaging process.
     */
    public abstract KeyStore generatePKCS12(User user, String password) throws NullPointerException, CryptoException;
    
   /***************************************************************************/
   /* Data Encoding Functions */
   /***************************************************************************/
    
    /**
     * Encodes a string into the standard ASCII format.
     * @param data The data to encode.
     * @param maxlen The maximum length of the encoding.
     * @return The encoded string or NULL if something went wrong.
     * @throws NullPointerException Thrown if data is NULL.
     * @throws CharacterCodingException Thrown if data contains non ASCII characters.
     */
    public abstract byte[] encodeASCII(CharSequence data) throws NullPointerException, CharacterCodingException;
    
    /**
     * Decodes a string from the encoded array.
     * @param data The data to decode.
     * @return The decoded string or NULL if something went wrong.
     * @throws NullPointerException Thrown if data is NULL.
     * @throws CharacterCodingException Thrown if data is not in ASCII format.
     */
    public abstract String decodeASCII(byte[] data) throws NullPointerException, CharacterCodingException;
    
    /**
     * Encodes the provided byte array into a base 64 format string.
     * @param data The data to be encoded.
     * @return The encoded string or NULL if something went wrong.
     * @throws NullPointerException Thrown if data is null.
     */
    public abstract String encodeBase64(byte[] data) throws NullPointerException;
    
    /**
     * Decodes the provided base64 string into a byte array.
     * @param data The base64 data to decode.
     * @return A byte array containing the decoded data or NULL if something went wrong.
     * @throws NullPointerException  Thrown if data is null.
     * @throws IllegalArgumentException Thrown if the data is not in base64 format.
     */
    public abstract byte[] decodeBase64(String data) throws NullPointerException, IllegalArgumentException;
    
}
