package main.java.security.impl;

import java.io.IOException;
import java.math.BigInteger;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.charset.CharacterCodingException;
import java.nio.charset.Charset;
import java.nio.charset.CharsetDecoder;
import java.nio.charset.CharsetEncoder;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.SecureRandom;
import java.security.Security;
import java.security.UnrecoverableKeyException;
import java.security.cert.Certificate;
import java.security.cert.CertificateEncodingException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import javax.xml.bind.DatatypeConverter;

import main.java.GlobalConstants;
import main.java.domain.User;
import main.java.logging.LoggingManager;
import main.java.logging.LoggingManager.Priority;
import main.java.security.SecurityManager;

import org.bouncycastle.asn1.x500.X500Name;
import org.bouncycastle.asn1.x509.SubjectPublicKeyInfo;
import org.bouncycastle.cert.X509CertificateHolder;
import org.bouncycastle.cert.X509v3CertificateBuilder;
import org.bouncycastle.cert.jcajce.JcaX509CertificateConverter;
import org.bouncycastle.cert.jcajce.JcaX509CertificateHolder;
import org.bouncycastle.crypto.CryptoException;
import org.bouncycastle.operator.ContentSigner;
import org.bouncycastle.operator.OperatorCreationException;
import org.bouncycastle.operator.jcajce.JcaContentSignerBuilder;


/**
 *
 * @author jbabb
 */
public class SecurityManagerImpl extends SecurityManager {
    public static final String KEYSTORE_PASS = "Xi08s6Z8Bi43xzH5a6Hl2NJhGxJfBBoS";
   /***************************************************************************/
   /* Constants */
   /***************************************************************************/
    
    /// Email address regex pattern 
    /// (from http://www.mkyong.com/regular-expressions/how-to-validate-email-address-with-regular-expression/)
    private static final String EMAIL_PATTERN = 
        "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
        + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
    
    /// Pattern to match amounts such as $10.50.
    private static final String AMOUNT_PATTERN = 
        "^\\$?\\d+(\\.\\d\\d?)?$";
    
    /// Pattern to match valid usernames against.
    private static final String USERNAME_PATTERN = 
            "^[a-zA-Z][a-zA-Z0-9_]*$";
    
    /// Pattern to match valid phone numbers against.
    private static final String PHONENUMBER_PATTERN = 
            "^(\\d{3}-\\d{3}-\\d{4})"
            + "|(\\(\\d{3}\\)\\s*\\d{3}-\\d{4})"
            + "|(\\d{3}-\\d{7})"
            + "|(\\d{10})$";
    
    
   /***************************************************************************/
   /* Members */
   /***************************************************************************/
    private static final KeyStore _keystore; 
    private static final PrivateKey _ca_priv_key;
    private static final X509Certificate _ca_cert;
    private static final SecretKey _pii_key;
    
    private final LoggingManager _log = LoggingManager.create(SecurityManager.class.getName());
    private final SecureRandom _random;
    private final KeyPairGenerator _keygen;
    
    /***************************************************************************/
    /* Constructors */
    /***************************************************************************/
    
    static {
        // Register the provider required to do our encryption.
        Security.addProvider(new org.bouncycastle.jce.provider.BouncyCastleProvider());
        
        // Load the keystore
        ClassLoader loader = Thread.currentThread().getContextClassLoader();
        try {
			_keystore = KeyStore.getInstance("JCEKS");
			_keystore.load(loader.getResourceAsStream("group1-ca.jceks"), KEYSTORE_PASS.toCharArray());
			
			 // Load the CA certificate and key pair
	        _ca_priv_key = (PrivateKey)_keystore.getKey("ca", KEYSTORE_PASS.toCharArray());
	        _ca_cert = (X509Certificate)_keystore.getCertificate("ca");
	        _pii_key = (SecretKey) _keystore.getKey("piikey", KEYSTORE_PASS.toCharArray());
			
		} catch (KeyStoreException | NoSuchAlgorithmException | CertificateException 
				| IOException | UnrecoverableKeyException | NullPointerException | ClassCastException e) {
			throw new ExceptionInInitializerError(e);
		}
        
        if (_ca_priv_key == null || _ca_cert == null || _pii_key == null) {
        	// could not recover CA key/cert
        	throw new ExceptionInInitializerError("CA Private key/certificate and/or database encryption key appear to be missing");
        }
    }
    
    @Deprecated
    public SecurityManagerImpl() {    	
        SecureRandom random = null; 
        try {
             random = SecureRandom.getInstance("SHA1PRNG");
        } catch (NoSuchAlgorithmException e) {
        	_log.catching(Priority.FATAL, e);
        }
        _random = random;
        
        KeyPairGenerator keygen = null;
        try {
        	keygen = KeyPairGenerator.getInstance("RSA");
        } catch (NoSuchAlgorithmException e) {
        	_log.catching(Priority.FATAL, e);
        }
        keygen.initialize(1024);
        _keygen = keygen;
    }

                  
   
                    
   /***************************************************************************/
   /* Input Validation Functions */
   /***************************************************************************/
    
    // Determines if the input string is a valid positive amount.
    @Override
    public boolean isAmount(String input) throws NullPointerException {
        if (input == null) { 
        	NullPointerException e = new  NullPointerException("Illegal attempt to validate a null string.");
            _log.catching(Priority.WARN, e);
            throw e;
        }
        return input.length() <= GlobalConstants.AMOUNT_MAX_LENTH && input.matches(AMOUNT_PATTERN);
    }
    
    
    // Determines if the input string is a valid username.
    @Override
    public boolean isUsername(String input) throws NullPointerException {
        if (input == null) { 
        	NullPointerException e = new  NullPointerException("Illegal attempt to validate a null string.");
            _log.catching(Priority.WARN, e);
            throw e;
        }
        return input.length() <= GlobalConstants.USERNAME_MAX_LENGTH 
                && input.length() >= GlobalConstants.USERNAME_MIN_LENGTH 
                && input.matches(USERNAME_PATTERN);
    }
    
    // Determines if the input string is a valid email address.
    @Override
    public boolean isEmail(String input) throws NullPointerException {
        if (input == null) { 
        	NullPointerException e = new  NullPointerException("Illegal attempt to validate a null string.");
            _log.catching(Priority.WARN, e);
            throw e;
        }
        return input.length() <= GlobalConstants.EMAIL_MAX_LENGTH && input.matches(EMAIL_PATTERN);
    }
    
    // Determines if the input string is a valid password.
    @Override
    public boolean isPassword(String input) throws NullPointerException {
        if (input == null) { 
        	NullPointerException e = new  NullPointerException("Illegal attempt to validate a null string.");
            _log.catching(Priority.WARN, e);
            throw e;
        }
        return input.length() >= GlobalConstants.PASSWORD_MIN_LENGTH && input.length() <= GlobalConstants.PASSWORD_MAX_LENGTH;
    }
    
    // Determines if the input string is a valid phone number.
    @Override
    public boolean isPhoneNumber(String input) throws NullPointerException {
        if (input == null) { 
        	NullPointerException e = new  NullPointerException("Illegal attempt to validate a null string.");
            _log.catching(Priority.WARN, e);
            throw e;
        }
        return input.matches(PHONENUMBER_PATTERN);
    }
    
            
    // ----------------------------------------------------- Primitives
            
    
    // Determines if the input string is a valid alphabetical string.
    @Override
    public boolean isAlpha(String input) throws NullPointerException {
        if (input == null) { 
        	NullPointerException e = new  NullPointerException("Illegal attempt to validate a null string.");
            _log.catching(Priority.WARN, e);
            throw e;
        }
        return input.matches("^[a-zA-Z]+$");
    }
    
    // Determines if the input string is a valid numerical string.
    @Override
    public boolean isNumeric(String input) throws NullPointerException {
        if (input == null) { 
        	NullPointerException e = new  NullPointerException("Illegal attempt to validate a null string.");
            _log.catching(Priority.WARN, e);
            throw e;
        }
        return input.matches("^\\d+(\\.\\d+)?$");
    }
    
    // Determines if the input string is a valid integral string.
    @Override
    public boolean isInteger(String input) throws NullPointerException {
        if (input == null) { 
        	NullPointerException e = new  NullPointerException("Illegal attempt to validate a null string.");
            _log.catching(Priority.WARN, e);
            throw e;
        }
        return input.matches("^\\d+$");
    }
    
    // Determines if the input string is a ASCII string.
    @Override
    public boolean isASCII(String input) throws NullPointerException {
        if (input == null) { 
        	NullPointerException e = new  NullPointerException("Illegal attempt to validate a null string.");
            _log.catching(Priority.WARN, e);
            throw e;
        }
        CharsetEncoder encoder = Charset.forName("US-ASCII").newEncoder();
        return encoder.canEncode(input);
    }
    
    // Determines if the input string is a valid alphabetical/numerical string 
    @Override
    public boolean isAlphaNumSpace(String input) throws NullPointerException {
        if (input == null) { 
        	NullPointerException e = new  NullPointerException("Illegal attempt to validate a null string.");
            _log.catching(Priority.WARN, e);
            throw e;
        }
        return input.matches("^[a-zA-Z0-9 \\t\\n\\x0b\\r\\f]+$");
    }

    // Determines if the input string is a valid alphabetical/numerical string 
    @Override
    public boolean isAlphaNum(String input) throws NullPointerException {
        if (input == null) { 
        	NullPointerException e = new  NullPointerException("Illegal attempt to validate a null string.");
            _log.catching(Priority.WARN, e);
            throw e;
        }
        return input.matches("^[a-zA-Z0-9]+$");
    }
    
    
   /***************************************************************************/
   /* Cryptographic Functions */
   /***************************************************************************/
    
    @Override
    public SecretKey getPIIKey() {
    	return _pii_key;
    }
    
    @Override
    public KeyStore generatePKCS12(User user, String password) throws NullPointerException, CryptoException {
    	if (user == null || password == null) {
    		NullPointerException e = new NullPointerException();
    		_log.catching(Priority.WARN, e);
    		throw e;
    	}
    	
    	KeyPair kp = genKeyPair();
    	Certificate cert = generatedSignedCert(user, kp.getPublic());
    	return packagePKCS12(user, password, kp, cert);
    	
    }
    
    
    
    // Get a random number
    @Override
    public int randomInt() {
        return _random.nextInt();
    }
    
    @Override
    public long randomLong() {
    	return _random.nextLong();
    }
    
    
    // Get a random boolean
    @Override
    public boolean randomBoolean() {
        return _random.nextBoolean();
    }
    
    // Get a random set of bytes.
    @Override
    public void randomBytes(byte[] buffer) throws NullPointerException {
        if (buffer == null) { 
        	NullPointerException e = new  NullPointerException("Illegal attempt to randomize a null buffer.");
            _log.catching(Priority.WARN, e);
            throw e;
        }
        _random.nextBytes(buffer);
    }
    
    @Override
    public String randomString(int bytes) throws IllegalArgumentException {
    	if (bytes <= 0) {
    		IllegalArgumentException e = new  IllegalArgumentException("Illegal attempt to randomize string with seed length <= 0.");
            _log.catching(Priority.WARN, e);
            throw e;
    	}
    	
		byte[] rand = new byte[bytes];
		randomBytes(rand);
		String candidate = encodeBase64(rand);
		
		// The padding marker shouldn't be exposed to user.
		return candidate.replaceAll("=", "");
    }

    
    // Calculate the hash of the data and salt provided.
    @Override
    public byte[] hash(byte[] data, byte[] salt) throws NullPointerException {
        byte[] tmp;
        
        if (data == null) {
        	NullPointerException e = new NullPointerException("Illegal attempt to hash a null data array.");
                _log.catching(Priority.WARN, e);
                throw e;
            }
        
        // properly handle the salt.
        if (salt != null) {
            tmp = new byte[data.length + salt.length];
            System.arraycopy(data, 0, tmp, 0, data.length);
            System.arraycopy(salt, 0, tmp, data.length, salt.length);
        } else {
            tmp = data;
        }
        
        try {
            MessageDigest sha256 = MessageDigest.getInstance("SHA-256");
            sha256.update(tmp);
            return sha256.digest();
        } catch (NoSuchAlgorithmException e) {
            _log.catching(Priority.ERROR, e);
            return null;
        }
    }
    
    // Get the valid key lengths.
    @Override
    public int[] validKeyLengths() {
        int[] ret = new int[]{16};
        return ret;
    }
    
    // Creates an encryption key object given the provided raw key material.
    @Override
    public SecretKey createEncryptionKey(byte[] key) throws NullPointerException, IllegalArgumentException {
        boolean valid = false;
        int[] validlens = validKeyLengths();
        for (int i = 0; i < validlens.length && !valid; i++) {
            if (key.length == validlens[i]) valid = true;
        }
        
        if (!valid) {
        	IllegalArgumentException e = new IllegalArgumentException("Invalid attempt to create a key of length " 
                    + key.length 
                    + ". Only the following lengths are supported "
                    + Arrays.toString(validlens) + ".");
        	_log.catching(Priority.WARN, e);
        	throw e;
        }
        
        
        if (key == null) {
        	NullPointerException e = new NullPointerException("Invalid attempt to initialize key object with null key material container.");
        	_log.catching(Priority.WARN, e);
        	throw e;
        }
        
        return new SecretKeySpec(key, "AES");
    }
    
    
    // Encrypts the provided data using the provided key.
    @Override
    public byte[] encrypt(byte[] cleartext, SecretKey key) throws NullPointerException, InvalidKeyException {
        if (cleartext == null) {
        	NullPointerException e =  new NullPointerException("Invalid attempt to encrypt null data container.");
        	_log.catching(Priority.WARN, e);
        	throw e;
        } if (key == null) {
        	NullPointerException e = new NullPointerException("Invalid attempt to encrypt data using null key object.");
        	_log.catching(Priority.WARN, e);
        	throw e;
        }
        
        try {
            // GCM provides for builtin authentication using a computed MAC
            // wich is encrypted with the cleartext. This provides us with
            // a means of ensuring data integrity as well as confidentiality.
            // NOTE: Rquires Java 7+
            final Cipher cipher = Cipher.getInstance("AES/GCM/NoPadding");
            final int blksz = cipher.getBlockSize();
            
            // Generate init vector
            final byte[] iv = new byte[blksz];
            randomBytes(iv);
            final IvParameterSpec params = new IvParameterSpec(iv);
            
            // Init the cypher to encrypt
            cipher.init(Cipher.ENCRYPT_MODE, key, params);
            
            // Create for us the cyphertext (+ IV information)
            final byte[] ciphertext = cipher.doFinal(cleartext);
            final byte[] result = new byte[iv.length + ciphertext.length];
            System.arraycopy(iv, 0, result, 0, iv.length);
            System.arraycopy(ciphertext, 0, result, iv.length, ciphertext.length);
            
            // Yay! we're done
            return result;
            
        } catch (NoSuchAlgorithmException 
                | NoSuchPaddingException 
                | IllegalBlockSizeException
                | BadPaddingException
                | InvalidAlgorithmParameterException e) {
        	_log.catching(Priority.ERROR, e);
            return null;
        }
    }
    
    // Decrypts the provided data using the provided key.
    @Override
    public byte[] decrypt(byte[] ciphertext, SecretKey key) throws NullPointerException, InvalidKeyException, BadPaddingException {
        if (ciphertext == null) {
        	NullPointerException e = new NullPointerException("Invalid attempt to decrypt null data container.");
        	_log.catching(Priority.WARN, e);
        	throw e;
        } if (key == null) {
        	NullPointerException e = new NullPointerException("Invalid attempt to decrypt data using null key object.");
        	_log.catching(Priority.WARN, e);
        	throw e;
        }
        
        try {
            // GCM authenticated cipher
            final Cipher cipher = Cipher.getInstance("AES/GCM/NoPadding");
            final int blksz = cipher.getBlockSize();
            
            // Read in init vector from the ciphertext and setup parameters
            final byte[] iv = new byte[blksz];
            System.arraycopy(ciphertext, 0, iv, 0, iv.length);
            final IvParameterSpec params = new IvParameterSpec(iv);
            
            // Get our cyphertext
            final byte[] encrypted = new byte[ciphertext.length - iv.length];
            System.arraycopy(ciphertext, iv.length, encrypted, 0, encrypted.length);
            
            // Decrypt the message
            cipher.init(Cipher.DECRYPT_MODE, key, params);
            return cipher.doFinal(encrypted);
            
        } catch (NoSuchAlgorithmException 
                | NoSuchPaddingException 
                | IllegalBlockSizeException
                | InvalidAlgorithmParameterException e) {
        	_log.catching(Priority.ERROR, e);
            return null;
        }
    }
    
   /***************************************************************************/
   /* Data Encoding Functions */
   /***************************************************************************/
    
    // Encodes a string into the standard ASCII format.
    @Override
    public byte[] encodeASCII(CharSequence data) throws NullPointerException, CharacterCodingException {
        if (data == null) {
        	NullPointerException e = new NullPointerException("Illegal attempt to hash a null data array.");
        	_log.catching(Priority.WARN, e);
        	throw e;
        }
        CharsetEncoder encoder = Charset.forName("US-ASCII").newEncoder();
        if (encoder.canEncode(data)) {
                return encoder.encode(CharBuffer.wrap(data)).array();
        } else throw new CharacterCodingException();

    }
    
    // Decodes a string from the encoded array.
    @Override
    public String decodeASCII(byte[] data) throws NullPointerException, CharacterCodingException {
        if (data == null) {
        	NullPointerException e = new NullPointerException("Illegal attempt to hash a null data array.");
        	_log.catching(Priority.WARN, e);
        	throw e;
        }
        CharsetDecoder decoder = Charset.forName("US-ASCII").newDecoder();
        return decoder.decode(ByteBuffer.wrap(data)).toString();
    }
    
    // Encodes the provided byte array into a base 64 format string.
    @Override
    public String encodeBase64(byte[] data) throws NullPointerException {
        if (data == null) {
        	NullPointerException e = new NullPointerException("Invalid attempt to encode null data container in base64.");
        	_log.catching(Priority.WARN, e);
        	throw e;
        }
        
        return DatatypeConverter.printBase64Binary(data);
    }
    
    // Decodes the provided base64 string into a byte array.
    @Override
    public byte[] decodeBase64(String data) throws NullPointerException, IllegalArgumentException {
        if (data == null) {
        	NullPointerException e = new NullPointerException("Invalid attempt to decode null base64 string.");
            _log.catching(Priority.WARN, e);
        	throw e;
        }
        
        return DatatypeConverter.parseBase64Binary(data);
    }
    
    
    
    public KeyPair genKeyPair() {
    	return _keygen.generateKeyPair();
    }
    
    public Certificate generatedSignedCert(User user, PublicKey key) throws NullPointerException, CryptoException {
    	if (user == null || key == null) {
    		NullPointerException e = new NullPointerException();
    		_log.catching(Priority.WARN, e);
    		throw e;
    	}
    	
    	
    	// Subject public-key info
    	SubjectPublicKeyInfo publicKeyInfo = SubjectPublicKeyInfo.getInstance(key.getEncoded());
    	
    	Calendar expiration = Calendar.getInstance();
    	expiration.add(Calendar.YEAR, 1);
    	
    	
    	// Generate Certificate Builder
    	X509v3CertificateBuilder certBuilder;
		try {
			certBuilder = new X509v3CertificateBuilder(
					new JcaX509CertificateHolder(_ca_cert).getSubject(),
					BigInteger.valueOf(randomLong()),
					new Date(System.currentTimeMillis()), 
					expiration.getTime(),
					new X500Name("CN=" + user.getUsername() + ", O=Bank of the Sundevil, ST=Arizona, C=US"), publicKeyInfo);
		} catch (CertificateEncodingException e) {
			_log.catching(Priority.ERROR, e);
			throw new CryptoException("", e);
		}
    	
    	ContentSigner signer;
		try {
			signer = new JcaContentSignerBuilder("Sha512withRSA").build(_ca_priv_key);
		} catch (OperatorCreationException e) {
			_log.catching(Priority.ERROR, e);
			throw new CryptoException("", e);
		}
    	X509CertificateHolder certHolder = certBuilder.build(signer);
		X509Certificate cert;
		try {
			cert = (new JcaX509CertificateConverter()).getCertificate(certHolder);
		} catch (CertificateException e) {
			_log.catching(Priority.ERROR, e);
			throw new CryptoException("", e);
		}
		
    	return cert;
    }
    
    public KeyStore packagePKCS12(User user, String password, KeyPair pair, Certificate cert) throws NullPointerException, CryptoException {
    	if (user == null || password == null || pair == null || cert == null) {
    		NullPointerException e = new NullPointerException();
    		_log.catching(Priority.WARN, e);
    		throw e;
    	}
    	
    	KeyStore ks = null;
    	try {
    		ks = KeyStore.getInstance("PKCS12");
    		ks.load(null);
    	} catch (KeyStoreException | CertificateException | NoSuchAlgorithmException | IOException e) {
    		_log.catching(Priority.ERROR, e);
    		throw new CryptoException("", e);
    	}
    	try {
			ks.setKeyEntry(user.getUsername(), pair.getPrivate(), password.toCharArray(), new Certificate[]{ cert, _ca_cert });
		} catch (KeyStoreException e) {
			_log.catching(Priority.ERROR, e);
			throw new CryptoException("", e);
		}
    	return ks;
    }
}
