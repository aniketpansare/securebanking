package main.java.security.impl;

import java.nio.charset.CharacterCodingException;
import java.security.cert.X509Certificate;
import java.util.Collection;

import javax.servlet.http.HttpServletRequest;

import main.java.domain.Password;
import main.java.domain.User;
import main.java.logging.LoggingManager;
import main.java.logging.LoggingManager.Priority;
import main.java.security.AuthenticationException;
import main.java.service.UserManager;
import main.java.util.TransactionWrapper;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.web.authentication.preauth.x509.SubjectDnX509PrincipalExtractor;


/**
 * @author Luke Taylor
 * @version $Id: X509PreAuthenticatedProcessingFilter.java 2526 2008-01-27 22:45:44Z luke_t $
 */
public class UserAuthenticationImpl implements AuthenticationProvider  {
	@Autowired
	private HttpServletRequest request;
	
	private String principleRegex;
    private LoggingManager log = LoggingManager.create(UserAuthenticationImpl.class.getName());


	@Override
	public Authentication authenticate(Authentication authentication)
			throws AuthenticationException {
		final SubjectDnX509PrincipalExtractor principalExtractor = new SubjectDnX509PrincipalExtractor();
		principalExtractor.setSubjectDnRegex(principleRegex);
		final UsernamePasswordAuthenticationToken auth = (UsernamePasswordAuthenticationToken) authentication;
		final String username = String.valueOf(auth.getPrincipal());
		final String password = String.valueOf(auth.getCredentials());
		
		log.info("Athenticating user '{}'...", username);
		
		String ipAddress = request.getHeader("X-FORWARDED-FOR");  
		if (ipAddress == null) {  
		   ipAddress = request.getRemoteAddr();  
		}

		
		final UserManager um = UserManager.create();
		User u = null;
		
		
		try {
			u = (new TransactionWrapper<User>() {
				@Override
				public User operation(Session s) throws HibernateException,
						RuntimeException {
					return um.getUser(username);
				}
				
			}).execute();
		} catch (RuntimeException e) {
			log.warn("Bad login attempt for user: '{}' from ip '{}'. An error occurred retrieving the user object.", username, ipAddress);
			log.catching(Priority.WARN, e);
			throw new BadCredentialsException("An error occurred during authentication.");
		}
		
		if (u == null) {
			log.info("Bad login attempt for user: '{}' from ip '{}'. The username was invalid.", username, ipAddress);
			throw new BadCredentialsException("Invalid username/password combination.");
		}
		
		// At this point the user needs to be authenticated. Step 1: Lets check for a certificate.
		X509Certificate cert = extractClientCertificate(request);
		
		if (cert != null) {
			log.debug("Testing certificate for user '{}'.", username);
			// Note: Tomcat has already checked the signature and performed the authenticating handshake.
			// Check if it's addressed to the recipient.
			Object o = null;
			try {
				o = (principalExtractor.extractPrincipal(cert));
			} catch (Exception e) {
				log.catching(Priority.ERROR, e);
				cert = null;
			}
			if (cert != null) {
				if (o == null) {
					log.info("Bad certificate for user '{}' from ip '{}'. Unable to extract CN field.", username, ipAddress);
					cert = null;
				} else {
					final String principle = String.valueOf(o);
					if (!principle.equals(username)) {
						// Doesn't match. Just pretend we didn't see it.
						log.info("Bad certificate for user '{}' from ip '{}'. Certificate is addressed to '{}'", username, ipAddress, principle);
						cert = null;
					} else {
						log.debug("Good certificate for user '{}' from ip '{}'. Certificate is addressed to '{}'", username, ipAddress, principle);
					}
				}
			}
		}
		
		// Check if user is enabled.
		if (!u.isEnabled()) {
			log.info("Bad login attempt for user: '{}' from ip '{}'. User is disabled.", username, ipAddress);
			throw new DisabledException("The account has been disabled.");
		}
		
		// Check against certificate enforcement policy.
		if (u.getCertAuthenticationLevel() == User.CertReqLevel.REQ_ON_LOGIN && cert == null) {
			log.info("Bad login attempt for user: '{}' from ip '{}'. No valid certificate provided when one was required.", username, ipAddress);
			throw new BadCredentialsException("A valid RSA certificate is required to login to this account.");
		}
		
		
		// Now authenticate against password
		Password pw = null;
		try {
			pw = Password.create(password, u.getPasswordContainer().getSalt());
		} catch (CharacterCodingException | NullPointerException e) {
			log.info("Bad login attempt for user: '{}' from ip '{}'. Invalid password.", username, ipAddress);
			throw new BadCredentialsException("Invalid password format.");
		}
		
		if (!u.getPasswordContainer().equals(pw)) {
			log.info("Bad login attempt for user: '{}' from ip '{}'. Invalid password.", username, ipAddress);
			throw new BadCredentialsException("Invalid username/password combination.");
		}
		

		
		// Generated the credentials
		final UserCredentials creds;
		if (cert != null) {
			log.debug("Generating Certificate Credential Container");
			creds = new UserCredentials(pw, cert);
		} else {
			log.debug("Generating non-Certificate Credential Container");
			creds = new UserCredentials(pw);
		}
		
		// Get authorizations
		final Collection<GrantedAuthority> auths = u.getAuthorities();
		
		
		// Yay! We're done. Go ahead and return a new authentication token.
		return new UsernamePasswordAuthenticationToken(username, creds, auths); 
	}
    
    private X509Certificate extractClientCertificate(HttpServletRequest request) {
        X509Certificate[] certs = (X509Certificate[]) request.getAttribute("javax.servlet.request.X509Certificate");

        if (certs != null && certs.length > 0) {
            return certs[0];
        }

        return null;
    }
    
    public void setPrincipleRegex(String regex) {
    	principleRegex = regex;
    }
    
    public String getPrincipleRegex() {
    	return principleRegex;
    }
    
    
	@Override
	public boolean supports(Class<?> authentication) {
		return authentication.equals(UsernamePasswordAuthenticationToken.class);
	}
}
