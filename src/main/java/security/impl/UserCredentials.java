package main.java.security.impl;

import java.nio.charset.CharacterCodingException;
import java.security.cert.X509Certificate;

import main.java.domain.Password;

/**
 * Simple container that tracks the user credentials.
 * @author jbabb
 */
public class UserCredentials {

	/********************************************************************/
	/* Members */
	/*******************************************************************/
	/**
	 * The secure password object that was used to authenticate the user with.
	 */
	final private Password password;
	
	/**
	 * Ther certificate that the user supplied (if applicable).
	 */
	final private X509Certificate certificate;
	
	/********************************************************************/
	/* Constructors */
	/*******************************************************************/
	
	/**
	 * Initializes the credentials object with a password.
	 * @param password The password to initialize it with.
	 * @throws CharacterCodingException Thrown if the password isn't an ASCII string.
	 * @throws NullPointerException Thrown if password is null;
	 */
	public UserCredentials(String password) throws CharacterCodingException, NullPointerException {	
		this.password = Password.create(password);
		certificate = null;
	}
	
	/**
	 * Initializes the credentials object with a password.
	 * @param password The password to initialize it with.
	 * @throws NullPointerException Thrown if password is null;
	 */
	public UserCredentials(Password password) throws NullPointerException {	
		this.password = password;
		certificate = null;
	}
	
	
	/**
	 * Initializes the credentials object with a password and a certificate.
	 * @param password The password to initialize it with.
	 * @param cert The certificate to initialize it with.
	 * @throws CharacterCodingException Thrown if the password isn't an ASCII string.
	 * @throws NullPointerException Thrown if password or certificate is null;
	 */
	public UserCredentials(String password, X509Certificate certificate) throws CharacterCodingException, NullPointerException {
		if (password == null || certificate == null) {
			throw new NullPointerException();
		}
		
		this.password = Password.create(password);
		this.certificate = certificate;
	}
	
	/**
	 * Initializes the credentials object with a password and a certificate.
	 * @param password The password to initialize it with.
	 * @param cert The certificate to initialize it with.
	 * @throws CharacterCodingException Thrown if the password isn't an ASCII string.
	 * @throws NullPointerException Thrown if password or certificate is null;
	 */
	public UserCredentials(Password password, X509Certificate certificate) throws NullPointerException {
		if (password == null || certificate == null) {
			throw new NullPointerException();
		}
		
		this.password = password;
		this.certificate = certificate;
	}

	/********************************************************************/
	/* Accessors */
	/*******************************************************************/
	
	/**
	 * @return the password
	 */
	public Password getPassword() {
		return password;
	}

	/**
	 * @return the certificate
	 */
	public X509Certificate getCertificate() {
		return certificate;
	}
	
	public boolean hasCertificate() {
		return certificate != null;
	}
	
	
	
	
	
	
}
