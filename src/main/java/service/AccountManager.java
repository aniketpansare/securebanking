package main.java.service;

import java.io.Serializable;

import main.java.domain.Account;
import main.java.domain.Account.StatusFilter;
import main.java.domain.Department;
import main.java.domain.User;
import main.java.service.impl.AccountManagerImpl;

import org.hibernate.HibernateException;

/**
 * An interface for basic account management.
 * @author jbabb
 */
public abstract class AccountManager implements Serializable {
	private static final long serialVersionUID = 1L;

	/****************************************************************************/
	/* Types */
	/****************************************************************************/
	
	/****************************************************************************/
	/* Static Factory Methods */
	/****************************************************************************/
	/**
	 * Create a new account manager instance.
	 * @return A new account manager instance.
	 */
	@SuppressWarnings("deprecation")
	public static AccountManager create() {
		return new AccountManagerImpl();
	}
	
	
	/****************************************************************************/
	/* Interface Specification */
	/****************************************************************************/
	/**
	 * Creates an account owned by the provided user and registers it. 
	 * @param owner The owner of the account.
	 * @param balance The starting balance of the account.
	 * @return A new account instance owned by owner.
	 * @throws NullPointerException Thrown if owner is null.
	 * @throws HibernateException Thrown if anything goes wrong with the database interaction.
	 * @throws IllegalArgumentException Thrown if balance < 0.
	 */
	public abstract Account createAccount(User owner, double balance) 
			throws HibernateException, NullPointerException, IllegalArgumentException;
	
	/**
	 * Creates an account owned by the provided user and registers it. 
	 * @param owner The owner of the account.
	 * @param balance The starting balance of the account.
	 * @param accountName The name of the account.
	 * @param accountDescription A description of the account.
	 * @return A new account instance owned by owner.
	 * @throws NullPointerException Thrown if any argument is null.
	 * @throws HibernateException Thrown if anything goes wrong with the database interaction.
	 * @throws IllegalArgumentException Thrown if balance < 0.
	 */
	public abstract Account createAccount(User owner, double balance, String accountName, String accountDescription) 
			throws HibernateException, NullPointerException, IllegalArgumentException;
	
	
	/**
	 * Creates an account owned by the provided user and registers it. 
	 * @param owner The owner of the account.
	 * @param balance The starting balance of the account.
	 * @return A new account instance owned by owner.
	 * @throws NullPointerException Thrown if owner is null.
	 * @throws HibernateException Thrown if anything goes wrong with the database interaction.
	 * @throws IllegalArgumentException Thrown if balance < 0.
	 */
	public abstract Account createAccount(Department owner, double balance) 
			throws HibernateException, NullPointerException, IllegalArgumentException;
	
	/**
	 * Creates an account owned by the provided user and registers it. 
	 * @param owner The owner of the account.
	 * @param balance The starting balance of the account.
	 * @param accountName The name of the account.
	 * @param accountDescription A description of the account.
	 * @return A new account instance owned by owner.
	 * @throws NullPointerException Thrown if any argument is null.
	 * @throws HibernateException Thrown if anything goes wrong with the database interaction.
	 * @throws IllegalArgumentException Thrown if balance < 0.
	 */
	public abstract Account createAccount(Department owner, double balance, String accountName, String accountDescription) 
			throws HibernateException, NullPointerException, IllegalArgumentException;
	
	
	/**
	 * Saves the provided account to the database.
	 * @param acct The account to save.
	 * @throws NullPointerException Thrown if acct is null.
	 * @throws HibernateException Thrown if anything goes wrong with the database interaction.
	 */
	public abstract void saveAccount(Account acct) throws HibernateException, NullPointerException;
	
	/**
	 * Marks the provided account as deleted.
	 * @param acct The account to "delete".
	 * @throws NullPointerException Thrown if acct is null.
	 * @throws HibernateException Thrown if anything goes wrong with the database interaction.
	 */
	public abstract void removeAccount(Account acct) throws HibernateException, NullPointerException;
	
	/**
	 * Gets the account object matching the provided ID number.
	 * @param id The Account number to search for.
	 * @return The corresponding account object, or NULL if the account doesn't exist.
	 * @throws HibernateException Thrown if anything goes wrong with the database interaction.
	 */
	public abstract Account getAccount(long id) throws HibernateException;

	/**
	 * Queries for a set of accounts.
	 * @param filter A flag to determine which status (if any) to filter for.
	 * @param first The first index to query for.
	 * @param max The maximum number of entries to retrieve.
	 * @return A set of up to max accounts matching the filter.
	 * @throws NullPointerException Thrown if filter is null.
	 * @throws HibernateException Thrown if a problem occurred interacting with the database.
	 * @throws IllegalArgumentException Thrown if first or max is negative.
	 */
	public abstract Iterable<? extends Account> getAccounts(StatusFilter filter, int first, int max) 
			throws NullPointerException, HibernateException, IllegalArgumentException;
	
 }