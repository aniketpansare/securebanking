package main.java.service;

import main.java.domain.Account;
import main.java.domain.Delegation;
import main.java.domain.Delegation.PrivilegeLevel;
import main.java.domain.User;
import main.java.security.AuthorizationException;
import main.java.service.impl.DelegationManagerImpl;

import org.hibernate.HibernateException;

public abstract class DelegationManager {
	/****************************************************************************/
	/* Types */
	/****************************************************************************/
	
	/****************************************************************************/
	/* Static Factory Methods */
	/****************************************************************************/
	/**
	 * Create a new delegation manager instance.
	 * @return A new delegation manager instance.
	 */
	@SuppressWarnings("deprecation")
	public static DelegationManager create() {
		return new DelegationManagerImpl();
	}
	
	
	/****************************************************************************/
	/* Interface Specification */
	/****************************************************************************/
	
	/**
	 * Initializes a new delegation instance.
	 * @param delegator The user whose requested the delegation.
	 * @param account The account whose privileges are being delegated.
	 * @param delegatedPrivileges The privilege level being delegated.
	 * @param delegatee The individual whose being delegated the privileges.
	 * @throws IllegalArgumentException Throw if delegator, account, or delegatee aren't valid user/account containers.
	 * @throws NullPointerException Thrown if any of the arguments are null.
	 * @throws AuthorizationException Thrown if delegator doesn't own account OR if the delegator isn't a manager in the department that does.
	 * @throws HibernateException Thrown if an error occurs while interacting with the database.
	 */
	public abstract void createDelegation(User delegator, Account account, PrivilegeLevel privilege, User delegatee)
			throws IllegalArgumentException, NullPointerException, AuthorizationException, HibernateException;
	
	/**
	 * Deletes the provided delegation.
	 * @param delegation The delegation to delete.
	 * @throws HibernateException Thrown if an error occurs while interacting with the database.
	 * @throws NullPointerException Thrown if delegation is null.
	 */
	public abstract void removeDelegation(Delegation delegation) throws NullPointerException, HibernateException;
	
	/**
	 * Saves the provided delegation to the database.
	 * @param delegation The delegation to save.
	 * @throws HibernateException Thrown if an error occurs while interacting with the database.
	 * @throws NullPointerException Thrown if delegation is null.
	 */
	public abstract void saveDelegation(Delegation delegation) throws HibernateException, NullPointerException;
	
	/**
	 * Gets the delegation object matching the provided ID number.
	 * @param id The delegation number to search for.
	 * @return The corresponding delegation object, or NULL if the account doesn't exist.
	 * @throws HibernateException Thrown if anything goes wrong with the database interaction.
	 */
	public abstract Delegation getDelegation(long id) throws HibernateException;

}
