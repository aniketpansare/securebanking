package main.java.service;

import java.io.Serializable;
import java.util.Set;

import org.hibernate.HibernateException;

import main.java.domain.Department;
import main.java.domain.User;
import main.java.service.impl.DepartmentManagerImpl;

public abstract class DepartmentManager implements Serializable {
	private static final long serialVersionUID = 1L;

	/****************************************************************************/
	/* Static Factory Methods */
	/****************************************************************************/
	/**
	 * Creates a new department manager instance.
	 */
	@SuppressWarnings("deprecation")
	public static DepartmentManager create() {
		return new DepartmentManagerImpl();
	}
	
	/****************************************************************************/
	/* Interface Specification */
	/****************************************************************************/
	/**
	 * Creates and registers a department with the provided name.
	 * @param name The department's name.
	 * @return The created department.
	 * @throws NullPointerException Thrown if name is null.
	 * @throws HibernateException Thrown if an exception occurs while interacting with the database.
	 */
	public abstract Department createDepartment(String name) throws NullPointerException, HibernateException;
	
	/**
	 * Creates and registers a sub-department with the provided name.
	 * @param parent The parent department.
	 * @param name The department's name.
	 * @return The created department.
	 * @throws NullPointerException Thrown if name is null.
	 * @throws HibernateException Thrown if an exception occurs while interacting with the database.
	 * @throws IllegalArgumentException Thrown if parent is not a supported container type.
	 */
	public abstract Department createDepartment(Department parent, String name) throws NullPointerException, HibernateException, IllegalArgumentException;
	
	/**
	 * Saves the department object to the database.
	 * @param department The department to save.
	 * @throws NullPointerException thrown if department is null.
	 * @throws HibernateException Thrown if an exception occurs while interacting with the database.
	 */
	public abstract void saveDepartment(Department department) throws NullPointerException, HibernateException;
	
	/**
	 * Gets the department with the provided department id.
	 * @param deptId The ID for the department to get.
	 * @return The department with the provided ID or NULL if it doesn't exist.
	 * @throws HibernateException Thrown if an exception occurs while interacting with the database.
	 */
	public abstract Department getDepartment(long deptId) throws HibernateException;
	
	/**
	 * Gets a set of all departments.
	 * @param first The result entry index to return.
	 * @param max The maximum number of result to return.
	 * @return The set of all departments.
	 * @throws HibernateException Thrown if an exception occurs while interacting with the database.
	 * @throws IllegalArgumentException thrown if first or max are negative.
	 */
	public abstract Iterable<? extends Department> getDepartments(int first, int max) throws HibernateException, IllegalArgumentException;

	/**
	 * Gets the department by name.
	 * @param name The name to query for.
	 * @return The department whose name matches the provided name or NULL.
	 * @throws NullPointerException Thrown if name is null.
	 * @throws HibernateException Thrown if an exception occurs while interacting with the database.
	 */
	public abstract Department getDepartment(String name) throws NullPointerException, HibernateException;
	
	/**
	 * Gets a set of departments whose name contains name.
	 * @param name The name substring to query for.
	 * @return The set of departments whose name contains name.
	 * @throws NullPointerException Thrown if name is null.
	 * @throws HibernateException Thrown if an exception occurs while interacting with the database.
	 */
	public abstract Iterable<? extends Department> getDepartmentsLike(String name) throws NullPointerException, HibernateException;

}
