package main.java.service;

import java.io.Serializable;

import main.java.domain.Account;
import main.java.domain.Recipient;
import main.java.domain.User;
import main.java.service.impl.RecipientManagerImpl;

import org.hibernate.HibernateException;

public abstract class RecipientManager implements Serializable {
	private static final long serialVersionUID = 1L;
	
	/****************************************************************************/
	/* Static Factory Methods */
	/****************************************************************************/
	
	/**
	 * Create a new instance of a recipient manager.
	 */
	@SuppressWarnings("deprecation")
	public static RecipientManager create() {
		return new RecipientManagerImpl();
	}
	
	/****************************************************************************/
	/* Methods */
	/****************************************************************************/
	
	/**
	 * Initializes a new recipient.
	 * @param user The user creating the recipient entry.
	 * @param name The nickname of the recipient entry.
	 * @param account The account the entry is pointing to.
	 * @throws NullPointerException Thrown if any of the arguments are null.
	 * @throws IllegalArgumentException Thrown if user or account isn't a valid user/account container.
	 * @throws HibernateException Thrown if an error occurs while interacting with the database.
	 */
	public abstract Recipient createRecipient(User user, String name, Account account) 
		throws NullPointerException, IllegalArgumentException, HibernateException;
	
	/**
	 * Delete the provided recipient entry.
	 * @param recipient The recipient to delete.
	 * @throws NullPointerException Thrown if recipient is null.
	 * @throws HibernateException Thrown if an error occurs while interacting with the database.
	 */
	public abstract void deleteRecipient(Recipient recipient) throws NullPointerException, HibernateException;
	
	/**
	 * Saves the recipient to the databsae.
	 * @param recipient The recipient to save.
	 * @throws NullPointerException Thrown if recipient is null.
	 * @throws HibernateException Thrown if an error occurs while interacting with the database.
	 */
	public abstract void saveRecipient(Recipient recipient) throws NullPointerException, HibernateException;
	

}
