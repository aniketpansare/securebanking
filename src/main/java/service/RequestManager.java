package main.java.service;

import java.io.Serializable;
import java.util.Set;

import main.java.domain.Request;
import main.java.domain.Request.RequestType;
import main.java.domain.User;
import main.java.service.impl.RequestManagerImpl;

import org.hibernate.HibernateException;

public abstract class RequestManager implements Serializable {

private static final long serialVersionUID = 1L;
	
	/****************************************************************************/
	/* Static Factory Methods */
	/****************************************************************************/
	
	/**
	 * Create a new instance of a recipient manager.
	 */
	public static RequestManager create() {
		return new RequestManagerImpl();
	}
	
	/****************************************************************************/
	/* Methods */
	/****************************************************************************/
	
	
	/**
	 * Create a new pending request.
	 * @param requestor The user submitting the request.
	 * @param subject The use whose the subject of the request.
	 * @param type The type of action requested.
	 * @param description A description of the request.
	 * @throws NullPointerException Thrown if any of the arguments are null.
	 * @throws IllegalArgumentException Thrown if requestor or subject aren't supported user containers.
	 * @throws HibernateException Thrown if an error occurs while interacting with the database.
	 */
	public abstract Request createRequest(User requestor, User subject, RequestType type, String description) 
			throws NullPointerException, IllegalArgumentException, HibernateException;
	
	
	/**
	 * Gets the request by request ID #.
	 * @param id The id of the request to get.
	 * @return The request matching the ID or NULL if the request doesn't exist.
	 * @throws HibernateException Thrown if an error occurs while interacting with the database.
	 */
	public abstract Request getRequest(long id);
	
	
	/**
	 * Gets all requests.
	 * @param first The first index to get.
	 * @param max The maximum number of requests to get.
	 * @return The set of all requests
	 */
	public abstract Iterable<? extends Request> getRequests(int first, int max);
	
	/**
	 * Gets all the requests matching the status and type.
	 * @param requestStatus The status to match (or null)
	 * @param requestType The type to match (or null)
	 * @param first The first index to return.
	 * @param max The maximum number of entries to return.
	 * @return The set of all requests matching the provided status and type.
	 * @throws IllegalArgumentException Thrown if first or max is negative.
	 */
	public abstract Iterable<? extends Request> getRequests(Request.Status requestStatus, Request.RequestType requestType, int first, int max) 
			throws IllegalArgumentException;
	
	
	/**
	 * Saves the request to the database.
	 * @param request The request to save.
	 * @throws NullPointerException Thrown if request is null.
	 * @throws HibernateException Thrown if an error occurs while interacting with the database.
	 */
	public abstract void saveRequest(Request request) throws NullPointerException, HibernateException;
	
	
	
}
