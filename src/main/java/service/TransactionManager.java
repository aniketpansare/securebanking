package main.java.service;

import java.io.Serializable;

import main.java.domain.Account;
import main.java.domain.BadTransactionException;
import main.java.domain.Transaction;
import main.java.domain.User;
import main.java.security.AuthorizationException;
import main.java.security.impl.UserCredentials;
import main.java.service.impl.TransactionManagerImpl;

import org.hibernate.HibernateException;

public abstract class TransactionManager implements Serializable {
	
	/****************************************************************************/
	/* Static Factory Methods */
	/****************************************************************************/
	/**
	 * Creates and returns a new transaction manager instance.
	 */
	@SuppressWarnings("deprecation")
	public static TransactionManager create() {
		return new TransactionManagerImpl();
	}
	
	
	/****************************************************************************/
	/* Interface Specification */
	/****************************************************************************/
	/**
	 * Creates and registers a new transaction.
	 * Requires from account delegations to be loaded.
	 * @param user The user that is triggering the transaction.
	 * @param from The account the transaction was sent from.
	 * @param to The destination account. 
	 * @param amount The value being sent.
	 * @return The created transaction.
	 * @throws NullPointerException Thrown if any argument other than creds is null.
	 * @throws IllegalArgumentException Thrown if amount is negative or if user/from/to are not supported container types.
	 * @throws BadTransactionException Thrown if the amount will overdraw the from account.
	 * @throws AuthorizationException Thrown if the user doesn't have privilege to modify the account OR if the user requires a certificate and doesn't have one.
	 * @throws HibernateException Thrown if anything goes wrong interacting with the database.
	 */
	public abstract Transaction createTransaction(User user, UserCredentials creds, Account from, Account to, double amount) 
			throws NullPointerException, IllegalArgumentException, BadTransactionException, AuthorizationException, HibernateException;
	
	/**
	 * Gets an iterable of all transactions associated with an account.
	 * @param acct The account to query for.
	 * @return An iterable of all transactions.
	 * @throws NullPointerException thrown if acct is null.
	 * @throws HibernateException Thrown if anything goes wrong interacting with the database.
	 */
	public abstract Iterable<? extends Transaction> getAccountTransactions(Account acct)
		throws NullPointerException, HibernateException;
	
	/**
	 * Gets a transaction by its transaction number.
	 * @param transnum The transaction number to query for.
	 * @return The transaction corresponding to the transaction number or NULL if it doesn't exist. 
	 * @throws HibernateException Thrown if anything goes wrong interacting with the database.
	 */
	public abstract Transaction getTransaction(long transnum) throws HibernateException;
	
	/**
	 * Saves a transaction to the database.
	 * @param transaction The transaction to save.
	 * @throws NullPointerException Thrown if transaction is null.
	 * @throws HibernateException Thrown if anything goes wrong interacting with the database.s
	 */
	public abstract void saveTransaction(Transaction transaction) throws NullPointerException, HibernateException;
	
	
	/**
	 * Queries for a set of transactions.
	 * @param statusFilter A flag to determine which status (if any) to filter for.
	 * @param first The first index to query for.
	 * @param max The maximum number of entries to retrieve.
	 * @return A set of up to max transactions matching the filter.
	 * @throws NullPointerException Thrown if statusFilter is null.
	 * @throws HibernateException Thrown if a problem occurred interacting with the database.
	 * @throws IllegalArgumentException Thrown if first or max is negative.
	 */
	public abstract Iterable<? extends Transaction> getTransactions(Transaction.StatusFilter statusFilter, int first, int max)
				throws NullPointerException, HibernateException, IllegalArgumentException;
	
	
	
	

}
