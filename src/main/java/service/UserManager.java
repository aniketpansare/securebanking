package main.java.service;

import java.nio.charset.CharacterCodingException;

import main.java.domain.User;
import main.java.domain.UserData;
import main.java.security.AuthenticationException;
import main.java.service.impl.UserManagerImpl;

import org.bouncycastle.crypto.CryptoException;
import org.hibernate.HibernateException;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.mail.MailException;

public abstract class UserManager {

	/***********************************************************************************************/
	/* Static Factory Methods */
	/***********************************************************************************************/
	
	/**
	 * Gets a user manager instance.
	 */
	@SuppressWarnings("deprecation")
	public static UserManager create() {
		return new UserManagerImpl();
	}
	
	/***********************************************************************************************/
	/* Interface Definition */
	/***********************************************************************************************/
	
	/**
	 * Attempts to create a new user and adds them to the database.
	 * @param username The username to register.
	 * @param password The password to use.
	 * @return A new user object for the user.
	 * @throws HibernateException Thrown if a problem occurred saving the user (such as an integrity constraint violation).
	 * @throws NullPointerException Thrown if either argument is null.
	 * @throws CharacterCodingException Thrown if password is not an ASCII string.
	 */
	public abstract User createUser(String username, String password) 
			throws  HibernateException, NullPointerException, CharacterCodingException;
	
	/**
	 * Attempts to create a new user and adds them to the database.
	 * @param username The username to register.
	 * @param password The password to use.
	 * @param data The user's P.I.I.
	 * @return A new user object for the user.
	 * @throws HibernateException Thrown if a problem occurred saving the user (such as an integrity constraint violation).
	 * @throws NullPointerException Thrown if either argument is null.
	 * @throws CharacterCodingException Thrown if password is not an ASCII string.
	 * @throws IllegalArgumentException Thrown id data is not a supported container.
	 */
	public abstract User createUser(String username, String password, UserData data) 
			throws  HibernateException, NullPointerException, CharacterCodingException, IllegalArgumentException;

	/**
	 * Saves the user to the database.
	 * @param user The user to save.
	 * @throws HibernateException Thrown if a problem occurred saving the user (such as an integrity constraint violation).
	 * @throws NullPointerException Thrown if user is null.
	 */
	public abstract void saveUser(User user) throws HibernateException, NullPointerException; 

	/**
	 * Attempts to authenticate a username-password combination.
	 * @param username The username.
	 * @param password The password.
	 * @return The user whose credentials match the provided ones.
	 * @throws HibernateException Thrown if a problem occured retrieving the user.
	 * @throws AuthenticationException Thrown if the user does not exist of the password is incorrect.
	 * @throws NullPointerException Thrown if either argument is null.
	 * @throws CharacterCodingException Thrown is password is not an ASCII string.
	 */
	public abstract User authenticateUser(String username, String password) 
			throws HibernateException, AuthenticationException, NullPointerException, CharacterCodingException;
	
	
	/**
	 * Generates a new one-time-password for the user and attempts to email the token to them.
	 * @param context The application context (used to send the email)
	 * @param user The user to generate the OTP for.
	 * @throws NullPointerException Thrown if any of the arguments are null OR if the user's email address is null.
	 * @throws MailException Thrown if an error occurred while sending the email.
	 * @throws HibernateException Thrown if an error occurred while communicating with the database.
	 */
	public abstract void generatePasswordResetRequest(BeanFactory context, User user) throws NullPointerException, MailException, HibernateException;
	
	/**
	 * Generates the welcome emails for the user containing password reset and certificate instructions.
	 * @param context The application context.
	 * @param user The user to generate teh welcome package for.
	 * @throws NullPointerException Thrown if any of the arguments are null, the user's email is null, or the user's name is null.
	 * @throws MailException Thrown if an error occurred while sending the mail.
	 * @throws CryptoException Thrown if an error occured while generating the user's certificates.
	 * @throws HibernateException If an error occurs while interacting with the database.
	 */
	public abstract void sendWelcomePackage(BeanFactory context, User user) throws NullPointerException, MailException, CryptoException, HibernateException;
	
	
	/**
	 * Gets a user by their userId number.
	 * @param id The user's ID number.
	 * @return The user or NULL if he doesn't exist.
	 * @throws HibernateException Thrown if a problem occurred retrieving the user.
	 */
	public abstract User getUser(long id) throws HibernateException;
	
	/**
	 * Gets a user by their username.
	 * @param username The user's username.
	 * @return The user or NULL if he doesn't exist.
	 * @throws HibernateException thrown if a problem occurred retrieving the user
	 */
	public abstract User getUser(String username) throws HibernateException;
	
	/**
	 * Flags the provided user and all their accounts as deleted.
	 * @param user The user to delete. 
	 * @throws NullPointerException Thrown if user is null.
	 * @throws HibernateException thrown if a problem occurred interacting with the database.
	 */
	public abstract void deleteUser(User user) throws NullPointerException, HibernateException;

	/**
	 * Queries for a set of users.
	 * @param statusFilter A flag to determine which status (if any) to filter for.
	 * @param typeFilter A flag to determine which user types (if any) to filter for.
	 * @param first The first index to query for.
	 * @param max The maximum number of entries to retrieve.
	 * @return A set of up to max users matching the filter.
	 * @throws NullPointerException Thrown if either filter is null.
	 * @throws HibernateException Thrown if a problem occurred interacting with the database.
	 * @throws IllegalArgumentException Thrown if first or max is negative.
	 */
	public abstract Iterable<? extends User> getUsers(User.StatusFilter statusFilter, User.TypeFilter typeFilter, int first, int max)
				throws NullPointerException, HibernateException, IllegalArgumentException;
}
	