package main.java.service.impl;

import main.java.domain.Account;
import main.java.domain.Account.StatusFilter;
import main.java.domain.Department;
import main.java.domain.User;
import main.java.domain.impl.AccountImpl;
import main.java.domain.impl.DepartmentImpl;
import main.java.domain.impl.UserImpl;
import main.java.logging.LoggingManager;
import main.java.logging.LoggingManager.Priority;
import main.java.service.AccountManager;
import main.java.util.HibernateUtil;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.stereotype.Service;

@Service
public class AccountManagerImpl extends AccountManager {
	private static final long serialVersionUID = 1L;
	private LoggingManager log = LoggingManager.create(AccountManagerImpl.class.getName());
	/***********************************************************************/
	/* Constructors */
	/***********************************************************************/
	/**
	 * Default constructor.
	 */
	@Deprecated
	public AccountManagerImpl() {
		// Intentionally left blank.
	}

	/***********************************************************************/
	/* Methods */
	/***********************************************************************/
	
	@Override
	public Account createAccount(User owner, double balance) throws HibernateException,
			NullPointerException, IllegalArgumentException {
		if (owner == null) {
			NullPointerException e = new NullPointerException();
			log.catching(Priority.WARN, e);
			throw e;
		}
		
		Account a = Account.create(owner, balance);
		((UserImpl)owner).addAccount(a);
		saveAccount(a);
		return a;
	}

	@Override
	public Account createAccount(User owner, double balance, String accountName,
			String accountDescription) throws HibernateException,
			NullPointerException, IllegalArgumentException {
		if (owner == null || accountName == null || accountDescription  == null) {
			NullPointerException e = new NullPointerException();
			log.catching(Priority.WARN, e);
			throw e;
		}
		
		Account a = Account.create(owner, balance, accountName, accountDescription);
		((UserImpl)owner).addAccount(a);
		saveAccount(a);
		return a;
	}
	
	
	@Override
	public Account createAccount(Department owner, double balance) throws HibernateException,
			NullPointerException, IllegalArgumentException {
		if (owner == null) {
			NullPointerException e = new NullPointerException();
			log.catching(Priority.WARN, e);
			throw e;
		}
		
		Account a = Account.create(owner, balance);
		((DepartmentImpl)owner).addAccount(a);
		saveAccount(a);
		return a;
	}

	@Override
	public Account createAccount(Department owner, double balance, String accountName,
			String accountDescription) throws HibernateException,
			NullPointerException, IllegalArgumentException {
		if (owner == null || accountName == null || accountDescription  == null) {
			NullPointerException e = new NullPointerException();
			log.catching(Priority.WARN, e);
			throw e;
		}
		
		Account a = Account.create(owner, balance, accountName, accountDescription);
		((DepartmentImpl)owner).addAccount(a);
		saveAccount(a);
		return a;
	}

	@Override
	public void saveAccount(Account acct) throws HibernateException,
			NullPointerException {
		if (acct == null) {
			NullPointerException e = new NullPointerException();
			log.catching(Priority.WARN, e);
			throw e;
		}
		
		Session s = HibernateUtil.getSessionFactory().getCurrentSession();
		s.saveOrUpdate(acct);
	}
	
	@Override
	public void removeAccount(Account acct) throws NullPointerException, HibernateException {
		if (acct == null) {
			NullPointerException e = new NullPointerException();
			log.catching(Priority.WARN, e);
			throw e;
		}
		acct.setAccountStatus(Account.Status.DELETED);
		saveAccount(acct);
	}

	@Override
	public Account getAccount(long id) throws HibernateException {
		Session s = HibernateUtil.getSessionFactory().getCurrentSession();
		Account ret = (Account) s.getNamedQuery(AccountImpl.QUERY.BY_ID).setParameter("id", id).uniqueResult();
		return ret;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Iterable<? extends Account> getAccounts(StatusFilter filter, int first,
			int max) throws NullPointerException, HibernateException,
			IllegalArgumentException {
		if (filter == null) {
			NullPointerException e = new NullPointerException();
			log.catching(Priority.WARN, e);
			throw e;
		}
		if (first < 0 || max < 0) {
			IllegalArgumentException e = new IllegalArgumentException();
			log.catching(Priority.WARN, e);
			throw e;
		}
		
		Session s = HibernateUtil.getSessionFactory().getCurrentSession();
		Query q = null;
		if (!filter.negate) {
			q = s.getNamedQuery(AccountImpl.QUERY.BY_STATUS);
		} else  {
			q = s.getNamedQuery(AccountImpl.QUERY.BY_NOT_STATUS);
		}
		
		q.setParameter("status", (filter.status == null) ? null : filter.status.name());
		
		return (Iterable<? extends Account>)q.setFirstResult(first).setMaxResults(max).list();
	}
	
	
	
}