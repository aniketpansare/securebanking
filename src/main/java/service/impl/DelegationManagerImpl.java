package main.java.service.impl;

import main.java.domain.Account;
import main.java.domain.Delegation;
import main.java.domain.Delegation.PrivilegeLevel;
import main.java.domain.User;
import main.java.domain.impl.AccountImpl;
import main.java.domain.impl.DelegationImpl;
import main.java.domain.impl.UserImpl;
import main.java.logging.LoggingManager;
import main.java.logging.LoggingManager.Priority;
import main.java.security.AuthorizationException;
import main.java.service.DelegationManager;
import main.java.util.HibernateUtil;

import org.hibernate.HibernateException;
import org.hibernate.Session;

public class DelegationManagerImpl extends DelegationManager {
	private LoggingManager log = LoggingManager.create(DelegationManagerImpl.class.getName());
	
	/****************************************************************************/
	/* Types */
	/****************************************************************************/

	/****************************************************************************/
	/* Methods */
	/****************************************************************************/
	
	@Override
	public void createDelegation(User delegator, Account account,
			PrivilegeLevel privilege, User delegatee)
			throws IllegalArgumentException, NullPointerException,
			AuthorizationException, HibernateException {
		
		if (delegator == null || account == null || privilege == null || delegatee == null) {
			NullPointerException e = new NullPointerException();
			log.catching(Priority.WARN, e);
			throw e;
		}
		
		switch (account.getAccountType()) {
		case PERSONAL:
			if (!account.getAccountOwner().equals(delegator)) {
				AuthorizationException e = new AuthorizationException();
				log.catching(Priority.WARN, e);
				throw e;
			}
			break;
		case DEPARTMENT:
			delegator.loadDepartments();
			if (delegator.getUserType() != User.Type.MANAGER || !delegator.getDepartments().contains(account.getAccountOwner())) {
				AuthorizationException e = new AuthorizationException();
				log.catching(Priority.WARN, e);
				throw e;
			}
			break;
		}
		
		
		

		Delegation del = Delegation.create(delegator, account, privilege, delegatee);
		((UserImpl)delegator).addOutgoingDelegation(del);
		((UserImpl)delegatee).addIncomingDelegation(del);
		((AccountImpl)account).addDelegation(del);
		saveDelegation(del);
	}

	@Override
	public void removeDelegation(Delegation delegation)
			throws NullPointerException, HibernateException, IllegalArgumentException {
		if (delegation == null) {
			NullPointerException e = new NullPointerException();
			log.catching(Priority.WARN, e);
			throw e;
		}
		
		((UserImpl)delegation.getCreatedBy()).removeOutgoingDelegation(delegation);
		((UserImpl)delegation.getDelegatedTo()).removeIncomingDelegation(delegation);
		((AccountImpl)delegation.getAccount()).removeDelegation(delegation);
		
		Session s = HibernateUtil.getSessionFactory().getCurrentSession();
		s.delete(delegation);

	}

	@Override
	public void saveDelegation(Delegation delegation)
			throws HibernateException, NullPointerException {
		Session s = HibernateUtil.getSessionFactory().getCurrentSession();
		s.saveOrUpdate(delegation);

	}

	@Override
	public Delegation getDelegation(long id) throws HibernateException {
		Session s = HibernateUtil.getSessionFactory().getCurrentSession();
		Delegation ret = (Delegation) s.getNamedQuery(DelegationImpl.QUERY.BY_ID).setParameter("id", id).uniqueResult();
		return ret;
	}

}
