package main.java.service.impl;

import main.java.domain.Department;
import main.java.domain.impl.DepartmentImpl;
import main.java.logging.LoggingManager;
import main.java.logging.LoggingManager.Priority;
import main.java.service.DepartmentManager;
import main.java.util.HibernateUtil;

import org.hibernate.HibernateException;
import org.hibernate.Session;

public class DepartmentManagerImpl extends DepartmentManager {
	private static final long serialVersionUID = 1L;
	private final LoggingManager log = LoggingManager.create(DepartmentManagerImpl.class.getName());
	
	/****************************************************************************/
	/* Constructors */
	/****************************************************************************/
	/**
	 * Default constructor.
	 */
	@Deprecated
	public DepartmentManagerImpl() {
		// Intentionally left blank 
	}
	
	/****************************************************************************/
	/* Methods */
	/****************************************************************************/

	@Override
	public Department createDepartment(String name)
			throws NullPointerException, HibernateException {
		Department d = Department.create(name);
		saveDepartment(d);
		return d;
	}
	
	@Override
	public Department createDepartment(Department parent, String name)
			throws NullPointerException, HibernateException,
			IllegalArgumentException {
		Department d = Department.create(name);
		d.setParent(parent);
		saveDepartment(d);
		return d;
	}

	@Override
	public void saveDepartment(Department department)
			throws NullPointerException, HibernateException {
		if (department == null) {
			NullPointerException e = new NullPointerException();
			log.catching(Priority.WARN, e);
			throw e;
		}		
		
		Session s = HibernateUtil.getSessionFactory().getCurrentSession();
		s.saveOrUpdate(department);
	}

	@Override
	public Department getDepartment(long deptId) throws HibernateException {
		Session s = HibernateUtil.getSessionFactory().getCurrentSession();
		Department ret = (Department) s.getNamedQuery(DepartmentImpl.QUERY.BY_ID).setParameter("id", deptId).uniqueResult();
		return ret;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Iterable<? extends Department> getDepartments(int first, int max) throws HibernateException, IllegalArgumentException {
		
		if (first < 0 || max < 0) {
			IllegalArgumentException e = new IllegalArgumentException();
			log.catching(Priority.WARN, e);
			throw e;
		}
		
		Session s = HibernateUtil.getSessionFactory().getCurrentSession();
		return (Iterable<? extends Department>)s.getNamedQuery(DepartmentImpl.QUERY.ALL).setFirstResult(first).setMaxResults(max).list();
	}

	@Override
	public Department getDepartment(String name)
			throws NullPointerException, HibernateException {
		
		if (name == null) {
			NullPointerException e = new NullPointerException();
			log.catching(Priority.WARN, e);
			throw e;
		}
		
		Session s = HibernateUtil.getSessionFactory().getCurrentSession();
		return (Department)(s.getNamedQuery(DepartmentImpl.QUERY.BY_NAME).setParameter("name", name)).uniqueResult();
	}

	@SuppressWarnings("unchecked")
	@Override
	public Iterable<? extends Department> getDepartmentsLike(String name)
			throws NullPointerException, HibernateException {
		
		if (name == null) {
			NullPointerException e = new NullPointerException();
			log.catching(Priority.WARN, e);
			throw e;
		}
		
		Session s = HibernateUtil.getSessionFactory().getCurrentSession();
		return (Iterable<DepartmentImpl>)(s.getNamedQuery(DepartmentImpl.QUERY.BY_NAME).setParameter("name", "%" + name + "%")).list();
	}
	
	
	

}
