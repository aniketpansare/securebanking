package main.java.service.impl;

import main.java.domain.Account;
import main.java.domain.Recipient;
import main.java.domain.User;
import main.java.domain.impl.UserImpl;
import main.java.logging.LoggingManager;
import main.java.logging.LoggingManager.Priority;
import main.java.service.RecipientManager;
import main.java.util.HibernateUtil;

import org.hibernate.HibernateException;
import org.hibernate.Session;

public class RecipientManagerImpl extends RecipientManager {
	private static final long serialVersionUID = 1L;
	private final LoggingManager log = LoggingManager.create(RecipientManagerImpl.class.getName());
	
	/****************************************************************************/
	/* Methods */
	/****************************************************************************/

	@Override
	public Recipient createRecipient(User user, String name, Account account)
			throws NullPointerException, IllegalArgumentException,
			HibernateException {
		Recipient r = Recipient.create(user, name, account);
		((UserImpl)user).addRecipient(r);
		saveRecipient(r);
		return r;
	}

	@Override
	public void deleteRecipient(Recipient recipient)
			throws NullPointerException, HibernateException {
		
		if (recipient == null) {
			NullPointerException e = new NullPointerException();
			log.catching(Priority.WARN, e);
			throw e;
		}
		
		((UserImpl)recipient.getUser()).removeRecipient(recipient);
		
		Session s = HibernateUtil.getSessionFactory().getCurrentSession();
		s.delete(recipient);
		
	}

	@Override
	public void saveRecipient(Recipient recipient) throws NullPointerException,
			HibernateException {
		if (recipient == null) {
			NullPointerException e = new NullPointerException();
			log.catching(Priority.WARN, e);
			throw e;
		}
		
		Session s = HibernateUtil.getSessionFactory().getCurrentSession();
		s.save(recipient);
	}

}
