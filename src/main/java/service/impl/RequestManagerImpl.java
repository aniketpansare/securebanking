package main.java.service.impl;

import main.java.domain.Request;
import main.java.domain.Request.RequestType;
import main.java.domain.Request.Status;
import main.java.domain.User;
import main.java.domain.impl.RequestImpl;
import main.java.domain.impl.UserImpl;
import main.java.logging.LoggingManager;
import main.java.logging.LoggingManager.Priority;
import main.java.service.RequestManager;
import main.java.util.HibernateUtil;

import org.hibernate.HibernateException;
import org.hibernate.Session;

public class RequestManagerImpl extends RequestManager {
	private static final long serialVersionUID = 1L;
	private LoggingManager log = LoggingManager.create(RequestManagerImpl.class.getName());
	
	
	@Override
	public Request createRequest(User requestor, User subject,
			RequestType type, String description) throws NullPointerException,
			IllegalArgumentException, HibernateException {
		Request r = Request.create(requestor, subject, type, description);
		((UserImpl)requestor).addRequest(r);
		saveRequest(r);
		return r;
	}

	@Override
	public Request getRequest(long id) {
		Session s = HibernateUtil.getSessionFactory().getCurrentSession();
		return (Request)s.getNamedQuery(RequestImpl.QUERY.BY_ID).setParameter("id", id).uniqueResult();
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public Iterable<? extends Request> getRequests(int first, int max) {
		Session s = HibernateUtil.getSessionFactory().getCurrentSession();
		return (Iterable<? extends Request>) s.getNamedQuery(RequestImpl.QUERY.ALL).setFirstResult(first).setMaxResults(max).list();
	}

	@SuppressWarnings("unchecked")
	@Override
	public Iterable<? extends Request> getRequests(Status requestStatus,
			RequestType requestType, int first, int max) throws IllegalArgumentException {
		
		if (first < 0 || max < 0) {
			IllegalArgumentException e = new IllegalArgumentException();
			log.catching(Priority.WARN, e);
			throw e;
		}
		
		Session s = HibernateUtil.getSessionFactory().getCurrentSession();
		return
			(Iterable<? extends Request>)
				s.getNamedQuery(RequestImpl.QUERY.BY_TYPE_STATUS)
					.setParameter("type", (requestType == null) ? null : requestType.name())
					.setParameter("status", (requestStatus == null) ? null : requestStatus.name())
					.setFirstResult(first)
					.setMaxResults(max).list();
	}

	@Override
	public void saveRequest(Request request) throws NullPointerException,
			HibernateException {
		if (request == null) {
			NullPointerException e = new NullPointerException();
			log.catching(Priority.WARN, e);
			throw e;
		}
		
		Session s = HibernateUtil.getSessionFactory().getCurrentSession();
		s.save(request);
	}

}
