package main.java.service.impl;

import main.java.domain.Account;
import main.java.domain.BadTransactionException;
import main.java.domain.Delegation;
import main.java.domain.Transaction;
import main.java.domain.Transaction.StatusFilter;
import main.java.domain.User;
import main.java.domain.impl.AccountImpl;
import main.java.domain.impl.TransactionImpl;
import main.java.logging.LoggingManager;
import main.java.logging.LoggingManager.Priority;
import main.java.security.AuthorizationException;
import main.java.security.impl.UserCredentials;
import main.java.service.TransactionManager;
import main.java.util.HibernateUtil;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.stereotype.Service;


/**
 * @author AP
 *
 */
@Service
public class TransactionManagerImpl extends TransactionManager {
	private static final long serialVersionUID = 1L;
	private final LoggingManager log = LoggingManager.create(TransactionManagerImpl.class.getName());
	/****************************************************************************/
	/* Constructors */
	/****************************************************************************/
	/**
	 * Default constructor.
	 */
	@Deprecated
	public TransactionManagerImpl() {
		// Intentionally left blank
	}

	/****************************************************************************/
	/* Methods */
	/****************************************************************************/
	
	@Override
	public Transaction createTransaction(User user, UserCredentials creds, Account from, Account to, double amount)
			throws NullPointerException, IllegalArgumentException,
			BadTransactionException, AuthorizationException, HibernateException {
		if (user == null || from == null || to == null) {
			NullPointerException e = new NullPointerException();
			log.catching(Priority.WARN, e);
			throw e;
		}
		
		if (!user.equals(from.getAccountOwner())) {
			
			// Check the delegations for the account.
			boolean authorized = false;
			from.loadDelegations();
			for (Delegation d : from.getDelegations()) {
				if (d.getPrivilege() == Delegation.PrivilegeLevel.MODIFY 
						&& d.getDelegatedTo().equals(user)) {
					authorized = true;
				}
			}
			
			if (!authorized) {
				AuthorizationException e = new AuthorizationException("You are not an authorized user of this account.");
				throw e;
			}
		}
		
		// Verify user credentials
		if ((user.getCertAuthenticationLevel() == User.CertReqLevel.REQ_ON_LOGIN
				|| user.getCertAuthenticationLevel() == User.CertReqLevel.REQ_ON_TRANSACTION)
				&& (creds == null || !creds.hasCertificate())) {
				AuthorizationException e = new AuthorizationException("Invalid or missing certificate when one is required.");
				throw e;
			
		}
		
		// Check whether the accounts are active
		if (from.getAccountStatus() != Account.Status.ACTIVE || to.getAccountStatus() != Account.Status.ACTIVE) {
			BadTransactionException e = new BadTransactionException("Invalid attempt to create transaction to/from an account which has been disabled or no longer exists.");
			throw e;
		}
		
		
		Transaction t = Transaction.create(user, from, to, amount);
		((AccountImpl)from).addTransaction(t);
		((AccountImpl)to).addTransaction(t);
		saveTransaction(t);
		return t;
	}

	@Override
	public Iterable<? extends Transaction> getAccountTransactions(Account acct)
			throws NullPointerException, HibernateException {
		if (acct == null) {
			NullPointerException e = new NullPointerException();
			log.catching(Priority.WARN, e);
			throw e;
		}
		return acct.getTransactions();
	}

	@Override
	public Transaction getTransaction(long transnum) throws HibernateException {
		Session s = HibernateUtil.getSessionFactory().getCurrentSession();
		Transaction ret = (Transaction) s.getNamedQuery(TransactionImpl.QUERY.BY_ID).setParameter("id", transnum).uniqueResult();
		return ret;
	}

	@Override
	public void saveTransaction(Transaction transaction)
			throws NullPointerException, HibernateException {
		if (transaction == null) {
			NullPointerException e = new NullPointerException();
			log.catching(Priority.WARN, e);
			throw e;
		}		
		
		Session s = HibernateUtil.getSessionFactory().getCurrentSession();
		s.saveOrUpdate(transaction);
	}

	@SuppressWarnings("unchecked")
	@Override
	public Iterable<? extends Transaction> getTransactions(
			StatusFilter statusFilter, int first, int max)
			throws NullPointerException, HibernateException,
			IllegalArgumentException {
		if (statusFilter == null) {
			NullPointerException e = new NullPointerException();
			log.catching(Priority.WARN, e);
			throw e;
		}
		if (first < 0 || max < 0) {
			IllegalArgumentException e = new IllegalArgumentException();
			log.catching(Priority.WARN, e);
			throw e;
		}
		
		Session s = HibernateUtil.getSessionFactory().getCurrentSession();
		Query q = null;
		if (!statusFilter.negate) {
			q = s.getNamedQuery(TransactionImpl.QUERY.BY_STATUS);
		} else  {
			q = s.getNamedQuery(TransactionImpl.QUERY.BY_NOT_STATUS);
		}
		
		q.setParameter("status", (statusFilter.status == null) ? null : statusFilter.status.name());
		
		return (Iterable<? extends Transaction>)q
				.setFirstResult(first)
				.setMaxResults(max).list();
	}
	
}
