package main.java.service.impl;

import java.io.Serializable;
import java.nio.charset.CharacterCodingException;
import java.security.KeyStore;

import main.java.domain.Account;
import main.java.domain.Delegation;
import main.java.domain.Department;
import main.java.domain.User;
import main.java.domain.User.CertReqLevel;
import main.java.domain.User.Status;
import main.java.domain.User.Type;
import main.java.domain.UserData;
import main.java.domain.impl.UserImpl;
import main.java.logging.LoggingManager;
import main.java.logging.LoggingManager.Priority;
import main.java.security.AuthenticationException;
import main.java.security.SecurityManager;
import main.java.service.DelegationManager;
import main.java.service.UserManager;
import main.java.util.HibernateUtil;
import main.java.util.MailService;

import org.bouncycastle.crypto.CryptoException;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.mail.MailException;

public class UserManagerImpl extends UserManager implements Serializable {
	private static final long serialVersionUID = 1L;
	private final SecurityManager security = SecurityManager.get();
	private final LoggingManager log = LoggingManager.create(UserManagerImpl.class.getName());
	/***********************************************************************************************/
	/* Constructors */
	/***********************************************************************************************/
	
	/**
	 * Default constructor.
	 */
	@Deprecated
	public UserManagerImpl() {
		// Intentionally left blank
	}
	
	/***********************************************************************************************/
	/* Methods */
	/***********************************************************************************************/
	
	
	@Override
	public User createUser(String username, String password)
			throws HibernateException, NullPointerException, CharacterCodingException  {
		User u = User.create(username, Type.UNVALIDATED, Status.DISABLED, password);
		saveUser(u);
		return u;
	}

	@Override
	public User createUser(String username, String password, UserData data)
			throws HibernateException, NullPointerException, CharacterCodingException, IllegalArgumentException  {
		User u = User.create(username, Type.UNVALIDATED, Status.DISABLED, password, data);
		saveUser(u);
		return u;
	}
	
	@Override
	public void saveUser(User user) throws HibernateException, NullPointerException {
		if (user == null) {
			NullPointerException e = new NullPointerException();
			log.catching(Priority.WARN, e);
			throw e;
		}		
		
		Session s = HibernateUtil.getSessionFactory().getCurrentSession();
		s.saveOrUpdate(user);
	}

	@Override
	public User authenticateUser(String username, String password)
			throws HibernateException, AuthenticationException, NullPointerException, CharacterCodingException {
		
		if (username == null || password == null) {
			NullPointerException e = new NullPointerException();
			log.catching(Priority.WARN, e);
			throw e;
		}
		
		
		try  {
			User u = getUser(username);
			if (u == null || u.getUserType() == Type.UNVALIDATED || u.getStatus() != Status.ACTIVE) {
				// No user found
				throw new AuthenticationException("User does not exist or is not enabled.");
			}
			
			if (u.getPasswordContainer().validate(password)) {
				return u;
			} else { 
				// Incorrect password
				throw new AuthenticationException("User/password combination not found.");
			}
			
		} catch (HibernateException e) {
			log.catching(Priority.WARN, e);
			throw e;
		}
	}
	
	@Override
	public void generatePasswordResetRequest(BeanFactory context, User user) throws NullPointerException, MailException, HibernateException {
		if (context == null || user == null || user.getData().getEmail() == null || user.getData().getName() == null) {
			NullPointerException e = new NullPointerException();
			log.catching(Priority.WARN, e);
			throw e;
		}
		
		// Get a new OTP token
		String token = user.generateOTP();
		
		// Send it off via email
		MailService mailer = (MailService)context.getBean("mailService");
		mailer.sendPasswordResetMail(user, token);
		
		// save the user
		saveUser(user);
	}

	
	@Override
	public void sendWelcomePackage(BeanFactory context, User user) throws NullPointerException, MailException, CryptoException, HibernateException {
		if (context == null || user == null || user.getData().getEmail() == null || user.getData().getName() == null) {
			NullPointerException e = new NullPointerException();
			log.catching(Priority.WARN, e);
			throw e;
		}
		
		// Get our mail service.		
		MailService mailer = (MailService)context.getBean("mailService");
	
		// Generate OTP
		String otp = user.generateOTP();
		
		// Generate user keys
		KeyStore keys = null;
		if (user.getCertAuthenticationLevel() != CertReqLevel.NONE) {
			keys = security.generatePKCS12(user, "");
		}
		
		// Send user welcome email information
		mailer.sendWelcomeMail(user, otp, keys);
		
		saveUser(user);
	}
	
	
	@Override
	public User getUser(long id) throws HibernateException {
		Session s = HibernateUtil.getSessionFactory().getCurrentSession();
		User ret = (User) s.getNamedQuery(UserImpl.QUERY.BY_ID).setParameter("id", id).uniqueResult();
		return ret;
	}
	
	@Override
	public User getUser(String username) throws NullPointerException, HibernateException {
		if (username == null) {
			NullPointerException e = new NullPointerException();
			log.catching(Priority.WARN, e);
			throw e;
		}
		
		Session s = HibernateUtil.getSessionFactory().getCurrentSession();
		User ret = (User) s.getNamedQuery(UserImpl.QUERY.BY_USERNAME).setParameter("username", username).uniqueResult();
		return ret;
	}
	
	@Override
	public void deleteUser(User user) throws NullPointerException, HibernateException {
		if (user == null) {
			NullPointerException e = new NullPointerException();
			log.catching(Priority.WARN, e);
			throw e;
		}
		
		// delete accounts
		for (Account a : user.getAccounts()) {
			a.setAccountStatus(Account.Status.DELETED);
		}
		
		for (Department d : user.getDepartments()) {
			d.removeEmployee(user);
		}
		
		DelegationManager dm = DelegationManager.create();
		
		for (Delegation d : user.getOutgoingDelegations()) {
			dm.removeDelegation(d);
		}
		
		
		// Delete user
		user.setStatus(Status.DELETED);
		saveUser(user);
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public Iterable<? extends User> getUsers(User.StatusFilter statusFilter, User.TypeFilter typeFilter, int first, int max) throws NullPointerException, HibernateException, IllegalArgumentException {
		if (statusFilter == null || typeFilter == null) {
			NullPointerException e = new NullPointerException();
			log.catching(Priority.WARN, e);
			throw e;
		}
		if (first < 0 || max < 0) {
			IllegalArgumentException e = new IllegalArgumentException();
			log.catching(Priority.WARN, e);
			throw e;
		}
		
		Session s = HibernateUtil.getSessionFactory().getCurrentSession();
		Query q = null;
		if (!statusFilter.negate && !typeFilter.negate) {
			q = s.getNamedQuery(UserImpl.QUERY.BY_TYPE_STATUS);
		} else if (!statusFilter.negate && typeFilter.negate) {
			q = s.getNamedQuery(UserImpl.QUERY.BY_NOT_TYPE_STATUS);
		} else if (statusFilter.negate && !typeFilter.negate) {
			q = s.getNamedQuery(UserImpl.QUERY.BY_TYPE_NOT_STATUS);
		} else {
			q = s.getNamedQuery(UserImpl.QUERY.BY_NOT_TYPE_NOT_STATUS);
		}
		
		q.setParameter("status", (statusFilter.status == null) ? null : statusFilter.status.name());
		q.setParameter("type", (typeFilter.type == null) ? null : typeFilter.type.name());
		
		return (Iterable<? extends User>)
				q.setFirstResult(first)
				.setMaxResults(max).list();
	}
	
	
	
	
	

}
