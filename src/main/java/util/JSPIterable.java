package main.java.util;

import java.util.Iterator;

/**
 * JSP compatible iterable wrapper.
 * @author jbabb
 * @param <T>
 */
public class JSPIterable<T> implements Iterable<T> {

    private final Iterable<T> iterable;
    
    public static <T2> JSPIterable<T2> create(Iterable<T2> it) {
    	return new JSPIterable<T2>(it);
    }
    

    public JSPIterable(Iterable<T> iterable) throws NullPointerException {
    	
    	if (iterable == null) {
    		throw new NullPointerException();
    	}
    	
        this.iterable = iterable;
    }

    public Iterator<T> getIterator() {
        return iterable.iterator();
    }

	@Override
	public Iterator<T> iterator() {
		return iterable.iterator();
	}
}