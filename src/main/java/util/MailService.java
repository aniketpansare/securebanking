package main.java.util;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import main.java.domain.Account;
import main.java.domain.User;
import main.java.domain.User.CertReqLevel;
import main.java.logging.LoggingManager;
import main.java.logging.LoggingManager.Priority;

import org.bouncycastle.crypto.CryptoException;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.mail.MailException;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;

/**
 * Service to send OTP email notifications. 
 * @author jbabb
 *
 */
public class MailService 
{
	private static String PASSWORD_RESET_URL = "https://cse545hybrid1.vlab.asu.edu/SecureBanking/ResetPasswordOTP.htm";
    private JavaMailSender mailSender;
    private LoggingManager log = LoggingManager.create(MailService.class.getName());
    
    public void setMailSender(JavaMailSender mailSender) {
    	this.mailSender = mailSender;
    }
 
    /**
     * Sends a one-time-password notification email.
     * @param recipient The recipient to send the notification to.
     * @param OTP the one-time-password to send.
     */
    public void sendPasswordResetMail(User recipient, String OTP)  throws NullPointerException, MailException
    {
        SimpleMailMessage message = new SimpleMailMessage();
        message.setTo(recipient.getData().getEmail());
        message.setSubject("The Bank of the Sundevil: Password Reset Token");
        
        StringBuilder sb = new StringBuilder();
        sb.append("Hello ").append(recipient.getData().getName()).append(",\n\n");
        sb.append("We've received a request to reset your password. Your one time reset token is: ").append(OTP).append("\n");
        sb.append("Please follow the link included below and input the token in order to reset your password.\n\n");
        
        sb.append(PASSWORD_RESET_URL).append("\n\n");
        
        sb.append("Thank you,\n");
        sb.append("The Bank of the Sundevil Team\n");
        message.setText(sb.toString());

        mailSender.send(message);
    }
    
    
    /**
     * Sends a welcome mail package to the user including their account's OTP, account numbers, and certificates.
     * @param recipient The user that the mail is being sent to.
     * @param OTP The one time password to set the user's password.
     * @param keys The certificate/keys for the user account (if applicable)
     * @throws NullPointerException Thrown if recipient or OTP is null OR if keys is null and the user requires a certificate.
     * @throws MailException Thrown if an error occurs sending the mail.
     * @throws CryptoException thrown if an error occurs serializing the keystore.
     */
    public void sendWelcomeMail(User recipient, String OTP, KeyStore keys) throws NullPointerException, MailException, CryptoException {
    	
    	MimeMessage message = mailSender.createMimeMessage();

    	try {
    		MimeMessageHelper helper = null;
    		if (recipient.getCertAuthenticationLevel() != CertReqLevel.NONE)
    			helper = new MimeMessageHelper(message, true);
    		else
    			helper = new MimeMessageHelper(message, false);
    		
    		helper.setFrom("bankofthesundevil@gmail.com");
    		helper.setTo(recipient.getData().getEmail());
            helper.setSubject("The Bank of the Sundevil: New Account");
            
            StringBuilder sb = new StringBuilder();
            sb.append("Hello ").append(recipient.getData().getName()).append(",\n\n");
            
            switch (recipient.getUserType()) {
            case ADMIN:
            case CORPORATE:
            case MANAGER:
            case EMPLOYEE:
            	sb.append("Welcome to the Bank of the Sundevil team!");
            	if (!recipient.isEnabled()) {
                	sb.append("Your account is currently awaiting confirmation by an administrator.");
                } else {
                	sb.append("Your online account has been created and is ready for use.");
                }
            	sb.append("In order to login, please install the attached certificate in your browser.\n\n");
                break;
            	
            case CUSTOMER:
            case MERCHANT:
            	sb.append("Thank you for signing up for our online banking service!");
                if (!recipient.isEnabled()) {
                	sb.append("Your account is currently awaiting confirmation by an administrator.\n\n");
                }
            	sb.append("Your account numbers are:\n");
            	for (Account a : recipient.getAccounts()) {
            		sb.append("\t").append(a.getAccountName()).append(": ").append(a.getAccountNo()).append("\n");
            	}
            	sb.append("\n\n");
            	
            	if (recipient.getUserType() == User.Type.MERCHANT) {
            		sb.append("In order to process transactions you will need to install the attached certificate in your browser.\n\n");
            	}
            	
                break;
            case UNVALIDATED:
            	sb.append("Thank you for signing up for our online banking service! Your account is currently awaiting confirmation by an administrator.\n\n");
            	break;
            }
            
            
            
            sb.append("In addition, you will need to follow the link below and input your one-time password reset token \"")
            	.append(OTP).append("\"").append("In order to set your password.\n\n");
        	sb.append(PASSWORD_RESET_URL).append("\n\n");
            
            sb.append("Thank you,\n");
            sb.append("The Bank of the Sundevil Team\n");
            
            helper.setText(sb.toString());

            if (recipient.getCertAuthenticationLevel() != CertReqLevel.NONE) {
	            ByteArrayOutputStream bos = new ByteArrayOutputStream(4096);
	            try {
	            	keys.store(bos, "".toCharArray());
	            } catch (KeyStoreException | NoSuchAlgorithmException | IOException | CertificateException e) {
	            	throw new CryptoException("", e);
	            }
	            byte[] bytes = bos.toByteArray();
	            
	            helper.addAttachment(recipient.getUsername() + ".p12", new ByteArrayResource(bytes));
            }
    		
    	} catch (MessagingException e) {
    		// This shouldn't happen.
    		log.catching(Priority.ERROR, e);
    	}
        

        mailSender.send(message);
    }
    
    public void sendNewAccountMail(User recipient, long accountNo)  throws MailException
    {
        SimpleMailMessage message = new SimpleMailMessage();
        message.setTo(recipient.getData().getEmail());
        message.setSubject("The Bank of the Sundevil: New Account");
        
        StringBuilder sb = new StringBuilder();
        sb.append("Hello ").append(recipient.getData().getName()).append(",\n\n");
        sb.append("Thank you for opening a new account with us. Your account No is: ").append(accountNo).append("\n");
            
        sb.append("Thank you,\n");
        sb.append("The Bank of the Sundevil Team\n");
        message.setText(sb.toString());

        mailSender.send(message);
    }
    
    public void sendNotifyMail(User recipient, String msg)  throws MailException
    {
        SimpleMailMessage message = new SimpleMailMessage();
        message.setTo(recipient.getData().getEmail());
        message.setSubject("The Bank of the Sundevil: Notification");
        
        StringBuilder sb = new StringBuilder();
        sb.append("Hello ").append(recipient.getData().getName()).append(",\n\n");
        sb.append(msg).append("\n");
            
        sb.append("Thank you,\n");
        sb.append("The Bank of the Sundevil Team\n");
        message.setText(sb.toString());

        mailSender.send(message);
    }
}