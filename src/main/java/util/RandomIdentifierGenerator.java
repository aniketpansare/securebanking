package main.java.util;

import java.io.Serializable;
import java.util.Properties;

import main.java.domain.Identifier;
import main.java.security.SecurityManager;

import org.hibernate.HibernateException;
import org.hibernate.MappingException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.dialect.Dialect;
import org.hibernate.engine.spi.SessionImplementor;
import org.hibernate.id.Configurable;
import org.hibernate.id.IdentifierGenerator;
import org.hibernate.type.Type;


/**
 * Custom generator used to create a random unique identifier.
 * @author jbabb
 *
 */
public class RandomIdentifierGenerator implements IdentifierGenerator, Configurable {

	private final SecurityManager s_security = SecurityManager.get();
	
	/**********************************************************************************/
	/* Members */
	/*********************************************************************************/
	
	private String table;
	   
	
	/**********************************************************************************/
	/* Methods */
	/*********************************************************************************/
	
	@Override
	public void configure( Type arg0, Properties props, Dialect arg2 ) throws MappingException {  
		setTable( props.getProperty( "table" ) );  
	} 
	
	
	@Override
	public Serializable generate(SessionImplementor session, Object  object)
			throws HibernateException {
		Identifier id = null;
		Session s = HibernateUtil.getSessionFactory().openSession();
		
		Transaction t = null;
		while (id == null) {
			// Attempt to generate an ID and add it to the DB.
			id = Identifier.create(Math.abs(s_security.randomLong()), getTable());
			try {
				t = s.beginTransaction();
		        s.save(id);
		        t.commit();
			} catch (org.hibernate.exception.ConstraintViolationException e) {
				t.rollback();
				id = null;
			}
			
		}
		
		
		return id.getIdentifier();
	}
	
	/**
	 * @return the tableName
	 */
	public String getTable() {
		return table;
	}
	
	
	/**
	 * @param tableName the tableName to set
	 */
	public void setTable(String table) {
		this.table = table;
	}

	
	
}
