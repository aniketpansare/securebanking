package main.java.util;

import main.java.logging.LoggingManager;
import main.java.logging.LoggingManager.Priority;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 * A basic wrapper class to encapsulate transaction operations.
 * @author jbabb
 * @param <T> The type (if any) returned by the operation.
 *
 */
public abstract class TransactionWrapper<T> {

	private final LoggingManager log = LoggingManager.create(TransactionWrapper.class.getName());
	
	/*****************************************************************************/
	/* Members */
	/*****************************************************************************/
	
	/**
	 * The session we're working with (or null for current session).
	 */
	private Session session;
	
	/*****************************************************************************/
	/* Constructors */
	/*****************************************************************************/
	/**
	 * Default constructor. Transaction is within the current session.
	 */
	public TransactionWrapper() {
		session = null;
	}
	
	/**
	 * Construct a transaction within the provided session.
	 * @param session The session to execute the transaction within.
	 */
	public TransactionWrapper(Session session) {
		this.session = session;
	}
	
	/*****************************************************************************/
	/* Interface Specification */
	/*****************************************************************************/
	
	/**
	 * The encapsulated transaction operation.
	 */
	public abstract T operation(Session s) throws HibernateException, RuntimeException;
	
	/*****************************************************************************/
	/* Methods */
	/*****************************************************************************/
	/**
	 * Executes the transaction operation.
	 * @return The results of the operation.
	 * @throws HibernateException Thrown if an error occurs while interacting with the database.
	 * @throws RuntimeException Thrown if a non-hibernate exception occurs during the transaction.
	 */
	public T execute() throws HibernateException, RuntimeException {
		T ret = null;
		Session s = null;
		if (session == null) s = HibernateUtil.getSessionFactory().getCurrentSession();
		else s = session;
		
		Transaction t = null;
		try {
			t = s.beginTransaction();
			ret = operation(s);
			t.commit();
			return ret;
		} catch (HibernateException e) {
			log.catching(Priority.INFO, e);
			if (t != null) t.rollback();
			throw e;
		} catch (RuntimeException e ) {
			log.catching(Priority.INFO, e);
			if (t != null) t.rollback();
			throw e;
		}
		
	}
	
}
