package test.java.domain;

import main.java.domain.impl.AccountImpl;
import junit.framework.TestCase;

/**
 * @author AP
 *
 */
public class AccountTests extends TestCase {

    private AccountImpl account;

    protected void setUp() throws Exception {
    	account = new AccountImpl();
    }

    public void testSetAndGetDescription() {
        String testDescription = "aDescription";
        assertNull(account.getAccountDescription());
        account.setAccountDescription(testDescription);
        assertEquals(testDescription, account.getAccountDescription());
    }

    public void testSetAndGetPrice() {
   //     Long accountNo = 123212004322L;
  //      assertEquals(0, 0, 0);    
 //       account.setAccountNo(accountNo);
//        assertEquals(accountNo, account.getAccountNo(), 0);
    }
  
}