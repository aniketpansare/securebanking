package test.java.domain;

import java.nio.charset.CharacterCodingException;

import junit.framework.TestCase;
import main.java.domain.Password;
import main.java.security.SecurityManager;

import org.junit.Assert;
import org.junit.Test;


public class PasswordTests extends TestCase {

	final SecurityManager s_security = SecurityManager.get();
	
	final String[] _crypto_cases = {
	        "sam@iam.net",
	         "); DROP TABLE lol; --",
	         "",
	         "5",
	         "555555-5555",
	         "asd-asd-asdf",
	         "(asd)asd-asdf",
	         "asd-asdasdf",
	         "asdasdasdf",
	         "(555)55$-5444",
	         "555a555a5555",
	         "5asdf212341",
	         "55555555555",
	         "(5555)555-5555",
	         "(555)5555-555",
	         "(555)555-55555",
	         "(555)55-55555",
	         "(555)555-55",
	         "555555555",
	         "(555)5555555",
	         "5555-5555555",
	         "555-555555",
	         "555555-5555",
	         "(555)555-5555",
	         "555-555-5555",
	         "5555555555",
	         "555-5555555",
	         "",
	         "); DROP TABLE lol; --",
	         "123asd",
	         " 123",
	         "123 ",
	         "sd",
	         "123_asdf",
	         "1235%",
	         "1#12",
	         "#1",
	         ".32",
	         ".332",
	         "123.23",
	         "123.001",
	         "0.32",
	         "123",
	         "5",
	         "100.00",
	         "$100.00",
	         "$0.99",
	         "as.ds", 
	         "#100.00",
	         "100@00",
	         "not@",
	         "a123123",
	         "samus",
	         "babb",
	         "babb517",
	         "as.ds",
	         "100.00",
	         "a_b",
	         "1337",
	         "",
	         "_",
	         "#YOLO",
	         "@.com",
	         "lol",
	         "123lol",
	         "asdf",
	         "$asdf",
	         ".00",
	         "100.000",
	         "100.010",
	         "",
	         "$1",
	         "1",
	         "0",
	         "@notemail.com",
	         "@not.email.com",     
	         "not@@email.com",
	         "not_email.com",
	         "not_email_com",
	         "@notemail.com",
	         "notemail@com",
	         "not@email..com",
	         "not<email.com",
	         "not@ema'il.com",
	         "n(o)t@email.com",
	         "!@notemail.com",
	         "&@not_email.com",
	         "@notemail.com",
	         "not@email.com:12312",
	         "not@email.com:asdf",
	         "123@an_email.com",
	         "this@is.an.email.com",
	         "this@isemail.net",
	         "this_is@an.email.com",
	         "); DROP TABLE lol; --",
	         "__L33THCKR__",
	         "xX1337H@CKRXx",
	         "this><issparta",
	         "What's my username?",
	         "Something Else",
	         "sam$$iam",
	         "sam'sname",
	         "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa",
	         "sam@iam.net",
	         "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa",
	         "unus_invictus",
	         "$100.001",
	         "); DROP TABLE lol; --",
	         "100000000000000000000000000000000000000000.00",
	         "100000000000000000000000000000000",
	         "10000000000000000000000000000000",
	         "$1231232",
	         "not_email_com",
	         "@notemail.com",
	         "notemail@com",
	         "not@email..com",
	         "not<email.com",
	         "not@ema'il.com",
	         "n(o)t@email.com",
	         "!@notemail.com",
	         "&@not_email.com",
	         "@notemail.com",
	         "not@email.com:12312",
	         "not@email.com:asdf",
	         "123@an_email.com",
	         "this@is.an.email.com",
	         "this@isemail.net",
	    };
	
	
	
	
	public void setUp() {
		
	}
	
	@Test
	public void testPassword() {
		Password[] pws = new Password[_crypto_cases.length];
		
		// Test password hashing and salting.
		for (int i = 0; i < pws.length; i++ ) {
			try {
				pws[i] = Password.create(_crypto_cases[i]);
			} catch (CharacterCodingException e) {
				Assert.assertTrue(false);
			}
			
			for (int j = 0; j < i; j++) {
				Assert.assertTrue(!pws[i].equals(pws[j]));
			}
		}
		
		// test encoding
		for (int i = 0; i < pws.length; i++ ) {
			try {
				String encoded = pws[i].getEncodedString();
				Password decoded = Password.fromEncodedString(encoded);
				Assert.assertEquals(pws[i], decoded);
			} catch (NullPointerException | IllegalArgumentException e) {
				Assert.assertTrue(false);
			}
		}
		
		
		// Test password validation
		for (int i = 0; i < pws.length; i++) {
			for (int j = 0; j < pws.length; j++) {
				if (_crypto_cases[i].equals(_crypto_cases[j])) {
					try {
						Assert.assertTrue(pws[i].validate(_crypto_cases[j]));
					} catch (NullPointerException | CharacterCodingException e) {
						Assert.assertTrue(false);
					}
				} else {
					try {
						Assert.assertTrue(!pws[i].validate(_crypto_cases[j]));
					} catch (NullPointerException | CharacterCodingException e) {
						Assert.assertTrue(false);
					}
				}
			}
		}
		
		// Test passwords with fixed salt.
		byte[] salt = new byte[16];
		s_security.randomBytes(salt);
		
		// Test password hashing and salting.
		for (int i = 0; i < pws.length; i++ ) {
			try {
				pws[i] = Password.create(_crypto_cases[i], salt);
			} catch (CharacterCodingException e) {
				Assert.assertTrue(false);
			}
			
			for (int j = 0; j < i; j++) {
				if (_crypto_cases[i].equals(_crypto_cases[j])) {
					try {
						Assert.assertTrue(pws[i].equals(pws[j]));
					} catch (NullPointerException e) {
						Assert.assertTrue(false);
					}
				} else {
					try {
						Assert.assertTrue(!pws[i].equals(pws[j]));
					} catch (NullPointerException e) {
						Assert.assertTrue(false);
					}
				}
			}
		}
		
		
		
		
		
		
		
		
		
	}
	
	
	
}
