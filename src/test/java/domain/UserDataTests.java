package test.java.domain;

import javax.crypto.BadPaddingException;

import junit.framework.TestCase;
import main.java.domain.UserData;
import main.java.domain.impl.UserDataImpl;
import main.java.security.SecurityManager;

import org.junit.Assert;
import org.junit.Test;


public class UserDataTests extends TestCase {

	final SecurityManager security =  SecurityManager.get();
	
	@Override
	public void setUp() {
		
	}
	
	@Test
	public void test() {
		UserDataImpl dat = new UserDataImpl();
		byte[] enc_dat = dat.getEncryptedData();

		try {
			UserData dat2 = new UserDataImpl(enc_dat);

			Assert.assertTrue(dat.equals(dat2));
			Assert.assertEquals("",dat2.getAddress());
			Assert.assertEquals("",dat2.getContactNumber());
			Assert.assertEquals("",dat2.getName());
			Assert.assertEquals("",dat2.getEmail());
			Assert.assertEquals("",dat2.getSsn());
			
		} catch (IllegalArgumentException | NullPointerException
				| BadPaddingException e) {
			Assert.assertTrue(false);
		}
		
		dat.setAddress("123 Fake Street");
		dat.setContactNumber("(555) 555-5555");
		dat.setEmail("foo@shmoe.com");
		dat.setName("Joe Schmoe");
		dat.setSsn("1");
		
		
		enc_dat = dat.getEncryptedData();
		
		try {
			UserData dat2 = new UserDataImpl(enc_dat);
			
			Assert.assertTrue(dat.equals(dat2));
			Assert.assertTrue(dat2.getAddress().equals(dat.getAddress()));
			Assert.assertTrue(dat2.getContactNumber().equals(dat2.getContactNumber()));
			Assert.assertTrue(dat2.getName().equals(dat.getName()));
			Assert.assertTrue(dat2.getSsn().equals(dat.getSsn()));
			
		} catch (IllegalArgumentException | NullPointerException
				| BadPaddingException e) {
			Assert.assertTrue(false);
		}
		
	}
	
}
