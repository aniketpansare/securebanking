package test.java.logging;

import java.util.Arrays;
import java.util.List;

import main.java.domain.LogEntry;
import main.java.domain.impl.LogEntryImpl;
import main.java.logging.LoggingManager;
import main.java.logging.LoggingManager.Priority;

import org.junit.Assert;


public class LoggingManagerTest {

	final LoggingManager log = LoggingManager.create(LoggingManagerTest.class.getName());
	
	protected void setUp() {
		
	}
	
	public void test() {

		String message = "Hi";
		
		log.trace("This doesn't get placed in the database log");
		log.debug("Neither does this.");
		log.info("But this does.");
		log.info("Message: '{}'", message);
		
		log.warn("Everything is good!");
		Exception e = new Exception();
		log.catching(Priority.ERROR, e);
		
		
		List<? extends LogEntry> logs = log.query(null, "DEBUG",  null, null, null, 0, 0);
		Assert.assertTrue(logs.size() == 0);
		
		logs = log.query(LoggingManagerTest.class.getName(), "WARN",  "asdf", null, null, 0, 0);
		Assert.assertTrue(logs.size() == 0);
		
		logs = log.query(LoggingManagerTest.class.getName(),  null, "asdf", null, null, 0, 0);
		
		
		logs = log.query(LoggingManagerTest.class.getName(), null, "thing", null, null, 0, 0);
		Assert.assertTrue(logs.size() > 0);
		
		log.info("Query output: " + Arrays.toString(logs.toArray()));
		
	}

}
