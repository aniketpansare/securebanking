package test.java.security;


import java.nio.charset.CharacterCodingException;
import java.security.InvalidKeyException;
import java.util.Arrays;

import javax.crypto.BadPaddingException;
import javax.crypto.SecretKey;

import org.junit.Assert;

import junit.framework.TestCase;
import main.java.security.SecurityManager;
import main.java.security.impl.SecurityManagerImpl;

public class SecurityManagerTest extends TestCase {

    private SecurityManager _security;

    
    final String[] _crypto_cases = {
        "sam@iam.net",
         "); DROP TABLE lol; --",
         "",
         "5",
         "555555-5555",
         "asd-asd-asdf",
         "(asd)asd-asdf",
         "asd-asdasdf",
         "asdasdasdf",
         "(555)55$-5444",
         "555a555a5555",
         "5asdf212341",
         "55555555555",
         "(5555)555-5555",
         "(555)5555-555",
         "(555)555-55555",
         "(555)55-55555",
         "(555)555-55",
         "555555555",
         "(555)5555555",
         "5555-5555555",
         "555-555555",
         "555555-5555",
         "(555)555-5555",
         "555-555-5555",
         "5555555555",
         "555-5555555",
         "",
         "); DROP TABLE lol; --",
         "123asd",
         " 123",
         "123 ",
         "sd",
         "123_asdf",
         "1235%",
         "1#12",
         "#1",
         ".32",
         ".332",
         "123.23",
         "123.001",
         "0.32",
         "123",
         "5",
         "100.00",
         "$100.00",
         "$0.99",
         "as.ds", 
         "#100.00",
         "100@00",
         "not@",
         "a123123",
         "samus",
         "babb",
         "babb517",
         "as.ds",
         "100.00",
         "a_b",
         "1337",
         "",
         "_",
         "#YOLO",
         "@.com",
         "lol",
         "123lol",
         "asdf",
         "$asdf",
         ".00",
         "100.000",
         "100.010",
         "",
         "$1",
         "1",
         "0",
         "@notemail.com",
         "@not.email.com",     
         "not@@email.com",
         "not_email.com",
         "not_email_com",
         "@notemail.com",
         "notemail@com",
         "not@email..com",
         "not<email.com",
         "not@ema'il.com",
         "n(o)t@email.com",
         "!@notemail.com",
         "&@not_email.com",
         "@notemail.com",
         "not@email.com:12312",
         "not@email.com:asdf",
         "123@an_email.com",
         "this@is.an.email.com",
         "this@isemail.net",
         "this_is@an.email.com",
         "); DROP TABLE lol; --",
         "__L33THCKR__",
         "xX1337H@CKRXx",
         "this><issparta",
         "What's my username?",
         "Something Else",
         "sam$$iam",
         "sam'sname",
         "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa",
         "sam@iam.net",
         "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa",
         "unus_invictus",
         "$100.001",
         "); DROP TABLE lol; --",
         "100000000000000000000000000000000000000000.00",
         "100000000000000000000000000000000",
         "10000000000000000000000000000000",
         "$1231232"
    };
    
    @Override
    protected void setUp() throws Exception {
    	_security = new SecurityManagerImpl();
    }
    
    public void test_isAmount() {
        Assert.assertTrue(!_security.isAmount("asdf"));
        Assert.assertTrue(!_security.isAmount("$asdf"));
        Assert.assertTrue(!_security.isAmount(".00"));
        Assert.assertTrue(!_security.isAmount("100.000"));
        Assert.assertTrue(!_security.isAmount("100.010"));
        Assert.assertTrue(!_security.isAmount("$100.001"));
        Assert.assertTrue(!_security.isAmount("#100.00"));
        Assert.assertTrue(!_security.isAmount("100@00"));
        Assert.assertTrue(!_security.isAmount("); DROP TABLE lol; --"));
        Assert.assertTrue(!_security.isAmount("as.ds"));
        Assert.assertTrue(!_security.isAmount(""));
        Assert.assertTrue(!_security.isAmount("100000000000000000000000000000000000000000.00"));
        Assert.assertTrue(!_security.isAmount("100000000000000000000000000000000"));
        
        Assert.assertTrue(_security.isAmount("10000000000000000000000000000000"));
        Assert.assertTrue(_security.isAmount("100.00"));
        Assert.assertTrue(_security.isAmount("$100.00"));
        Assert.assertTrue(_security.isAmount("$0.99"));
        Assert.assertTrue(_security.isAmount("$0.9"));
        Assert.assertTrue(_security.isAmount("$1231232"));
        Assert.assertTrue(_security.isAmount("$1"));
        Assert.assertTrue(_security.isAmount("1"));
        Assert.assertTrue(_security.isAmount("0"));
        Assert.assertTrue(_security.isAmount("10.1"));
        
        boolean good = false;
        try {
            _security.isAmount(null);
        } catch (NullPointerException e) {
            good = true;
        }
        Assert.assertTrue(good);
    }
   
    public void test_isUsername() {
        Assert.assertTrue(!_security.isUsername("123lol"));
        Assert.assertTrue(!_security.isUsername("lol"));
        Assert.assertTrue(!_security.isUsername("); DROP TABLE lol; --"));
        Assert.assertTrue(!_security.isUsername("#YOLO"));
        Assert.assertTrue(!_security.isUsername("__L33THCKR__"));
        Assert.assertTrue(!_security.isUsername("xX1337H@CKRXx"));
        Assert.assertTrue(!_security.isUsername("_"));
        Assert.assertTrue(!_security.isUsername("this><issparta"));
        Assert.assertTrue(!_security.isUsername("What's my username?"));
        Assert.assertTrue(!_security.isUsername("Something Else"));
        Assert.assertTrue(!_security.isUsername("a_b"));
        Assert.assertTrue(!_security.isUsername("1337"));
        Assert.assertTrue(!_security.isUsername(""));
        Assert.assertTrue(!_security.isUsername("as.ds"));
        Assert.assertTrue(!_security.isUsername("100.00"));
        Assert.assertTrue(!_security.isUsername("sam$$iam"));
        Assert.assertTrue(!_security.isUsername("sam'sname"));
        Assert.assertTrue(!_security.isUsername("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"));
        Assert.assertTrue(!_security.isUsername("sam@iam.net"));
        
        Assert.assertTrue(_security.isUsername("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"));
        Assert.assertTrue(_security.isUsername("samus"));
        Assert.assertTrue(_security.isUsername("babb"));
        Assert.assertTrue(_security.isUsername("babb517"));
        Assert.assertTrue(_security.isUsername("unus_invictus"));
        Assert.assertTrue(_security.isUsername("a123123"));
        
        
        boolean good = false;
        try {
            _security.isUsername(null);
        } catch (NullPointerException e) {
            good = true;
        }
        Assert.assertTrue(good);
        
    }
    
    public void test_isEmail() {
        Assert.assertTrue(!_security.isEmail(""));
	Assert.assertTrue(!_security.isEmail("@notemail.com"));
        Assert.assertTrue(!_security.isEmail("@not.email.com"));     
        Assert.assertTrue(!_security.isEmail("not@@email.com"));
        Assert.assertTrue(!_security.isEmail("not_email.com"));
        Assert.assertTrue(!_security.isEmail("not_email_com"));
        Assert.assertTrue(!_security.isEmail("@notemail.com"));
        Assert.assertTrue(!_security.isEmail("notemail@com"));
        Assert.assertTrue(!_security.isEmail("not@email..com"));
        Assert.assertTrue(!_security.isEmail("not@"));
        Assert.assertTrue(!_security.isEmail("not<email.com"));
        Assert.assertTrue(!_security.isEmail("not@ema'il.com"));
        Assert.assertTrue(!_security.isEmail("@.com"));
        Assert.assertTrue(!_security.isEmail("n(o)t@email.com"));
        Assert.assertTrue(!_security.isEmail("!@notemail.com"));
        Assert.assertTrue(!_security.isEmail("&@not_email.com"));
        Assert.assertTrue(!_security.isEmail("@notemail.com"));
        Assert.assertTrue(!_security.isEmail("not@email.com:12312"));
        Assert.assertTrue(!_security.isEmail("not@email.com:asdf"));
        Assert.assertTrue(!_security.isEmail("123@an_email.com"));
        Assert.assertTrue(!_security.isEmail("); DROP TABLE lol; --"));
        
        Assert.assertTrue(_security.isEmail("this@is.an.email.com"));
        Assert.assertTrue(_security.isEmail("this@isemail.net"));
        Assert.assertTrue(_security.isEmail("this_is@an.email.com"));
        
        
        boolean good = false;
        try {
            _security.isEmail(null);
        } catch (NullPointerException e) {
            good = true;
        }
        Assert.assertTrue(good);
        
    }
    
    public void test_isPassword() {
        Assert.assertTrue(!_security.isPassword("100.00"));
        Assert.assertTrue(!_security.isPassword("$100.00"));
        Assert.assertTrue(!_security.isPassword("$0.99"));
        Assert.assertTrue(!_security.isPassword("as.ds")); 
        Assert.assertTrue(!_security.isPassword("#100.00"));
        Assert.assertTrue(!_security.isPassword("100@00"));
        Assert.assertTrue(!_security.isPassword("not@"));
        Assert.assertTrue(!_security.isPassword("a123123"));
        Assert.assertTrue(!_security.isPassword("samus"));
        Assert.assertTrue(!_security.isPassword("babb"));
        Assert.assertTrue(!_security.isPassword("babb517"));
        Assert.assertTrue(!_security.isPassword("as.ds"));
        Assert.assertTrue(!_security.isPassword("100.00"));
        Assert.assertTrue(!_security.isPassword("a_b"));
        Assert.assertTrue(!_security.isPassword("1337"));
        Assert.assertTrue(!_security.isPassword(""));
        Assert.assertTrue(!_security.isPassword("_"));
        Assert.assertTrue(!_security.isPassword("#YOLO"));
        Assert.assertTrue(!_security.isPassword("@.com"));
        Assert.assertTrue(!_security.isPassword("lol"));
        Assert.assertTrue(!_security.isPassword("123lol"));
        Assert.assertTrue(!_security.isPassword("asdf"));
        Assert.assertTrue(!_security.isPassword("$asdf"));
        Assert.assertTrue(!_security.isPassword(".00"));
        Assert.assertTrue(!_security.isPassword("100.000"));
        Assert.assertTrue(!_security.isPassword("100.010"));
        Assert.assertTrue(!_security.isPassword(""));
        Assert.assertTrue(!_security.isPassword("$1"));
        Assert.assertTrue(!_security.isPassword("1"));
        Assert.assertTrue(!_security.isPassword("0"));
        
	Assert.assertTrue(_security.isPassword("@notemail.com"));
        Assert.assertTrue(_security.isPassword("@not.email.com"));     
        Assert.assertTrue(_security.isPassword("not@@email.com"));
        Assert.assertTrue(_security.isPassword("not_email.com"));
        Assert.assertTrue(_security.isPassword("not_email_com"));
        Assert.assertTrue(_security.isPassword("@notemail.com"));
        Assert.assertTrue(_security.isPassword("notemail@com"));
        Assert.assertTrue(_security.isPassword("not@email..com"));
        Assert.assertTrue(_security.isPassword("not<email.com"));
        Assert.assertTrue(_security.isPassword("not@ema'il.com"));
        Assert.assertTrue(_security.isPassword("n(o)t@email.com"));
        Assert.assertTrue(_security.isPassword("!@notemail.com"));
        Assert.assertTrue(_security.isPassword("&@not_email.com"));
        Assert.assertTrue(_security.isPassword("@notemail.com"));
        Assert.assertTrue(_security.isPassword("not@email.com:12312"));
        Assert.assertTrue(_security.isPassword("not@email.com:asdf"));
        Assert.assertTrue(_security.isPassword("123@an_email.com"));
        Assert.assertTrue(_security.isPassword("this@is.an.email.com"));
        Assert.assertTrue(_security.isPassword("this@isemail.net"));
        Assert.assertTrue(_security.isPassword("this_is@an.email.com"));
        Assert.assertTrue(_security.isPassword("); DROP TABLE lol; --"));
        Assert.assertTrue(_security.isPassword("__L33THCKR__"));
        Assert.assertTrue(_security.isPassword("xX1337H@CKRXx"));
        Assert.assertTrue(_security.isPassword("this><issparta"));
        Assert.assertTrue(_security.isPassword("What's my username?"));
        Assert.assertTrue(_security.isPassword("Something Else"));
        Assert.assertTrue(_security.isPassword("sam$$iam"));
        Assert.assertTrue(_security.isPassword("sam'sname"));
        Assert.assertTrue(_security.isPassword("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"));
        Assert.assertTrue(_security.isPassword("sam@iam.net"));
        Assert.assertTrue(_security.isPassword("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"));
        Assert.assertTrue(_security.isPassword("unus_invictus"));
        Assert.assertTrue(_security.isPassword("$100.001"));
        Assert.assertTrue(_security.isPassword("); DROP TABLE lol; --"));
        Assert.assertTrue(_security.isPassword("100000000000000000000000000000000000000000.00"));
        Assert.assertTrue(_security.isPassword("100000000000000000000000000000000"));
        Assert.assertTrue(_security.isPassword("10000000000000000000000000000000"));
        Assert.assertTrue(_security.isPassword("$1231232"));

        boolean good = false;
        try {
            _security.isPassword(null);
        } catch (NullPointerException e) {
            good = true;
        }
        Assert.assertTrue(good);
    }
    
    public void test_isPhoneNumber() {
	
        Assert.assertTrue(!_security.isPhoneNumber("sam@iam.net"));
        Assert.assertTrue(!_security.isPhoneNumber("); DROP TABLE lol; --"));
        Assert.assertTrue(!_security.isPhoneNumber(""));
        Assert.assertTrue(!_security.isPhoneNumber("5"));
        Assert.assertTrue(!_security.isPhoneNumber("555555-5555"));
        Assert.assertTrue(!_security.isPhoneNumber("asd-asd-asdf"));
        Assert.assertTrue(!_security.isPhoneNumber("(asd)asd-asdf"));
        Assert.assertTrue(!_security.isPhoneNumber("asd-asdasdf"));
        Assert.assertTrue(!_security.isPhoneNumber("asdasdasdf"));
        Assert.assertTrue(!_security.isPhoneNumber("(555)55$-5444"));
        Assert.assertTrue(!_security.isPhoneNumber("555a555a5555"));
        Assert.assertTrue(!_security.isPhoneNumber("5asdf212341"));
        Assert.assertTrue(!_security.isPhoneNumber("55555555555"));
        Assert.assertTrue(!_security.isPhoneNumber("(5555)555-5555"));
        Assert.assertTrue(!_security.isPhoneNumber("(555)5555-555"));
        Assert.assertTrue(!_security.isPhoneNumber("(555)555-55555"));
        Assert.assertTrue(!_security.isPhoneNumber("(555)55-55555"));
        Assert.assertTrue(!_security.isPhoneNumber("(555)555-55"));
        Assert.assertTrue(!_security.isPhoneNumber("555555555"));
        Assert.assertTrue(!_security.isPhoneNumber("(555)5555555"));
        Assert.assertTrue(!_security.isPhoneNumber("5555-5555555"));
        Assert.assertTrue(!_security.isPhoneNumber("555-555555"));
        Assert.assertTrue(!_security.isPhoneNumber("555555-5555"));
        
        Assert.assertTrue(_security.isPhoneNumber("(555)555-5555"));
        Assert.assertTrue(_security.isPhoneNumber("555-555-5555"));
        Assert.assertTrue(_security.isPhoneNumber("5555555555"));
        Assert.assertTrue(_security.isPhoneNumber("555-5555555"));
        
        boolean good = false;
        try {
            _security.isPhoneNumber(null);
        } catch (NullPointerException e) {
            good = true;
        }
        Assert.assertTrue(good);
    }

    public void test_isAlpha() {
	Assert.assertTrue(!_security.isAlpha(""));
        Assert.assertTrue(!_security.isAlpha("); DROP TABLE lol; --"));
        Assert.assertTrue(!_security.isAlpha("123"));
        Assert.assertTrue(!_security.isAlpha("asdf_ds"));
        Assert.assertTrue(!_security.isAlpha("asdf@asdf"));
        Assert.assertTrue(!_security.isAlpha("asdf.asdf"));
        Assert.assertTrue(!_security.isAlpha(".asdf"));
        Assert.assertTrue(!_security.isAlpha("$12312.123"));
        Assert.assertTrue(!_security.isAlpha("as^asdf"));
        Assert.assertTrue(!_security.isAlpha("asf/.][]"));
        Assert.assertTrue(!_security.isAlpha("asf "));
        Assert.assertTrue(!_security.isAlpha(" asf"));
        
        Assert.assertTrue(_security.isAlpha("asdfasdfasd"));
        Assert.assertTrue(_security.isAlpha("sadfasSDFASDE"));
        Assert.assertTrue(_security.isAlpha("poinasldfWRWERFasdfa"));

        boolean good = false;
        try {
            _security.isAlpha(null);
        } catch (NullPointerException e) {
            good = true;
        }
        Assert.assertTrue(good);
    }
    
    public void test_isNumeric() {
	Assert.assertTrue(!_security.isNumeric(""));
        Assert.assertTrue(!_security.isNumeric("); DROP TABLE lol; --"));
        Assert.assertTrue(!_security.isNumeric("123asd"));
        Assert.assertTrue(!_security.isNumeric(" 123"));
        Assert.assertTrue(!_security.isNumeric("123 "));
        Assert.assertTrue(!_security.isNumeric("sd"));
        Assert.assertTrue(!_security.isNumeric("123_asdf"));
        Assert.assertTrue(!_security.isNumeric("1235%"));
        Assert.assertTrue(!_security.isNumeric("1#12"));
        Assert.assertTrue(!_security.isNumeric("#1"));
        Assert.assertTrue(!_security.isNumeric(".32"));
        Assert.assertTrue(!_security.isNumeric(".332"));
        
        Assert.assertTrue(_security.isNumeric("123.23"));
        Assert.assertTrue(_security.isNumeric("123"));
        Assert.assertTrue(_security.isNumeric("123.001"));
        Assert.assertTrue(_security.isNumeric("0.32"));
        Assert.assertTrue(_security.isNumeric("5"));
        
        boolean good = false;
        try {
            _security.isNumeric(null);
        } catch (NullPointerException e) {
            good = true;
        }
        Assert.assertTrue(good);
    }
    
    public void test_isInteger() {
	Assert.assertTrue(!_security.isInteger(""));
        Assert.assertTrue(!_security.isInteger("); DROP TABLE lol; --"));
        Assert.assertTrue(!_security.isInteger("123asd"));
        Assert.assertTrue(!_security.isInteger(" 123"));
        Assert.assertTrue(!_security.isInteger("123 "));
        Assert.assertTrue(!_security.isInteger("sd"));
        Assert.assertTrue(!_security.isInteger("123_asdf"));
        Assert.assertTrue(!_security.isInteger("1235%"));
        Assert.assertTrue(!_security.isInteger("1#12"));
        Assert.assertTrue(!_security.isInteger("#1"));
        Assert.assertTrue(!_security.isInteger(".32"));
        Assert.assertTrue(!_security.isInteger(".332"));
        Assert.assertTrue(!_security.isInteger("123.23"));
        Assert.assertTrue(!_security.isInteger("123.001"));
        Assert.assertTrue(!_security.isInteger("0.32"));
        
        Assert.assertTrue(_security.isInteger("123"));
        Assert.assertTrue(_security.isInteger("5"));
        
        boolean good = false;
        try {
            _security.isInteger(null);
        } catch (NullPointerException e) {
            good = true;
        }
        Assert.assertTrue(good);
    }

    public void test_isASCII() {
	Assert.assertTrue(!_security.isASCII("\u0109"));
        
        Assert.assertTrue(_security.isASCII("1234567890-=qwertyuiop[]asdfghjkl;'zxcvbnm,./!@#$%^&*()_+QWERTYUIOP{}ASDFGHJKL:ZXCVBNM<>?|\\  "));
        
        
        
        boolean good = false;
        try {
            _security.isASCII(null);
        } catch (NullPointerException e) {
            good = true;
        }
        Assert.assertTrue(good);
    }
    
    public void test_isAlphaNumSpace() {
	Assert.assertTrue(!_security.isAlphaNumSpace("asdfasdf% "));
        Assert.assertTrue(!_security.isAlphaNumSpace("12391&"));
        Assert.assertTrue(!_security.isAlphaNumSpace("); DROP TABLE lol; --"));
        Assert.assertTrue(!_security.isAlphaNumSpace(""));
        Assert.assertTrue(!_security.isAlphaNumSpace("asdf_ds"));
        Assert.assertTrue(!_security.isAlphaNumSpace("asdf@asdf"));
        Assert.assertTrue(!_security.isAlphaNumSpace("asdf.asdf"));
        Assert.assertTrue(!_security.isAlphaNumSpace(".asdf"));
        Assert.assertTrue(!_security.isAlphaNumSpace("$12312.123"));
        Assert.assertTrue(!_security.isAlphaNumSpace("as^asdf"));
        Assert.assertTrue(!_security.isAlphaNumSpace("asf/.][]"));
        
        Assert.assertTrue(_security.isAlphaNumSpace("asf "));
        Assert.assertTrue(_security.isAlphaNumSpace(" asf"));
        Assert.assertTrue(_security.isAlphaNumSpace("123"));
        Assert.assertTrue(_security.isAlphaNumSpace("123 asdf"));
        Assert.assertTrue(_security.isAlphaNumSpace("There once was1 a man named bill1232"));
        Assert.assertTrue(_security.isAlphaNumSpace("asdfasdfasd"));
        Assert.assertTrue(_security.isAlphaNumSpace("sadfasSDFASDE"));
        Assert.assertTrue(_security.isAlphaNumSpace("poinasldfWRWERFasdfa"));
        
        boolean good = false;
        try {
            _security.isAlphaNumSpace(null);
        } catch (NullPointerException e) {
            good = true;
        }
        Assert.assertTrue(good);
    }

    public void test_isAlphaNum() {
	Assert.assertTrue(!_security.isAlphaNum("asdfasdf% "));
        Assert.assertTrue(!_security.isAlphaNum("12391&"));
        Assert.assertTrue(!_security.isAlphaNum("); DROP TABLE lol; --"));
        Assert.assertTrue(!_security.isAlphaNum(""));
        Assert.assertTrue(!_security.isAlphaNum("asdf_ds"));
        Assert.assertTrue(!_security.isAlphaNum("asdf@asdf"));
        Assert.assertTrue(!_security.isAlphaNum("asdf.asdf"));
        Assert.assertTrue(!_security.isAlphaNum(".asdf"));
        Assert.assertTrue(!_security.isAlphaNum("$12312.123"));
        Assert.assertTrue(!_security.isAlphaNum("as^asdf"));
        Assert.assertTrue(!_security.isAlphaNum("asf/.][]"));
        Assert.assertTrue(!_security.isAlphaNum("123 asdf"));
        Assert.assertTrue(!_security.isAlphaNum("There once was1 a man named bill1232"));
        Assert.assertTrue(!_security.isAlphaNum("asf "));
        Assert.assertTrue(!_security.isAlphaNum(" asf"));
        
        Assert.assertTrue(_security.isAlphaNum("123"));
        Assert.assertTrue(_security.isAlphaNum("asdfasdfasd"));
        Assert.assertTrue(_security.isAlphaNum("sadfasSDFASDE"));
        Assert.assertTrue(_security.isAlphaNum("poin1232asldfWRWERFasdfa"));
        
        boolean good = false;
        try {
            _security.isAlphaNum(null);
        } catch (NullPointerException e) {
            good = true;
        }
        Assert.assertTrue(good);
    }
    
   /***************************************************************************/
   /* Cryptographic Functions */
   /***************************************************************************/
    
    public void test_hash() {
        byte[][] encodings = new byte[_crypto_cases.length][];
        byte[][][] hashes = new byte[11][_crypto_cases.length][];
        byte[][] salts = new byte[11][];
        
        // Generate encodings
        for (int i = 0; i < _crypto_cases.length; i++) {
             try {
                 encodings[i] = _security.encodeASCII(_crypto_cases[i]);
             } catch (CharacterCodingException e) {
                 Assert.assertTrue(false);
             }
        }
        
        for (int n = 0; n < salts.length; n++) {
            salts[n] = null;
            if (n != 0) {
                salts[n] = new byte[n*5];
            
                // Select a random salt.
                _security.randomBytes(salts[n]);
            }

            // Generate hashes
            for (int i = 0; i < _crypto_cases.length; i++) {
                hashes[n][i] = _security.hash(encodings[i], salts[n]);
                Assert.assertNotNull(hashes[n][i]);
            }
            
            // Check each hash against all other generated hashes.
            for (int i = 0; i < _crypto_cases.length; i++) {
                for (int j = 0; j < _crypto_cases.length; j++) {
                    for (int k = 0; k < n; k++) {
                        Assert.assertTrue(
                                  "\"" + _crypto_cases[i] + "\" (salt: \"" + ((salts[n] != null) ? _security.encodeBase64(salts[n]) : "") + "\")"
                                + " and " 
                                + "\"" + _crypto_cases[j] + "\" (salt: \"" + ((salts[k] != null) ? _security.encodeBase64(salts[k]) : "") + "\")"
                                + "produce the same hash.",
                                !Arrays.equals(hashes[n][i], hashes[k][j])
                        );
                    }
                    if (_crypto_cases[i].equals(_crypto_cases[j])) {
                        Assert.assertTrue(
                                  "\"" + _crypto_cases[i] + "\" produces inconsistent hash values "
                                + "\"" + _security.encodeBase64(hashes[n][i]) + "\" and "
                                + "\"" + _security.encodeBase64(hashes[n][j]) + "\""
                                + "with salt \"" + ((salts[n] != null) ? _security.encodeBase64(salts[n]) : "") + "\".", 
                                Arrays.equals(hashes[n][i], hashes[n][j])
                        );
                    } else {
                        Assert.assertTrue("\"" + _crypto_cases[i] + "\" and \"" + _crypto_cases[j] + "\" produce the same hash value.", 
                                !Arrays.equals(hashes[n][i], hashes[n][j]));
                    }
                }
            }
        }
        
        byte[] one = _security.hash(new byte[0], null);        
        byte[] two =_security.hash(new byte[0], null); 
        Assert.assertTrue(Arrays.equals(one, two));
        
        
        // Check bad input cases
        boolean good = false;
        try {
            _security.hash(null, salts[0]);
        } catch (NullPointerException e) {
            good = true;
        }
        Assert.assertTrue(good);
        
    }
          
    public void test_createEncryptionKey() {
        
        byte[] test;
        
        test = new byte[16];
        
        _security.randomBytes(test);
        SecretKey res3 = _security.createEncryptionKey(test);
                
        _security.randomBytes(test);
        SecretKey res4 = _security.createEncryptionKey(test);
        SecretKey res5 = _security.createEncryptionKey(test);
        Assert.assertTrue(!res4.equals(res3));
        Assert.assertTrue(res5.equals(res4));
       
        // Check bad input cases
        boolean good = false;
        try {
            _security.createEncryptionKey(null);
        } catch (NullPointerException e) {
            good = true;
        }
        Assert.assertTrue(good);
        
        good = false;
        try {
            _security.createEncryptionKey(new byte[0]);
            
        } catch (IllegalArgumentException e) {
            good = true;
        }
        Assert.assertTrue(good);
        
        good = false;
        try {
            _security.createEncryptionKey(new byte[5]);
            
        } catch (IllegalArgumentException e) {
            good = true;
        }
        
        Assert.assertTrue(good);
        
    }
    
    
    public void test_encrypt_decrypt() {

        for (String c : _crypto_cases) {
            byte[] keymaterial, encoded = null, cyphertext = null, cleartext = null;
            
            // Generate secret key
            keymaterial = new byte[16];
            _security.randomBytes(keymaterial);
            SecretKey key = _security.createEncryptionKey(keymaterial);
            
            // Encode string
            try {
                encoded = _security.encodeASCII(c);
            } catch (CharacterCodingException e) {
                Assert.assertTrue(false);
            }
            
            // Generate encrypted instance
            try {
                cyphertext = _security.encrypt(encoded, key);
            } catch (InvalidKeyException e) {
                Assert.assertTrue("Invalid key \"" + _security.encodeBase64(keymaterial) + "\" of length " + keymaterial.length + ".", false);
            }
            
            // Decrypt
            try {
                cleartext = _security.decrypt(cyphertext, key);
            } catch (BadPaddingException e) {
                Assert.assertTrue(false);
            } catch (InvalidKeyException e) {
                Assert.assertTrue(false);
            }
            
            // Ensure that it is the same.
            Assert.assertTrue(Arrays.equals(encoded, cleartext));
            
        }
        
        
        // Try some random test data
        for (int i = 0; i < 255; i++) {
            // Create our key.
            byte[] keymaterial = new byte[16];
            _security.randomBytes(keymaterial);
            SecretKey key = _security.createEncryptionKey(keymaterial);
            
            // Create the message
            byte[] original, cyphertext = null, cleartext = null;
            original = new byte[i*30];
            _security.randomBytes(original);
            
            // encrypt
            try {
                cyphertext = _security.encrypt(original, key);
            } catch (InvalidKeyException e) {
                Assert.assertTrue(false);
            } catch (NullPointerException e) {
                Assert.assertTrue(false);
            }
            
            // decrypt
            try {
                cleartext = _security.decrypt(cyphertext, key);
            } catch (BadPaddingException e) {
                Assert.assertTrue(false);
            } catch (InvalidKeyException e) {
                Assert.assertTrue(false);
            } catch (NullPointerException e) {
                Assert.assertTrue(false);
            }
            
            // Ensure they're the same.
            Assert.assertTrue(Arrays.equals(original, cleartext));
            
            // Try a differnt key
            _security.randomBytes(keymaterial);
            SecretKey key2 = _security.createEncryptionKey(keymaterial);
            
            boolean good = false;
            try {
                cleartext = _security.decrypt(cyphertext, key2);
            } catch (BadPaddingException e) {
                good = true;
            } catch (InvalidKeyException e) {
                Assert.assertTrue(false);
            } catch (NullPointerException e) {
                Assert.assertTrue(false);
            }
            Assert.assertTrue(good);
            
            // Alter the message
            cyphertext[Math.abs(_security.randomInt()) % cyphertext.length] -= 0x01;
            good = false;
            try {
                cleartext = _security.decrypt(cyphertext, key);
            } catch (BadPaddingException e) {
                good = true;
            } catch (InvalidKeyException e) {
                Assert.assertTrue(false);
            } catch (NullPointerException e) {
                Assert.assertTrue(false);
            }
            Assert.assertTrue(good);

        }
            boolean good;
        
            byte[] keymaterial = new byte[16];
            _security.randomBytes(keymaterial);
            SecretKey key = _security.createEncryptionKey(keymaterial);
            
            // Null stuff
            good = false;
            try {
                _security.encrypt(null, key);
            } catch (InvalidKeyException e) {
                Assert.assertTrue(false);
            } catch (NullPointerException e) {
                good = true;
            }
            Assert.assertTrue(good);
            
            good = false;
            try {
                _security.encrypt(keymaterial, null);
            } catch (InvalidKeyException e) {
                Assert.assertTrue(false);
            } catch (NullPointerException e) {
                good = true;
            }
            Assert.assertTrue(good);
            
            good = false;
            try {
                _security.encrypt(null, null);
            } catch (InvalidKeyException e) {
                Assert.assertTrue(false);
            } catch (NullPointerException e) {
                good = true;
            }
            Assert.assertTrue(good);
            
            good = false;
            try {
                _security.decrypt(null, key);
            } catch (BadPaddingException e) {
                Assert.assertTrue(false);
            } catch (InvalidKeyException e) {
                Assert.assertTrue(false);
            } catch (NullPointerException e) {
                good = true;
            }
            Assert.assertTrue(good);
            
            good = false;
            try {
                _security.decrypt(keymaterial, null);
            } catch (BadPaddingException e) {
                Assert.assertTrue(false);
            } catch (InvalidKeyException e) {
                Assert.assertTrue(false);
            } catch (NullPointerException e) {
                good = true;
            }
            Assert.assertTrue(good);
            
            good = false;
            try {
                _security.decrypt(null, null);
            } catch (BadPaddingException e) {
                Assert.assertTrue(false);
            } catch (InvalidKeyException e) {
                Assert.assertTrue(false);
            } catch (NullPointerException e) {
                good = true;
            }
            Assert.assertTrue(good);
            
            
    }
    
   /***************************************************************************/
   /* Data Encoding Functions */
   /***************************************************************************/
    
    public void test_encodeBase64_decodeBase64() {
	for (int i = 0; i < 255; i++) {
            byte[] b = new byte[i*32];
            
            String s = _security.encodeBase64(b);
            byte[] b2 = _security.decodeBase64(s);
            Assert.assertTrue(Arrays.equals(b,b2));
        }
    }
    
    public void test_encodeASCII_decodeASCII() {
           
        byte[][] encodings = new byte[_crypto_cases.length][];

        // Generate encodings
        for (int i = 0; i < _crypto_cases.length; i++) {
             try {
                 encodings[i] = _security.encodeASCII(_crypto_cases[i]);
             } catch (CharacterCodingException e) {
                 Assert.assertTrue(false);
             }

             String decoded = null;
             try {
                 decoded = _security.decodeASCII(encodings[i]);
             } catch (CharacterCodingException e) {
                 Assert.assertTrue(e.toString(), false);
             }

             if (!decoded.equals(_crypto_cases[i])) {
                 Assert.assertTrue("Could not decode \"" + _crypto_cases[i] + "\".", false);
             }

        }

        // Check some bad content.
        boolean good = false;
        
        String bad = "\u0109";
        try {
           _security.encodeASCII(bad);
        } catch (CharacterCodingException e) {
            good = true;
        }
        Assert.assertTrue(good);
        
        good = false;
        try {
            _security.encodeASCII(null);
        } catch ( CharacterCodingException e) {
            
        } catch (NullPointerException e) {
            good = true;
        }
        Assert.assertTrue(good);
        
        
        byte[] bb = new byte[2];
        bb[0] = (byte)0xFF;
        bb[1] = (byte)0x40;
        good = false;
        try {
            _security.decodeASCII(bb);
        } catch (CharacterCodingException e) {
            good = true;
        }
        Assert.assertTrue(good);
        
        
        good = false;
        try {
            _security.decodeASCII(null);
        } catch (CharacterCodingException e) {
            
        } catch (NullPointerException e) {
            good = true;
        }
        Assert.assertTrue(good);

    }
    
}
