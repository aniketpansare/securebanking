package test.java.service;

import java.nio.charset.CharacterCodingException;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Vector;

import junit.framework.TestCase;
import main.java.domain.Account;
import main.java.domain.BadTransactionException;
import main.java.domain.Delegation.PrivilegeLevel;
import main.java.domain.Department;
import main.java.domain.User;
import main.java.domain.User.CertReqLevel;
import main.java.domain.User.Status;
import main.java.domain.User.Type;
import main.java.domain.UserData;
import main.java.security.SecurityManager;
import main.java.service.AccountManager;
import main.java.service.DelegationManager;
import main.java.service.DepartmentManager;
import main.java.service.RecipientManager;
import main.java.service.TransactionManager;
import main.java.service.UserManager;
import main.java.util.TransactionWrapper;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.junit.Test;

/**
 * @author AP
 *
 */
public class PopulateDummyDataTests extends TestCase {
	

	private Map<String, User> users = new HashMap<String, User>();
	private Vector<Account> useraccounts = new Vector<Account>();
	private Map<String,Department> departments = new HashMap<String,Department>();
	private Vector<Account> deptaccounts = new Vector<Account>();
	
    @Test
    public void test() throws Exception, org.hibernate.exception.ConstraintViolationException, com.mysql.jdbc.exceptions.jdbc4.MySQLIntegrityConstraintViolationException {
    	
    	final UserManager usermanager = UserManager.create();
 	    final AccountManager accountmanager = AccountManager.create();
 	    final TransactionManager transmanager = TransactionManager.create();
    	final DepartmentManager deptmanager = DepartmentManager.create();
    	final SecurityManager secmanager = SecurityManager.get();
    	final DelegationManager delmanager = DelegationManager.create();
    	final RecipientManager recmanager = RecipientManager.create();
    	
    	

        /***************************************************************************/
                                // Add External Account for Credit / Debit
        /****************************************************************************/
        
        
        try {
                (new TransactionWrapper<Void>() {
                                @Override
                                public Void operation(Session s) throws HibernateException {
                                        try {
                                                
                                                User externalUser = usermanager.createUser("external", "externalpass", UserData.create("Aniket Pansare", "(555)555-5595", "123 Fake Street", "apansare@asu.edu",  "2342099"));
                                                
                                                externalUser.setStatus(Status.ACTIVE);
                                                externalUser.setUserType(Type.CUSTOMER);
                                                
                                                usermanager.saveUser(externalUser);
                                                
                                                Account extAccount = accountmanager.createAccount(externalUser, 999999999.0, "External Account", "External Account for Credit Debit");
                                                extAccount.setAccountStatus(Account.Status.ACTIVE);
                                                accountmanager.saveAccount(extAccount);
                                                
                                        } catch (CharacterCodingException e) {
                                        assertTrue(e.toString(), false);
                                }
                                        return null;
                                }
        
                }).execute();
                } catch (HibernateException e) {
                        // test user already exists! skip populating the database.
                        return;
                }
        
    	
    	
 	    /*****************************************************************/
	   	// dummy user
 	    /*****************************************************************/
    	
		// Add users
		try {
	    	(new TransactionWrapper<Void>() {
				@Override
				public Void operation(Session s) throws HibernateException {
					try {
						users.put("aniket", usermanager.createUser("aniket", "pass", UserData.create("Aniket Pansare", "(555)555-5595", "123 Fake Street", "apansare@asu.edu",  "2099")));
						users.put("joe", usermanager.createUser("joe", "s3cur3", UserData.create("Joe Babb", "(123)456-7890", "456 Fake Street", "jbabb1@asu.edu", "32")));
						users.put("ashish", usermanager.createUser("ashish", "p4ss", UserData.create("Ashish Bagade", "(000)000-0000", "700 Fake Street", "abagade@asu.edu", "322")));
						users.put("dhanyatha", usermanager.createUser("dhanyatha", "passcode", UserData.create("Dhanyatha Manjunath", "(123)123-1234", "300 Fake Street", "dmanjuna@asu.edu", "12312")));
						users.put("sushma", usermanager.createUser("sushma", "p4ss2", UserData.create("Sushma Banda", "(444)444-0000", "5 Fake Street", "sbanda2@asu.edu", "32212321")));
						users.put("suma", usermanager.createUser("suma", "p4ss3", UserData.create("Suma Cherukuri", "(123)123-1000", "100 Fake Street", "scheruku@asu.edu", "9999")));
			    	} catch (CharacterCodingException e) {
			    		assertTrue(e.toString(), false);
			    	}
					return null;
				}
	
	    	}).execute();
		} catch (HibernateException e) {
			// test user already exists! skip populating the database.
			return;
		}

    	
		/*****************************************************************/
	   	// dummy accounts / transactions
 	    /*****************************************************************/
		
		
 	    try {
 	    	(new TransactionWrapper<Void>() {
 	    			    		
 				@Override
 				public Void operation(Session s) throws HibernateException {
 					try {
 						// generate accounts (5 per user)
 						for (Entry<String, User> u : users.entrySet()) {
 							for (int i = 0; i < 5; i++) {
 								useraccounts.add(accountmanager.createAccount(u.getValue(), i * 1000.0, u.getKey() + " #" + Integer.toString(i), "Account Description"));
 								useraccounts.lastElement().setAccountStatus(Account.Status.ACTIVE);
 								accountmanager.saveAccount(useraccounts.lastElement());
 							}
 						}
 			    		
 						// generate transactions
 						// (10 per account)
 						byte[] b = new byte[10 * useraccounts.size()];
 						secmanager.randomBytes(b);
 						int i = 0;
 						for (Account a : useraccounts) {
 							
 							for (int j = 0; j < 10; j++) {
 								Account rec = useraccounts.get((Math.abs((int)b[i++])) % useraccounts.size());
 								if (a.getAccountBalance() > 0.0)
 									transmanager.createTransaction((User)a.getAccountOwner(), null, a, rec, 0.1 * a.getAccountBalance());
 							}
 						}

 			    		return null;
 			    	} catch (NullPointerException e) {
 			    		e.printStackTrace();
 			    		assertTrue(e.toString(), false);
 			    	} catch (IllegalArgumentException e) {
 			    		assertTrue(e.toString(), false);
					} catch (BadTransactionException e) {
						assertTrue(e.toString(), false);
					}
 					return null;
 				}
 	    	}).execute();
     	} catch (HibernateException e) {
     		assertTrue(e.toString(), false);
     	}
 	    
		// setup user parameters
		users.get("aniket").setStatus(Status.ACTIVE);
		users.get("aniket").setUserType(Type.CUSTOMER);

		users.get("joe").setStatus(Status.ACTIVE);
		users.get("joe").setUserType(Type.ADMIN);

		users.get("dhanyatha").setStatus(Status.ACTIVE);
		users.get("dhanyatha").setUserType(Type.MERCHANT);
		
		users.get("ashish").setUserType(Type.MERCHANT);
		
		users.get("sushma").setUserType(Type.ADMIN);
		users.get("sushma").setStatus(Status.ACTIVE);
		
		users.get("suma").setUserType(Type.CUSTOMER);
		
		
		// save users
		try {
	    	(new TransactionWrapper<Void>() {
	    			    		
				@Override
				public Void operation(Session s) throws HibernateException {
					try {
						for (User u : users.values()) {
							usermanager.saveUser(u);
						}
			    	} catch (NullPointerException e) {
			    		assertTrue(e.toString(), false);
			    	}	
					return null;
				}
	    	}).execute();
		} catch (HibernateException e) {
			assertTrue(e.toString(), false);
			//return;
		}
 	    
 	   /*****************************************************************/
	   // dummy departments
 	   /*****************************************************************/
 	    
 	   try {
 	    	(new TransactionWrapper<Void>() {
 	    			    		
 				@Override
 				public Void operation(Session s) throws HibernateException {
 					try {
 						// generate departments
 						departments.put("Finance", deptmanager.createDepartment("Finance"));
 						departments.put("HR", deptmanager.createDepartment("HR"));
 						departments.put("Technology",deptmanager.createDepartment("Technology"));
 						departments.put("IT", deptmanager.createDepartment(departments.get("Technology"), "IT"));
 						departments.put("Sales", deptmanager.createDepartment("Sales"));
 						departments.put("Telemarketing", deptmanager.createDepartment(departments.get("Sales"), "Telemarketing"));
 						departments.put("Engineering", deptmanager.createDepartment("Engineering"));
 						
 						
 						// Add employees to departments (2 per user).
 						byte[] b = new byte[2 * users.size()];
 						secmanager.randomBytes(b);
 						int i = 0;
 						for (User u : users.values()) {
 							for (int j = 0; j < 2; j++) {
 								((Department)departments.values().toArray()[Math.abs(b[i++]) % departments.size()]).addEmployee(u);
 							}
 						}
 						
 			    		return null;
 			    	} catch (NullPointerException e) {
 			    		e.printStackTrace();
 			    		assertTrue(e.toString(), false);
 			    	} catch (IllegalArgumentException e) {
 						assertTrue(e.toString(), false);
 					}
					return null;
 				}
 	    	}).execute();
     	} catch (HibernateException e) {
     		e.printStackTrace();
     		assertTrue(e.toString(), false);
     	}
 	   
 	  /***************************************************************************/
		// Add External Account for Credit / Debit
 	   /****************************************************************************/


		try {
		(new TransactionWrapper<Void>() {
			@Override
			public Void operation(Session s) throws HibernateException {
				try {
					
					Account extAccount = accountmanager.createAccount(departments.get("Finance"), 0.5*Double.MAX_VALUE, "External Account", "External Account for Credit Debit");
					extAccount.setAccountStatus(Account.Status.ACTIVE);
					accountmanager.saveAccount(extAccount);
					
				} catch (RuntimeException e) {
					e.printStackTrace();
		    		assertTrue(e.toString(), false);
				}
				return null;
			}
		
		}).execute();
		} catch (HibernateException e) {
		// test user already exists! skip populating the database.
		return;
		}
 	   
		
	  /***************************************************************************/
	  // Dummy accounts for departments
 	  /****************************************************************************/
		
		try {
 	    	(new TransactionWrapper<Void>() {
 	    			    		
 				@Override
 				public Void operation(Session s) throws HibernateException {
 					try {
 						// generate accounts (5 per user)
 						for (Department u : departments.values()) {
 							for (int i = 0; i < 5; i++) {
 								deptaccounts.add(accountmanager.createAccount(u, i * 10000.0, u.getDepartmentName() + " #" + Integer.toString(i), "Account Description"));
 								deptaccounts.lastElement().setAccountStatus(Account.Status.ACTIVE);
 								accountmanager.saveAccount(deptaccounts.lastElement());
 							}
 						}

 			    		return null;
 			    	} catch (NullPointerException e) {
 			    		e.printStackTrace();
 			    		assertTrue(e.toString(), false);
 			    	} catch (IllegalArgumentException e) {
 			    		assertTrue(e.toString(), false);
					}
 					return null;
 				}
 	    	}).execute();
     	} catch (HibernateException e) {
     		assertTrue(e.toString(), false);
     	}
		
 	  /*****************************************************************/
	  // dummy delegations
 	  /*****************************************************************/
 	  try {
	    	(new TransactionWrapper<Void>() {
	    			    		
				@Override
				public Void operation(Session s) throws HibernateException {
					try {						
						
						// Add delegations to accounts. 1 per user (not evenly distributed).
						byte[] b = new byte[3 * users.size()];
						secmanager.randomBytes(b);
						int i = 0;
						for (int j = 0; j < users.size(); j++) {
							Account from = useraccounts.get(Math.abs(b[i++]) % useraccounts.size());
							Account to = useraccounts.get(Math.abs(b[i++]) % useraccounts.size());
							delmanager.createDelegation(
									(User)from.getAccountOwner(), 
									from, 
									PrivilegeLevel.values()[Math.abs(b[i++])%PrivilegeLevel.values().length], 
									(User)to.getAccountOwner()
							);
						}
						
			    		return null;
			    	} catch (NullPointerException e) {
			    		e.printStackTrace();
			    		assertTrue(e.toString(), false);
			    	} catch (IllegalArgumentException e) {
			    		e.printStackTrace();
						assertTrue(e.toString(), false);
					}
					return null;
				}
	    	}).execute();
   	} catch (HibernateException e) {
   		e.printStackTrace();
   		assertTrue(e.toString(), false);
   	}
 	   
 	  /*****************************************************************/
	  // dummy recipients
 	  /*****************************************************************/
 	  
 	 try {
	    	(new TransactionWrapper<Void>() {
	    			    		
				@Override
				public Void operation(Session s) throws HibernateException {
					try {						
						
						// Add recipients to user. 2 per user.
						byte[] b = new byte[users.size()];
						secmanager.randomBytes(b);
						int i = 0;
						for (User u : users.values()) {
							Account target = useraccounts.get(Math.abs(b[i++]) % useraccounts.size());
							recmanager.createRecipient(u, "Recipient #" + Integer.toString(i), target);
						}
						
			    		return null;
			    	} catch (NullPointerException e) {
			    		assertTrue(e.toString(), false);
			    	} catch (IllegalArgumentException e) {
						assertTrue(e.toString(), false);
					}
					return null;
				}
	    	}).execute();
	} catch (HibernateException e) {
		e.printStackTrace();
		assertTrue(e.toString(), false);
	}
 	  
 	   
	}
}
