package test.java.service;


import java.nio.charset.CharacterCodingException;
import java.util.Set;

import junit.framework.TestCase;
import main.java.domain.User;
import main.java.domain.User.Status;
import main.java.domain.User.Type;
import main.java.domain.UserData;
import main.java.domain.impl.UserImpl;
import main.java.security.AuthenticationException;
import main.java.service.UserManager;
import main.java.util.TransactionWrapper;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.junit.Test;


public class UserManagerImplTests extends TestCase{
	
	private UserManager s_usermanager = null;
	
    @Override
    protected void setUp() throws Exception {
    	 s_usermanager = UserManager.create();
    }
    
    @Test
    public void test() {
    	
    	try {
	    	(new TransactionWrapper<Void>() {
				@Override
				public Void operation(Session s) throws HibernateException {
					s.createQuery("DELETE FROM UserImpl WHERE " +  UserImpl.COLUMN.USERNAME + " LIKE :username").setParameter("username", "test::%").executeUpdate();
					return null;
				}
	    	}).execute();
    	} catch (HibernateException e) {
    		assertTrue(e.toString(), false);
    	}
    	
    	
    	final User u, u2, u3;
    	User tmp = null;
    	try {
	    	tmp = (new TransactionWrapper<User>() {
				@Override
				public User operation(Session s) throws HibernateException {
					try {
						return s_usermanager.createUser("test::foo", "bar");
			    	} catch (CharacterCodingException e) {
			    		assertTrue(e.toString(), false);
			    		return null;
			    	}			
				}
	
	    	}).execute();
    	} catch (HibernateException e) {
    		assertTrue(e.toString(), false);
    	}
    	u = tmp;
    	
    	assertNotNull(u);
    	assertEquals(Type.UNVALIDATED, u.getUserType());
    	assertEquals(Status.DISABLED, u.getStatus());
    	assertEquals("test::foo", u.getUsername());
    	assertNotSame(0L, u.getUserId());
    	
    	tmp = null;
    	try {
	    	tmp = (new TransactionWrapper<User>() {
				@Override
				public User operation(Session s) throws HibernateException {
					try {
						return s_usermanager.createUser("test::foo2", "bar2");
			    	} catch (CharacterCodingException e) {
			    		assertTrue(e.toString(), false);
			    		return null;
			    	}			
				}
	
	    	}).execute();
    	} catch (HibernateException e) {
    		assertTrue(e.toString(), false);
    	}
    	u2 = tmp;
    	
    	assertNotNull(u2);
    	assertEquals(Type.UNVALIDATED, u2.getUserType());
    	assertEquals(Status.DISABLED, u2.getStatus());
    	assertEquals("test::foo2", u2.getUsername());
    	assertNotSame(0L, u2.getUserId());
    	
    	assertNotSame(u2.getUserId(), u.getUserId());
    	
    	tmp = null;
    	try {
	    	tmp = (new TransactionWrapper<User>() {
				@Override
				public User operation(Session s) throws HibernateException {
					try {
						return s_usermanager.createUser("test::foo3", "bar3", UserData.create("Foo Bar", "(555)555-5555", "123 Fake Street", "5@5.com", "1"));
			    	} catch (CharacterCodingException e) {
			    		assertTrue(e.toString(), false);
			    		return null;
			    	}			
				}
	
	    	}).execute();
    	} catch (HibernateException e) {
    		assertTrue(e.toString(), false);
    	}
    	u3 = tmp;
 
    	assertEquals("Foo Bar", u3.getData().getName());
    	assertEquals("(555)555-5555", u3.getData().getContactNumber());
    	assertEquals("123 Fake Street", u3.getData().getAddress());
    	assertEquals("1", u3.getData().getSsn());
    	assertNotSame(0L, u3.getUserId());
    	
    	assertNotSame(u2.getUserId(), u3.getUserId());
    	assertNotSame(u.getUserId(), u3.getUserId());
    	
    	/*
    	
		boolean good = false;
    	try {
	    	(new TransactionWrapper<Void>() {
				@Override
				public Void operation(Session s) throws HibernateException {
					try {
						s_usermanager.createUser("test::foo3", "bar3");
					} catch (NullPointerException | CharacterCodingException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					return null;
				}
	    	}).execute();
    	} catch (HibernateException e) {
    		good = true;
    	}
    	assertTrue(good);
    	*/
    	
    	// disabled account
    	
    	
    	try {
	    	(new TransactionWrapper<Void>() {
				@Override
				public Void operation(Session s) throws HibernateException {
					boolean good = false;
			    	try {
			    		s_usermanager.authenticateUser("test::foo3", "bar3");
			    	} catch (CharacterCodingException | NullPointerException e) {
			    		assertTrue(e.toString(), false);
			    	} catch (AuthenticationException e) {
			    		good = true;
			    	}
			    	assertTrue(good);	
			    	return null;
				}
	    	}).execute();
    	} catch (HibernateException e) {
    		assertTrue(e.toString(), false);
    	}
    	
    	u3.setStatus(Status.ACTIVE);
    	u3.setUserType(Type.CUSTOMER);
    	
    	
    	try {
	    	(new TransactionWrapper<Void>() {
	    			    		
				@Override
				public Void operation(Session s) throws HibernateException {
					try {
			    		s_usermanager.saveUser(u3);
			    	} catch (NullPointerException e) {
			    		assertTrue(e.toString(), false);
			    	}	
					return null;
				}
	    	}).execute();
    	} catch (HibernateException e) {
    		assertTrue(e.toString(), false);
    	}
    	
    	
    	
    	// bad username
    	try {
	    	(new TransactionWrapper<User>() {
	    			    		
				@Override
				public User operation(Session s) throws HibernateException {
					boolean good = false;
					try {
			    		s_usermanager.authenticateUser("test::foo_bad", "bar");
			    	} catch (NullPointerException e) {
			    		assertTrue(e.toString(), false);
			    	} catch (AuthenticationException e) {
			    		good = true;
			    	} catch (CharacterCodingException e) {
			    		assertTrue(e.toString(), false);
					}
					assertTrue(good);
					return null;
				}
	    	}).execute();
    	} catch (HibernateException e) {
    		assertTrue(e.toString(), false);
    	}
    	
    	
    	// bad password
    	try {
	    	(new TransactionWrapper<User>() {
	    			    		
				@Override
				public User operation(Session s) throws HibernateException {
					boolean good = false;
					try {
			    		s_usermanager.authenticateUser("test::foo3", "pass");
			    	} catch (NullPointerException e) {
			    		assertTrue(e.toString(), false);
			    	}	catch (AuthenticationException e) {
			    		good = true;
			    	} catch (CharacterCodingException e) {
			    		assertTrue(e.toString(), false);
					}
					assertTrue(good);
					return null;
				}
	    	}).execute();
    	} catch (HibernateException e) {
    		assertTrue(e.toString(), false);
    	}
    	
    	
    	// good login
    	User t3 = null;
    	try {
	    	t3 = (new TransactionWrapper<User>() {
	    			    		
				@Override
				public User operation(Session s) throws HibernateException {
					try {
			    		return s_usermanager.authenticateUser("test::foo3", "bar3");
			    	} catch (NullPointerException | AuthenticationException | CharacterCodingException e) {
			    		assertTrue(e.toString(), false);
			    		return null;
			    	}
				}
	    	}).execute();
    	} catch (HibernateException e) {
    		assertTrue(e.toString(), false);
    	}
    	
    	assertNotNull(t3);
    	assertEquals(u3.getUserType(), t3.getUserType());
    	assertEquals(u3.getStatus(), t3.getStatus());
    	assertEquals(u3.getUsername(), t3.getUsername());
    	assertEquals(u3.getData().getName(), t3.getData().getName());
    	assertEquals(u3.getData().getContactNumber(), t3.getData().getContactNumber());
    	assertEquals(u3.getData().getAddress(), t3.getData().getAddress());
    	assertEquals(u3.getData().getSsn(), t3.getData().getSsn());

    	
    	
    	try {
	    	(new TransactionWrapper<Void>() {
				@Override
				public Void operation(Session s) throws HibernateException {
					
					System.out.println("Results:");
					Iterable<? extends User> set = s_usermanager.getUsers(User.StatusFilter.NOT_DELETED, User.TypeFilter.ALL, 0, 2);
			    	for (User usr : set) {
			    		System.out.println("User: " + usr.getUsername());
			    	}
			    	return null;
				}
	
	    	}).execute();
    	} catch (HibernateException e) {
    		assertTrue(e.toString(), false);
    	}
    	
    	// Clear the test users from the database
    	try {
	    	(new TransactionWrapper<Void>() {
				@Override
				public Void operation(Session s) throws HibernateException {
					
					s.createQuery("DELETE FROM UserImpl WHERE " + UserImpl.COLUMN.USERNAME + " LIKE :username").setParameter("username", "test::%").executeUpdate();	
					return null;
				}
	
	    	}).execute();
    	} catch (HibernateException e) {
    		assertTrue(e.toString(), false);
    	}

	}	
    	
  
    
    
    
}
